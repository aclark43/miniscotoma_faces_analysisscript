% plot the cone density as an overlay on the cone mosaic.
% 
% Ben Moon
% 7/21/23
% =========================================================================

% have the user select which cone density file to load
[fname,pth] = uigetfile('*_CD_data.mat','Select Cone Density File',...
    'Y:\Adaptive_Optics\APLab_System\');

% load the workspace
load(fullfile(pth,fname));

f1 = figure;
% first figure: cone mosaic with cone tags
imagesc(refImage)
colormap('gray')
daspect([1 1 1])
hold on
p = plot(ConeCoord(:,1)+1,ConeCoord(:,2)+1,'.r');
p.MarkerSize = 5;
title(fname,'Interpreter','none')
hold off

% mask the density data to only be valid where cone tags exist
imMask = false(size(refImage));     % initialize binary mask
coneInds = sub2ind(size(refImage),ConeCoord(:,2)+1,ConeCoord(:,1)+1); % linear indices of cone locations
imMask(coneInds) = 1;               % make the cone coordinate pixels equal to 1
se = strel('square',20);            % make a structuring element that is a square with width of 20 pixels
imMaskClose = imclose(imMask,se);   % fill gaps between cones and close the image

% figure
% imshow(imMaskClose);            % show figure to make sure no gaps are left

CD_forPlotting = CD_conePerDegsq;   % make a copy for plotting
CD_forPlotting(~imMaskClose) = nan; % assign NaN to values outside the boundary
CD_forPlotting(CD_forPlotting<4000) = nan;  % trim low values (not enough nearest neighbors for accurate density)

% second figure: cone mosaic with density map overlaid
f2 = figure;
% create truecolor image of cone mosaic by stacking 3 copies (for plotting)
coneImgRGB = cat(3,refImage,refImage,refImage); 

% display the cone image
a1 = imagesc(coneImgRGB);
daspect([1 1 1]);   % make axis square
hold on
title(fname,'Interpreter','none')

% overlay the cone density map
a2 = surf(CD_forPlotting);
view(2);        % top-down view
shading interp
colormap('jet')
a2.FaceAlpha = 0.5; % change the opacity
colorbar

% add markers for the CDC, PCD, and PRL
leg(1) = plot3(oursCDC_x,oursCDC_y,25000,'*b','MarkerSize',10,'LineWidth',1.5);
leg(2) = plot3(oursPCD_x,oursPCD_y,25000,'^k','MarkerSize',6,'LineWidth',1.5);
leg(3) = plot3(PRL_X,PRL_Y,25000,'og','MarkerSize',7,'LineWidth',1.5);
legend(leg,'CDC','PCD','PRL')

% add scale bar
hSca = 20;                  % height: 20 pixels                          
wSca = (10/60)*pixPerdeg;   % width: 10 arcmin
xSca = 20;                  % left edge
ySca = size(refImage,1)-20-hSca;    % top edge
r = rectangle('Position',[xSca,ySca,wSca,hSca]);
r.FaceColor = [1 1 1];
r.EdgeColor = 'none';
t = text(xSca,ySca-1.2*hSca,25000,'10 arcmin');
t.Color = 'w';
t.FontSize = 12;
t.FontWeight = 'bold';

hold off

% save outputs
% figure 1: cone tags
f1Name = strcat(fname(1:end-11),'coneTags.fig');
if exist(fullfile(pth,f1Name),'file')
    saveAns = questdlg('The cone tag figure already exists. Do you want to overwrite it?',...
        'File Overwrite Warning',...
        'Yes','No','No');
    if strcmp(saveAns,'Yes')
        saveas(f1,fullfile(pth,f1Name))     % save as a figure
        saveas(f1,fullfile(pth,strcat(f1Name(1:end-4),'.tif')));    % save as an image
        fprintf('File %s was overwritten.\n',f1Name);
    else
        fprintf('File %s already exists. It was not overwritten.\n',f1Name);
    end
else
    saveas(f1,fullfile(pth,f1Name))         % save as an image
    saveas(f1,fullfile(pth,strcat(f1Name(1:end-4),'.tif')));    % save as an image
end

% figure 2: cone density
f2Name = strcat(fname(1:end-8),'figure.fig');
if exist(fullfile(pth,f2Name),'file')
    saveAns = questdlg('The cone density figure already exists. Do you want to overwrite it?',...
        'File Overwrite Warning',...
        'Yes','No','No');
    if strcmp(saveAns,'Yes')
        saveas(f2,fullfile(pth,f2Name));    % save as a figure
        saveas(f2,fullfile(pth,strcat(f2Name(1:end-4),'.tif')));    % save as an image
        fprintf('File %s was overwritten.\n',f2Name);
    else
        fprintf('File %s already exists. It was not overwritten.\n',f2Name);
    end
else
    saveas(f2,fullfile(pth,f2Name));    % save as a figure
    saveas(f2,fullfile(pth,strcat(f2Name(1:end-4),'.tif')));    % save as an image
end

    