function eDist = eucDist(x, y)

for i = 1:length(x)
    eDist(i) = mean(sqrt(x(i)^2 + y(i)^2));
end

end

