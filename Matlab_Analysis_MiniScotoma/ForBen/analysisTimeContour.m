function pointCoords = analysisTimeContour(dataStruct, timeBins, mapBins, name)

% define limits and axes for histogram and plotting
xMax = 40;      % maximum value in x (arcmin)
yMax = 40;      % maximum value in y (arcmin)
binWidth = 1;   % bin width for histogram (arcmin)
xHist = (-xMax:binWidth:xMax);  % x values for histogram bin centers
yHist = (-yMax:binWidth:yMax);  % y values for histogram bin centers
[XH,YH] = meshgrid(xHist,yHist);    % generate grid of x and y points
xEdges = xHist-binWidth/2;      % x values for histogram bin edges
xEdges(end+1) = xEdges(end)+binWidth;   % extra value for last edge
yEdges = yHist-binWidth/2;      % y values for histogram bin edges
yEdges(end+1) = yEdges(end)+binWidth;   % extra value for last edge


fixMat = dataStruct.(name).cleanFixation;

dataXMatrix = dataStruct.(name).allXCleanFIXmat(find(fixMat),:);
dataYMatrix = dataStruct.(name).allYCleanFIXmat(find(fixMat),:);

[trials,samples] = size(dataXMatrix);
if timeBins == samples
    numParts = [1 1440];
else
    numParts = 1:timeBins:samples;
end

timeSXX = []; timeSYY = [];
for combsTime = 1:length(numParts)-1
    timeSXX(combsTime,:) = [reshape(dataXMatrix(:,numParts(combsTime):numParts(combsTime+1))',1,[])];
    timeSYY(combsTime,:) = [reshape(dataYMatrix(:,numParts(combsTime):numParts(combsTime+1))',1,[])];
end
dists = hypot(mean(timeSXX'),mean(timeSYY'));
idxBigDif = find(dists == max(dists));

% figure;
% [matrixProbs,limits] = generateHeatMapSimple( ...
%     timeSXX(idxBigDif,:), ...
%     timeSYY(idxBigDif,:), ...
%     'Bins', 40,...
%     'StimulusSize', 7,...
%     'AxisValue', 20,...
%     'Uncrowded', 4,...
%     'Borders', 1);

% reformat data with all x-values in column 1 and y-values in column 2
data = [ timeSXX(idxBigDif,:)', ...
    timeSYY(idxBigDif,:)'];

% generate histogram with the data, save results to variable h1
figure;
h1 = histogram2(data(:,1),data(:,2),...
    xEdges,yEdges,'DisplayStyle','tile','ShowEmptyBins','off',...
    'Normalization','probability','EdgeColor','none');
daspect([1 1 1])
colormap jet
hold on
% draw x and y axes to locate origin
line([0 0],[-yMax,yMax],[1 1],'Color','k')  
line([-xMax,xMax],[0 0],[1 1],'Color','k')
grid off
xlabel('Horizontal gaze position (armin)')
ylabel('Vertical gaze position (arcmin)')
title(sprintf('Gaze histogram for %s condition',lower(name)))
hold off

% extract and manipulate histogram data
histData = h1.Values.';     % transpose for proper x-y orientation
histDataNorm = histData./max(max(histData));    % normalize to peak value of 1
pkInd = find(histDataNorm==1);    % find linear index of histogram peak
xPk = XH(pkInd);            % x-coordinate for peak
yPk = YH(pkInd);            % y-coordinate for peak
xMean = mean(data(:,1));    %x-coordinate for mean gaze position
yMean = mean(data(:,2));    %y-coordinate for mean gaze position

histDataVector = reshape(histData,[],1);  % make a vector for sorting
histDataSorted = sort(histDataVector,'descend');    % sort by descending probability
sumHistData = cumsum(histDataSorted);               % sum probabilities
thLevel = 0.68;          % fraction of histogram data to include
thInd = find(sumHistData>thLevel,1);   % find index of first element to cross threshold
thVal = histDataSorted(thInd)/histDataSorted(1);    % find threshold value and normalize (for peak value of 1)

% generate binary image and compute weighted centroid
thMask = zeros(size(histDataNorm));     % initialize empty array
thMask(histDataNorm>thVal) = 1;         % set values above threshold to 1
WC = regionprops(thMask,histDataNorm,'WeightedCentroid'); % weighted centroid
% get weighted centroid coordinates by interpolating b/t pixels
xWC = interp1((1:1:length(xHist)),xHist,WC.WeightedCentroid(1));
yWC = interp1((1:1:length(yHist)),yHist,WC.WeightedCentroid(2));

% clip data for plotting
clipLevel = 0.95;       % fraction of histogram data to include
clipInd = find(sumHistData>clipLevel,1);   % find index of first element to cross threshold
clipVal = histDataSorted(clipInd)/histDataSorted(1);    % find threshold value and normalize
histDataNormClip = histDataNorm;    % make a copy for plotting
histDataNormClip(histDataNormClip<clipVal) = nan;   % set clipped values to NaN 

% generate contour map and find the location of the peak coutour interval
figure;
[contourMatrix,~] = contourf(XH,YH,histDataNorm);   % contour map of normalized histogram values
colormap('jet');
colorbar
contourTable = getContourLineCoordinates(contourMatrix);
peakValIdk = length([contourTable.Level]);
xPeakCont = contourTable.X(peakValIdk);
yPeakCont = contourTable.Y(peakValIdk);
daspect([1 1 1])
hold on
plot(xPeakCont,yPeakCont,'d','MarkerFaceColor','k')
xlabel('Horizontal gaze position (armin)')
ylabel('Vertical gaze position (arcmin)')
title(sprintf('Contour map for %s condition',lower(name)))
hold off

% generate figure with clipped data and important points labeled
figure;
surf(XH,YH,histDataNormClip);   
colormap('jet');
view(2)
shading interp
grid off
daspect([1 1 1])
axis([-xMax xMax -yMax yMax])
hold on
% draw x and y axes to show origin
line([0 0],[-yMax,yMax],[1 1],'Color','k')  
line([-xMax,xMax],[0 0],[1 1],'Color','k')
leg(1) = plot3(xMean,yMean,1,'og','LineWidth',0.7);   % mean of fixation points
leg(2) = plot3(xWC,yWC,1,'*c','LineWidth',0.7,'MarkerSize',8);   % weighted centroid
leg(3) = plot3(xPeakCont,yPeakCont,1,'^w','LineWidth',0.7);    % peak of contour map
leg(4) = plot3(xPk,yPk,1,'sb','LineWidth',0.7,'MarkerSize',8);   % peak of histogram

l1 = legend(leg,{'mean gaze position','weighted centroid','contour peak','histogram peak'});
l1.Color = [0.7 0.7 0.7];
xlabel('Horizontal gaze position (armin)')
ylabel('Vertical gaze position (arcmin)')
title(sprintf('Gaze heatmap for %s condition with labeled points',lower(name)))

% create struct for reporting the data
pointCoords = struct;
pointCoords.mean = [xMean,yMean];   % mean of gaze position
pointCoords.wCentroid = [xWC,yWC];  % weighted centroid
pointCoords.contourPeak = [xPeakCont,yPeakCont];    % peak of contour map
pointCoords.histogramPeak = [xPk,yPk];              % peak of histogram

% combine the data and report in a table
value = {'Mean';'Weighted Centroid';'Contour Peak';'Histogram Peak'};   % to specify order   
xAll = [xMean;xWC;xPeakCont;xPk];       % all the x-values from above
yAll = [yMean;yWC;yPeakCont;yPk];       % all the y-values from above
euclAll = hypot(xAll,yAll);             % Euclidean distance from origin
disp(name)                      % print the condition to the command line
table(value,xAll,yAll,euclAll)  % display table in command line

% add data to the output struct
pointCoords.labels = value;
pointCoords.xAll = xAll;
pointCoords.yAll = yAll;
pointCoords.euclideanAll = euclAll;
pointCoords.xRange = range(xAll);
pointCoords.yRange = range(yAll);
pointCoords.euclideanRange = range(euclAll);
pointCoords.xMax = max(xAll);
pointCoords.yMax = max(yAll);
pointCoords.euclideanMax = max(euclAll);
pointCoords.xMin = min(xAll);
pointCoords.yMin = min(yAll);
pointCoords.euclideanMin = min(euclAll);


% figure;
% [n,c] = hist3(data, 'Nbins',[1 1]*40);
% axis([-20 20 -20 20])
% xb = linspace(limits.xmin,limits.xmax,40);
% yb = linspace(limits.ymin,limits.ymax,40);
% [xx,yy]=meshgrid(xb,yb,40);
% figure;
% [M,c] = contourf(xx,yy,matrixProbs');
% 
% v = [1,1];
% 
% colormap(jet)
% colorbar
% contourTable = getContourLineCoordinates(M);
% peakValIdk = length([contourTable.Level]);
% XPeak = contourTable.X(peakValIdk);
% YPeak = contourTable.Y(peakValIdk);
% hold on
% plot(XPeak,YPeak,'d','MarkerFaceColor','k')
% 
% PeaksIdx = [XPeak,YPeak];






end