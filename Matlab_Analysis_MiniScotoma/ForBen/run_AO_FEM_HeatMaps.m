%%run code for Ben to show current heatmap of FEM + AO
% ET Data    AO Data Imaged    AO Data Processed
%     A144        X             X                  
%     A188        X             X             
%     Z055        X                            
%     Z091        X             X (Z190)            
%     Z124        -                                  
%     Z151        X             X                    
%     Z162        X             X (Z126)                   



% subjects = {'A188','Z091','Z151'};  % tested on 3/27/23 by BM
clear all
clc

subjects = {'A144','A188','Z124',...
    'Z162','Z151','Z091'};    % specify subject here for single processing run
    
for ii = 1:length(subjects)
    
    % for eyetrace data, load the correct workspace
    temp = load(sprintf('EyeTrack_Data_RowEs/%s_EyeData_MiniScot.mat',string(subjects(ii))));
    
    % for AO data, it's somewhat more complicated
    switch char(subjects(ii))
        case 'A188'
            pathName = ('AO_Data/A188_R_2022_10_17_CD_data.mat');
        case 'A144'
            pathName = ('AO_Data/A144_R_2022_12_14_CD_data.mat');
        case 'Z091'
            pathName = ('AO_Data/Z190_R_2022_06_03_CD_data.mat');
        case 'Z151'
            pathName = ('AO_Data/Z151_R_2022_07_29_CD_data.mat');
        case 'Z162'
            pathName = ('AO_Data/Z126_R_2022_11_03_CD_data.mat');
    end
    
    peaksLocScot{ii} = analysisTimeContour(temp.dataAll{1, 1}, 1440, 20, 'Scotoma'); 
    peaksLocCont{ii} = analysisTimeContour(temp.dataAll{1, 1}, 1440, 20, 'Control'); 

    redifinedCones{ii} = plotAOImages(pathName,temp.dataAll{1},subjects{ii});   % comment out if no AO data
end

 



