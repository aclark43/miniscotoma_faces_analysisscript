clear all
close all
clc
% name with _2 indicates second day
subjectsAll = {'Z055','A144','Ben','Z091','Z114',...
    'A144_2','Z091_2','Ben_2','Z055_2',...
    'Z151','Z151_2','Z114','Z114_2'}; %'Z171','Z055',,|||'Z104',
reProcess = [1 1 1 1];
conditions = {'Control','Scotoma'};
% reProcess = [0 0 0 0];
whichDay = [1 1 1 1 1 2 2 2 2 1 2 1 2];
idx = [2 6; 4 7; 3 8; 1 9; 10 11; 12 13]; %idx of day 1 and 2 subjects

% cleanTrials = [0 0 0 0];
clrs = parula(length(subjectsAll));

for ii = 1:length(subjectsAll)
    %     if reProcess(ii)
    subjDataPath = strcat(sprintf('X:/Ashley/SingleOpto_VA_Scot/%s', subjectsAll{ii}));
    allfiles = dir(subjDataPath);
    data{ii}.Scotoma = [];
    data{ii}.Control = [];
    for ss = find(~[allfiles.isdir])
        clear temp
        if startsWith(string(allfiles(ss).name),["scot"]) %scotoma condition
            
            temp = load(sprintf('%s/%s',subjDataPath,allfiles(ss).name));
            dateCollect = datetime(str2double(temp.eis_data.filename(5:8)),...
                str2double(temp.eis_data.filename(9:10)),...
                str2double(temp.eis_data.filename(11:12)));
            %             if temp.eis_data.user_data.variables.trial2Data.m_scotomaPresent
            temppptrials = convertDataToPPtrialsSinlgeOpto(temp.eis_data,dateCollect);
            processedPPTrialsS = newEyerisEISRead(temppptrials, subjectsAll{ii});
            data{ii}.Scotoma = [data{ii}.Scotoma processedPPTrialsS];
            %             else
            %                 error('Mislabelled File');
            %             end
            
        elseif startsWith(string(allfiles(ss).name),["cont"])
            
            temp = load(sprintf('%s/%s',subjDataPath,allfiles(ss).name));
            dateCollect = datetime(str2double(temp.eis_data.filename(5:8)),...
                str2double(temp.eis_data.filename(9:10)),...
                str2double(temp.eis_data.filename(11:12)));
            
            temppptrials = convertDataToPPtrialsSinlgeOpto(temp.eis_data,dateCollect);
            processedPPTrialsC = newEyerisEISRead(temppptrials, subjectsAll{ii});
            data{ii}.Control = [data{ii}.Control processedPPTrialsC];
            
        else
            error('Change name of File');
        end
        %         save(sprintf('%sSnellScot',subjectsAll{ii}),'data');
    end
    
    lengthTrial = 500;
    condition = {'Control','Scotoma'};
    counter = 1;
    
    for c = 1:2
        counterP = 1;
        xAllT = [];
        yAllT = [];
        xAllF = [];
        yAllF = [];
        xAllTime = [];
        yAllTime = [];
        processedPPTrialsX = data{ii}.(condition{c});
        counterTrials = 1;
        outALLamps = [];
        outALLdirs = [];
        outLandLocsX =[];
        outLandLocsY =[];
        outStartLocsX =[];
        outStartLocsY =[];
        outALLtimes = [];
        outALLtrialIdx = [];
        for pp = 1:length(processedPPTrialsX)
            if processedPPTrialsX{pp}.fixationTrial
                xAllF = [xAllF processedPPTrialsX{pp}.x(1:1500)];
                yAllF = [yAllF processedPPTrialsX{pp}.y(1:1500)];
                Perf{ii}{c}(pp) = processedPPTrialsX{pp}.Correct;
            else
               
                xAllT = [xAllT processedPPTrialsX{pp}.x];
                yAllT = [yAllT processedPPTrialsX{pp}.y];                
                RT{ii}{c}(pp) = (processedPPTrialsX{pp}.TimeTargetOFF - ...
                    processedPPTrialsX{pp}.TimeTargetON)/5;                
                Perf{ii}{c}(pp) = processedPPTrialsX{pp}.Correct; 
                eachOptoPerf{ii}{c}(counterTrials,1) = ...
                    processedPPTrialsX{pp}.Response;               
                counterD = 1;
                drifts{ii}{c}.duration(pp) = median(processedPPTrialsX{pp}.drifts.duration);
                
                for dd = 1:length(processedPPTrialsX{pp}.drifts.start)
                    startIdx = processedPPTrialsX{pp}.drifts.start(dd);
                    endIdx = startIdx + processedPPTrialsX{pp}.drifts.duration(dd);
                    if endIdx > length(processedPPTrialsX{pp}.x)
                        endIdx = length(processedPPTrialsX{pp}.x);
                    end
                    if length([startIdx:1:endIdx]) < 15
                        continue
                    end
                    drifts{ii}{c}.Performance = processedPPTrialsX{pp}.Correct;
                    drifts{ii}{c}.x{pp,counterD} = processedPPTrialsX{pp}.x(startIdx:endIdx);
                    drifts{ii}{c}.y{pp,counterD} = processedPPTrialsX{pp}.y(startIdx:endIdx);
                    
                    %                     sgFitVal = min([length([startIdx:1:endIdx]
                    [~, drifts{ii}{c}.instSpX{pp,counterD},...
                        drifts{ii}{c}.instSpY{pp,counterD},...
                        drifts{ii}{c}.mn_speed{pp,counterD},...
                        drifts{ii}{c}.driftAngle{pp,counterD},...
                        drifts{ii}{c}.curvature{pp,counterD},...
                        drifts{ii}{c}.varx{pp,counterD},...
                        drifts{ii}{c}.vary{pp,counterD},...
                        drifts{ii}{c}.bcea{pp,counterD},...
                        drifts{ii}{c}.span(pp,counterD), ...
                        drifts{ii}{c}.amplitude(pp,counterD), ...
                        drifts{ii}{c}.prlDistance(pp,counterD),...
                        drifts{ii}{c}.prlDistanceX(pp,counterD), ...
                        drifts{ii}{c}.prlDistanceY(pp,counterD)] = ...
                        getDriftChars(processedPPTrialsX{pp}.x(startIdx:endIdx),...
                        processedPPTrialsX{pp}.y(startIdx:endIdx), 11, 1, 250, 1000);
                    counterD = counterD + 1;
                end
                out{ii}{c} = struct('amps', [], 'dirs', [], 'trialIdx', [], 'times', []);
                
                params.em.saccade.amps_bins = linspace(0, 60, 30);
                params.em.saccade.dirs_bins = linspace(0, 2*pi, 30);
                params.em.saccade.pkvel_bins = linspace(120, 300, 60);
                
                flds = {'microsaccades', 'saccades'};
                for fi = 1:length(flds)
                    starts = processedPPTrialsX{pp}.(flds{fi}).start;
                    durs = processedPPTrialsX{pp}.(flds{fi}).duration;
                    stops = starts + durs - 1;
                    
                    start = 1;
                    stop = length(processedPPTrialsX{pp}.x);
                    mi = (starts + stops) / 2;
                    use = mi >= start & mi <= stop;
                    
                    trueStarts = starts(starts < stop);
                    trueEnds = stops(stops<stop);
                    
                    x1 = processedPPTrialsX{pp}.x(trueStarts);
                    y1 = processedPPTrialsX{pp}.y(trueStarts);
                    x2 = processedPPTrialsX{pp}.x(trueEnds);
                    y2 = processedPPTrialsX{pp}.y(trueEnds);
                    
                    dirs = deg2rad(atan2d(x1.*y2-y1.*x2,x1.*x2+y1.*y2));
                    if ~isempty(use)
                        use = use(find(use > 0));
                        if ~isempty(processedPPTrialsX{ii}.(flds{fi}).amplitude)
                            if length(use) > length(processedPPTrialsX{ii}.(flds{fi}).amplitude)
                                use = use(1:length(processedPPTrialsX{ii}.(flds{fi}).amplitude));
                            end
                            outALLamps = [outALLamps, processedPPTrialsX{ii}.(flds{fi}).amplitude(use)];
                            outALLdirs = [outALLdirs, dirs];%processedPPTrialsS{ii}.(flds{fi}).angle(use)];
                            outALLtimes = [outALLtimes, mi(use)];
                            outALLtrialIdx = [outALLtrialIdx, ii*ones(1, sum(use))];
                            outLandLocsX = [outLandLocsX x2];%processedPPTrialsX{pp}.x(stops(use))];
                            outLandLocsY = [outLandLocsY y2];%processedPPTrialsX{pp}.y(stops(use))];
                            outStartLocsX = [outStartLocsX x1];%processedPPTrialsX{pp}.x(stops(use))];
                            outStartLocsY = [outStartLocsY y1];%processedPPTrialsX{pp}.y(stops(use))];
                            
                            %                             [out.sRates, out.msRates] = buildRates(pptrials);
                            
                            %                 out.rate =
                        end
                    end
                end
                
                if length(processedPPTrialsX{pp}.x) >= lengthTrial
                    xAllTime(counterTrials,:) = processedPPTrialsX{pp}.x(1:lengthTrial);
                    yAllTime(counterTrials,:) = processedPPTrialsX{pp}.y(1:lengthTrial);
                    counterTrials = counterTrials + 1;
                end
            end
            
        end
        out{ii}{c}.amps = outALLamps(outALLamps < 90);
        out{ii}{c}.dirs = outALLdirs(outALLamps < 90);
        out{ii}{c}.times = outALLtimes(outALLamps < 90);
        out{ii}{c}.trialIdx = outALLtrialIdx(outALLamps < 90);
        out{ii}{c}.sRates = cellfun(@(z) rateCalculationSingleOpto(z, 's'), processedPPTrialsX);
        out{ii}{c}.msRates = cellfun(@(z) rateCalculationSingleOpto(z, 'ms'), processedPPTrialsX);
        out{ii}{c}.outLandLocsX = outLandLocsX;
        out{ii}{c}.outLandLocsY = outLandLocsY;
        out{ii}{c}.startLandLocsX = outLandLocsX;
        out{ii}{c}.startLandLocsY = outLandLocsY;
        
        tempX = [true,diff(xAllF)~=0];
        tempY = [true,diff(yAllF)~=0];
        
        idxF = find(xAllF < 40 & xAllF > -40 &...
            yAllF < 40 & yAllF > -40 & tempX & tempY);
        
        tempX = [true,diff(xAllT)~=0];
        tempY = [true,diff(yAllT)~=0];
        
        idxT = find(xAllT < 40 & xAllT > -40 &...
            yAllT < 40 & yAllT > -40 & tempX & tempY);
        
        figure(100+ii);
        subplot(2,2,counter)
        resultF = generateHeatMapSimple( ...
            xAllF(idxF), ...
            yAllF(idxF), ...
            'Bins', 20,...
            'StimulusSize', 7,...
            'AxisValue', 15,...
            'Uncrowded', 0,...
            'Borders', 1);
%         figure;
        [C,m] = contour( resultF',[.68 1]);
%         [C,m] = contour( resultF','ShowText','on');

        areaByConditionALL(ii,counter) = polyarea(C(1,:),C(2,:))*(30/20)^2;
        rectangle('Position',[-3.5,-3.5,7,7]);
        ylabel(condition(c))
        title('Fixation')
        axis square
%         figure;
        counter = counter + 1;
        subplot(2,2,counter)
        resultT = generateHeatMapSimple( ...
            xAllT(idxT), ...
            yAllT(idxT), ...
            'Bins', 20,...
            'StimulusSize', 10,...
            'AxisValue', 15,...
            'Uncrowded', 0,...
            'Borders', 1);
        hold on
        ylabel(condition(c))
        %         plot(xAllT,yAllT,'.')
        %         axis([-40 40 -40 40])
        rectangle('Position',[-2.5,-2.5,5,5]);
        %         rectangle('Position',[-2.5+10,-2.5,5,5]);
        %         rectangle('Position',[-2.5+20,-2.5,5,5]);
        %         rectangle('Position',[-2.5-10,-2.5,5,5]);
        %         rectangle('Position',[-2.5-20,-2.5,5,5]);
        axis square
        title('Task');
        suptitle(subjectsAll(ii))
        [C,m] = contour( resultT,[.68 1]);
        areaByConditionALL(ii,counter) = polyarea(C(1,:),C(2,:))*(30/20)^2;
        counter = counter + 1;
        
        timeTraces{ii}.(condition{c}).xAllTime = xAllTime;
        timeTraces{ii}.(condition{c}).yAllTime = yAllTime;
        tracesTask{ii}.(condition{c}).xAll =  xAllT(idxT);
        tracesTask{ii}.(condition{c}).yAll =  yAllT(idxT);
        tracesFix{ii}.(condition{c}).xAll =  xAllF(idxF);
        tracesFix{ii}.(condition{c}).yAll =  yAllF(idxF);
        
        
       
        
    end
    %         data{ii} = who;
    %
    %     else
    %         data{ii} = load(sprintf('AllProcessedData_%s',subjectsAll{ii}))
    %     end
    %
end
save('AllProcessedData.mat');


%% Create Giant Data Structure for Comparison in systems
% idx = [2 6; 4 7; 3 8; 1 9; 10 11; 12 13]; %idx of day 1 and 2 subjects
counter = 1;
for ii = 1:length(subjectsAll)
    for s = 1:2
        if s == 1
            pathWay = data{ii}.Control;
        else
            pathWay = data{ii}.Scotoma;
        end
        for pp = 1:length(pathWay)
            endDrift = min([pathWay{pp}.drifts.start + ...
                        pathWay{pp}.drifts.duration, length(pathWay{pp}.x)]);
            x = pathWay{pp}.x(pathWay{pp}.drifts.start:endDrift);
            y = pathWay{pp}.y(pathWay{pp}.drifts.start:endDrift);

            if length(x) < 20
                continue;
            end
            [~,~,dcTemp,~,~,~,~,~] = ...
                CalculateDiffusionCoef(1000,struct('x',x,'y',y));
            if dcTemp > 100
                continue;
            end
            
            [~, ~,...
                    ~,...
                    ~,...
                    ~,...
                    ~,...
                    ~,...
                    ~,...
                    ~,...
                    ~, ...
                    a] = ...
                    getDriftChars(x,...
                    y, ...
                    11, 1, 250,  pathWay{pp}.sRate);
            if a > 100
                continue;
            end
            
            dataAll(counter).subjects = ii;
            dataAll(counter).x = x;
            dataAll(counter).y = y;
            
            if sum(idx(:,2) == ii)
                dataAll(counter).subjects = idx(find(idx(:,2) == ii),1);
            end
            dataAll(counter).day = whichDay(ii);
            dataAll(counter).scotoma = s-1;
            dataAll(counter).task = abs(pathWay{pp}.fixationTrial-1);
            dataAll(counter).performance = pathWay{pp}.Correct;
                
             [~, dataAll(counter).instSpX,...
                    dataAll(counter).instSpY,...
                    dataAll(counter).mn_speed,...
                    dataAll(counter).driftAngle,...
                    dataAll(counter).curvature,...
                    dataAll(counter).varx,...
                    dataAll(counter).vary,...
                    dataAll(counter).bcea,...
                    dataAll(counter).span, ...
                    dataAll(counter).amplitude, ...
                    dataAll(counter).prlDistance,...
                    dataAll(counter).prlDistanceX, ...
                    dataAll(counter).prlDistanceY] = ...
                    getDriftChars(x,...
                    y, ...
                    11, 1, 250,  pathWay{pp}.sRate);
                
                    dataAll(counter).duration = length(x);
%                 if abs(pathWay{pp}.fixationTrial-1)
%                     position{ii}.forDsq(pp).x = x;
%                     position{ii}.forDsq(pp).y = y;
%                 else
%                     positionFix{ii}.forDsq(pp).x = x;
%                     positionFix{ii}.forDsq(pp).y = y;
%                 end
                
            counter = counter + 1;
        end
    end
end

counter = 1;
for ii = (unique([dataAll(:).subjects]))
    for t = 1:2
        for s = 1:2
            dataSummaryAll(counter).subject = ii;
            dataSummaryAll(counter).task = t-1;
            dataSummaryAll(counter).scotoma = s-1;

            idxCurrent = find([dataAll(:).task] == t-1 & ...
                [dataAll(:).scotoma] == s-1 & ...
                [dataAll(:).subjects] == ii & ...
                [dataAll(:).duration] > 300 &...
                [dataAll(:).duration] < 600);
            for dd = 1:length(idxCurrent)
                forPosition(dd).x = dataAll(idxCurrent(dd)).x;
                forPosition(dd).y = dataAll(idxCurrent(dd)).y;
            end
                [~,~,individualTrialsDC,~,~,~,~,~] = ...
                    CalculateDiffusionCoef(1000,forPosition);
                
            dataSummaryAll(counter).dc = individualTrialsDC;
            dataSummaryAll(counter).cur = nanmean([dataAll(idxCurrent).curvature]);
            dataSummaryAll(counter).spd = nanmean([dataAll(idxCurrent).mn_speed]);
            dataSummaryAll(counter).bcea_drift = nanmean([dataAll(idxCurrent).bcea]);
            dataSummaryAll(counter).drift_amp = nanmean([dataAll(idxCurrent).amplitude]);
            dataSummaryAll(counter).duration = nanmean([dataAll(idxCurrent).duration]);

            counter = counter + 1;
        end
    end
end

drift_char_names = {'bcea_drift','dc','cur','spd','drift_amp','duration'};
limsY = [0 60;0 22;2 20;0 50;2 8;300 600];
subIDs = unique([dataAll(:).subjects]);
figure;
for d = 1:length(drift_char_names)
    hold on
    subplot(2,3,d)
    for ii = 1:7
        counter = 1;
        for t = 1:2
            for s = 1:2 
                idxTaskCon = find([dataSummaryAll(:).task] == t-1 & ...
                    [dataSummaryAll(:).scotoma] == s-1 & ...
                    [dataSummaryAll(:).subject] == subIDs(ii));
                plot(counter,...
                    mean([dataSummaryAll(idxTaskCon).(drift_char_names{d})]),...
                    'o');
                hold on
                counter = counter + 1;
            end
        end
    end
    idx1 = find([dataSummaryAll(:).task] == 0 & ...
        [dataSummaryAll(:).scotoma] == 0);
    idx2 = find([dataSummaryAll(:).task] == 0 & ...
        [dataSummaryAll(:).scotoma] == 1);
    idx3 = find([dataSummaryAll(:).task] == 1 & ...
        [dataSummaryAll(:).scotoma] == 0);
    idx4 = find([dataSummaryAll(:).task] == 1 & ...
        [dataSummaryAll(:).scotoma] == 1);
    errorbar([1:4],...
        [mean([dataSummaryAll(idx1).(drift_char_names{d})]),...
        mean([dataSummaryAll(idx2).(drift_char_names{d})]),...
        mean([dataSummaryAll(idx3).(drift_char_names{d})]),...
        mean([dataSummaryAll(idx4).(drift_char_names{d})])],...
        [sem([dataSummaryAll(idx1).(drift_char_names{d})]),...
        sem([dataSummaryAll(idx2).(drift_char_names{d})]),...
        sem([dataSummaryAll(idx3).(drift_char_names{d})]),...
        sem([dataSummaryAll(idx4).(drift_char_names{d})])],...
        '-o','Color','k');
    
    title(drift_char_names{d});
    xticks([1:4])
    xticklabels({'Fix Con','Fix Scot','Task Con','Task Scot'});
    xlim([0 5])
    xtickangle(35);
    ylim([limsY(d,1),limsY(d,2)])
end
suptitle('DDPI Drift Char');


%% Performance

figure;
for ii = 1:length(subjectsAll(whichDay == 1))
    perfIdx = find(Perf{ii}{1} ~= 3);
    perfIdx2 = find(Perf{ii}{2} ~= 3);
    if ii == 4  || ii == 1
        perfIdx2 = perfIdx2(20:end);
        perfIdx = perfIdx(20:end);
    end
    forlegs(ii) = plot([1 2],[sum(Perf{ii}{1}(perfIdx))/length(Perf{ii}{1}(perfIdx)) ...
        sum(Perf{ii}{2}(perfIdx2))/length(Perf{ii}{2}(perfIdx2))],...
        '-o','MarkerSize',7);
    %     errorbar([1 2],[sum(Perf{ii}{1}(perfIdx))/length(Perf{ii}{1}(perfIdx)) ...
    %         sum(Perf{ii}{2}(perfIdx2))/length(Perf{ii}{2}(perfIdx2))],...
    %         [sem(Perf{ii}{1}) sem(Perf{ii}{2})],'-o');
    hold on
    performanceAll(ii,1) = sum(Perf{ii}{1}(perfIdx))/length(Perf{ii}{1}(perfIdx));
    performanceAll(ii,2) = sum(Perf{ii}{2}(perfIdx2))/length(Perf{ii}{2}(perfIdx2));
    hold on
end

errorbar([1 2],[mean(performanceAll(:,1)) mean(performanceAll(:,2))],...
    [sem(performanceAll(:,1)) sem(performanceAll(:,2))],'-o',...
    'Color','k','MarkerFaceColor','k','MarkerSize',7);
line([0 3],[.25 .25],'Color',[127 127 127]/255,'LineStyle','--');
xlim([.5 2.5])
xticks([1 2])
xticklabels({'Control','Scotoma'})
ylabel('Proportion Correct')
ylim([.12 1.05])
[h,p] = ttest(performanceAll(:,1), performanceAll(:,2));
legend(forlegs,subjectsAll{whichDay == 1})

%% Compare with exact AO figures (In same format as Bens ppt)

binSizeArcmin = 1;
tasks = {'Task','Fixation'};

for ii = idx(:,1)'
    figure;
    counter = 0;
    for s = 1:2
        xCorrect = [];
        yCorrect = [];
        xWrong = [];
        yWrong = [];
        counter = counter + 1;
        subplot(2,2,counter)
        pathTemp = data{ii}.(conditions{s});
        for i = 1:length(pathTemp)
            if pathTemp{i}.Correct
                forLeg(1) = scatter(pathTemp{i}.x,pathTemp{i}.y,2,'MarkerFaceColor','g',...
                    'MarkerEdgeColor','g',...
                    'MarkerFaceAlpha',.1,'MarkerEdgeAlpha',.1);
                xCorrect = [xCorrect pathTemp{i}.x];
                yCorrect = [yCorrect pathTemp{i}.y];
                hold on
            else
                forLeg(2) = scatter(pathTemp{i}.x,pathTemp{i}.y,2,'MarkerFaceColor','r',...
                    'MarkerEdgeColor','r',...
                    'MarkerFaceAlpha',.5,'MarkerEdgeAlpha',.5);
                hold on
                xWrong = [xWrong pathTemp{i}.x];
                yWrong = [yWrong pathTemp{i}.y];
            end
        end
        axis square
        title(conditions{s});
        axis([-30 30 -30 30]);
        line([-30 30],[0 0]);
        line([0 0],[-30 30]);
        legend(forLeg,{'Correct','Incorrect'});
        if s == 2
            rectangle('Position',[-2.5 -2.5 5 5]);
        end
        xlabel(sprintf('%i trials, %.2f correct', ...
            length(find(Perf{ii}{s} < 2)),...
            performanceAll(find(ii == idx(:,1)),s)));
        
        %%%% Create Contour maps for correct trials
        
        figure(100);
        resultF = generateHeatMapSimple( ...
            xCorrect, ...
            yCorrect, ...
            'Bins', 41,...
            'StimulusSize', 7,...
            'AxisValue', 21,...
            'Uncrowded', 0,...
            'Borders', 1);
        
        limsArcmin = [-20,20,-20,20];   % x- and y-axis limits for histograms (arcmin)
        x = (limsArcmin(1):binSizeArcmin:limsArcmin(2));    % x-values for bin centers
        y = (limsArcmin(3):binSizeArcmin:limsArcmin(4));    % y-values for bin centers
        xEdges = x-binSizeArcmin/2;     % shift by half of bin width for edges
        xEdges(end+1) = xEdges(end)+binSizeArcmin;  % add last edge in x
        yEdges = y-binSizeArcmin/2;     % shift by half of bin width
        yEdges(end+1) = yEdges(end)+binSizeArcmin;  % last edge in y
        [X,Y] = meshgrid(x,y);
        close figure 100
%         counter = counter + 1;
        subplot(2,2,[3 4])
        hold on
        axis square
        
        Val68 = 30;
        [contourHeatMap{ii}{c,1}] = makeHeatmap(X,Y,resultF,...
            limsArcmin,'Heatmap and 68% contour for all control trials',...
            s-1,Val68); 
        
        areaCondition{ii}(c,1) = polyarea(contourHeatMap{ii}{c,1}(:,1),...
            contourHeatMap{ii}{c,1}(:,2));   
       hold on
        if s == 2
            text(-18,-18,sprintf('Scotoma area: %.1f arcmin^2',areaCondition{ii}(c,1)),...
                'Color','m');
            plot(median(xCorrect(~isnan(xCorrect))),...
               median(yCorrect(~isnan(yCorrect))),...
                'd','Color','k','MarkerFaceColor','m')
        else
           text(-18,-15,sprintf('Control area: %.1f arcmin^2',areaCondition{ii}(c,1)),...
                'Color','b'); 
            plot(median(xCorrect(~isnan(xCorrect))),...
               median(yCorrect(~isnan(yCorrect))),...
               'd','Color','k','MarkerFaceColor','b')
        end
        hold on
        if s == 2
            rectangle('Position',[-2.5 -2.5 5 5]);
        end

    end    
    suptitle(subjectsAll{ii})  
end




%% Changes in Maps from 1 to 2 day
idx = [2 6; 4 7; 3 8; 1 9; 10 11; 12 13]; %idx of day 1 and 2 subjects

for ii = 1:size(idx)
    counter = 1;
    figure('Position',[100,0,500, 1400])
    for cnds = 1:2
        for dys = 1:2
            subplot(4,2,counter)
            resultT = generateHeatMapSimple( ...
                tracesTask{idx(ii,dys)}.(condition{cnds}).xAll, ...
                tracesTask{idx(ii,dys)}.(condition{cnds}).yAll, ...
                'Bins', 20,...
                'StimulusSize', 10,...
                'AxisValue', 15,...
                'Uncrowded', 0,...
                'Borders', 1);
            counter = counter + 1;
            hold on
            line([0 0],[ -10 10]);
            line([-10 10],[ 0 0]);
            title(sprintf('%s,%s,Day %i Task',...
                subjectsAll{idx(ii,dys)}, condition{cnds}, dys))
            subplot(4,2,counter)
            resultT = generateHeatMapSimple( ...
                tracesFix{idx(ii,dys)}.(condition{cnds}).xAll, ...
                tracesFix{idx(ii,dys)}.(condition{cnds}).yAll, ...
                'Bins', 20,...
                'StimulusSize', 10,...
                'AxisValue', 15,...
                'Uncrowded', 0,...
                'Borders', 1);
            title(sprintf('%s,%s,Day %i Fixation',...
                subjectsAll{idx(ii,dys)}, condition{cnds}, dys))
            counter = counter + 1;
            hold on
            line([0 0],[ -10 10]);
            line([-10 10],[ 0 0]);
        end
    end
end

xTracesCorrect = [];
yTracesCorrect = [];
xTracesWrong = [];
yTracesWrong = [];

% Right/Wrong Visibility Maps with 95% CI Circles
for ii = 1:size(idx)
    for dys = 1:2
        for cnds = 1:2
            correctCounter = 1;
            wrongCounter = 1;
            for pp = 1:length(data{idx(ii,dys)}.(conditions{cnds}))
                if data{idx(ii,dys)}.(conditions{cnds}){pp}.Correct == 1 &&...
                        data{idx(ii,dys)}.(conditions{cnds}){pp}.fixationTrial == 0
                    xTracesCorrect{ii,dys}.(conditions{cnds}){correctCounter} = ...
                        data{idx(ii,dys)}.(conditions{cnds}){pp}.x;
                    yTracesCorrect{ii,dys}.(conditions{cnds}){correctCounter} = ...
                        data{idx(ii,dys)}.(conditions{cnds}){pp}.y;
                    correctCounter = correctCounter + 1;
                elseif data{idx(ii,dys)}.(conditions{cnds}){pp}.Correct == 0 &&...
                        data{idx(ii,dys)}.(conditions{cnds}){pp}.fixationTrial == 0
                    xTracesWrong{ii,dys}.(conditions{cnds}){wrongCounter} = ...
                        data{idx(ii,dys)}.(conditions{cnds}){pp}.x;
                    yTracesWrong{ii,dys}.(conditions{cnds}){wrongCounter} = ...
                        data{idx(ii,dys)}.(conditions{cnds}){pp}.y;
                    wrongCounter = wrongCounter + 1;
                end
            end
        end
    end
    
    figure;
    counterF = 1;
    for s = 1:2
        for c = 1:2
            subplot(2,2,counterF)
            resultC{counterF} = generateHeatMapSimple( ...
                cell2mat(xTracesCorrect{ii,s}.(conditions{c})), ...
                cell2mat(yTracesCorrect{ii,s}.(conditions{c})), ...
                'Bins', 20,...
                'StimulusSize', 10,...
                'AxisValue', 15,...
                'Uncrowded', 0,...
                'Borders', 1);
            title(sprintf('Day %i, Correct, %s',s,(conditions{c})));
            hold on
            line([0 0],[ -10 10]);
            line([-10 10],[ 0 0]);
            nameConditions{counterF} = sprintf('Day %i, %s',s,(conditions{c}));
            counterF = counterF + 1;
        end
    end
    figure;
    counterF = 1;
    for s = 1:2
        for c = 1:2
            subplot(2,2,counterF)
            resultW{counterF} = generateHeatMapSimple( ...
                cell2mat(xTracesWrong{ii,s}.(conditions{c})), ...
                cell2mat(yTracesWrong{ii,s}.(conditions{c})), ...
                'Bins', 20,...
                'StimulusSize', 10,...
                'AxisValue', 15,...
                'Uncrowded', 0,...
                'Borders', 1);
            title(sprintf('Day %i, Wrong, %s',s,(conditions{c})));
            hold on
            line([0 0],[ -10 10]);
            line([-10 10],[ 0 0]);
            counterF = counterF + 1;
        end
    end
    
    axisSize = 20;
    
    % Create Stimulus Visibility Map
    stimulusMat(1:axisSize*2,1:axisSize*2) = 0;
    stimulusMat((axisSize)-2:(axisSize)+2,...
        (axisSize)-2:(axisSize)+2) = 1;
    stimulusMat((axisSize)-1,(axisSize)-1:(axisSize)+2) = 0;
    stimulusMat((axisSize)+1,(axisSize)-1:(axisSize)+2) = 0;
    
    
    figure;
    counterF = 1;
    radiusScot = 3;
    counter = 1;
    for s = 1:2
        for c = 1:2
            for t = 1:length(xTracesCorrect{ii,s}.(conditions{c}))
                %             subplot(2,2,counterF)
                %             colormap(flipud(gray))
                %                 imagesc(-axisSize:axisSize,-axisSize:axisSize,flipud(stimulusMat))
                hold on
                x = (xTracesCorrect{ii,s}.(conditions{c}){t});
                y = (yTracesCorrect{ii,s}.(conditions{c}){t});
                for sampleInTrace = 1:length(x)
                    newStimulusMat = stimulusMat;
                    xPos = (floor(x(sampleInTrace)-radiusScot:...
                        x(sampleInTrace)+radiusScot))+axisSize;
                    yPos =(floor(y(sampleInTrace)-radiusScot:...
                        y(sampleInTrace)+radiusScot))+axisSize;
                    xPos(xPos > axisSize*2) = axisSize*2;
                    yPos(yPos > axisSize*2) = axisSize*2;
                    xPos(xPos < 1) = 1;
                    yPos(yPos < 1) = 1;
                    newStimulusMat(xPos,yPos) = 0;
                    visibilityMap(:,:,sampleInTrace) = newStimulusMat;
                end
                avgVisMapAllTrials(:,:,counterF) = normalize(mean(visibilityMap,3),'range');
                counterF = counterF + 1;
                %                 figure;  imagesc(-axisSize:axisSize,-axisSize:axisSize,flipud((avg)))
            end
            averageMapAverage(ii).Correct(counter) = mean(mean(mean(avgVisMapAllTrials)))*100;
            counter = counter + 1;
        end
    end

    subplot(2,2,1); colormap(flipud(gray))
    imagesc(-axisSize:axisSize,-axisSize:axisSize,...
        flipud(avgVisMapAllTrials(:,:,1)));
    title(sprintf('Correct, %s Day %i',(conditions{1}),1))
    subplot(2,2,2); colormap(flipud(gray))
    imagesc(-axisSize:axisSize,-axisSize:axisSize,...
        flipud(avgVisMapAllTrials(:,:,2)))
    title(sprintf('Correct, %s Day %i',(conditions{2}),1))
    subplot(2,2,3); colormap(flipud(gray))
    imagesc(-axisSize:axisSize,-axisSize:axisSize,...
        flipud(avgVisMapAllTrials(:,:,3)))
    title(sprintf('Correct, %s Day %i',(conditions{1}),2))
    subplot(2,2,4); colormap(flipud(gray))
    imagesc(-axisSize:axisSize,-axisSize:axisSize,...
        flipud(avgVisMapAllTrials(:,:,4)))
    title(sprintf('Correct, %s Day %i',(conditions{2}),2))
    suptitle(subjectsAll{idx(ii)})
    
    figure;
    counterF = 1;
    counter = 1;
    for s = 1:2
        for c = 1:2
            for t = 1:length(xTracesWrong{ii,s}.(conditions{c}))
                %             subplot(2,2,counterF)
                %             colormap(flipud(gray))
%                 imagesc(-axisSize:axisSize,-axisSize:axisSize,flipud(stimulusMat))
                hold on
                x = (xTracesWrong{ii,s}.(conditions{c}){t});
                y = (yTracesWrong{ii,s}.(conditions{c}){t});
                for sampleInTrace = 1:length(x)
                    newStimulusMat = stimulusMat;
                    xPos = (floor(x(sampleInTrace)-radiusScot:...
                        x(sampleInTrace)+radiusScot))+axisSize;
                    yPos =(floor(y(sampleInTrace)-radiusScot:...
                        y(sampleInTrace)+radiusScot))+axisSize;
                    xPos(xPos > axisSize*2) = axisSize*2;
                    yPos(yPos > axisSize*2) = axisSize*2;
                    xPos(xPos < 1) = 1;
                    yPos(yPos < 1) = 1;
                    if any(isnan(xPos)) || any(isnan(yPos))
                        continue;
                    end
                    newStimulusMat(xPos,yPos) = 0;
                     visibilityMap(:,:,sampleInTrace) = newStimulusMat;
                end
                avgVisMapAllTrials(:,:,counterF) = normalize(mean(visibilityMap,3),'range');
                counterF = counterF + 1;
%                 figure;  imagesc(-axisSize:axisSize,-axisSize:axisSize,flipud((avg)))
            end
                averageMapAverage(ii).Wrong(counter) = mean(mean(mean(avgVisMapAllTrials)))*100;
                counter = counter + 1;
        end
    end
    
    
    subplot(2,2,1); colormap(flipud(gray))
    imagesc(-axisSize:axisSize,-axisSize:axisSize,...
        flipud(avgVisMapAllTrials(:,:,1)));
    title(sprintf('Wrong, %s Day %i',(conditions{1}),1));
    subplot(2,2,2); colormap(flipud(gray))
    imagesc(-axisSize:axisSize,-axisSize:axisSize,...
        flipud(avgVisMapAllTrials(:,:,2)))
    title(sprintf('Wrong, %s Day %i',(conditions{2}),1));
    subplot(2,2,3); colormap(flipud(gray))
    imagesc(-axisSize:axisSize,-axisSize:axisSize,...
        flipud(avgVisMapAllTrials(:,:,3)))
    title(sprintf('Wrong, %s Day %i',(conditions{1}),2));
    subplot(2,2,4); colormap(flipud(gray))
    imagesc(-axisSize:axisSize,-axisSize:axisSize,...
        flipud(avgVisMapAllTrials(:,:,4)))
    title(sprintf('Wrong, %s Day %i',(conditions{2}),1));
    suptitle(subjectsAll{idx(ii)})

    %     figure;imagesc(stimulusMat)
    figure;
    counterF = 1;
    for s = 1:2
        for c = 1:2
            subplot(2,2,counterF)
            colormap(flipud(gray))
            imagesc(-axisSize:axisSize,-axisSize:axisSize,flipud(stimulusMat))
            hold on
            x = horzcat(xTracesCorrect{ii,s}.(conditions{c}){:});
            y = horzcat(yTracesCorrect{ii,s}.(conditions{c}){:});
            idxTrace = find(x < 400 & x > -400 & y < 400 & y > -400);
            x = x(idxTrace);
            y = y(idxTrace);

            scatter(x,y,...
                1800,'MarkerFaceColor','w',...
                'MarkerFaceAlpha', 0.05,...
                'MarkerEdgeAlpha',0);
            hold on
            co = cov(x,y);
%             if strcmp(subjectsAll{idx(ii)},'Z091') && i == 2 &&...
%                     c == 1
%                 R = crr(double(co),.003);
%             else
                R = crr(double(co),.02);
%             end
            
            circle(mode(x),mode(y),R);
            title(sprintf('Correct, %s Day %i',(conditions{c}),s));
            locCircCorrect{ii,counterF} = [mode(x),mode(y),R];
            counterF = counterF + 1;
            
            axis square
        end
    end
    suptitle(subjectsAll{idx(ii)})
    
    figure;
    counterF = 1;
    for s = 1:2
        for c = 1:2
            subplot(2,2,counterF)
            %         figure;
            colormap(flipud(gray))
            imagesc(-axisSize:axisSize,-axisSize:axisSize,flipud(stimulusMat))
            hold on
            x = horzcat(xTracesWrong{ii,s}.(conditions{c}){:});
            y = horzcat(yTracesWrong{ii,s}.(conditions{c}){:});
               idxTrace = find(x < 400 & x > -400 & y < 400 & y > -400);
            x = x(idxTrace);
            y = y(idxTrace);
            scatter(x,y,...
                1800,'MarkerFaceColor','w',...
                'MarkerFaceAlpha', 0.05,...
                'MarkerEdgeAlpha',0);
            hold on
            co = cov(x(~isnan(x)),y(~isnan(y)));
            R = crr(double(co),.02);
            legCir = circle(mode(x),mode(y),R);
            title(sprintf('Wrong, %s Day %i',(conditions{c}),s));
            locCircWrong{ii,counterF} = [mode(x),mode(y),R];
            counterF = counterF + 1;
        end
    end
    legend(legCir,{'5% CI'})
    suptitle(subjectsAll{idx(ii)})
end

figure;
for m = 1:4
subplot(2,2,m)
boxplot((cat(2,averageMapAverage(m).Correct',...
    averageMapAverage(m).Wrong')),{'Correct','Wrong'});
% ylim([0 1.1])
ylim([.8 1])
title(nameConditions{m});
ylabel('Proportion of visible Target');
end
% suptitle(subjectsAll{idx(ii,1)})


% Variance Magnitude
clear temp
figure;
subplot(2,1,1)
for ii = 1:size(idx)
    varR(ii,:) = [locCircCorrect{ii,1}(3) locCircCorrect{ii,3}(3) ...
        locCircCorrect{ii,2}(3) locCircCorrect{ii,4}(3)];
    eucDist(ii,:) = [...
        hypot(locCircCorrect{ii,1}(1),locCircCorrect{ii,1}(2)),...
        hypot(locCircCorrect{ii,2}(1),locCircCorrect{ii,2}(2)) ...
        hypot(locCircCorrect{ii,3}(1),locCircCorrect{ii,3}(2)) ...
        hypot(locCircCorrect{ii,4}(1),locCircCorrect{ii,4}(2))];
    legsVar(ii) = plot([1 2 3 4], ...
        varR(ii,:).*eucDist(ii,:),'o');
    hold on
end
xticks([1:4])
xlim([0 5])
% ylim([0 5])
xticklabels({'Cont 1','Cont 2','Scot 1','Scot 2'});
hold on
errorbar([1 2],[mean(varR(:,1)).*mean(eucDist(:,1)) ...
    mean(varR(:,2)).*mean(eucDist(:,2))],...
    [sem(varR(:,1)).*sem(eucDist(:,1)) ...
    sem(varR(:,2)).*sem(eucDist(:,2))],'-o',...
    'MarkerFaceColor','k');

errorbar([3 4],[mean(varR(:,3)).*mean(eucDist(:,3)) ...
    mean(varR(:,4)).*mean(eucDist(:,4))],...
    [sem(varR(:,3)).*sem(eucDist(:,3)) ...
    sem(varR(:,4)).*sem(eucDist(:,4))],'-o',...
    'MarkerFaceColor','k');

ylabel('Magnitude of EM Offset (variance * mean x y)')
title('Correct');
legend(legsVar,subjectsAll{idx(:,1)})


subplot(2,1,2)
for ii = 1:size(idx)
    varR(ii,:) = [locCircWrong{ii,1}(3) locCircWrong{ii,3}(3) ...
        locCircWrong{ii,2}(3) locCircWrong{ii,4}(3)];
    eucDist(ii,:) = [...
        hypot(locCircWrong{ii,1}(1),locCircWrong{ii,1}(2)),...
        hypot(locCircWrong{ii,2}(1),locCircWrong{ii,2}(2)) ...
        hypot(locCircWrong{ii,3}(1),locCircWrong{ii,3}(2)) ...
        hypot(locCircWrong{ii,4}(1),locCircWrong{ii,4}(2))];
    legsVar(ii) = plot([1 2 3 4], ...
        varR(ii,:).*eucDist(ii,:),'o');
    hold on
end
xticks([1:4])
xlim([0 5])
% ylim([0 5])
xticklabels({'Cont 1','Cont 2','Scot 1','Scot 2'});
hold on
errorbar([1 2],[mean(varR(:,1)).*mean(eucDist(:,1)) ...
    mean(varR(:,2)).*mean(eucDist(:,2))],...
    [sem(varR(:,1)).*sem(eucDist(:,1)) ...
    sem(varR(:,2)).*sem(eucDist(:,2))],'-o',...
    'MarkerFaceColor','k');

errorbar([3 4],[mean(varR(:,3)).*mean(eucDist(:,3)) ...
    mean(varR(:,4)).*mean(eucDist(:,4))],...
    [sem(varR(:,3)).*sem(eucDist(:,3)) ...
    sem(varR(:,4)).*sem(eucDist(:,4))],'-o',...
    'MarkerFaceColor','k');

ylabel('Magnitude of EM Offset (variance * mean x y)')
title('Incorrect');

legend(legsVar,subjectsAll{idx(:,1)})




for ii = reshape([idx],1,[])
    perfCont1(ii) = sum(Perf{ii}{1} == 1)/sum(Perf{ii}{1} == 1 | Perf{ii}{1} == 0);
    %     perfCont2(idx(ii)) = sum(Perf{idx(ii)}{1} == 1)/sum(Perf{idx(ii)}{1} == 1 | Perf{idx(ii)}{1} == 0);
    perfScot1(ii) = sum(Perf{ii}{2} == 1)/sum(Perf{ii}{2} == 1 | Perf{ii}{2} == 0);
    %     perfScot2(ii) = sum(Perf{ii}{1} == 1)/sum(Perf{ii}{1} == 1 | Perf{ii}{1} == 0);
end

figure;
subplot(1,2,1)
for ii = 1:size(idx)
    forleg1(ii) = plot([1 2],[perfCont1(idx(ii,1)) perfCont1(idx(ii,2))],...
        '-o');hold on
end
title('Control');
xticks([1:2])
xlim([0.5 2.5])
ylim([0.25 1])
xticklabels({'Day 1','Day 2'})
hold on
errorbar([1 2],[mean(perfCont1(idx(:,1))) mean(perfCont1(idx(:,2)))],...
    [sem(perfCont1(idx(:,1))) sem(perfCont1(idx(:,2)))],'-o',...
    'MarkerFaceColor','k');
[h,pCont] = ttest2((perfCont1(idx(:,1))), (perfCont1(idx(:,2))))
line([1 2],[ .3 .3],'Color','k')
text(1.5,.32,sprintf('p = %.2f',pCont));
% figure;

subplot(1,2,2)
for ii = 1:size(idx)
    forleg1(ii) = plot([1 2],[perfScot1(idx(ii,1)) perfScot1(idx(ii,2))],...
        '-o');hold on
end

title('Scotoma');
xticks([1:2])
xlim([0.5 2.5])
ylim([0.25 1])
xticklabels({'Day 1','Day 2'})
hold on
errorbar([1 2],[mean(perfScot1(idx(:,1))) mean(perfScot1(idx(:,2)))],...
    [sem(perfScot1(idx(:,1))) sem(perfScot1(idx(:,2)))],'-o',...
    'MarkerFaceColor','k');
[h,pScot] = ttest2((perfScot1(idx(:,1))), (perfScot1(idx(:,2))))
line([1 2],[ .3 .3],'Color','k')
text(1.5,.32,sprintf('p = %.2f',pScot));
legend(forleg1,subjectsAll{idx(:,1)},'Location','northwest');

ylabel('Performance')


%% Save All Figures Up TO HERE
tempdir = sprintf('%s/AshleyFigures',cd);
FolderName = tempdir;   % Your destination folder
FigList = findobj(allchild(0), 'flat', 'Type', 'figure');
for iFig = 1:length(FigList)
  FigHandle = FigList(iFig);
  FigName   = num2str(get(FigHandle, 'Number'));
  set(0, 'CurrentFigure', FigHandle);
  savefig(fullfile(FolderName, [FigName '.fig']));
  saveas(gcf,fullfile(FolderName, [FigName '.png']))
end



%% Make Single Trial Movies

% runVideoStimVisibility(drifts{1}{1}.x{78},...
%     drifts{1}{1}.y{78},...
%     sprintf('%s,Control,Task',subjectsAll{1}));


%% Performance across trials (start-end of SESSION)
numChunks = 6;
clear perfInTimeScotomaSubjectsAll ;
clear perfInTimeControlSubjectsAll ;
for ii = 1:length(subjectsAll(whichDay == 1))
    binTrials = round(1:length(data{ii}.Control)/numChunks:length(data{ii}.Control));
    for i = 1:length(binTrials)-1
        counter = 1;
        for j = binTrials(i):binTrials(i+1)
            if data{ii}.Control{j}.Correct == 3
                continue;
            end
            %                 perfInTimeControl(counter,i) = NaN;
            %             else
            perfInTimeControl(counter,i) =  data{ii}.Control{j}.Correct;
            %             end
            counter = counter + 1;
        end
    end
    binTrials = round(1:length(data{ii}.Scotoma)/numChunks:length(data{ii}.Scotoma));
    for i = 1:length(binTrials)-1
        counter = 1;
        for j = binTrials(i):binTrials(i+1)
            if data{ii}.Scotoma{j}.Correct == 3
                continue;
            end
            %                 perfInTimeScotoma(counter,i) = NaN;
            %             else
            perfInTimeScotoma(counter,i) =  data{ii}.Scotoma{j}.Correct;
            %             end
            counter = counter + 1;
        end
    end
    [numTrials,~] = size(perfInTimeControl);
    perfInTimeControlSubjectsAll{ii,:} = nansum(perfInTimeControl)/numTrials;
    [numTrials,~] = size(perfInTimeScotoma);
    perfInTimeScotomaSubjectsAll{ii,:} = nansum(perfInTimeScotoma)/numTrials;
end

figure;
subplot(2,1,1)
for ii = 1:length(subjectsAll(whichDay == 1))
    plot([1:numChunks-1],perfInTimeControlSubjectsAll{ii,:},'-o',...
        'Color',clrs(ii,:),'MarkerFaceColor',clrs(ii,:));
    hold on
end
xlabel('In Time Across Session (10x Bins)')
ylabel('Proportion Correct')
ylim([.1 .9])
title('Control')
subplot(2,1,2)
for ii = 1:length(subjectsAll(whichDay == 1))
    plot([1:numChunks-1],perfInTimeScotomaSubjectsAll{ii,:},'-o',...
        'Color',clrs(ii,:),'MarkerFaceColor',clrs(ii,:));
    hold on
end
xlabel('In Time Across Session')
ylabel('Proportion Correct')
ylim([.1 .9])
title('Scotoma')

%% Overall variance of drift measures
figure;
subplot(2,3,1)
for ii = 1:length(subjectsAll(whichDay == 1))
    idx1 = find([drifts{ii}{1}.bcea{:,1}] > 0 & [drifts{ii}{1}.bcea{:,1}] < 160);
    idx2 = find([drifts{ii}{2}.bcea{:,1}] > 0 & [drifts{ii}{2}.bcea{:,1}] < 160);
    temp1 = [drifts{ii}{1}.bcea{:,1}];
    temp2 = [drifts{ii}{2}.bcea{:,1}];
    plot([1 2],[mean(temp1(idx1)), mean(temp2(idx2)')],'-o');
    hold on
    meansAll(ii,1:2) = [mean(temp1(idx1)), mean(temp2(idx2)')];
    %     semAll(ii,1:2) = [sem([drifts{ii}{1}.amplitude(idx1,1)])' sem([drifts{ii}{2}.amplitude(idx2,1)])'];
end
errorbar([1 2],mean(meansAll),std(meansAll),'-o','Color','k','MarkerFaceColor','k');
xlim([.5 2.5])
xticks([1 2])
xticklabels({'Control','Scotoma'});
ylabel('BCEA');
title('Task');
[h,p] = ttest(meansAll(:,1),meansAll(:,2));
xlabel(sprintf('p = %.3f',p));

subplot(2,3,2)
for ii = 1:length(subjectsAll(whichDay == 1))
    idx1 = find([drifts{ii}{1}.curvature{:,1}] > 0 & [drifts{ii}{1}.curvature{:,1}] < 160);
    idx2 = find([drifts{ii}{2}.curvature{:,1}] > 0 & [drifts{ii}{2}.curvature{:,1}] < 160);
    temp1 = [drifts{ii}{1}.curvature{:,1}];
    temp2 = [drifts{ii}{2}.curvature{:,1}];
    plot([1 2],[mean(temp1(idx1)), mean(temp2(idx2)')],'-o');
    hold on
    meansAll(ii,1:2) = [mean(temp1(idx1)), mean(temp2(idx2)')];
    %     semAll(ii,1:2) = [sem([drifts{ii}{1}.amplitude(idx1,1)])' sem([drifts{ii}{2}.amplitude(idx2,1)])'];
end
errorbar([1 2],mean(meansAll),std(meansAll),'-o','Color','k','MarkerFaceColor','k');
xlim([.5 2.5])
xticks([1 2])
xticklabels({'Control','Scotoma'});
ylabel('curvature');
title('Task');
[h,p] = ttest(meansAll(:,1),meansAll(:,2));
xlabel(sprintf('p = %.3f',p));


subplot(2,3,3)
for ii = 1:length(subjectsAll(whichDay == 1))
    idx1 = find([drifts{ii}{1}.span(:,1)] > 0 & [drifts{ii}{1}.span(:,1)] < 60);
    idx2 = find([drifts{ii}{2}.span(:,1)] > 0 & [drifts{ii}{2}.span(:,1)] < 60);
    plot([1 2],[mean([drifts{ii}{1}.span(idx1,1)])' mean([drifts{ii}{2}.span(idx2,1)])'],'-o');
    hold on
    meansAll(ii,1:2) = [mean([drifts{ii}{1}.span(idx1,1)])' mean([drifts{ii}{2}.span(idx2,1)])'];
    %     semAll(ii,1:2) = [sem([drifts{ii}{1}.amplitude(idx1,1)])' sem([drifts{ii}{2}.amplitude(idx2,1)])'];
end
errorbar([1 2],mean(meansAll),std(meansAll),'-o','Color','k','MarkerFaceColor','k');
xlim([.5 2.5])
xticks([1 2])
xticklabels({'Control','Scotoma'});
ylabel('Drift span');
title('Task')
[h,p] = ttest(meansAll(:,1),meansAll(:,2));
xlabel(sprintf('p = %.3f',p));


subplot(2,3,4)
for ii = 1:length(subjectsAll(whichDay == 1))
    idx1 = find([drifts{ii}{1}.amplitude(:,1)] > 0 & [drifts{ii}{1}.amplitude(:,1)] < 60);
    idx2 = find([drifts{ii}{2}.amplitude(:,1)] > 0 & [drifts{ii}{2}.amplitude(:,1)] < 60);
    plot([1 2],[mean([drifts{ii}{1}.amplitude(idx1,1)])' mean([drifts{ii}{2}.amplitude(idx2,1)])'],'-o');
    hold on
    meansAll(ii,1:2) = [mean([drifts{ii}{1}.amplitude(idx1,1)])' mean([drifts{ii}{2}.amplitude(idx2,1)])'];
    %     semAll(ii,1:2) = [sem([drifts{ii}{1}.amplitude(idx1,1)])' sem([drifts{ii}{2}.amplitude(idx2,1)])'];
end
errorbar([1 2],mean(meansAll),std(meansAll),'-o','Color','k','MarkerFaceColor','k');
xlim([.5 2.5])
xticks([1 2])
xticklabels({'Control','Scotoma'});
ylabel('Drift Amplitude');
title('Task')
[h,p] = ttest(meansAll(:,1),meansAll(:,2));
xlabel(sprintf('p = %.3f',p));

subplot(2,3,5)
for ii = 1:length(subjectsAll(whichDay == 1))
    idx1 = find([drifts{ii}{1}.prlDistance(:,1)] > 0 & [drifts{ii}{1}.prlDistance(:,1)] < 60);
    idx2 = find([drifts{ii}{2}.prlDistance(:,1)] > 0 & [drifts{ii}{2}.prlDistance(:,1)] < 60);
    plot([1 2],[mean([drifts{ii}{1}.prlDistance(idx1,1)])' mean([drifts{ii}{2}.prlDistance(idx2,1)])'],'-o');
    hold on
    meansAll(ii,1:2) = [mean([drifts{ii}{1}.prlDistance(idx1,1)])' mean([drifts{ii}{2}.prlDistance(idx2,1)])'];
    %     semAll(ii,1:2) = [sem([drifts{ii}{1}.amplitude(idx1,1)])' sem([drifts{ii}{2}.amplitude(idx2,1)])'];
end
errorbar([1 2],mean(meansAll),std(meansAll),'-o','Color','k','MarkerFaceColor','k');
xlim([.5 2.5])
xticks([1 2])
xticklabels({'Control','Scotoma'});
ylabel('Drift prl Distance');
title('Task')
[h,p] = ttest(meansAll(:,1),meansAll(:,2));
xlabel(sprintf('p = %.3f',p));


%% MS Analysis TASK

for c = 1:2
    for ii = 1:length(subjectsAll(whichDay == 1))
        x1 = [];x2=[];y1=[];y2=[];
        fX = tracesTask{ii}.(condition{c}).xAll;
        fY = tracesTask{ii}.(condition{c}).yAll;
        
        fXClean = fX;
        [~,TFoutlier1] = rmoutliers(fX);
        fXClean(TFoutlier1) = NaN;
        fXClean = hampel(fXClean,4,2);
        
        fYClean = fY;
        [~,TFoutlier2] = rmoutliers(fY);
        fYClean(TFoutlier2) = NaN;
        fYClean = hampel(fYClean,4,2);
        
        fYClean(TFoutlier1) = NaN;
        fXClean(TFoutlier2) = NaN;
        
        [tempX,locsX] = findpeaks(fXClean,'MinPeakProminence',10);
        [tempY,locsY] = findpeaks(fYClean,'MinPeakProminence',10);
        
        
        realMS = unique([locsX,locsY]);
        numFTrials = length(fX)/5000;
        num_SPerSecond{c}(ii) = (length(realMS)/numFTrials)/5;
        for s = 1:length(realMS)
            
            [newValsX] = selectWindowFromVectorAverage(fXClean,realMS(s),10);
            [newValsY] = selectWindowFromVectorAverage(fYClean,realMS(s),10);
            
            x1 = fXClean(newValsX(1));
            x2 = fXClean(newValsY(end));
            y1 = fYClean(newValsX(1));
            y2 = fYClean(newValsX(end));
            amp_S{c}(ii,s) = pdist([x1,y1;...
                x2,y2],'euclidean');
            if amp_S{c}(ii,s) <= 4 || amp_S{c}(ii,s) == 0 || ...
                    isnan(x1) || isnan(x2) || ...
                    isnan(y1) || isnan(y2)
                amp_S{c}(ii,s) = NaN;
                startMSPos{c}{ii}(1,s) = NaN;
                startMSPos{c}{ii}(2,s) = NaN;
                endMSPos{c}{ii}(1,s) = NaN;
                endMSPos{c}{ii}(2,s) = NaN;
            else
                startMSPos{c}{ii}(1,s) = x1;
                startMSPos{c}{ii}(2,s) = y1;
                endMSPos{c}{ii}(1,s) = x2;
                endMSPos{c}{ii}(2,s) = y2;
            end
            
            
            %         vel_S(ii,s)= diff(amp_S(ii,s))
        end
        %     plot([1 2],[mean(out{ii}{1}.msRates)*1000  mean(out{ii}{2}.msRates)*1000 ],'-o');
        %     hold on
    end
end


figure;
subplot(2,1,1)
for ii = 1:length(subjectsAll(whichDay == 1))
    plot([1 2],[nanmean(amp_S{1}(ii,:))  nanmean(amp_S{2}(ii,:))],'-o');
    hold on
end
xlim([.5 2.5])
xticks([1 2])
xticklabels({'Control','Scotoma'})
ylabel('Amplitude');


subplot(2,1,2)
for ii = 1:length(subjectsAll(whichDay == 1))
    plot([1 2],[num_SPerSecond{1}(ii)  num_SPerSecond{2}(ii)],'-o');
    hold on
end
xlim([.5 2.5])
xticks([1 2])
xticklabels({'Control','Scotoma'})
ylabel('S/MS Rate');


figure;
for ii = 1:length(subjectsAll(whichDay == 1))
    subplot(3,3,ii);
    histogram(amp_S{1}(ii,:),20)
    hold on
    histogram(amp_S{2}(ii,:),20);
    hold on
    xlim([0 75])
end
%
figure;
for ii = 1:length(subjectsAll(whichDay == 1))
    subplot(3,3,ii);
    leg1(1) = plot(startMSPos{1}{ii}(1,:), startMSPos{1}{ii}(2,:),'+');
    hold on
    leg1(2) = plot(endMSPos{1}{ii}(1,:), endMSPos{1}{ii}(2,:),'x');
    %     legend(leg1,{'Start','End'});
    axis([-20 20 -20 20]);
    axis square
    for t = 1:length(startMSPos{1}{ii}(1,:))
        lh = plot([startMSPos{1}{ii}(1,t) endMSPos{1}{ii}(1,t)],...
            [startMSPos{1}{ii}(2,t) endMSPos{1}{ii}(2,t)],...
            '-','Color','k');
        lh.Color=[0,0,0,0.25];
        hold on
    end
    rectangle('Position',[-7/2,-7/2,7,7]);
    %     legend off
end
suptitle('Start and End Positions Control Task');

figure;
for ii = 1:length(subjectsAll(whichDay == 1))
    subplot(3,3,ii);
    leg1(1) = plot(startMSPos{2}{ii}(1,:), startMSPos{2}{ii}(2,:),'+');
    hold on
    leg1(2) = plot(endMSPos{2}{ii}(1,:), endMSPos{2}{ii}(2,:),'x');
    %
    axis([-20 20 -20 20]);
    axis square
    for t = 1:length(startMSPos{2}{ii}(1,:))
        lh = plot([startMSPos{2}{ii}(1,t) endMSPos{2}{ii}(1,t)],...
            [startMSPos{2}{ii}(2,t) endMSPos{2}{ii}(2,t)],...
            '-','Color','k'); 
        lh.Color=[0,0,0,0.25];
        hold on
    end
    rectangle('Position',[-7/2,-7/2,7,7]);
    %     legend off
    if ii == 1
        legend(leg1,{'Start','End'});
    end
end
suptitle('Start and End Positions Scotoma Task');


% idxBlinksNoTrack = (find(xAllF > 70 | xAllF < -70));
% idxBlinksNoTrack = [];
%
% xAllF_Clean = xAllF;
% [~,TFoutlier] = rmoutliers(xAllF_Clean);
% xAllF_Clean(TFoutlier) = NaN;
% xAllF_Clean = hampel(xAllF_Clean,4,2);
% figure;
% plot(1:length(xAllF_Clean),(xAllF_Clean),'-');
% % findpeaks(xAllF_Clean,'MinPeakProminence',10);
% ylim([-50 50])
% xlim([2500 5000])
% % plot(1:length(xAllF_Clean),smoothdata(xAllF_Clean,"gaussian",20),'-');
% temp = findpeaks(xAllF_Clean,'MinPeakProminence',10);
% numFTrials = length(xAllF_Clean)/5000;

% num_SPerSecond = (length(temp)/numFTrials)/5;

%% MS Analysis Fixation

for c = 1:2
    for ii = 1:length(subjectsAll(whichDay == 1))
        x1 = [];x2=[];y1=[];y2=[];
        fX = tracesFix{ii}.(condition{c}).xAll;
        fY = tracesFix{ii}.(condition{c}).yAll;
        
        fXClean = fX;
        [~,TFoutlier1] = rmoutliers(fX);
        fXClean(TFoutlier1) = NaN;
        fXClean = hampel(fXClean,4,2);
        
        fYClean = fY;
        [~,TFoutlier2] = rmoutliers(fY);
        fYClean(TFoutlier2) = NaN;
        fYClean = hampel(fYClean,4,2);
        
        fYClean(TFoutlier1) = NaN;
        fXClean(TFoutlier2) = NaN;
        
        [tempX,locsX] = findpeaks(fXClean,'MinPeakProminence',10);
        [tempY,locsY] = findpeaks(fYClean,'MinPeakProminence',10);
        
        
        realMS = unique([locsX,locsY]);
        numFTrials = length(fX)/5000;
        num_SPerSecond{c}(ii) = (length(realMS)/numFTrials)/5;
        for s = 1:length(realMS)
            
            [newValsX] = selectWindowFromVectorAverage(fXClean,realMS(s),10);
            [newValsY] = selectWindowFromVectorAverage(fYClean,realMS(s),10);
            
            x1 = fXClean(newValsX(1));
            x2 = fXClean(newValsY(end));
            y1 = fYClean(newValsX(1));
            y2 = fYClean(newValsX(end));
            amp_S{c}(ii,s) = pdist([x1,y1;...
                x2,y2],'euclidean');
            if amp_S{c}(ii,s) <= 4 || amp_S{c}(ii,s) == 0 || ...
                    isnan(x1) || isnan(x2) || ...
                    isnan(y1) || isnan(y2)
                amp_S{c}(ii,s) = NaN;
                startMSPos{c}{ii}(1,s) = NaN;
                startMSPos{c}{ii}(2,s) = NaN;
                endMSPos{c}{ii}(1,s) = NaN;
                endMSPos{c}{ii}(2,s) = NaN;
            else
                startMSPos{c}{ii}(1,s) = x1;
                startMSPos{c}{ii}(2,s) = y1;
                endMSPos{c}{ii}(1,s) = x2;
                endMSPos{c}{ii}(2,s) = y2;
            end
            
            
            %         vel_S(ii,s)= diff(amp_S(ii,s))
        end
        %     plot([1 2],[mean(out{ii}{1}.msRates)*1000  mean(out{ii}{2}.msRates)*1000 ],'-o');
        %     hold on
    end
end

amp_S{1}(amp_S{1} == 0) = NaN;
amp_S{2}(amp_S{2} == 0) = NaN;

figure;
subplot(2,1,1)
for ii = 1:length(subjectsAll(whichDay == 1))
    plot([1 2],[nanmean(amp_S{1}(ii,:))  nanmean(amp_S{2}(ii,:))],'-o');
    hold on
end
xlim([.5 2.5])
xticks([1 2])
xticklabels({'Control','Scotoma'})
ylabel('Amplitude');


subplot(2,1,2)
for ii = 1:length(subjectsAll(whichDay == 1))
    plot([1 2],[num_SPerSecond{1}(ii)  num_SPerSecond{2}(ii)],'-o');
    hold on
end
xlim([.5 2.5])
xticks([1 2])
xticklabels({'Control','Scotoma'})
ylabel('S/MS Rate');


figure;
for ii = 1:length(subjectsAll(whichDay == 1))
    subplot(3,3,ii);
    histogram(amp_S{1}(ii,:),20)
    hold on
    histogram(amp_S{2}(ii,:),20);
    hold on
    xlim([0 75])
end
%
figure;
for ii = 1:length(subjectsAll(whichDay == 1))
    subplot(3,3,ii);
    leg1(1) = plot(startMSPos{1}{ii}(1,:), startMSPos{1}{ii}(2,:),'+');
    hold on
    leg1(2) = plot(endMSPos{1}{ii}(1,:), endMSPos{1}{ii}(2,:),'x');
    %     legend(leg1,{'Start','End'});
    axis([-20 20 -20 20]);
    axis square
    for t = 1:length(startMSPos{1}{ii}(1,:))
        lh = plot([startMSPos{1}{ii}(1,t) endMSPos{1}{ii}(1,t)],...
            [startMSPos{1}{ii}(2,t) endMSPos{1}{ii}(2,t)],...
            '-','Color','k');
        lh.Color=[0,0,0,0.25];
        hold on
    end
    rectangle('Position',[-7/2,-7/2,7,7]);
    %     legend off
end
suptitle('Start and End Positions Control');

figure;
for ii = 1:length(subjectsAll(whichDay == 1))
    subplot(3,3,ii);
    leg1(1) = plot(startMSPos{2}{ii}(1,:), startMSPos{2}{ii}(2,:),'+');
    hold on
    leg1(2) = plot(endMSPos{2}{ii}(1,:), endMSPos{2}{ii}(2,:),'x');
    %
    axis([-20 20 -20 20]);
    axis square
    for t = 1:length(startMSPos{2}{ii}(1,:))
        lh = plot([startMSPos{2}{ii}(1,t) endMSPos{2}{ii}(1,t)],...
            [startMSPos{2}{ii}(2,t) endMSPos{2}{ii}(2,t)],...
            '-','Color','k');
        lh.Color=[0,0,0,0.25];
        hold on
    end
    rectangle('Position',[-7/2,-7/2,7,7]);
    %     legend off
    if ii == 1
        legend(leg1,{'Start','End'});
    end
end
suptitle('Start and End Positions Scotoma');

% Polar plots of ms

figure;
subplot(1,3,1)
for ii = 1:length(subjectsAll(whichDay == 1))
    clear theta
    clear rho
    clear Pos; clear vals; clear idxTheta
    x1 = startMSPos{2}{ii}(1,:)';
    x2 = endMSPos{2}{ii}(1,:)';
    y1 = startMSPos{2}{ii}(1,:)';
    y2 = endMSPos{2}{ii}(2,:)';
    idxNaN = ~isnan(x1) | ~isnan(x2) | ~isnan(y1) | ~isnan(y2);
    slope = (y2(idxNaN) - y1(idxNaN)) ./ (x2(idxNaN) - x1(idxNaN));
    [theta,rho] = cart2pol((x2(idxNaN) - x1(idxNaN)), (y2(idxNaN) - y1(idxNaN)) );
     if sign(theta) >= 0
      theta = theta;
    else
      theta = 2 * pi + theta;
     end
    polarplot(theta-0.7236,rho,'o');%,rho)
    thetaSubjVarScot(ii) = var(theta);

    hold on
end
title('Scotoma');

% figure;
subplot(1,3,2)
for ii = 1:length(subjectsAll(whichDay == 1))
    clear theta
    clear rho
    clear Pos; clear vals; clear idxTheta
    x1 = startMSPos{1}{ii}(1,:)';
    x2 = endMSPos{1}{ii}(1,:)';
    y1 = startMSPos{1}{ii}(1,:)';
    y2 = endMSPos{1}{ii}(2,:)';
    idxNaN = ~isnan(x1) | ~isnan(x2) | ~isnan(y1) | ~isnan(y2);
    slope = (y2(idxNaN) - y1(idxNaN)) ./ (x2(idxNaN) - x1(idxNaN));
    [theta,rho] = cart2pol((x2(idxNaN) - x1(idxNaN)), (y2(idxNaN) - y1(idxNaN)) );
     if sign(theta) >= 0
      theta = theta;
    else
      theta = 2 * pi + theta;
     end
    polarplot(theta-0.7236,rho,'s');%,rho)
%     polarhistogram(theta);%,rho,'o');%,rho)
    thetaSubjVar(ii) = var(theta);
    hold on
end
title(' Control');

subplot(1,3,3)
errorbar([1 2],[nanmean(thetaSubjVarScot) nanmean(thetaSubjVar)],...
    [nanstd(thetaSubjVarScot) nanstd(thetaSubjVar)],'-o');
xticks([1 2])
xticklabels({'Scotoma','Control'});
ylabel('Variance of Angle');
xlim([0 3])
axis square
[h,p] = ttest(thetaSubjVarScot,thetaSubjVar);
suptitle(sprintf('MS angle and amplitude, p = %.2f',p))

%% Offset Difference comparison **Note Fixation was 5000ms long in this condition
for ii = 1:length(subjectsAll)
    peaksLocControl{ii} = analysisTimeContour(tracesFix{ii}.Control.xAll,...
        tracesFix{ii}.Control.yAll, NaN, 10, 0);
    %     close all
    peaksLocScotoma{ii} = analysisTimeContour(tracesFix{ii}.Scotoma.xAll,...
        tracesFix{ii}.Scotoma.yAll, NaN, 10, 0);
    %     close all
    
end

symbs = {'o' 's' 'd' 'p' '^' 'v' '<' '>'};
for ii = 1:length(subjectsAll(whichDay == 1))
    %      centeredPeak(ii,1:2) = peaksLocScotoma{ii}.wCentroid - peaksLocControl{ii}.wCentroid;
    %          eucDist(ii,1) = hypot(peaksLocControl{ii}.wCentroid(1),peaksLocControl{ii}.wCentroid(2));
    %          eucDist(ii,2) = hypot(peaksLocScotoma{ii}.wCentroid(1),peaksLocScotoma{ii}.wCentroid(2));
    %      if ii == 3|| ii == 1
    %          centeredPeak(ii,1:2) = peaksLocScotoma{ii}.contourPeak - peaksLocControl{ii}.mean;
    %          eucDist(ii,1) = hypot(peaksLocControl{ii}.mean(1),peaksLocControl{ii}.mean(2));
    %          eucDist(ii,2) = hypot(peaksLocScotoma{ii}.contourPeak(1),peaksLocScotoma{ii}.mean(2));
    %      else
    centeredPeak(ii,1:2) = peaksLocScotoma{ii}.contourPeak - peaksLocControl{ii}.contourPeak;
    eucDist(ii,1) = hypot(peaksLocControl{ii}.contourPeak(1),peaksLocControl{ii}.contourPeak(2));
    eucDist(ii,2) = hypot(peaksLocScotoma{ii}.contourPeak(1),peaksLocScotoma{ii}.contourPeak(2));
    %      end
end

figure;
%  clrs = parula(5);
subplot(1,2,1)
for ii = 1:length(subjectsAll(whichDay == 1))
    plot([1 2],[eucDist(ii,1) eucDist(ii,2)],'-o',...
        'Color',clrs(ii,:),...
        'MarkerFaceColor',clrs(ii,:),'MarkerSize',8);
    hold on
end
xticks([1 2])
xticklabels({'Control','Scotoma'});
ylabel('Euclidean Distance (arcmin)');
xlim([0 3])
errorbar([1.1 2.1],[mean(eucDist(:,1)) mean(eucDist(:,2))],...
    [sem(eucDist(:,1)) sem(eucDist(:,2))],'-o','Color','k',...
    'MarkerFaceColor','k','MarkerSize',10)
[h,p1]=ttest( (eucDist(:,1)), (eucDist(:,2)))
title(sprintf('p = %.3f',p1));
subplot(1,2,2)
rectangle('Position',[-10.5,-10.5,25,25],'FaceColor',[130 130 130]./255);
rectangle('Position',[-3.5,-3.5,7,7],'FaceColor',[0 0 0]);
pos = [-2.5 -2.5 5 5];
% rectangle('Position',pos,'Curvature',[1 1],'FaceColor',[130 130 130]./255)
hold on
for i = 1:length(subjectsAll(whichDay == 1))
    %      plot(peaksLocScotoma{ii}.contourPeak(1),peaksLocScotoma{ii}.contourPeak(2)
    plot(peaksLocScotoma{i}.contourPeak(1),peaksLocScotoma{i}.contourPeak(2), ...
        'Marker', symbs{i},'MarkerSize',13,'Color',clrs(i,:),...
        'MarkerFaceColor',clrs(i,:));
    hold on
end
axis ([-10 10 -10 10])
line([0 0], [-10 10],'Color','r','LineStyle','--');
line([-10 10],[0 0],'Color','r','LineStyle','--');

title('Recentered Offsets')
forleg(1) = plot(0,0,'k','Marker','*');
suptitle('Fixation');

%% Offset of fixation in time
for ii = find((whichDay == 1))
    counter = 1;
    for pp = 1:length(data{ii}.Control)
        if data{ii}.Control{pp}.fixationTrial == 1
            fixationsC{ii}.xFixation(counter,:) = data{ii}.Control{pp}.x(1:4900);
            fixationsC{ii}.yFixation(counter,:) = data{ii}.Control{pp}.y(1:4900);
            counter = counter + 1;
        end
    end
    counter = 1;
    for pp = 1:length(data{ii}.Scotoma)
        if data{ii}.Scotoma{pp}.fixationTrial == 1
            fixationsS{ii}.xFixation(counter,:) = data{ii}.Scotoma{pp}.x(1:4900);
            fixationsS{ii}.yFixation(counter,:) = data{ii}.Scotoma{pp}.y(1:4900);
            counter = counter + 1;
            
        end
    end
end

for ii = find((whichDay == 1))
    figure('Position',[100 0 1600 1000]);
    numTrialsClump = 1:500:4900;
    for i = 1:length(numTrialsClump)-1
        subplot(4,5,i)
        resultT = generateHeatMapSimple( ...
            reshape(fixationsC{ii}.xFixation(:,numTrialsClump(i):numTrialsClump(i+1)),1,[]), ...
            reshape(fixationsC{ii}.yFixation(:,numTrialsClump(i):numTrialsClump(i+1)),1,[]), ...
            'Bins', 20,...
            'StimulusSize', 10,...
            'AxisValue', 15,...
            'Uncrowded', 0,...
            'Borders', 1);
        rectangle('Position',[-2.5 -2.5 5 5])
        ylabel('Control');
        title(i);
    end
    for i = 1:length(numTrialsClump)-1
        subplot(4,5,10+i)
        resultT = generateHeatMapSimple( ...
            reshape(fixationsS{ii}.xFixation(:,numTrialsClump(i):numTrialsClump(i+1)),1,[]), ...
            reshape(fixationsS{ii}.yFixation(:,numTrialsClump(i):numTrialsClump(i+1)),1,[]), ...
            'Bins', 15,...
            'StimulusSize', 10,...
            'AxisValue', 10,...
            'Uncrowded', 0,...
            'Borders', 1);
        rectangle('Position',[-2.5 -2.5 5 5])
        ylabel('Scotoma');
        title(i);
    end
    suptitle(subjectsAll{ii})
end

%% Offset Difference comparison FOR TASKS
for ii = find((whichDay == 1))
    peaksLocControl{ii} = analysisTimeContour(tracesTask{ii}.Control.xAll,...
        tracesTask{ii}.Control.yAll, NaN, 10, 1);
    close all
    peaksLocScotoma{ii} = analysisTimeContour(tracesTask{ii}.Scotoma.xAll,...
        tracesTask{ii}.Scotoma.yAll, NaN, 10, 1);
    close all
    
end

symbs = {'o' 's' 'd' 'p' '^' 'v' '<' '>','*','o','s','d','p','^'};
for ii = 1:length(subjectsAll)
    %      centeredPeak(ii,1:2) = peaksLocScotoma{ii}.wCentroid - peaksLocControl{ii}.wCentroid;
    %          eucDist(ii,1) = hypot(peaksLocControl{ii}.wCentroid(1),peaksLocControl{ii}.wCentroid(2));
    %          eucDist(ii,2) = hypot(peaksLocScotoma{ii}.wCentroid(1),peaksLocScotoma{ii}.wCentroid(2));
    %      if ii == 3|| ii == 1
    %          centeredPeak(ii,1:2) = peaksLocScotoma{ii}.contourPeak - peaksLocControl{ii}.mean;
    %          eucDist(ii,1) = hypot(peaksLocControl{ii}.mean(1),peaksLocControl{ii}.mean(2));
    %          eucDist(ii,2) = hypot(peaksLocScotoma{ii}.contourPeak(1),peaksLocScotoma{ii}.mean(2));
    %      else
    centeredPeak(ii,1:2) = peaksLocScotoma{ii}.contourPeak - peaksLocControl{ii}.contourPeak;
    eucDist(ii,1) = hypot(peaksLocControl{ii}.contourPeak(1),peaksLocControl{ii}.contourPeak(2));
    eucDist(ii,2) = hypot(peaksLocScotoma{ii}.contourPeak(1),peaksLocScotoma{ii}.contourPeak(2));
    %      end
end

figure;
%  clrs = parula(6);
subplot(1,2,1)
for i = find((whichDay == 1))
    plot([1 2],[eucDist(i,1) eucDist(i,2)],'-o',...
        'Color',clrs(i,:),...
        'MarkerFaceColor',clrs(i,:),'MarkerSize',8);
    hold on
end
xticks([1 2])
xticklabels({'Control','Scotoma'});
ylabel('Euclidean Distance (arcmin)');
xlim([0 3])
errorbar([1.1 2.1],[mean(eucDist(:,1)) mean(eucDist(:,2))],...
    [sem(eucDist(:,1)) sem(eucDist(:,2))],'-o','Color','k',...
    'MarkerFaceColor','k','MarkerSize',10)
[h,p1]=ttest( (eucDist(:,1)), (eucDist(:,2)))
title(sprintf('p = %.3f',p1));
subplot(1,2,2)
rectangle('Position',[-10.5,-10.5,25,25],'FaceColor',[130 130 130]./255);
rectangle('Position',[-3.5,-3.5,7,7],'FaceColor',[0 0 0]);
pos = [-2.5 -2.5 5 5];
% rectangle('Position',pos,'Curvature',[1 1],'FaceColor',[130 130 130]./255)
hold on
for i = find((whichDay == 1))
    %      plot(peaksLocScotoma{ii}.contourPeak(1),peaksLocScotoma{ii}.contourPeak(2)
    plot(peaksLocScotoma{i}.contourPeak(1),peaksLocScotoma{i}.contourPeak(2), ...
        'Marker', symbs{i},'MarkerSize',13,'Color',clrs(i,:),...
        'MarkerFaceColor',clrs(i,:));
    hold on
end
axis ([-10 10 -10 10])
line([0 0], [-10 10],'Color','r','LineStyle','--');
line([-10 10],[0 0],'Color','r','LineStyle','--');

title('Recentered Offsets')
forleg(1) = plot(0,0,'k','Marker','*');
suptitle('Task');

%% the difference between  scotoma vs control heat maps to determine where...
%  subjects were spending more time in the scotoma condition Compared to
% the control. Also we can limit the analysis to the correct trials in both
% conditions...are the heat maps different for correct vs wrong trials...

for ii = 1:length((whichDay == 1))
    counter = 1;
    for pp = 1:length(data{ii}.Control)
        if data{ii}.Control{pp}.fixationTrial == 0
            taskC{ii}.xFixation(counter,:) = data{ii}.Control{pp}.x(1:500);
            taskC{ii}.yFixation(counter,:) = data{ii}.Control{pp}.y(1:500);
            taskC{ii}.correct(counter,:) = data{ii}.Control{pp}.Correct;
            taskC{ii}.ms(counter,:) = ~isempty(data{ii}.Control{pp}.microsaccades.start);
            if ~isempty(data{ii}.Control{pp}.microsaccades.start)
                taskC{ii}.msNumInTrial(counter,:) = length(data{ii}.Control{pp}.microsaccades.start);
            else
                taskC{ii}.msNumInTrial(counter,:) = 0;
                
            end
            counter = counter + 1;
        end
    end
    counter = 1;
    for pp = 1:length(data{ii}.Scotoma)
        if data{ii}.Scotoma{pp}.fixationTrial == 0
            taskS{ii}.xFixation(counter,:) = data{ii}.Scotoma{pp}.x(1:500);
            taskS{ii}.yFixation(counter,:) = data{ii}.Scotoma{pp}.y(1:500);
            taskS{ii}.correct(counter,:) = data{ii}.Scotoma{pp}.Correct;
            taskS{ii}.ms(counter,:) = ~isempty(data{ii}.Scotoma{pp}.microsaccades.start);
            if ~isempty(data{ii}.Scotoma{pp}.microsaccades.start)
                taskS{ii}.msNumInTrial(counter,:) = length(data{ii}.Scotoma{pp}.microsaccades.start);
            else
                taskS{ii}.msNumInTrial(counter,:) = 0;
            end
            counter = counter + 1;
            
        end
    end
end

%%% Difference in MS behavior
figure;
subplot(1,2,1)
for ii = find((whichDay == 1))
    probMSScotoma(ii) = sum(taskS{ii}.msNumInTrial)/length(taskS{ii}.msNumInTrial)*2;
    probMSControl(ii) = sum(taskC{ii}.msNumInTrial)/length(taskC{ii}.msNumInTrial)*2;
    plot([1 2],[probMSControl(ii), probMSScotoma(ii)],'-o',...
        'Color',clrs(ii,:),'MarkerFaceColor',clrs(ii,:)','MarkerSize',7);
    hold on
end
axis([0.5 2.5 0 4])
xticks([1 2])
xticklabels({'Control','Scotoma'});
ylabel('Rate of Microsaccades ms/sec');
errorbar([1 2],[mean(probMSControl) mean(probMSScotoma)],...
    [sem(probMSControl) sem(probMSScotoma)],...
    '-o','Color','k','MarkerFaceColor','k','MarkerSize',7);
[h,p] = ttest(probMSControl,probMSScotoma);
title(sprintf('p = %.2f',p))

subplot(1,2,2)
for ii = find((whichDay == 1))
   trialIdx = find(taskS{ii}.ms == 1);
    probEucScotoma(ii) = nanmean(mean(hypot(taskS{ii}.xFixation(trialIdx,:),...
        taskS{ii}.yFixation(trialIdx,:))))
   trialIdx = find(taskS{ii}.ms == 0);
    probEucControl(ii) = nanmean(mean(hypot(taskS{ii}.xFixation(trialIdx,:),...
        taskS{ii}.yFixation(trialIdx,:))))
    plot([1 2],[probEucScotoma(ii), probEucControl(ii)],'-o',...
        'Color',clrs(ii,:),'MarkerFaceColor',clrs(ii,:)','MarkerSize',7);
    hold on
end
axis([0.5 2.5 0 100])
xticks([1 2])
xticklabels({'MS','No MS'});
ylabel('Distance of Gaze Scotoma (arcmin)');
errorbar([1 2],[mean(probEucScotoma( find((whichDay == 1))))...
    mean(probEucControl( find((whichDay == 1))))],...
    [sem(probEucScotoma) sem(probEucControl)],...
    '-o','Color','k','MarkerFaceColor','k','MarkerSize',7);
[h,p] = ttest(probEucScotoma( find((whichDay == 1))),probEucControl( find((whichDay == 1))));
title(sprintf('p = %.2f',p))



%% Correct vs Incorrect

%%% Difference HeatMap
%  close all
for ii = find((whichDay == 1))
    allXTaskC = reshape(taskC{ii}.xFixation,1,[]);
    allYTaskC = reshape(taskC{ii}.yFixation,1,[]);
    allXTaskCUse = allXTaskC(find(~isnan(allXTaskC) ...
        & allXTaskC < 100 & allXTaskC > -100 ...
        & allYTaskC < 100 & allYTaskC > -100));
    allYTaskCUse = allYTaskC(find(~isnan(allXTaskC) ...
        & allXTaskC < 100 & allXTaskC > -100 ...
        & allYTaskC < 100 & allYTaskC > -100));
    
    allXTaskS = reshape(taskS{ii}.xFixation,1,[]);
    allYTaskS = reshape(taskS{ii}.yFixation,1,[]);
    allXTaskSUse = allXTaskS(find(~isnan(allXTaskS) ...
        & allXTaskS < 100 & allXTaskS > -100 ...
        & allYTaskS < 100 & allYTaskS > -100));
    allYTaskSUse = allYTaskS(find(~isnan(allXTaskS) ...
        & allXTaskS < 100 & allXTaskS > -100 ...
        & allYTaskS < 100 & allYTaskS > -100));
    
    peaksLocControl{ii} = analysisTimeContour(allXTaskCUse, ...
        allYTaskCUse, NaN, 10, 1);
    
    figure(ii);
    subplot(2,2,1)
    resultC = generateHeatMapSimple( ...
        allXTaskCUse, ...
        allYTaskCUse, ...
        'Bins', 15,...
        'StimulusSize', 7,...
        'AxisValue', 15,...
        'Uncrowded', 0,...
        'Borders', 1);
    axis square
    title('Control')
    rectangle('Position',[-2.5 -2.5 5 5])
    
    subplot(2,2,2)
    resultS = generateHeatMapSimple( ...
        allXTaskSUse, ...
        allYTaskSUse, ...
        'Bins', 15,...
        'StimulusSize', 7,...
        'AxisValue', 15,...
        'Uncrowded', 0,...
        'Borders', 1);
    rectangle('Position',[-2.5 -2.5 5 5])
    
    axis square
    title('Scotoma')
    
    differenceMatrix = abs(resultS - resultC);
    subplot(2,2,3)
    temp = pcolor([-14.5 :2:14.5],[-14.5:2:14.5],normalize(differenceMatrix,'range'));
    shading interp; %interp/flat
    colormap(flipud(hot))
    colorbar
    axis square
    rectangle('Position',[-2.5 -2.5 5 5])
    
    title('Difference')
    
    subplot(2,2,4)
    resultS = generateHeatMapSimple( ...
        allXTaskSUse -  peaksLocControl{ii}.wCentroid(1), ...
        allYTaskSUse -  peaksLocControl{ii}.wCentroid(2), ...
        'Bins', 15,...
        'StimulusSize', 7,...
        'AxisValue', 15,...
        'Uncrowded', 0,...
        'Borders', 1);
    rectangle('Position',[-2.5 -2.5 5 5])
    
    axis square
    title('Scotoma - Control Center')
    suptitle(subjectsAll{ii});
end

%%% Correct vs Incorrect HeatMaps
%   valName = 'wCentroid'; %contourPeak
%Scotoma
for ii = find((whichDay == 1))
    WxFixation = []; WyFixation = []; CxFixation = []; CyFixation = [];
    counter1 = 1;
    counter2 = 1;
    for pp = 1:length(taskS{ii}.correct)
        if taskS{ii}.correct(pp,:) == 0
            WxFixation(counter1,:) = data{ii}.Scotoma{pp}.x(1:500);
            WyFixation(counter1,:) = data{ii}.Scotoma{pp}.y(1:500);
            WmsHappened(counter1) = ~isempty(data{ii}.Scotoma{pp}.microsaccades.start);
            counter1 = counter1 + 1;
        else
            CxFixation(counter2,:) = data{ii}.Scotoma{pp}.x(1:500);
            CyFixation(counter2,:) = data{ii}.Scotoma{pp}.y(1:500);
            CmsHappened(counter2) = ~isempty(data{ii}.Scotoma{pp}.microsaccades.start);
            counter2 = counter2 + 1;
        end
    end
    
    if ii == 4
        Bins = 35;
        limit = 15;
        valName = 'wCentroid';
    elseif ii == 1
        Bins = 40;
        limit = 15;
        valName = 'wCentroid';
        %     elseif ii == 5
        %         Bins = 20;
        %         limit = 15;
        %         valName = 'mean';
    else
        Bins = 20;
        limit = 15;
        valName = 'contourPeak';
    end
    
    figure;
    subplot(1,2,1)
    
    tempWrong{ii} = analysisTimeContour(reshape(WxFixation,1,[]), ...
        reshape(WyFixation,1,[]), NaN, 10, 0);
    resultW = generateHeatMapSimple( ...
        reshape(WxFixation,1,[]), ...
        reshape(WyFixation,1,[]), ...
        'Bins', Bins,...
        'StimulusSize', 7,...
        'AxisValue', limit,...
        'Uncrowded', 0,...
        'Borders', 1);
    title('Incorrect Trials')
    axis square
    rectangle('Position',[-2.5 -2.5 5 5]);
    probOfMSHappen(ii,1) = sum(WmsHappened)/length( WmsHappened);
    textWrong(ii) = hypot(tempWrong{ii}.(valName)(1), tempWrong{ii}.(valName)(2));
    %       text( -10,10,sprintf('%.2f Euc Dist', textWrong(ii)));
    
    subplot(1,2,2)
    tempRight{ii} = analysisTimeContour(reshape(CxFixation,1,[]), ...
        reshape(CyFixation,1,[]), NaN, 10, 0);
    resultR = generateHeatMapSimple( ...
        reshape(CxFixation,1,[]), ...
        reshape(CyFixation,1,[]), ...
        'Bins', Bins,...
        'StimulusSize', 7,...
        'AxisValue', limit,...
        'Uncrowded', 0,...
        'Borders', 1);
    title('Correct Trials')
    rectangle('Position',[-2.5 -2.5 5 5]);
    probOfMSHappen(ii,2) = sum(CmsHappened)/length( CmsHappened);
    suptitle(subjectsAll{ii});
    textRight(ii) = hypot(tempRight{ii}.(valName)(1), tempRight{ii}.(valName)(2));
    %       text( -10,10,sprintf('%.2f Euc Dist', textRight(ii)));
    axis square
    
    [C,m] = contour( resultR,[.68 1]);
    areaByCondition(ii).CorScot = polyarea(C(1,:),C(2,:));
    
    [C,m] = contour( resultW,[.68 1]);
    areaByCondition(ii).WrongScot = polyarea(C(1,:),C(2,:));
end

%Control
for ii = find((whichDay == 1))
    WxFixationC = []; WyFixationC = []; CxFixationC = []; CyFixationC = [];
    counter1 = 1;
    counter2 = 1;
    for pp = 1:length(taskC{ii}.correct)
        if taskC{ii}.correct(pp,:) == 0
            WxFixationC(counter1,:) = data{ii}.Control{pp}.x(1:500);
            WyFixationC(counter1,:) = data{ii}.Control{pp}.y(1:500);
            WmsHappenedC(counter1) = ~isempty(data{ii}.Control{pp}.microsaccades.start);
            counter1 = counter1 + 1;
        else
            CxFixationC(counter2,:) = data{ii}.Control{pp}.x(1:500);
            CyFixationC(counter2,:) = data{ii}.Control{pp}.y(1:500);
            CmsHappenedC(counter2) = ~isempty(data{ii}.Control{pp}.microsaccades.start);
            counter2 = counter2 + 1;
        end
    end
    
    if ii == 4
        Bins = 35;
        limit = 15;
        valName = 'wCentroid';
    elseif ii == 1
        Bins = 40;
        limit = 15;
        valName = 'wCentroid';
    elseif ii == 2
        Bins = 35;
        limit = 15;
        valName = 'contourPeak';
        %     elseif ii == 5
        %         Bins = 20;
        %         limit = 15;
        %         valName = 'mean';
    else
        Bins = 20;
        limit = 15;
        valName = 'contourPeak';
    end
    
    figure;
    subplot(1,2,1)
    
    tempWrongC{ii} = analysisTimeContour(reshape(WxFixationC,1,[]), ...
        reshape(WyFixationC,1,[]), NaN, 10, 0);
    resultW = generateHeatMapSimple( ...
        reshape(WxFixationC,1,[]), ...
        reshape(WyFixationC,1,[]), ...
        'Bins', Bins,...
        'StimulusSize', 7,...
        'AxisValue', limit,...
        'Uncrowded', 0,...
        'Borders', 1);
    title('Incorrect Trials')
    axis square
    rectangle('Position',[-2.5 -2.5 5 5]);
    probOfMSHappenC(ii,1) = sum(WmsHappenedC)/length( WmsHappenedC);
    textWrongC(ii) = hypot(tempWrongC{ii}.(valName)(1), tempWrongC{ii}.(valName)(2));
    %     text( -10,10,sprintf('%.2f Euc Dist', textWrongC(ii)));
    
    if ii == 2
        valName = 'wCentroid';
    end
    subplot(1,2,2)
    tempRightC{ii} = analysisTimeContour(reshape(CxFixationC,1,[]), ...
        reshape(CyFixationC,1,[]), NaN, 10, 0);
    resultR = generateHeatMapSimple( ...
        reshape(CxFixationC,1,[]), ...
        reshape(CyFixationC,1,[]), ...
        'Bins', Bins,...
        'StimulusSize', 7,...
        'AxisValue', limit,...
        'Uncrowded', 0,...
        'Borders', 1);
    title('Correct Trials')
    rectangle('Position',[-2.5 -2.5 5 5]);
    probOfMSHappenC(ii,2) = sum(CmsHappenedC)/length( CmsHappenedC);
    suptitle(subjectsAll{ii});
    textRightC(ii) = hypot(tempRightC{ii}.(valName)(1), tempRightC{ii}.(valName)(2));
    %     text( -10,10,sprintf('%.2f Euc Dist', textRightC(ii)));
    axis square
    
    [C,m] = contour( resultR,[.68 1]);
    areaByCondition(ii).CorCon = polyarea(C(1,:),C(2,:));
    
    [C,m] = contour( resultW,[.68 1]);
    areaByCondition(ii).WrongCon = polyarea(C(1,:),C(2,:));

end

%% 68% Contour Ellipse Vals (from Ben Code)

AOAreas = NaN(length(subjectsAll),4);
AOAreas(3,:) = [45.1 26.9 43.6 38.2];
AOAreas(1,:) = [30.5 23.0 58.9 39.0];
AOAreas(4,:) = [58.1 51.0 161.9 84.8];
AOAreas(2,:) = [63.4 NaN 60.8 72.4];
AOAreas(5,:) = [89.7 56.8 87.7 74.9];


figure;
clear forlegs
for ii = find((whichDay == 1))
    forlegs(1) = plot([1:2], areaByConditionALL(ii,[1,3]),'-o');
    hold on
    plot([3:4], areaByConditionALL(ii,[2,4]),'-o');
    hold on
    plot([1:2], AOAreas(ii,[1,2]),'--*');
    hold on
    forlegs(2) = plot([3:4], AOAreas(ii,[3,4]),'--*');

end
xlim([0 5])
xticks([1:4])
xticklabels({'Fix Cont', 'Fix Scot', ...
    'Task Cont', 'Task Scot'})
ylabel('68% Area');
xlabel('Condition');
legend(forlegs,{'DDPI','AO'})
% [h,p] = anova(scotAreas(:,1),scotAreas(:,2))

isVals = [1 2 3 4 ...
        1 3 4 ...
        1 2 3 4 ...
        1 2 3 4 ...
        1 2 3 4];
[~,~,stats] = anovan([AOAreas(~isnan(AOAreas))]',{isVals});
[results,~,~,gnames] = multcompare(stats);
yticklabels({'Fix Cont', 'Fix Scot', ...
    'Task Cont', 'Task Scot'});
title('AO Areas');

scotAreas = NaN(length(subjectsAll),2);
scotAreas(3,:) = [54.5 16.1];
scotAreas(1,:) = [31.8 19.3];
scotAreas(4,:) = [75.3 NaN];
scotAreas(2,:) = [70.8 44.9];
scotAreas(5,:) = [74.3 28.8];


figure;
for ii = find((whichDay == 1))
  
    forlegs(1) = plot([1:2], [areaByCondition(ii).CorScot,...
        areaByCondition(ii).WrongScot],...
        '-o');
    hold on
    forlegs(2) = plot([1:2], scotAreas(ii,[1,2]),'--*');
   
end
xlim([0 3])
xticks([1:2])
xticklabels({'Scot Correct', 'Scot Incorrect'})
ylabel('68% Area');
xlabel('Condition');
legend(forlegs,{'DDPI','AO'},'Location','northwest')
[h,p] = ttest(scotAreas(:,1),scotAreas(:,2))
title(sprintf('p = %.2f',p))

figure;
for ii = find((whichDay == 1))
    forlegs(1) = plot([1:2], [nanmean([areaByCondition(ii).CorScot,scotAreas(ii,1)]),...
        nanmean([areaByCondition(ii).WrongScot,scotAreas(ii,2)])],...
        '-s');
    hold on
    avMatrix(ii,:) = [nanmean([areaByCondition(ii).CorScot,scotAreas(ii,1)]),...
        nanmean([areaByCondition(ii).WrongScot,scotAreas(ii,2)])];
end

xlim([0 3])
xticks([1:2])
xticklabels({'Scot Correct', 'Scot Incorrect'})
ylabel('68% Area');
xlabel('Condition');
[h,p] = ttest(avMatrix(find(avMatrix(:,1) > 0),1),...
    avMatrix(find(avMatrix(:,2) > 0),2))
title(sprintf('p = %.2f',p))

%% MS vs NO MS
% idx = [2 6; 4 7; 3 8; 1 9; 10 11; 12 13]; %idx of day 1 and 2 subjects
dataAll = [];
counter = 1;
for ii = 1:length(subjectsAll)
    if whichDay(ii) == 1
        for c = 1:2
            for pp = 1:length(data{ii}.(conditions{c}))
                if data{ii}.(conditions{c}){pp}.fixationTrial ||...
                         data{ii}.(conditions{c}){pp}.Correct == 3
                    continue
                end
                dataAll(counter).subject = ii;
                dataAll(counter).condition = c - 1;
                dataAll(counter).day = 1;
                dataAll(counter).trial = pp;
                dataAll(counter).correct = data{ii}.(conditions{c}){pp}.Correct;
                if isempty(data{ii}.(conditions{c}){pp}.microsaccades.start)
                    dataAll(counter).msTrial = 0;
                else
                    dataAll(counter).msTrial = 1;
                end
                counter = counter + 1;
            end
        end
    else
         for c = 1:2
            for pp = 1:length(data{ii}.(conditions{c}))
                if data{ii}.(conditions{c}){pp}.fixationTrial || ...
                         data{ii}.(conditions{c}){pp}.Correct == 3
                    continue
                end
                dataAll(counter).subject = idx(find(idx(:,2) == ii),1);
                dataAll(counter).condition = c-1;
                dataAll(counter).day = 2;
                dataAll(counter).trial = pp;
                dataAll(counter).correct = data{ii}.(conditions{c}){pp}.Correct;
                if isempty(data{ii}.(conditions{c}){pp}.microsaccades.start)
                    dataAll(counter).msTrial = 0;
                else
                    dataAll(counter).msTrial = 1;
                end
                counter = counter + 1;
            end
         end
    end
end

% dataAllT = struct2table(dataAll)

indSubjectRows = (unique([dataAll.subject]));
for ii = 1:length(indSubjectRows)
    i = indSubjectRows(ii);
    curIdx1 = find([dataAll.subject] == i & ...
        [dataAll.condition] == 1 & ...
        [dataAll.msTrial] == 1);
    if length(curIdx1) > 0
       perfMSTrials(ii) = sum([dataAll(curIdx1).correct])/length((curIdx1));
    else
       perfMSTrials(ii) = NaN; 
    end
    curIdx2 = find([dataAll.subject] == i & ...
        [dataAll.condition] == 1 & ... 
        [dataAll.msTrial] == 0);
    if length(curIdx2) > 0
        perfMSXTrials(ii) = sum([dataAll(curIdx2).correct])/length((curIdx2));
    else
        perfMSXTrials(ii) = NaN;
    end
end

figure;
subplot(1,2,1)
errorbar([1 2],[mean(perfMSTrials) mean(perfMSXTrials)],...
    [sem(perfMSTrials) sem(perfMSXTrials)],'-o');
xticks([1 2])
xticklabels({'MS Trial','No MS'});
xlim([0 3])
ylabel('Performance')
[h,p] = ttest(perfMSTrials, perfMSXTrials);
title(sprintf('Scotoma, p = %.2f',p));
ylim([.25 .8])


indSubjectRows = (unique([dataAll.subject]));
for ii = 1:length(indSubjectRows)
    i = indSubjectRows(ii);
    curIdx1 = find([dataAll.subject] == i & ...
        [dataAll.condition] == 0 & ...
        [dataAll.msTrial] == 1);
    if length(curIdx1) > 0
       perfMSTrials(ii) = sum([dataAll(curIdx1).correct])/length((curIdx1));
    else
       perfMSTrials(ii) = NaN; 
    end
    curIdx2 = find([dataAll.subject] == i & ...
        [dataAll.condition] == 0 & ... 
        [dataAll.msTrial] == 0);
    if length(curIdx2) > 0
        perfMSXTrials(ii) = sum([dataAll(curIdx2).correct])/length((curIdx2));
    else
        perfMSXTrials(ii) = NaN;
    end
end

subplot(1,2,2)
errorbar([1 2],[mean(perfMSTrials) mean(perfMSXTrials)],...
    [sem(perfMSTrials) sem(perfMSXTrials)],'-o');
xticks([1 2])
xticklabels({'MS Trial','No MS'});
xlim([0 3])
ylabel('Performance')
ylim([.25 .8])
[h,p] = ttest(perfMSTrials, perfMSXTrials);
title(sprintf('Control, p = %.2f',p));

%% % Difference in Euc Dist for Control vs Scotoma
figure;
for ii = find((whichDay == 1))
    plot([2 1],[textRight(ii),textRightC(ii)],'-o',...
        'Color',clrs(ii,:),'MarkerSize',7,'MarkerFaceColor',clrs(ii,:));
    hold on
end
errorbar([2 1],[mean(textRight) mean(textRightC)],...
    [sem(textRight) sem(textRightC)],'-o',...
    'color','k','MarkerFaceColor','k','MarkerSize',7);
xticks([1 2])
ylim([0 10])
xlim([0.5 2.5])
xticklabels({'Control','Scotoma'});
ylabel('Euclidean Distance (arcmin)');
[h,p] = ttest(textRight,textRightC);


%%% Probability of MS in wrong or right trials
figure;
%   subplot(1,2,1)
for ii = find((whichDay == 1))
    plot([1 2],[probOfMSHappen(ii,1), probOfMSHappen(ii,2)],'-o',...
        'Color',clrs(ii,:),'MarkerFaceColor',clrs(ii,:)','MarkerSize',7);
    hold on
end
xlim([0.5 2.5])
xticks([1 2])
xticklabels({'Incorrect','Correct'});
ylabel('Probability of MS in Trial');
errorbar([1 2],[mean(probOfMSHappen(find((whichDay == 1)),:))],[sem(probOfMSHappen(:,1)) sem(probOfMSHappen(:,2))],...
    '-o','Color','k','MarkerFaceColor','k','MarkerSize',7);
[h,p] = ttest(probOfMSHappen(:,1),probOfMSHappen(:,2));
title(sprintf('Scotoma, p = %.2f',p))

figure;
%   subplot (1,2,2)
for ii = find((whichDay == 1))
    plot([1 2],[textWrong(ii), textRight(ii)],'-o');
    hold on
end
%   axis([0.5 2.5 .4 .6])
xticks([1 2])
xlim([0.5 2.5])
xticklabels({'Wrong','Right'});
ylabel('Euclidean Distance of Gaze');
errorbar([1 2],mean([textWrong(find(whichDay == 1))', textRight(find(whichDay == 1))']),[sem(textWrong) sem(textRight)],...
    '-o','Color','k','MarkerFaceColor','k');
[h,p] = ttest(textWrong,textRight);
title(sprintf('p = %.2f',p))

%%
