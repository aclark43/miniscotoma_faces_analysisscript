function pptrials = convertDataToPPtrialsTemp(dataALL, dateCollect)
%%% Fixed code to correctly count fixation trials: 09/19/202 & correctly
%%% save x shift
% save xshift in pptrials: 09/10/2022
dateFix2 = datetime(2022,09,10);
dateFix1 = datetime(2022,09,24);


timingsRounded = round(dataALL.eye_data.timing.elapsed, 2);

dataI = dataALL.user_data.variables;
dataTracesX = dataALL.eye_data.eye_1.calibrated_x;
dataTracesY = dataALL.eye_data.eye_1.calibrated_y;

allFields = fieldnames(dataI);
fixationCounter = 0;
pptrials = [];
for ii = 1:length(allFields)
    if startsWith(allFields{ii},'trial')
        trialNumber = str2double((regexp(allFields{ii},'\d*','Match')));
        pptrials{trialNumber} = dataI.(allFields{ii});
        
        if dateCollect < dateFix1
            pptrials{trialNumber}.pixelAngle = .2526;
            pptrials{trialNumber}.sRate = 1000;
            if isfield(dataI,(sprintf('recal%iData',trialNumber)))
                fixationCounter = 0;
            else
                fixationCounter = 1;
            end
            pptrials{trialNumber}.xshift = dataI.(sprintf('recal%iData',trialNumber-fixationCounter)).xshift;
            pptrials{trialNumber}.yshift = dataI.(sprintf('recal%iData',trialNumber-fixationCounter)).yshift;
            
            if pptrials{trialNumber}.Correct == 3
                onTemp = (round(pptrials{trialNumber}.TimeFixationON*.001,3));
                [~,on]=min(abs(timingsRounded-onTemp));
                
                offTemp = (round(pptrials{trialNumber}.TimeFixationOFF*.001,3));
                [~,off]=min(abs(timingsRounded-offTemp));
                pptrials{trialNumber}.fixationTrial = 1;
                
            else
                onTemp = (round(pptrials{trialNumber}.TimeTargetON*.001,3));
                [~,on]=min(abs(timingsRounded-onTemp));
                
                offTemp = (round(pptrials{trialNumber}.TimeTargetOFF*.001,3));
                [~,off]=min(abs(timingsRounded-offTemp));
                pptrials{trialNumber}.fixationTrial = 0;
            end
        else
            if pptrials{trialNumber}.fixationTrial
                onTemp = (round(pptrials{trialNumber}.TimeFixationON*.001,3));
                [~,on]=min(abs(timingsRounded-onTemp));
                
                offTemp = (round(pptrials{trialNumber}.TimeFixationOFF*.001,3));
                [~,off]=min(abs(timingsRounded-offTemp));
                
                
            else
                onTemp = (round(pptrials{trialNumber}.TimeTargetON*.001,3));
                [~,on]=min(abs(timingsRounded-onTemp));
                
                offTemp = (round(pptrials{trialNumber}.TimeTargetOFF*.001,3));
                [~,off]=min(abs(timingsRounded-offTemp));
                
            end
        end
    pptrials{trialNumber}.sRate = (off-on)*2;
            x = (dataTracesX(on:off)+ ...
                (pptrials{trialNumber}.xshift*pptrials{trialNumber}.pixelAngle));
            y = (dataTracesY(on:off)+...
                (pptrials{trialNumber}.yshift*pptrials{trialNumber}.pixelAngle));
    
    pptrials{trialNumber}.x = x;
    pptrials{trialNumber}.y = y;
    end
end


% % 
% % dataI = dataALL.user_data.variables;
% % dataTracesX = dataALL.eye_data.eye_1.calibrated_x;
% % dataTracesY = dataALL.eye_data.eye_1.calibrated_y;
% % 
% % allFields = fieldnames(dataI);
% % fixationCounter = 0;
% % 
% % for ii = 1:length(allFields)
% %     if startsWith(allFields{ii},'trial')
% %         trialNumber = str2double((regexp(allFields{ii},'\d*','Match')));
% %         pptrials{trialNumber} = dataI.(allFields{ii});
% %         if dateCollect < dateFix1
% %             if pptrials{trialNumber}.TimeFixationON > 0 || pptrials{trialNumber}.TimeFixationOFF > 0
% %                 on = pptrials{trialNumber}.TimeFixationON;
% %                 off = on + 5000;
% %                 if off == 0
% %                    off = on + 5000;
% %                 end
% %                 if on == 0
% %                     on =  pptrials{trialNumber}.FrameTargetON;
% %                     off =  pptrials{trialNumber}.FrameTargetON + 5000;
% % %                     continue;
% %                 end
% %                 if on > length(dataTracesX)
% %                     on =  pptrials{trialNumber}.FrameTargetON;
% %                     off =  pptrials{trialNumber}.FrameTargetON + 5000;
% % %                     continue
% %                 end
% %             else
% %                 on = pptrials{trialNumber}.TimeTargetON;
% %                 off = pptrials{trialNumber}.TimeTargetOFF;
% %             end
% %             if dateCollect < dateFix2
% %                 if isfield(dataI,(sprintf('recal%iData',trialNumber)))
% %                     fixationCounter = 0;
% % %                     xOff1 = dataI.(sprintf('recal%iData',trialNumber-fixationCounter)).xshift * pptrials{trialNumber}.pixelAngle;
% % %                     yOff1 = dataI.(sprintf('recal%iData',trialNumber-fixationCounter)).yshift * pptrials{trialNumber}.pixelAngle;
% % %                     
% % %                     xOff = dataI.(sprintf('recal%iData',trialNumber-fixationCounter)).xshift *...
% % %                         pptrials{trialNumber}.pixelAngle - xOff1;
% % %                     yOff = dataI.(sprintf('recal%iData',trialNumber-fixationCounter)).yshift * ...
% % %                         pptrials{trialNumber}.pixelAngle - yOff1;
% %                     
% %                 else
% %                     fixationCounter = 1;
% % %                     xOff = 0;
% % %                     yOff = 0;
% % %                     xOff1 = dataI.(sprintf('recal%iData',trialNumber-fixationCounter)).xshift * pptrials{trialNumber}.pixelAngle;
% % %                     yOff1 = dataI.(sprintf('recal%iData',trialNumber-fixationCounter)).yshift * pptrials{trialNumber}.pixelAngle;
% % %                     
% % %                     xOff = dataI.(sprintf('recal%iData',trialNumber-fixationCounter)).xshift *...
% % %                         pptrials{trialNumber}.pixelAngle - xOff1;
% % %                     yOff = dataI.(sprintf('recal%iData',trialNumber-fixationCounter)).yshift * ...
% % %                         pptrials{trialNumber}.pixelAngle - yOff1;
% %                 end
% %                 
% %                %
% % %                  xOff = 0;
% % %                  yOff = 0;
% %                   if on > length(dataTracesX)
% %                       x = 0;
% %                       y = 0;
% %                       continue;
% %                   end
% %                 x = (dataTracesX(on:off)-dataTracesX(on));%+xOff);%dataI.(sprintf('recal%iData',trialNumber-fixationCounter)).xshift*pptrials{trialNumber}.pixelAngle);
% %                 y = (dataTracesY(on:off)-dataTracesY(on));%+yOff);%dataI.(sprintf('recal%iData',trialNumber-fixationCounter)).yshift*pptrials{trialNumber}.pixelAngle);
% %             else
% %                 x = (dataTracesX(on:off)+pptrials{trialNumber}.xshift*pptrials{trialNumber}.pixelAngle);
% %                 y = (dataTracesY(on:off)+pptrials{trialNumber}.yshift*pptrials{trialNumber}.pixelAngle);
% %             end
% %         elseif dateCollect > dateFix1
% %             if pptrials{trialNumber}.fixationTrial%pptrials{trialNumber}.Correct == 3 
% %                  on = pptrials{trialNumber}.TimeFixationON*.960;
% %                  off = pptrials{trialNumber}.TimeFixationOFF*.960;
% %             else
% %                 on = pptrials{trialNumber}.TimeTargetON*.960;
% %                 off = pptrials{trialNumber}.TimeTargetOFF*.960;
% %             end
% %             if dateCollect < dateFix2
% %                 if isfield(dataI,(sprintf('recal%iData',trialNumber)))
% %                     fixationCounter = 0;
% %                 else
% %                     fixationCounter = 1;
% %                 end
% %                 
% % %                 xOff = dataI.(sprintf('recal%iData',trialNumber-fixationCounter)).xshift * pptrials{trialNumber}.pixelAngle;
% % %                 yOff = dataI.(sprintf('recal%iData',trialNumber-fixationCounter)).yshift * pptrials{trialNumber}.pixelAngle;
% %                 %
% %                 x = (dataTracesX(on:off))+dataI.(sprintf('recal%iData',trialNumber-fixationCounter)).xshift*pptrials{trialNumber}.pixelAngle;
% %                 y = (dataTracesY(on:off))+dataI.(sprintf('recal%iData',trialNumber-fixationCounter)).yshift*pptrials{trialNumber}.pixelAngle;
% %             else
% %                 x = (dataTracesX(on:off)+pptrials{trialNumber}.xshift*pptrials{trialNumber}.pixelAngle);
% %                 y = (dataTracesY(on:off)+pptrials{trialNumber}.yshift*pptrials{trialNumber}.pixelAngle);
% %             end
% %         end
% % 
% %         pptrials{trialNumber}.x = x;
% %         pptrials{trialNumber}.y = y; 
% %     end
% % end
% %  
end