function [p, meanValsCL, meanValsPT, meanVals] = histogramNormalizedForRotation(subjectsAll, control, rotatedValsPT, rotatedValsControl, coordVal, task)

figure;
    hold on
    for ii = 1:length(subjectsAll)
        if strcmp(subjectsAll{ii},'HUX5') && task == 1
            meanVals(ii) = NaN;
            continue;
        end
        if find(control(ii))
            allHistOut{ii} = histogram(rotatedValsControl{ii}(:,coordVal)','BinLimits',[-60 60],...
                'BinEdges', [-60:0.5:60]);
%             meanVals(ii) = allControl{ii};
        else
            allHistOut{ii} = histogram(rotatedValsPT{ii}(:,coordVal)','BinLimits',[-60 60],...
                'BinEdges', [-60:0.5:60]);
%             meanVals(ii) = allRotated{ii}(:,2)';
        end
        meanVals(ii) = mean(allHistOut{ii}.Data);
        temp = find(allHistOut{ii}.Values == max(allHistOut{ii}.Values));
        if length(temp) > 1
            maxBin(ii) = temp(1);
        else
            maxBin(ii) = temp;
        end
        valBin(ii) = allHistOut{ii}.BinEdges(maxBin(ii));
    end
    
    [h,p] = ttest2(meanVals(control == 1), meanVals(control == 0));
    
    %%plot meaned new histogram
    for ii = 1:length(subjectsAll)
        if strcmp(subjectsAll{ii},'HUX5') && task == 1
            meanBinNorm(ii,:) = NaN;
             continue;
         end
        for i = 1:length(allHistOut{1}.BinEdges)-1
            meanBin(ii,i) = (allHistOut{ii}.Values(i));
        end
        meanBinNorm(ii,:) = normalizeVector(meanBin(ii,:));
    end
%     close figure 20
    
    figure; %normalizes
    if task == 1
        figsHist(1) = plot(allHistOut{1}.BinEdges(1:end-1), ...
            normalizeVector(mean(meanBinNorm(control == 1,:))),...
            'r');
        hold on
        figsHist(2) =plot(allHistOut{1}.BinEdges(1:end-1), ...
            normalizeVector(nanmean(meanBinNorm(control == 0,:))),...
            'b');
    else
        figsHist(1) = plot(allHistOut{1}.BinEdges(1:end-1), ...
            mean(meanBinNorm(control == 1,:)),...
            'r');
        hold on
        figsHist(2) =plot(allHistOut{1}.BinEdges(1:end-1), ...
            mean(meanBinNorm(control == 0,:)),...
            'b');
    end
    line([mean(meanVals(control == 1)) mean(meanVals(control == 1))],...
        [0 1],'Color','r');
    line([nanmean(meanVals(control == 0)) nanmean(meanVals(control == 0))],...
        [0 1],'Color','b');
    legend(figsHist,{'Control','Patient'})
    ylabel('Normalized Bin Means');
    if coordVal == 1
        xlabel('X Gaze Position Rotated');
    else
        xlabel('Y Gaze Position Rotated');
    end
    
%     xlim([-45 45])
    title(sprintf(' p = %.3f',p));
    
    meanValsCL.mean = mean(meanVals(control == 1));
    meanValsCL.std = std(meanVals(control == 1));
    meanValsCL.sem = sem(meanVals(control == 1));
    meanValsPT.mean = mean(meanVals(control == 0));
    meanValsPT.std = std(meanVals(control == 0));
    meanValsPT.sem = sem(meanVals(control == 0));
    