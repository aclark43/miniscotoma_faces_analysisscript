function [meanAllAmps allEM] = getAmplitudes(subjectMSCondition, subNum)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

for ii = 1:subNum
    if isfield(subjectMSCondition(ii).em,'ecc_0')
        swAll = fieldnames(subjectMSCondition(ii).em.ecc_0);
        counter = 1;
        for i = 1:numel(fieldnames(subjectMSCondition(ii).em.ecc_0)) %loop through SW
            sw = char(swAll(i));
            
            for numA = 1:length(subjectMSCondition(ii).em.ecc_0.(sw).msAmplitude)
                
                for numAA = 1:length(subjectMSCondition(ii).em.ecc_0.(sw).msAmplitude{numA})
                    temp = subjectMSCondition(ii).em.ecc_0.(sw).msAmplitude{numA}(numAA);
                    msAmp(counter) = temp;
                    
                    temp2 = subjectMSCondition(ii).em.ecc_0.(sw).msAngle{numA}(numAA);
                    msAngle(counter) = temp2;
                    
                    %                     temp3 = subjectMSCondition(ii).em.ecc_0.(sw).velocityMax;
                   temp3 = subjectMSCondition(ii).em.ecc_0.(sw).velocityMax{numA}(numAA);
                   msVel(counter) = temp3;
                    
%                     temp3 = subjectMSCondition(ii).em.ecc_0.(sw).velocityMax{numA}(numAA);
%                     msAngle(counter) = temp2;
%                     stimSize = 
                    counter = counter + 1;
                end
                
            end
        end
        meanAllAmps(ii) = mean(msAmp(~isnan(msAmp)));
        allEM{1,ii} = msAmp; 
        allEM{2,ii} = msAngle;
        allEM{3,ii} = msVel;
    else
        meanAllAmps(ii) = NaN;
        allEM{1,ii} = [];
        allEM{2,ii} = [];
        allEM{3,ii} = [];

    end
end

end

