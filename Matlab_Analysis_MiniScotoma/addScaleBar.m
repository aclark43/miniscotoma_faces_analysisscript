function addScaleBar(img,lengthArcmin,pixPerDeg)
% function for adding a scale bar of a certain length to a figure. Make
% sure 'hold on' is applied before calling this function

hSca = 20;      % height of 20 pixels for scale bar
wSca = (lengthArcmin/60)*pixPerDeg;   % width: 10 arcmin
xSca = 20;                      % left edge
ySca = size(img,1)-20-hSca;     % top edge
r = rectangle('Position',[xSca,ySca,wSca,hSca]);
r.FaceColor = [1 1 1];
r.EdgeColor = 'none';
textToDisplay = sprintf('%d arcmin',lengthArcmin);
t = text(xSca,ySca-1.2*hSca,textToDisplay);
t.Color = 'w';
t.FontSize = 12;
t.FontWeight = 'bold';

end