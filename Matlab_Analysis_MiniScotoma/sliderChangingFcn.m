function sliderChangingFcn(~,event,data,heatObj)
    % Update heatmap and title with new selection of data
    value = round(event.Value);
    heatObj.ColorData = data(:,:,value);
    heatObj.Title = sprintf('Frame #%d',value);
end
