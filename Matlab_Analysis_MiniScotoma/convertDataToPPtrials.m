function pptrials = convertDataToPPtrials(dataALL, dateCollect)
%%% Fixed code to correctly count fixation trials: 09/19/202 & correctly
%%% save x shift
% save xshift in pptrials: 09/10/2022
dateFix2 = datetime(2022,09,10);
dateFix1 = datetime(2022,09,24);

timingsRounded = round(dataALL.eye_data.timing.elapsed, 2);
counter = dataALL.eye_data.timing.spd(1);
framesAddedAll = [];sampRateAll = [];
for i = 1:length(dataALL.eye_data.timing.spd)
    framesAdded =(ones(1,dataALL.eye_data.timing.spd(i))...
        * dataALL.eye_data.timing.elapsed...
        (counter));
    framesAddedAll = [framesAddedAll framesAdded];
    sampRate =ones(1,dataALL.eye_data.timing.spd(i))...
        * double(dataALL.eye_data.timing.spd(i));
    sampRateAll = [sampRateAll sampRate];
    counter = length(framesAddedAll)+1;
end
dataI = dataALL.user_data.variables;
dataTracesX = dataALL.eye_data.eye_1.calibrated_x;
dataTracesY = dataALL.eye_data.eye_1.calibrated_y;

allFields = fieldnames(dataI);
fixationCounter = 0;
pptrials = [];
for ii = 1:length(allFields)
    if startsWith(allFields{ii},'trial')
        trialNumber = str2double((regexp(allFields{ii},'\d*','Match')));
        pptrials{trialNumber} = dataI.(allFields{ii});
        if dateCollect > datetime(2023,05,01)
            fixName = 'fixationTrial';
        else
            fixName = 'FixationTrial';
        end
        if pptrials{trialNumber}.(fixName) 
            onTemp = (round(pptrials{trialNumber}.TimeFixationOn*.001,3));
            [~,on]=min(abs(timingsRounded-onTemp));

            offTemp = (round(pptrials{trialNumber}.TimeFixationOff*.001,3));
            [~,off]=min(abs(timingsRounded-offTemp));
            
            onTemp = (round(pptrials{trialNumber}.TimeFixationOn*.001,3));
            [~,onStab]=min(abs(framesAddedAll-onTemp));
            
            offTemp = (round(pptrials{trialNumber}.TimeFixationOff*.001,3));
            [~,offStab]=min(abs(framesAddedAll-offTemp));

        else
            if dateCollect > datetime(2023,05,01)
                onTemp = (round(pptrials{trialNumber}.TimeTargetON*.001,3));
                [~,on]=min(abs(timingsRounded-onTemp));
                
                offTemp = (round(pptrials{trialNumber}.TimeTargetOFF*.001,3));
                [~,off]=min(abs(timingsRounded-offTemp));
                
                onTemp = (round(pptrials{trialNumber}.TimeTargetON*.001,3));
                [~,onStab]=min(abs(framesAddedAll-onTemp));
                
                offTemp = (round(pptrials{trialNumber}.TimeTargetOFF*.001,3));
                [~,offStab]=min(abs(framesAddedAll-offTemp));
            else
                onTemp = (round(pptrials{trialNumber}.TimeImageOn*.001,3));
                [~,on]=min(abs(timingsRounded-onTemp));
                
                offTemp = (round(pptrials{trialNumber}.TimeImageOff*.001,3));
                [~,off]=min(abs(timingsRounded-offTemp));
                
                onTemp = (round(pptrials{trialNumber}.TimeImageOn*.001,3));
                [~,onStab]=min(abs(framesAddedAll-onTemp));
                
                offTemp = (round(pptrials{trialNumber}.TimeImageOff*.001,3));
                [~,offStab]=min(abs(framesAddedAll-offTemp));
            end

        end
        
            x = (dataTracesX(on:off)+ ...
                (pptrials{trialNumber}.xoffset*pptrials{trialNumber}.pixelAngle));
            y = (dataTracesY(on:off)+...
                (pptrials{trialNumber}.yoffset*pptrials{trialNumber}.pixelAngle));
            
            offset = abs(onStab-on);
            
%             sampRateChange = sampRateAll(onStab:offStab);
%             
%             xStab = (dataTracesX(onStab:sampRateChange:offStab)+ ...
%                     (pptrials{trialNumber}.xoffset*pptrials{trialNumber}.pixelAngle));
%             yStab = (dataTracesY(onStab:sampRateChange:offStab)+...
%                     (pptrials{trialNumber}.yoffset*pptrials{trialNumber}.pixelAngle));
                
%             figure;
%             plot(1:length(x),x,'-o');hold on
%             plot([offset:sampRateChange:(length(x))]+offset,...
%                 xStab(1:length(offset:sampRateChange:(length(x)))),'-o');
                
    
    pptrials{trialNumber}.x = x;
    pptrials{trialNumber}.y = y;
    pptrials{trialNumber}.offsetTime = offset;
    end
end
end

% end