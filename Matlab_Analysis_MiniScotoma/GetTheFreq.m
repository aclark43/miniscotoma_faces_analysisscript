function F = GetTheFreq(z, rangeXY, numBins)

X=[imag(z)' real(z)'];
edgesXY = linspace(-rangeXY,rangeXY,numBins);  
edges={edgesXY,edgesXY};
[F] = hist3([imag(z)' real(z)'],'Edges',edges); 
end