function drift_bias_ndhist_task(patients,controls)
    
    cP = jet(length(patients));
    cC = winter(length(controls));

    for ii_p = 1:length(patients)
        [inst_sp_x_P,inst_sp_y_P] = deal([]);
        dir_path = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\patients\',patients{ii_p},'\matFiles\');
        file_to_read = strcat('em_info_for_drift_bias_task_',patients{ii_p},'.mat');
        load(strcat(dir_path,file_to_read))
        ln_em = numel(fieldnames(em.ecc_0));
        for t_size = 1:ln_em
            f_name = fieldnames(em.ecc_0);

            all_chars = eval(strcat('em.ecc_0.',f_name{t_size}));
            if ~isfield(all_chars,'instSpX')
                continue
            else
                for all = 1:length(all_chars.instSpX)
                    x = all_chars.instSpX{all};
                    y = all_chars.instSpY{all};
                    inst_sp_x_P = [inst_sp_x_P,x];
                    inst_sp_y_P = [inst_sp_y_P,y];
                end
            end

        end
        clear em;
        temp_inst_sp_x_P = inst_sp_x_P(inst_sp_x_P > -200 & inst_sp_x_P < 200);
        temp_inst_sp_x_P = inst_sp_x_P(inst_sp_y_P > -200 & inst_sp_y_P < 200);
        temp_inst_sp_y_P = inst_sp_y_P(inst_sp_x_P > -200 & inst_sp_x_P < 200);
        temp_inst_sp_y_P = inst_sp_y_P(inst_sp_y_P > -200 & inst_sp_y_P < 200);
        figure()
% ndhist(temp_inst_sp_x_P(~isnan(temp_inst_sp_x_P)), temp_inst_sp_y_P(~isnan(temp_inst_sp_y_P)), 'bins',1, 'radial','axis',[-200 200 -200 200],'nr','filt');
        ndhist(inst_sp_x_P(~isnan(inst_sp_x_P)), inst_sp_y_P(~isnan(inst_sp_y_P)), 'bins',10, 'radial','axis',[-200 200 -200 200],'nr','filt');
        set(gcf,'units','normalized','outerposition',[0 0 1 1], 'Color', 'w');
%         axis off
        title(patients{ii_p})
    end
    for ii_c = 1:length(controls)
        [inst_sp_x_C,inst_sp_y_C] = deal([]);
        dir_path = strcat('C:\Users\sanjana\OneDrive - University of Rochester\Documents\Sanjana\Projects\schizophrenia\data\controls\',controls{ii_c},'\matFiles\');
        file_to_read = strcat('em_info_for_drift_bias_task_',controls{ii_c},'.mat');
        load(strcat(dir_path,file_to_read))
        ln_em = numel(fieldnames(em.ecc_0));
        for t_size = 1:ln_em
            f_name = fieldnames(em.ecc_0);

            all_chars = eval(strcat('em.ecc_0.',f_name{t_size}));
            if ~isfield(all_chars,'instSpX')
                continue
            else
                for all = 1:length(all_chars.instSpX)
                    x = all_chars.instSpX{all};
                    y = all_chars.instSpY{all};
                    inst_sp_x_C = [inst_sp_x_C,x];
                    inst_sp_y_C = [inst_sp_y_C,y];
                end
            end

        end
        clear em;
        figure()
%         maxc = .25;
%         caxis([0 maxc])
%         load('./MyColormaps')
%         h = gcf;
%         set(gca, 'FontSize', 20)
%         set(h,'Colormap',mycmap);
%         hold on
        ndhist(inst_sp_x_C(~isnan(inst_sp_x_C)), inst_sp_y_C(~isnan(inst_sp_y_C)), 'bins',2, 'radial','axis',[-200 200 -200 200],'nr','filt');
        title(controls{ii_c})
    end
   
    

end