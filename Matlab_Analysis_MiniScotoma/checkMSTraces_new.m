function pptrials = checkMSTraces_new(trialId, pptrials, filepath, nameFile)

if isempty(trialId)
    trialId = 1:length(pptrials);
end

PLOT_DRIFTS = input('Plot individual segments (y/n): ','s');

if PLOT_DRIFTS == 'y'
    
    %     snellen = input('Is this the snellen task? (y/n)','s');
    
    trialCounter = 1;
    for driftIdx = 1:inf
        figure(2)%,'position',[200, 100, 1500, 800])
        clf('reset')
        if (trialCounter) > length(trialId)
            return;
        end
        currentTrialId = trialId(trialCounter);
        
      
        poiStart = 0;
        poiEnd = 1500;
        axisWindow = 40;

        
        
        poi = fill([poiStart, poiStart ...
            poiEnd, poiEnd], ...
            [-50, 50, 50, -50], ...
            'g', 'EdgeColor', 'g', 'LineWidth', 2, 'Clipping', 'On', 'FaceAlpha', 0);
        
        xTrace = pptrials{currentTrialId}.x;%.position + pptrials{currentTrialId}.xoffset * pptrials{currentTrialId}.pixelAngle;
        yTrace = pptrials{currentTrialId}.y;%.position + pptrials{currentTrialId}.yoffset * pptrials{currentTrialId}.pixelAngle;
        
        hold on
        
            hx = plot(xTrace, 'Color', [0 0 200] / 255, 'HitTest', 'off', 'LineWidth', 2);
            hy = plot(yTrace, 'Color', [0 180 0] / 255, 'HitTest', 'off', 'LineWidth', 2);
            sampling = 1;
            ylim([-60 60])
%             xlim([0 1500])
        xlabel('Time','FontWeight','bold')
        ylabel('arcmin','FontWeight','bold')
        title(sprintf('Trial: %i out of %i', currentTrialId, length(pptrials))); %
       
        
        set(gca, 'FontSize', 12)
        poiMS = [1 0 0];
        poiS = [1 0 0];
        poiD = [0 1 0];
        poiN = [0 0 0];
        poiI = [0 .423 .521];
        poiB = [.749 .019 1];
            poiMS = plotColorsOnTraces(pptrials, currentTrialId, 'microsaccades', [1 0 0], 1); %red
            poiS = plotColorsOnTraces(pptrials, currentTrialId, 'saccades', [1 0 0], 1); %red
            poiD = plotColorsOnTraces(pptrials, currentTrialId, 'drifts', [0 1 0], 1); %green
            poiN = plotColorsOnTraces(pptrials, currentTrialId, 'notracks', [0 0 0], 1); %black
            poiI = plotColorsOnTraces(pptrials, currentTrialId, 'invalid', [0 0 1], 1); %blue
            poiB = plotColorsOnTraces(pptrials, currentTrialId, 'blinks', [.749 .019 1], 1000); %pink
        pptrials{currentTrialId}.checkedManually = 1;
        contType = input...
            ('Saccade(1), Microsaccade(2), Drift(3), \n No Track(4), Invalid(5), Blinks(6), \n Back a trial(7), Stop(0), AutoPrune(8), Clear All (9), \n Fill In With Drift (10), Fix Spike (11), Undo spike fix (12), \n Next Trial(enter)?\n');
        cont = [];
        if contType == 1
            cont = input('Tag a Saccade(a) or Change Saccade Labelled(b)?','s');
            em = 'saccades'; % Post analysis will check amps and organize as MS or S
        elseif contType == 2
            cont = input('Tag a microsaccade(a) or Change Microsaccade Labelled(b)?','s');
            em = 'microsaccades'; % +- 10msec
        elseif contType == 3
            cont = input('Tag a Drift(a) or Change Drift Labelled(b)?','s');
            em = 'drifts';
        elseif contType == 4
            cont = input('Tag a No Track(a) or Change no track Labelling?(b)','s');
            em = 'notracks'; %flat no tracks
        elseif contType == 5
            cont = input('Tag a Invalid(a) or Change invalid Labelling?(b)','s');
            em = 'invalid'; %bad drifts
        elseif contType == 6
            cont = input('Tag a Blink(a) or Change a Blink Labelling?(b)','s');
            em = 'blinks';
        elseif contType == 7
            cont = 'z';
        elseif contType == 11
            cont = 'y';
        elseif contType == 12
            cont = 'u';
        elseif contType == 0
            cont = 's';
        elseif contType == 8
            pptrials = autoPrune( pptrials, currentTrialId, 'drifts', 75);
            pptrials = autoPrune( pptrials, currentTrialId, 'microsaccades', 30);
            pptrials = autoPrune( pptrials, currentTrialId, 'saccades', 30);
            pptrials = autoPrune( pptrials, currentTrialId, 'blinks', 10);
            
            MinSaccSpeed = 60;
            MinSaccAmp = 30;
            MinMSaccSpeed = 1;
            MinMSaccAmp = 2;
            MaxMSaccAmp = 60;
            MinVelocity = 10;
            
            pptrialsTemp1 = findSaccades(pptrials{currentTrialId}, ...
                'minvel',MinSaccSpeed, ...
                'minsa',MinSaccAmp);
            
            Trial = pptrials{currentTrialId};
            Events = findEvents(Trial.velocity, ...
                'minvel', MinVelocity, ...
                'minterval', 25, ...
                'mduration', 25 );                %%% YB@2018.10.27.
            
            % Filter all movements within a blink event
            Events = filterIntersected(Events, Trial.blinks);
            
            % Filter all movements within the track trace
            Events = filterIntersected(Events, Trial.notracks);
            
            % Filter all movements within the invalid list
            Events = filterIntersected(Events, Trial.invalid);
            
            % Filter all movements that do not fall within
            % the stimulus timeframe
            Events = filterNotIncluded(Events, Trial.analysis);
            
            % Calculate the amplitudes/angles of the movements
            counter = 1;
            startTemp = [];
%             if isempty(Events.start)
                for i = 1:Trial.samples-15
                    if diff([Trial.x(i+15), Trial.x(i)]) > 6 && ...
                           diff([Trial.x(i+15), Trial.x(i)]) < 200
                        startTemp(counter) = abs(i-8);%Trial.x(i);
                        counter = counter + 1;
                    else 
                        blinks(counterB) = abs(i-8);
                        counterB = counterB + 1; 
                    end
                end
                %             end
               
                for l = 1:length(startTemp)-1
                    if diff([startTemp(l), startTemp(l+1)]) < 30
                        startTemp(l+1) = startTemp(l);
                    end
                end
                
%                 if length(startTemp) > 10
                    t = unique(floor(startTemp));
%                 else
%                     t = uniquetol(floor(startTemp), .01);
%                 end
                t(t == 0) = 1;
                
                Events.start = t;
                
                Events.duration = ones(1,length(Events.start))*25;
                Events = updateAmplitudeAngle(Trial, Events);
            
            % Filter all movements with an amplitude according to
            % minimum and maximum amplitude
            Events = filterBy('amplitude', Events, ...
                MinMSaccAmp, MaxMSaccAmp);
            
            
            
            % Save the filtered events into the trial
            if ~isempty(Events)
                pptrialsTemp2.microsaccades = Events;
            end

            pptrials{currentTrialId}.saccades = pptrialsTemp1.saccades;
            pptrials{currentTrialId}.microsaccades = pptrialsTemp2.microsaccades;
%             pptrials{currentTrialId}.blinks =
            continue;
        elseif contType == 9
            pptrials = autoPrune( pptrials, currentTrialId, 'drifts', 10000);
            pptrials = autoPrune( pptrials, currentTrialId, 'microsaccades', 10000);
            pptrials = autoPrune( pptrials, currentTrialId, 'saccades', 10000);
            pptrials = autoPrune( pptrials, currentTrialId, 'blinks', 10000);
            pptrials = autoPrune( pptrials, currentTrialId, 'invalid', 10000);
            pptrials = autoPrune( pptrials, currentTrialId, 'notracks', 10000);
            continue;
        elseif contType == 10
            
            Trial = pptrials{currentTrialId};
            AllMovements = joinEvents(Trial.saccades, Trial.microsaccades);
            AllMovements = joinEvents(AllMovements, Trial.blinks);
            AllMovements = joinEvents(AllMovements, Trial.notracks);
            AllMovements = joinEvents(AllMovements, Trial.invalid);
            Events = invertEvents(AllMovements, Trial.samples);
            Events = updateAmplitudeAngle(Trial, Events);
            pptrials{currentTrialId}.drifts  = Events;
            cont = 'f';
        else
            trialCounter = trialCounter + 1;
            continue;
        end
        
        if cont == 's'
            save(fullfile(sprintf('%s',filepath), nameFile), 'pptrials')
            break;
        elseif cont == 'a'
            numTaggedInTrace = length(pptrials{currentTrialId}.(em).start)+1;%input('Which ones (in order) do you want to tag?');
            startTime = input('What is the start time?');
            duration = input('What is the end time?');
                pptrials{currentTrialId}.(em).start(numTaggedInTrace) = ...
                    round(startTime);
                pptrials{currentTrialId}.(em).duration(numTaggedInTrace) = ...
                    round((duration) - round(startTime));
%             end
            fprintf('Saving pptrials\n')
            save(fullfile(sprintf('%s',filepath), nameFile), 'pptrials')
            continue;
        elseif cont == 'b'
            fprintf('Total start times: %.0f \n', round(pptrials{currentTrialId...
                }.(em).start, 3)*(1000/330))
            startTime = input('When does the wrong EM start?');
            newStartTime = input('When does the EM actually start? (set to 0 if not present)');
            newDurationTime = input('When does the EM end? (set to 0 if not present)');
            for numW = 1:length(startTime)
                wrong = find((pptrials{currentTrialId}.(em).start) == round(startTime(numW)/(1000/330)));
                pptrials{currentTrialId}.(em).start(wrong) = round(newStartTime(numW)/(1000/330));
                pptrials{currentTrialId}.(em).duration(wrong) = ...
                    (round(newDurationTime(numW)/(1000/330))) - round(newStartTime(numW)/(1000/330));
            end
            fprintf('Saving pptrials\n')
            save(fullfile(sprintf('%s',filepath), nameFile), 'pptrials')
            continue;
        elseif cont == 'z'
            trialCounter = trialCounter-1;
            continue;
        elseif cont == 'f'
            save(fullfile(sprintf('%s',filepath), nameFile), 'pptrials')
            continue;
        elseif cont == 'y' %%fixing spike
            man_or_auto = input('Manual (1) or Automatic (2)?');
            %             numTaggedInTrace = length(pptrials{currentTrialId}.(em).start)+1;%input('Which ones (in order) do you want to tag?');
            if man_or_auto == 2
                hold on
                [~,peakidxX] = findpeaks(abs(pptrials{currentTrialId}.x),'MinPeakDistance',25);
                hold on
                plot(peakidxX,pptrials{currentTrialId}.x(peakidxX),'o')
                for i = 1:length(peakidxX)
                    text(peakidxX(i),...
                        double(pptrials{currentTrialId}.x(peakidxX(i))),...
                        string(i),'FontSize',14);
                end
                xpeaks = input('Which X Peaks?');
                pptrials{currentTrialId}.x(peakidxX(xpeaks)) = ...
                    pptrials{currentTrialId}.x(peakidxX(xpeaks) - 1);
                pptrials{currentTrialId}.y(peakidxX(xpeaks)) = ...
                    pptrials{currentTrialId}.y(peakidxX(xpeaks) - 1);
               
               [~,peakidxY] = findpeaks(abs(pptrials{currentTrialId}.y),'MinPeakDistance',25);
                hold on
                plot(peakidxY,pptrials{currentTrialId}.y(peakidxY),'o')
                for i = 1:length(peakidxY)
                    text(peakidxY(i),...
                        double(pptrials{currentTrialId}.y(peakidxY(i))),...
                        string(i),'FontSize',14);
                end
                ypeaks = input('Which Y Peaks?');
                pptrials{currentTrialId}.y(peakidxY(ypeaks)) = ...
                    pptrials{currentTrialId}.y(peakidxY(ypeaks) - 1);
                pptrials{currentTrialId}.x(peakidxY(ypeaks)) = ...
                    pptrials{currentTrialId}.x(peakidxY(ypeaks) - 1);
                
            elseif man_or_auto == 1
                startTime = input('What is the start time?');
                duration = input('What is the end time?');
                
                saveTillNextTime{1} = pptrials{currentTrialId}.x;
                saveTillNextTime{2} = pptrials{currentTrialId}.y;
                    pptrials{currentTrialId}.x...
                        (startTime:duration) = ...
                        pptrials{currentTrialId}.x(startTime);
                    
                    pptrials{currentTrialId}.y...
                        (startTime:duration) = ...
                        pptrials{currentTrialId}.y(startTime);
%                 end
            end
            
            fprintf('Saving pptrials\n')
            save(fullfile(sprintf('%s',filepath), nameFile), 'pptrials')
            continue;
        elseif cont == 'u'
            fprintf('Redoing last spike tag \n')
 
                pptrials{currentTrialId}.x...
                    (startTime:duration) = ...
                    saveTillNextTime{1}(startTime:duration);
                
                pptrials{currentTrialId}.y...
                    (startTime:duration) = ...
                    saveTillNextTime{2}(startTime:duration);
%             end
            fprintf('Saving pptrials\n')
            save(fullfile(sprintf('%s',filepath), nameFile), 'pptrials')
            continue;
        end
        
        save(fullfile(sprintf('%s',filepath), nameFile), 'pptrials')
        trialCounter = trialCounter + 1;
    end
end

close

end
