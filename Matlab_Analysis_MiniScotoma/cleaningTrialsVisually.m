% This function plots x y traces from processed data, and allows tagging
% and tossing trials for each individual type of em
%

%%%%%%%%%%% INPUT:
% pptrials: structure of processed data including trial information and
% data
%
%%%%%%%%%%% Properties: Optional properties
%
% 'StartTime': string of variable of period of
% interest (which part of the trace are you interest in?

% 'EndTime': string of variable of period of
% interest (which part of the trace are you interest in?

% 'AxisWindow': size of viewing window (one positive value = -val to +val).

% 'Folder': select filepath you want your new clean data to save.

% 'NewFileName' = what .mat file name do you want it saved as?

% 'Conversion' = 1; %machine sampling rate conversion factor (ie DPI = 1, dDPI = 1000/330)

% 'WhichTrials = if you only want subset of trials to be plotted.
%
%%%%%%%%%%%%% OUTPUT:
% pptrials = new clean data

% HISTORY:
% This function is based on the pptrials generated from the basicEIS
% function

% 2020 @APLAB Ashley Clark



function pptrials = cleaningTrialsVisually(pptrials, varargin)

poiStartName = 'TimeTargetON';
poiEndName = 'TimeTargetOFF';
axisWindow = 60;
filepath = pwd;
newFileName = 'pptrials.mat';
conversionFactor = 1; %machine sampling rate conversion factor (ie DPI = 1, dDPI = 1000/330)
trialId = 1:length(pptrials);

k = 1;
Properties = varargin;
while k <= length(Properties) && ischar(Properties{k})
    switch (Properties{k})
        case 'StartTime'
            poiStartName =  Properties{k + 1};
            Properties(k:k+1) = [];
        case 'EndTime'
            poiEndName = Properties{k + 1};
            Properties(k:k+1) = [];
        case 'AxisWindow'
            axisWindow = Properties{k + 1};
            Properties(k:k+1) = [];
        case 'Folder'
            filepath = Properties{k + 1};
            Properties(k:k+1) = [];
        case 'NewFileName'
            newFileName = Properties{k + 1};
            Properties(k:k+1) = [];
        case 'Conversion'
            conversionFactor = Properties{k + 1};
            Properties(k:k+1) = [];
        case 'WhichTrials'
            trialId = Properties{k + 1};
            Properties(k:k+1) = [];
        otherwise
            k = k + 1;
    end
end

% nameFile = sprintf('%s.mat',newFileName);
PLOT_DRIFTS = input('Plot individual segments (y/n): ','s');

if PLOT_DRIFTS == 'y'
    
    figure('position',[2300, 100, 1500, 800])
    trialCounter = 1;
    for driftIdx = 1:inf
        if (trialCounter) > length(trialId)
            return;
        end
        currentTrialId = trialId(trialCounter);
        hold off
        
        if strcmp('1',poiStartName)
            poiStart = 1;
        else
            poiStart = round(pptrials{currentTrialId}.(poiStartName));
        end
            poiEnd = double(min(pptrials{currentTrialId}.(poiEndName)));

        if pptrials{currentTrialId}.(poiEndName) <= 0
            return;
        end
        
        poi = fill([poiStart, poiStart ...
            poiEnd, poiEnd], ...
            [-50, 50, 50, -50], ...
            'g', 'EdgeColor', 'g', 'LineWidth', 2, 'Clipping', 'On', 'FaceAlpha', 0);
        
%         xTrace = pptrials{currentTrialId}.x.position + pptrials{currentTrialId}.xoffset * pptrials{currentTrialId}.pixelAngle;
%         yTrace = pptrials{currentTrialId}.y.position + pptrials{currentTrialId}.yoffset * pptrials{currentTrialId}.pixelAngle;
        xTrace = pptrials{currentTrialId}.x.position + pptrials{currentTrialId}.xoffset * pptrials{currentTrialId}.pixelAngle;
        yTrace = pptrials{currentTrialId}.y.position + pptrials{currentTrialId}.yoffset * pptrials{currentTrialId}.pixelAngle;
        
        hold on
        hx = plot(1:conversionFactor:length(xTrace)*conversionFactor,xTrace, 'Color', [0 0 200] / 255, 'HitTest', 'off', 'LineWidth', 2);
        hy = plot(1:conversionFactor:length(yTrace)*conversionFactor,yTrace, 'Color', [0 180 0] / 255, 'HitTest', 'off', 'LineWidth', 2);
        axis([0 180 0 150])
        
        axis([poiStart - 400, poiEnd + 400, -(axisWindow), axisWindow])
        xlabel('Time','FontWeight','bold')
        ylabel('arcmin','FontWeight','bold')
        title(sprintf('Trial: %i', currentTrialId)); %
        
        set(gca, 'FontSize', 12)
        
        poiMS = plotColorsOnTraces(pptrials, currentTrialId, 'microsaccades', [1 0 0], conversionFactor); %red
        poiS = plotColorsOnTraces(pptrials, currentTrialId, 'saccades', [1 0 0], conversionFactor); %red - will still plot, just not saved seperate from MS
        poiD = plotColorsOnTraces(pptrials, currentTrialId, 'drifts', [0 1 0], conversionFactor); %green
        poiN = plotColorsOnTraces(pptrials, currentTrialId, 'notracks', [0 0 0], conversionFactor); %black
        poiI = plotColorsOnTraces(pptrials, currentTrialId, 'invalid', [0 0 1], conversionFactor); %blue
        poiB = plotColorsOnTraces(pptrials, currentTrialId, 'blinks', [.749 .019 1], conversionFactor); %pink
        
        legend([hx, hy, poiMS, poiD, poiN, poiI, poiB], ...
            {'X','Y','MS/S','D','NoTrack','Invalid','Blink'},'FontWeight','bold') %%%Ignores Saccades
        
        pptrials{currentTrialId}.checkedManually = 1;
        contType = input...
            ('PROBLEM with: Saccade(1), Microsaccade(2), Drift(3), \n No Track(4), Invalid(5), Blinks(6), \n Back a trial(7), Stop(0), AutoPrune(8), Clear All (9), \n Fill In With Drift (10), Fix Spike (11), Undo spike fix (12), \n Next Trial(enter)?\n');
        cont = [];
        if contType == 1
            cont = input('Tag a Saccade(a) or Change Saccade Labelled(b)?','s');
            em = 'saccades'; % Post analysis will check amps and organize as MS or S
        elseif contType == 2
            cont = input('Tag a microsaccade(a) or Change Microsaccade Labelled(b)?','s');
            em = 'microsaccades'; % +- 10msec
        elseif contType == 3
            cont = input('Tag a Drift(a) or Change Drift Labelled(b)?','s');
            em = 'drifts';
        elseif contType == 4
            cont = input('Tag a No Track(a) or Change no track Labelling?(b)','s');
            em = 'notracks'; %flat no tracks
        elseif contType == 5
            cont = input('Tag a Invalid(a) or Change invalid Labelling?(b)','s');
            em = 'invalid'; %bad drifts
        elseif contType == 6
            cont = input('Tag a Blink(a) or Change a Blink Labelling?(b)','s');
            em = 'blinks';
        elseif contType == 7
            cont = 'z';
        elseif contType == 11
            cont = 'y';
        elseif contType == 12
            cont = 'u';
        elseif contType == 0
            cont = 's';
        elseif contType == 8
             pptrials = autoPrune( pptrials, currentTrialId, 'drifts', 75);
            pptrials = autoPrune( pptrials, currentTrialId, 'microsaccades', 30);
            pptrials = autoPrune( pptrials, currentTrialId, 'saccades', 30);
            pptrials = autoPrune( pptrials, currentTrialId, 'blinks', 10);
            
            MinSaccSpeed = 60;
            MinSaccAmp = 30;
            MinMSaccSpeed = 1;
            MinMSaccAmp = 2;
            MaxMSaccAmp = 60;
            MinVelocity = 10;
            
            pptrialsTemp1 = findSaccades(pptrials{currentTrialId}, ...
                'minvel',MinSaccSpeed, ...
                'minsa',MinSaccAmp);
            
            Trial = pptrials{currentTrialId};
            Events = findEvents(Trial.velocity, ...
                'minvel', MinVelocity, ...
                'minterval', 25, ...
                'mduration', 25 );                %%% YB@2018.10.27.
            
            % Filter all movements within a blink event
            Events = filterIntersected(Events, Trial.blinks);
            
            % Filter all movements within the track trace
            Events = filterIntersected(Events, Trial.notracks);
            
            % Filter all movements within the invalid list
            Events = filterIntersected(Events, Trial.invalid);
            
            % Filter all movements that do not fall within
            % the stimulus timeframe
            Events = filterNotIncluded(Events, Trial.analysis);
            
            % Calculate the amplitudes/angles of the movements
            counter = 1;
            startTemp = [];
            counterB = 1;
            blinks = [];
            %             if isempty(Events.start)
            for i = 1:Trial.samples-15
                if diff([Trial.x.position(i+15), Trial.x.position(i)]) > 6 && ...
                        diff([Trial.x.position(i+15), Trial.x.position(i)]) < 200
                    startTemp(counter) = abs(i-8);%Trial.x.position(i);
                    counter = counter + 1;
                elseif diff([Trial.x.position(i+5), Trial.x.position(i)]) > 75
                    blinks(counterB) = abs(i-8);
                    counterB = counterB + 1;
                    
                end
            end
            %             end
            
            for l = 1:length(startTemp)-1
                if diff([startTemp(l), startTemp(l+1)]) < 30
                    startTemp(l+1) = startTemp(l);
                end
            end
            
            for l = 1:length(blinks)-1
                if diff([blinks(l), blinks(l+1)]) < 30
                    blinks(l+1) = blinks(l);
                end
            end
            b = unique(floor(blinks));
            idxB = [];
            temp = [];
            for i = 1:length(b)
                temp = blinks(find(b(i) == blinks));
                 idxB(i,1) = temp(1);
                 idxB(i,2) = length(temp)+30;
            end
            b = [];
           
            idxB(idxB == 0) = 1;
            if isempty(idxB)
                pptrials{currentTrialId}.blinks.start = [];
                pptrials{currentTrialId}.blinks.duration = [];
            else
                pptrials{currentTrialId}.blinks.start = round(idxB(:,1)');
                pptrials{currentTrialId}.blinks.duration = idxB(:,2)';
            end
            x = Trial.x.position;
            idxN = [];
            idxN = find(diff(x) == 0)
            for l = 1:length(idxN)-1
                if diff([idxN(l), idxN(l+1)]) < 30
                    idxN(l+1) = idxN(l);
                end
            end
            n = unique(floor(idxN));
            n(n == 0) = 1;
            n = unique(floor(n));
            for i = 1:length(n)
                temp = idxN(find(n(i) == idxN));
                 idxNo(i,1) = temp(1);
                 idxNo(i,2) = length(temp)+15;
            end
%             pptrials{currentTrialId}.notracks.start = round(idxNo(:,1)');
%             pptrials{currentTrialId}.notracks.duration = idxNo(:,2)';
%             
            t = unique(floor(startTemp));
            t(t == 0) = 1;
            Events.start = t;
            Events.duration = ones(1,length(Events.start))*25;
            Events = updateAmplitudeAngle(Trial, Events);
            
            % Filter all movements with an amplitude according to
            % minimum and maximum amplitude
            Events = filterBy('amplitude', Events, ...
                MinMSaccAmp, MaxMSaccAmp);
            
            % Save the filtered events into the trial
            if ~isempty(Events)
                pptrialsTemp2.microsaccades = Events;
            end
            
            pptrials{currentTrialId}.saccades = pptrialsTemp1.saccades;
            pptrials{currentTrialId}.microsaccades = pptrialsTemp2.microsaccades;
            
            continue;
        elseif contType == 9
            pptrials = autoPrune( pptrials, currentTrialId, 'drifts', 10000);
            pptrials = autoPrune( pptrials, currentTrialId, 'microsaccades', 10000);
            pptrials = autoPrune( pptrials, currentTrialId, 'saccades', 10000);
            pptrials = autoPrune( pptrials, currentTrialId, 'blinks', 10000);
            pptrials = autoPrune( pptrials, currentTrialId, 'invalid', 10000);
            pptrials = autoPrune( pptrials, currentTrialId, 'notracks', 10000);
            continue;
        elseif contType == 10
            
            Trial = pptrials{currentTrialId};
            AllMovements = joinEvents(Trial.saccades, Trial.microsaccades);
            AllMovements = joinEvents(AllMovements, Trial.blinks);
            AllMovements = joinEvents(AllMovements, Trial.notracks);
            AllMovements = joinEvents(AllMovements, Trial.invalid);
            Events = invertEvents(AllMovements, Trial.samples);
            Events = updateAmplitudeAngle(Trial, Events);
            pptrials{currentTrialId}.drifts  = Events;
            cont = 'f';
        else
            save(fullfile(sprintf('%s',filepath), newFileName))
            trialCounter = trialCounter + 1;
            continue;
        end
        
        if cont == 's'
            save(fullfile(sprintf('%s',filepath), newFileName))
            break;
        elseif cont == 'a'
            numTaggedInTrace = length(pptrials{currentTrialId}.(em).start)+1;%input('Which ones (in order) do you want to tag?');
            startTime = input('What is the start time?');
            duration = input('What is the end time?');
%             if strcmp('D',params.machine)
                pptrials{currentTrialId}.(em).start(numTaggedInTrace) = ...
                    round(startTime/(conversionFactor));
                pptrials{currentTrialId}.(em).duration(numTaggedInTrace) = ...
                    round((duration/(conversionFactor)) - round(startTime/(conversionFactor)));
%             else
%                 pptrials{currentTrialId}.(em).start(numTaggedInTrace) = ...
%                     round(startTime);
%                 pptrials{currentTrialId}.(em).duration(numTaggedInTrace) = ...
%                     round((duration) - round(startTime));
%             end
            fprintf('Saving pptrials\n')
            save(fullfile(sprintf('%s',filepath), newFileName))
            continue;
        elseif cont == 'b'
            fprintf('Total start times: %.0f \n', round(pptrials{currentTrialId...
                }.(em).start, 3)*(conversionFactor))
            startTime = input('When does the wrong EM start?');
            newStartTime = input('When does the EM actually start? (set to 0 if not present)');
            newDurationTime = input('When does the EM end? (set to 0 if not present)');
            for numW = 1:length(startTime)
                wrong = find((pptrials{currentTrialId}.(em).start) == round(startTime(numW)/(conversionFactor)));
                pptrials{currentTrialId}.(em).start(wrong) = round(newStartTime(numW)/(conversionFactor));
                pptrials{currentTrialId}.(em).duration(wrong) = ...
                    (round(newDurationTime(numW)/(conversionFactor))) - round(newStartTime(numW)/(conversionFactor));
            end
            fprintf('Saving pptrials\n')
            save(fullfile(sprintf('%s',filepath), newFileName))
            continue;
        elseif cont == 'z'
            trialCounter = trialCounter-1;
            continue;
        elseif cont == 'f'
            save(fullfile(sprintf('%s',filepath), newFileName))
            continue;
        elseif cont == 'y' %%fixing spike
            man_or_auto = input('Manual (1) or Automatic (2)?');
            %             numTaggedInTrace = length(pptrials{currentTrialId}.(em).start)+1;%input('Which ones (in order) do you want to tag?');
            if man_or_auto == 2
                hold on
                [~,peakidxX] = findpeaks(abs(pptrials{currentTrialId}.x.position),'MinPeakDistance',25);
                hold on
                plot(peakidxX*(conversionFactor),pptrials{currentTrialId}.x.position(peakidxX)+ pptrials{currentTrialId}.xoffset * pptrials{currentTrialId}.pixelAngle,'o')
                for i = 1:length(peakidxX)
                    text(peakidxX(i)*(conversionFactor),...
                        double(pptrials{currentTrialId}.x.position(peakidxX(i)))+ ...
                        pptrials{currentTrialId}.xoffset * pptrials{currentTrialId}.pixelAngle,...
                        string(i),'FontSize',14);
                end
                xpeaks = input('Which X Peaks?');
                pptrials{currentTrialId}.x.position(peakidxX(xpeaks)) = ...
                    pptrials{currentTrialId}.x.position(peakidxX(xpeaks) - 1);
                pptrials{currentTrialId}.y.position(peakidxX(xpeaks)) = ...
                    pptrials{currentTrialId}.y.position(peakidxX(xpeaks) - 1);
               
               [~,peakidxY] = findpeaks(abs(pptrials{currentTrialId}.y.position),'MinPeakDistance',25);
                hold on
                plot(peakidxY*(conversionFactor),pptrials{currentTrialId}.y.position(peakidxY)+ ...
                    pptrials{currentTrialId}.yoffset * pptrials{currentTrialId}.pixelAngle,'o')
                for i = 1:length(peakidxY)
                    text(peakidxY(i)*(conversionFactor),...
                        double(pptrials{currentTrialId}.y.position(peakidxY(i))+ pptrials{currentTrialId}.yoffset * pptrials{currentTrialId}.pixelAngle),...
                        string(i),'FontSize',14);
                end
                ypeaks = input('Which Y Peaks?');
                pptrials{currentTrialId}.y.position(peakidxY(ypeaks)) = ...
                    pptrials{currentTrialId}.y.position(peakidxY(ypeaks) - 1);
                pptrials{currentTrialId}.x.position(peakidxY(ypeaks)) = ...
                    pptrials{currentTrialId}.x.position(peakidxY(ypeaks) - 1);
                
            elseif man_or_auto == 1
                startTime = input('What is the start time?');
                duration = input('What is the end time?');
                
                saveTillNextTime{1} = pptrials{currentTrialId}.x.position;
                saveTillNextTime{2} = pptrials{currentTrialId}.y.position;
%                 if strcmp('D',params.machine)
                    pptrials{currentTrialId}.x.position...
                        (startTime/(conversionFactor):duration/(conversionFactor)) = ...
                        pptrials{currentTrialId}.x.position(round(startTime/(conversionFactor)));
                    
                    pptrials{currentTrialId}.y.position...
                        (startTime/(conversionFactor):duration/(conversionFactor)) = ...
                        pptrials{currentTrialId}.y.position(round(startTime/(conversionFactor)));
                    
%                 else
%                     pptrials{currentTrialId}.x.position...
%                         (startTime:duration) = ...
%                         pptrials{currentTrialId}.x.position(startTime);
%                     
%                     pptrials{currentTrialId}.y.position...
%                         (startTime:duration) = ...
%                         pptrials{currentTrialId}.y.position(startTime);
%                 end
            end
            
            fprintf('Saving pptrials\n')
            save(fullfile(sprintf('%s',filepath), newFileName))
            continue;
        elseif cont == 'u'
            fprintf('Redoing last spike tag \n')
            %             if strcmp ('y',answer)
            %                 saveTillNextTime{1} = pptrials{currentTrialId}.x.position;
            %                 saveTillNextTime{2} = pptrials{currentTrialId}.y.position;
            if strcmp('D',params.machine)
                pptrials{currentTrialId}.x.position...
                    (startTime/(conversionFactor):duration/(conversionFactor)) = ...
                    saveTillNextTime{1}(startTime/(conversionFactor):duration/(conversionFactor));
                
                pptrials{currentTrialId}.y.position...
                    (startTime/(conversionFactor):duration/(conversionFactor)) = ...
                    saveTillNextTime{2}(startTime/(conversionFactor):duration/(conversionFactor));
                
            else
                pptrials{currentTrialId}.x.position...
                    (startTime:duration) = ...
                    saveTillNextTime{1}(startTime:duration);
                
                pptrials{currentTrialId}.y.position...
                    (startTime:duration) = ...
                    saveTillNextTime{2}(startTime:duration);
            end
            fprintf('Saving pptrials\n')
            save(fullfile(sprintf('%s',filepath), newFileName))
            continue;
        end
        
            save(fullfile(sprintf('%s',filepath), newFileName))
        trialCounter = trialCounter + 1;
    end
end

close

function [ poiEM ] = plotColorsOnTraces( pptrials, currentTrialId, em, c, convert )
%Plots the color and location of the wanted EM in the gui
poiEM = [];

for i = 1:length(pptrials{currentTrialId}.(em).start)
    startTime = pptrials{currentTrialId}.(em).start(i) * convert;
    if isempty(pptrials{currentTrialId}.(em).duration)
        durationTime = 5;
    else
        durationTime = pptrials{currentTrialId}.(em).duration(i) * convert;
    end
    poiEM = fill([startTime, startTime ...
        startTime + durationTime, ...
        startTime + durationTime], ...
        [-35, 35, 35, -35], ...
        c, 'EdgeColor', c, 'LineWidth', 2, 'Clipping', 'On', 'FaceAlpha', 0.25);
end

function [ pptrials ] = autoPrune( pptrials, currentTrialId, em, threshold )
%Automatically prunes tags that are too short to be real
%   em = the type of eye movements (ie drifts, saccades, notracks...)
%   threshold = the minimum number of samples that are required for it
%   to be real

for ii = 1:length(pptrials{currentTrialId}.(em).duration)
    if pptrials{currentTrialId}.(em).duration(ii) < threshold
        pptrials{currentTrialId}.(em).start(ii) = 0;
        pptrials{currentTrialId}.(em).duration(ii) = 0;
    end
end