function pptrials = newEyerisEISRead(pptrials, subjName)
counter = 1;
for ii = 1:length(pptrials)
    if isempty(pptrials{ii})
        emptyIdx(ii) = 0;
        continue;
    end
    emptyIdx(ii) = 1;
    x = pptrials{ii}.x;
    y = pptrials{ii}.y;
    
%     fXClean = x;
%     [~,TFoutlier1] = rmoutliers(x);
%     fXClean(TFoutlier1) = NaN;
%     fXClean = hampel(fXClean,4,2);
%     
%     fYClean = y;
%     [~,TFoutlier2] = rmoutliers(y);
%     fYClean(TFoutlier2) = NaN;
%     fYClean = hampel(fYClean,4,2);
%     
%     fYClean(TFoutlier1) = NaN;
%     fXClean(TFoutlier2) = NaN;
%     Trial.OGX = x;
%     Trial.OGY = y;
%     x = fXClean;
%     y = fYClean;
%     Trial.CleanX = x;
%     Trial.CleanY = y;
    %%
    MinSaccSpeed = 180;
    MinSaccAmp = 30;
    MinMSaccSpeed = 60;
    MinMSaccAmp = 5;
    MaxMSaccAmp = 30;
    MinVelocity = 40;
    if strcmp(subjName,'Z091')
        MinMSaccSpeed = 60;
    elseif strcmp(subjName,'A144')
        MinMSaccAmp = 3;
        MinVelocity = 30;
    elseif strcmp(subjName,'Z184')
        MinMSaccSpeed = 80;
        MinVelocity = 70;
    end
    Blink = zeros(1,length(x));
    NoTrack = zeros(1,length(x));
    DataValid = ones(1,length(x));
    DataInvalid = zeros(1,length(x));
    Trial = createTrial(ii, x, y, Blink, NoTrack, DataValid, DataInvalid, 1000);
    Trial = preprocessSignals(Trial, 'minvel', MinVelocity, 'noanalysis');
    %%%% 'noanalysis' keeps the saccades at the very onset of the trial
    
    % Find all valid saccades with speed greater than 3 deg/sec
    % and bigger than 30 arcmin
    Trial = findSaccades(Trial, 'minvel',MinSaccSpeed, 'minsa',MinSaccAmp);
    Trial = findMicrosaccades(Trial, 'minvel',MinMSaccSpeed,'minmsa', MinMSaccAmp,'maxmsa', MaxMSaccAmp);
    Trial = findDrifts(Trial);
    fn=fieldnames(Trial);
    
%         figure;
%         plot(1:length(x),x,'-')
%         ylim([-60 60])
%         hold on
%         plot(Trial.microsaccades.start,ones(1,length(Trial.microsaccades.start))*40,'*')
    for ff=1:size(fn,1)-2
        entry = char(fn{ff});
        pptrials{ii}.(entry) = ...
            Trial.(entry);
    end
end
if length(pptrials) < 1
    pptrials = [];
else
    pptrials = pptrials(find(emptyIdx));
end
end

