clear all
close all
clc

subjectsAll = {'Z055','Ben','Z091','A144','Z184','Z055'};%'A144',}; %'Z171','Z055',,|||
reProcess = [1 1 1 1];
cleanTrials = [0 0 0 0];
clrs = parula(5);

for ii = 1:length(subjectsAll)
%     if reProcess(ii)
    subjDataPath = strcat(sprintf('X:/Ashley/VisualAcuity_Scotoma/%s', subjectsAll{ii}));
    allfiles = dir(subjDataPath);
    data{ii}.Scotoma = [];
    data{ii}.Control = [];
    for ss = find(~[allfiles.isdir])
        clear temp
        if startsWith(string(allfiles(ss).name),["scot"]) %scotoma condition
            
            temp = load(sprintf('%s/%s',subjDataPath,allfiles(ss).name));
            dateCollect = datetime(str2double(temp.eis_data.filename(5:8)),...
                str2double(temp.eis_data.filename(9:10)),...
                str2double(temp.eis_data.filename(11:12)));
            %             if temp.eis_data.user_data.variables.trial2Data.m_scotomaPresent
            temppptrials = convertDataToPPtrialsSnellen(temp.eis_data,dateCollect);
            processedPPTrialsS = newEyerisEISRead(temppptrials, subjectsAll{ii});
            data{ii}.Scotoma = [data{ii}.Scotoma processedPPTrialsS];
            %             else
            %                 error('Mislabelled File');
            %             end
            
        elseif startsWith(string(allfiles(ss).name),["cont"])
            
            temp = load(sprintf('%s/%s',subjDataPath,allfiles(ss).name));
            dateCollect = datetime(str2double(temp.eis_data.filename(5:8)),...
                str2double(temp.eis_data.filename(9:10)),...
                str2double(temp.eis_data.filename(11:12)));
            
            temppptrials = convertDataToPPtrialsSnellen(temp.eis_data,dateCollect);
            processedPPTrialsC = newEyerisEISRead(temppptrials, subjectsAll{ii});
            data{ii}.Control = [data{ii}.Control processedPPTrialsC];
            
        else
            error('Change name of File');
        end
%         save(sprintf('%sSnellScot',subjectsAll{ii}),'data');
    end
%     else
%         load(sprintf('%sSnellScot',subjectsAll{ii}));
%     end
%     if cleanTrials(ii)
%         if isfile( sprintf('%sSnellScot_Temp.mat',subjectsAll{ii}))
%             load(sprintf('%sSnellScot_Temp.mat',subjectsAll{ii}));
%             data{ii}.Control = checkMSTraces_new([], data{ii}.Control, cd, ...
%                 sprintf('%sSnellScot_Temp',subjectsAll{ii}));
%             data{ii}.Scotoma = checkMSTraces_new([], data{ii}.Scotoma, cd, ...
%                 sprintf('%sSnellScot_Temp',subjectsAll{ii}));
%         else         
%             save(sprintf('%sSnellScot_Temp',subjectsAll{ii}),'data');
%             data{ii}.Control = checkMSTraces_new([], data{ii}.Control, cd, ...
%                 sprintf('%sSnellScot_Temp',subjectsAll{ii}));
%             data{ii}.Scotoma = checkMSTraces_new([], data{ii}.Scotoma, cd, ...
%                 sprintf('%sSnellScot_Temp',subjectsAll{ii}));
%         end
%         save(sprintf('%sSnellScot',subjectsAll{ii}),'data');
%     end
    %% figures
    lengthTrial = 2000;
    condition = {'Control','Scotoma'};
    counter = 1;
    
    for c = 1:2
        % counter = 1;
        xAllT = [];
        yAllT = [];
        xAllF = [];
        yAllF = [];
        xAllTime = [];
        yAllTime = [];
        processedPPTrialsS = data{ii}.(condition{c});
        counterTrials = 1;
        outALLamps = [];
        outALLdirs = [];
        outLandLocsX =[];
        outLandLocsY =[];
        outALLtimes = [];
        outALLtrialIdx = [];
        for pp = 1:length(processedPPTrialsS)
            if processedPPTrialsS{pp}.FixationTrial
                xAllF = [xAllF processedPPTrialsS{pp}.x];
                yAllF = [yAllF processedPPTrialsS{pp}.y];
            else
%                 taskTrial{c}(counterTrials) = pp;
                xAllT = [xAllT processedPPTrialsS{pp}.x];
                yAllT = [yAllT processedPPTrialsS{pp}.y];
                
%                 if strcmp('Z055',subjectsAll{ii})
%                     RT{ii}{c}(pp) = NaN;
%                 else
                 RT{ii}{c}(counterTrials) = (processedPPTrialsS{pp}.TimeStimulusOFF - ...
                        processedPPTrialsS{pp}.TimeStimulusON)/5;
%                     RT{ii}{c}(pp) = mean([processedPPTrialsS{pp}.Responses.responseTiming1 - ...
%                         processedPPTrialsS{pp}.TimeStimulusON, ...
%                         processedPPTrialsS{pp}.Responses.responseTiming2 - ...
%                         processedPPTrialsS{pp}.Responses.responseTiming1...
%                         processedPPTrialsS{pp}.Responses.responseTiming3 - ...
%                         processedPPTrialsS{pp}.Responses.responseTiming2...
%                         processedPPTrialsS{pp}.Responses.responseTiming4 - ...
%                         processedPPTrialsS{pp}.Responses.responseTiming3...
%                         processedPPTrialsS{pp}.Responses.responseTiming5 - ...
%                         processedPPTrialsS{pp}.Responses.responseTiming4]);
%                 end
                Perf{ii}{c}(counterTrials,1:5) = ([processedPPTrialsS{pp}.Responses.response1, ...
                    processedPPTrialsS{pp}.Responses.response2,...
                    processedPPTrialsS{pp}.Responses.response3,...
                    processedPPTrialsS{pp}.Responses.response4,...
                    processedPPTrialsS{pp}.Responses.response5]);
                
%                 for o = 1:5
                    eachOptoPerf{ii}{c}(counterTrials,1) = ...
                        processedPPTrialsS{pp}.Responses.response1;
                   eachOptoPerf{ii}{c}(counterTrials,2) = ...
                        processedPPTrialsS{pp}.Responses.response2;
                    eachOptoPerf{ii}{c}(counterTrials,3) = ...
                        processedPPTrialsS{pp}.Responses.response3;
                    eachOptoPerf{ii}{c}(counterTrials,4) = ...
                        processedPPTrialsS{pp}.Responses.response4;
                    eachOptoPerf{ii}{c}(counterTrials,5) = ...
                        processedPPTrialsS{pp}.Responses.response5;
                    
                    
                    
                    
%                 end
                counterD = 1;
                drifts{ii}{c}.duration(pp) = median(processedPPTrialsS{pp}.drifts.duration);

                for d = 1:length(processedPPTrialsS{pp}.drifts.start)
                    startIdx = processedPPTrialsS{pp}.drifts.start(d);
                    endIdx = startIdx + processedPPTrialsS{pp}.drifts.duration(d);
                    if endIdx > length(processedPPTrialsS{pp}.x)
                        endIdx = length(processedPPTrialsS{pp}.x);
                    end
                    if length([startIdx:1:endIdx]) < 15
                        continue
                    end
                    drifts{ii}{c}.x{pp,counterD} = processedPPTrialsS{pp}.x(startIdx:endIdx);
                    drifts{ii}{c}.y{pp,counterD} = processedPPTrialsS{pp}.y(startIdx:endIdx);
                    
%                     sgFitVal = min([length([startIdx:1:endIdx]
                    [~, drifts{ii}{c}.instSpX{pp,counterD},...
                        drifts{ii}{c}.instSpY{pp,counterD},...
                        drifts{ii}{c}.mn_speed{pp,counterD},...
                        drifts{ii}{c}.driftAngle{pp,counterD},...
                        drifts{ii}{c}.curvature{pp,counterD},...
                        drifts{ii}{c}.varx{pp,counterD},...
                        drifts{ii}{c}.vary{pp,counterD},...
                        drifts{ii}{c}.bcea{pp,counterD},...
                        drifts{ii}{c}.span(pp,counterD), ...
                        drifts{ii}{c}.amplitude(pp,counterD), ...
                        drifts{ii}{c}.prlDistance(pp,counterD),...
                        drifts{ii}{c}.prlDistanceX(pp,counterD), ...
                        drifts{ii}{c}.prlDistanceY(pp,counterD)] = ...
                        getDriftChars(processedPPTrialsS{pp}.x(startIdx:endIdx),...
                        processedPPTrialsS{pp}.y(startIdx:endIdx), 11, 1, 250);
                    counterD = counterD + 1;
                end
                out{ii}{c} = struct('amps', [], 'dirs', [], 'trialIdx', [], 'times', []);
                
                params.em.saccade.amps_bins = linspace(0, 60, 30);
                params.em.saccade.dirs_bins = linspace(0, 2*pi, 30);
                params.em.saccade.pkvel_bins = linspace(120, 300, 60);
                
                flds = {'microsaccades', 'saccades'};
                for fi = 1:length(flds)
                    starts = processedPPTrialsS{pp}.(flds{fi}).start;
                    durs = processedPPTrialsS{pp}.(flds{fi}).duration;
                    stops = starts + durs - 1;
                    
                    start = 1;
                    stop = length(processedPPTrialsS{pp}.x);
                    mi = (starts + stops) / 2;
                    use = mi >= start & mi <= stop;
                    
                    trueStarts = starts(starts < stop);
                    trueEnds = stops(stops<stop);
                    
                    x1 = processedPPTrialsS{pp}.x(trueStarts);
                    y1 = processedPPTrialsS{pp}.y(trueStarts);
                    x2 = processedPPTrialsS{pp}.x(trueEnds);
                    y2 = processedPPTrialsS{pp}.y(trueEnds);
                    
                    dirs = deg2rad(atan2d(x1.*y2-y1.*x2,x1.*x2+y1.*y2));
                    if ~isempty(use)
                        use = use(find(use > 0));
                        if ~isempty(processedPPTrialsS{ii}.(flds{fi}).amplitude)
                            if length(use) > length(processedPPTrialsS{ii}.(flds{fi}).amplitude)
                                use = use(1:length(processedPPTrialsS{ii}.(flds{fi}).amplitude));
                            end
                            outALLamps = [outALLamps, processedPPTrialsS{ii}.(flds{fi}).amplitude(use)];
                            outALLdirs = [outALLdirs, dirs];%processedPPTrialsS{ii}.(flds{fi}).angle(use)];
                            outALLtimes = [outALLtimes, mi(use)];
                            outALLtrialIdx = [outALLtrialIdx, ii*ones(1, sum(use))];
                            outLandLocsX = [outLandLocsX processedPPTrialsS{pp}.x(stops(use))];
                            outLandLocsY = [outLandLocsY processedPPTrialsS{pp}.y(stops(use))];
%                             [out.sRates, out.msRates] = buildRates(pptrials);

                            %                 out.rate =
                        end
                    end
                end
%                 flds = {'amps', 'dirs'};
%                 for fi = 1:length(flds)
%                     edges = params.em.saccade.(sprintf('%s_bins', flds{fi}));
%                     ce = edges(1:end-1) + mean(diff(edges))/2;
%                     n = histcounts(out{ii}{c}.(flds{fi}){pp}, edges);
%                     n = n / sum(n) / mean(diff(ce));
%                     
%                     out{ii}{c}.(sprintf('%s_hist', flds{fi})) = n;
%                     out{ii}{c}.(sprintf('%s_bins', flds{fi})) = ce;
%                     out{ii}{c}.(sprintf('%s_MS', flds{fi})) = [mean(out{ii}{c}.(flds{fi})), std(out{ii}{c}.(flds{fi}))];
%                 end    
                
                if length(processedPPTrialsS{pp}.x) >= lengthTrial
                    xAllTime(counterTrials,:) = processedPPTrialsS{pp}.x(1:lengthTrial);
                    yAllTime(counterTrials,:) = processedPPTrialsS{pp}.y(1:lengthTrial);
                    counterTrials = counterTrials + 1;
                end
            end
            
        end
        out{ii}{c}.amps = outALLamps(outALLamps < 90);
        out{ii}{c}.dirs = outALLdirs(outALLamps < 90);
        out{ii}{c}.times = outALLtimes(outALLamps < 90);
        out{ii}{c}.trialIdx = outALLtrialIdx(outALLamps < 90);
        out{ii}{c}.sRates = cellfun(@(z) rateCalculation(z, 's'), processedPPTrialsS);
        out{ii}{c}.msRates = cellfun(@(z) rateCalculation(z, 'ms'), processedPPTrialsS);
        out{ii}{c}.outLandLocsX = outLandLocsX;
        out{ii}{c}.outLandLocsY = outLandLocsY;
        
        tempX = [true,diff(xAllF)~=0];
        tempY = [true,diff(yAllF)~=0];
        
        idxF = find(xAllF < 40 & xAllF > -40 &...
            yAllF < 40 & yAllF > -40 & tempX & tempY);
        
        tempX = [true,diff(xAllT)~=0];
        tempY = [true,diff(yAllT)~=0];
        
        idxT = find(xAllT < 40 & xAllT > -40 &...
            yAllT < 40 & yAllT > -40 & tempX & tempY);
        
        figure(100+ii);
        subplot(2,2,counter)
        resultF = generateHeatMapSimple( ...
            xAllF(idxF), ...
            yAllF(idxF), ...
            'Bins', 20,...
            'StimulusSize', 7,...
            'AxisValue', 15,...
            'Uncrowded', 0,...
            'Borders', 1);
        rectangle('Position',[-3.5,-3.5,7,7]);
        title('Fixation')
        axis square
        %         figure;
        counter = counter + 1;
        subplot(2,2,counter)
        resultT = generateHeatMapSimple( ...
            xAllT(idxT), ...
            yAllT(idxT), ...
            'Bins', 100,...
            'StimulusSize', 10,...
            'AxisValue', 40,...
            'Uncrowded', 0,...
            'Borders', 1);
        hold on
        %         plot(xAllT,yAllT,'.')
        axis([-40 40 -40 40])
        rectangle('Position',[-2.5,-2.5,5,5]);
        rectangle('Position',[-2.5+10,-2.5,5,5]);
        rectangle('Position',[-2.5+20,-2.5,5,5]);
        rectangle('Position',[-2.5-10,-2.5,5,5]);
        rectangle('Position',[-2.5-20,-2.5,5,5]);
        axis square
        title(condition(c));
        suptitle(subjectsAll(ii))
        counter = counter + 1;
        
        timeTraces{ii}.(condition{c}).xAllTime = xAllTime;
        timeTraces{ii}.(condition{c}).yAllTime = yAllTime;
        tracesTask{ii}.(condition{c}).xAll =  xAllT(idxT);
        tracesTask{ii}.(condition{c}).yAll =  yAllT(idxT);
        tracesFix{ii}.(condition{c}).xAll =  xAllF(idxF);
        tracesFix{ii}.(condition{c}).yAll =  yAllF(idxF);
    end
end
%% Load in other fixation Data

%% Performance per Each Optotype
figure;
clrs = parula(5);

for ii = 1:length(subjectsAll)
    plot([1 2],[mean(sum(eachOptoPerf{ii}{1})/length(eachOptoPerf{ii}{1})), ...
        mean(sum(eachOptoPerf{ii}{2})/length(eachOptoPerf{ii}{2}))],'-o');
    hold on
end

figure;
for ii = 1:length(subjectsAll)
    subplot(2,1,1)
    errorbar([1 2 3 4 5],sum(eachOptoPerf{ii}{1})/length(eachOptoPerf{ii}{1}),...
        std(eachOptoPerf{ii}{1}), '-o','Color',clrs(ii,:));
    hold on
    errorbar([6],mean(sum(eachOptoPerf{ii}{1})/length(eachOptoPerf{ii}{1})),...
        sem((sum(eachOptoPerf{ii}{1})/length(eachOptoPerf{ii}{1}))),'o','Color',clrs(ii,:));
    axis([0 7  .25 1.1])
    ylabel('Control')
    xticks([1:5])
    subplot(2,1,2)
   errorbar([1 2 3 4 5],sum(eachOptoPerf{ii}{2})/length(eachOptoPerf{ii}{2}),...
        std(eachOptoPerf{ii}{1}), '-o','Color',clrs(ii,:));
    hold on
    errorbar([6],mean(sum(eachOptoPerf{ii}{2})/length(eachOptoPerf{ii}{2})),...
        sem((sum(eachOptoPerf{ii}{2})/length(eachOptoPerf{ii}{2}))),'o','Color',clrs(ii,:));
    xticks([1:5])
    axis([0 7 .25 1.1])
    ylabel('Scotoma')
    xlabel('Ooptotype')
    allSubOpto(ii,:) = mean(eachOptoPerf{ii}{2});
end
[p,tbl,stats] = anova1(allSubOpto);
results = multcompare(stats);

%% Fixation Duration
figure
clrs = parula(5);

for ii = 1:length(subjectsAll)
    controlDrifts = drifts{ii}{1}.duration;
    scotDrifts = drifts{ii}{2}.duration;
    plot([1 2],[mean(controlDrifts(find(controlDrifts ~= 0))) mean(scotDrifts(find(scotDrifts ~= 0)))],...
        '-o','Color',clrs(ii,:));
    hold on
    tempTimes(ii,:) = [mean(controlDrifts(find(controlDrifts ~= 0))) mean(scotDrifts(find(scotDrifts ~= 0)))];
end
errorbar([1.1 2.1],[mean(tempTimes(:,1)) mean(tempTimes(:,2))],...
    [sem(tempTimes(:,1)) sem(tempTimes(:,2))],'-o','Color','k');

[h,p] = ttest(tempTimes(:,1), tempTimes(:,2));
xticks([1 2])
xticklabels({'Control','Scotoma'});
ylabel('Fixation Duration');
xlim([0 3])
title(sprintf('p = %.3f',p)); 

%% Plot just fixation HeatMaps
% figure;
% counter = 1;
for c = [1 2]
    for ii = [1 2 4]
        figure;
 
        idxAll = find(tracesFix{ii}.(condition{c}).xAll < 15 &...
            tracesFix{ii}.(condition{c}).xAll > -15 &...
            tracesFix{ii}.(condition{c}).yAll< 15 & ...
            tracesFix{ii}.(condition{c}).yAll > - 15);

        x = tracesFix{ii}.(condition{c}).xAll(idxAll);
        y = tracesFix{ii}.(condition{c}).yAll(idxAll);
        h2 = scatterhist(x,y,'Kernel','on',...
            'Location','SouthWest','Direction','out','Marker','.','Color','k');
        hold on
   
        resultF = generateHeatMapSimple( ...
            x, ...
            y, ...
            'Bins', 20,...
            'StimulusSize', 7,...
            'AxisValue', 15,...
            'Uncrowded', 0,...
            'Borders', 1);
        %         counter = counter + 1;
        axis square
        rectangle('Position',[-3.5 -3.5 7 7])
        axis off
        title(sprintf('Subject %i, %s',ii,(condition{c})));
        line([0 0],[-15 15],'Color','k')%,'symbol','','orientation','horizontal','PlotStyle','compact');
        line([-15 15],[0 0],'Color','k')%,'symbol','','orientation','horizontal','PlotStyle','compact');
        
        %         line(h2(2),[-15 15],[-15 15],'Color','k')%,'symbol','','orientation','horizontal','PlotStyle','compact');
        %         line(h2(3),[-10 10],[-10 10],'Color','k')%,'symbol','','orientation','horizontal','PlotStyle','compact');
        hold on
%         line(h2(1),[0 0],[-15 15],'Color','k')%,'symbol','','orientation','horizontal','PlotStyle','compact');
%         line(h2(1),[-15 15],[0 0],'Color','k')%,'symbol','','orientation','horizontal','PlotStyle','compact');
        
        %         line(h2(2),[mean(x) mean(x)],[-15 15],'Color','r')%,'symbol','','orientation','horizontal','PlotStyle','compact');
        line(h2(1),[median(x) median(x)],[-15 15],'Color','r')%,'symbol','','orientation','horizontal','PlotStyle','compact');
        
        %         line(h2(3),[-15 15],[mean(y) mean(y)],'Color','r')%,'symbol','','orientation','horizontal','PlotStyle','compact');
        line(h2(1),[-15 15],[median(y) median(y)],'Color','r')%,'symbol','','orientation','horizontal','PlotStyle','compact');
        
        %         set(h2(2:3),'XTickLabel','');
%         set(h2(2:3),'YTickLabel','');
        
        saveas(gcf,sprintf('../../../Documents/Overleaf_MiniScotoma/figures/HistScatter%i%s.png',ii,(condition{c})));
        saveas(gcf,sprintf('../../../Documents/Overleaf_MiniScotoma/figures/HistScatter%i%s.epsc',ii,(condition{c})));
    end
end

figure;
counter = 1;
% for c = [1 2]
for ii = [1 2 3 4]
    %         figure;
    subplot(3,2,counter)
    [N,edges] = histcounts((tracesFix{ii}.(condition{1}).xAll),....
        'Normalization','pdf');
    edges = edges(2:end) - (edges(2)-edges(1))/2;
    forlegs(1) = plot(edges, normalizeVector(N),'Color','b');
    hold on
    line([mean(tracesFix{ii}.(condition{1}).xAll)...
        mean(tracesFix{ii}.(condition{1}).xAll)],[0 1],'Color','b')
    hold on
    [N,edges] = histcounts((tracesFix{ii}.(condition{2}).xAll),....
        'Normalization','pdf');
    edges = edges(2:end) - (edges(2)-edges(1))/2;
    forlegs(2) = plot(edges, normalizeVector(N),'Color','r');
     line([mean(tracesFix{ii}.(condition{2}).xAll)...
        mean(tracesFix{ii}.(condition{2}).xAll)],[0 1],'Color','r')
    
    title('X')
    legend(forlegs,{'Control','Scotoma'});
    counter = counter + 1;
    xlim([-10 10])
    hold on
     
    
    subplot(3,2,counter)
    [N,edges] = histcounts((tracesFix{ii}.(condition{1}).yAll),....
        'Normalization','pdf');
    edges = edges(2:end) - (edges(2)-edges(1))/2;
    forlegs(1) = plot(edges, normalizeVector(N),'Color','b');
    hold on
     line([mean(tracesFix{ii}.(condition{1}).yAll)...
        mean(tracesFix{ii}.(condition{1}).yAll)],[0 1],'Color','b')
    hold on
    [N,edges] = histcounts((tracesFix{ii}.(condition{2}).yAll),....
        'Normalization','pdf');
    edges = edges(2:end) - (edges(2)-edges(1))/2;
    forlegs(2) = plot(edges, normalizeVector(N),'Color','r');
     line([mean(tracesFix{ii}.(condition{2}).yAll)...
        mean(tracesFix{ii}.(condition{2}).yAll)],[0 1],'Color','r')
    title('Y')
    counter = counter + 1;
    xlim([-10 10])
%     legend(forlegs,{'Control','Scotoma'});
end

%% EM Analysis
for ii = 1:length(subjectsAll)
    figure;
    subplot(2,3,1)
    errorbar([1 2],[mean(out{ii}{1}.msRates) mean(out{ii}{2}.msRates)]*100,...
        [sem(out{ii}{1}.msRates) sem(out{ii}{2}.msRates)]*100,'-o');
    hold on
     errorbar([3 4],[mean(out{ii}{1}.sRates) mean(out{ii}{2}.sRates)]*100,...
        [sem(out{ii}{1}.sRates) sem(out{ii}{2}.sRates)]*100,'-o');
    xlim([.5 4.5])
    xticks([1 2 3 4])
    xticklabels({'Control','Scotoma','SControl','SScotoma'})
    ylabel('MS/S Rate');
    title(subjectsAll{ii})
    subplot(2,3,2)
    validMS1 = find(out{ii}{1,1}.amps < 90);
    validMS2 = find(out{ii}{1,2}.amps < 90);
    errorbar([1 2],[median(out{ii}{1,1}.amps(validMS1)) median(out{ii}{1,2}.amps(validMS2))],...
        [sem(out{ii}{1}.amps(validMS1)) sem(out{ii}{2}.amps(validMS2))],'-o');
     xlim([.5 2.5])
    xticks([1 2])
    xticklabels({'Control','Scotoma'})
    ylabel('MS/S Amplitude');
    
    subplot(2,3,3)
    errorbar([1 2],[nanmean(drifts{ii}{1}.span(find(drifts{ii}{1}.span > 0))) ...
        nanmean(drifts{ii}{2}.span(find(drifts{ii}{2}.span > 0)))],...
        [nanstd(drifts{ii}{1}.span(find(drifts{ii}{1}.span > 0))) ...
        nanstd(drifts{ii}{2}.span(find(drifts{ii}{2}.span > 0)))],'-o');
     xlim([.5 2.5])
    xticks([1 2])
    xticklabels({'Control','Scotoma'})
    ylabel('Span');
    
    subplot(2,3,4)
%     idxPRL = find(drifts{ii}{1}.prlDistance > 0 & (drifts{ii}{1}.prlDistance > 200))
    errorbar([1 2],[nanmean(drifts{ii}{1}.prlDistance(find(drifts{ii}{1}.prlDistance > 0))) ...
        nanmean(drifts{ii}{2}.prlDistance(find(drifts{ii}{2}.prlDistance > 0)))],...
        [sem(drifts{ii}{1}.prlDistance(find(drifts{ii}{1}.prlDistance > 0))) ...
        sem(drifts{ii}{2}.prlDistance(find(drifts{ii}{2}.prlDistance > 0)))],'-o');
     xlim([.5 2.5])
    xticks([1 2])
    xticklabels({'Control','Scotoma'})
    ylabel('PRL Distance');
    
    subplot(2,3,5)
%     figure;
    tempC = polarhistogram(out{ii}{1,1}.dirs(validMS1),10)
    valsNormedC = normalizeVector(tempC.Values);
    rightNumsC(ii) = sum([valsNormedC(5) valsNormedC(6)]);
    otherNumsC(ii) = sum([valsNormedC(8:10) valsNormedC(1:3)]);
    title('Control')
    subplot(2,3,6)
% hold on
    tempS = polarhistogram(out{ii}{1,2}.dirs(validMS2),10)
    title('Scotoma, MS Direction')
%     xlabel('MS Direction');
    valsNormed = normalizeVector(tempS.Values);
    rightNumsS(ii) = sum([valsNormed(5) valsNormed(6)]);
    otherNumsS(ii) = sum([valsNormed(8:10) valsNormed(1:3)]);

    allMS.dirs{ii,1} = (out{ii}{1,1}.dirs(validMS1));
    allMS.dirs{ii,2} = (out{ii}{1,2}.dirs(validMS2));
 
    allMS.amp{ii,1} = (out{ii}{1,1}.amps(validMS1));
    allMS.amp{ii,2} = (out{ii}{1,2}.amps(validMS2));
end

for ii = 1:length(subjectsAll)
    figure;
    [N,edges,bin] = histcounts(allMS.dirs{ii,1});
    numBins = unique(bin);
    subplot(1,2,1)
    for i = numBins
        idcBin = allMS.amp{ii,1}(find(numBins==i));
        sz = 10*N(i);
        polarscatter(edges(i),mean(idcBin),sz,'k');
        hold on
    end
    [N,edges,bin] = histcounts(allMS.dirs{ii,2});
    numBins = unique(bin);
    subplot(1,2,2)
    for i = numBins
        idcBin = allMS.amp{ii,2}(find(numBins==i));
        sz = 10*N(i);
        polarscatter(edges(i),mean(idcBin),sz,'k');
        hold on
    end
end
figure;
for ii = 1:length(subjectsAll)
    subplot(1,2,1)
    forLegs(1) = plot([1.1 2.1], ([otherNumsC(ii) rightNumsC(ii)]),'-o','Color','r');
    hold on
    subplot(1,2,2)
    hold on
    forLegs(2) = plot([1 2], [otherNumsS(ii) rightNumsS(ii)],'-o','Color','b');
end
%     xlim([.5 2.5])
%     ylim([-1 600])
%     xticks([1 2])
%     xticklabels({'Left MS','Right MS'});
%     legend(forLegs,{'Control','Scotoma'});
    
%     figure;
    subplot(1,2,1)
    
    templeg(1) = errorbar([1 2],[mean(otherNumsC) mean(rightNumsC)],...
        [sem(otherNumsC) sem(rightNumsC)],'-o','Color','k');
%     hold on
    xlim([.5 2.5])
    xticks([1 2])
    xticklabels({'Left MS','Right MS'});
    title('Control');
    subplot(1,2,2)
    templeg(2) = errorbar([1 2],[mean(otherNumsS) mean(rightNumsS)],...
        [sem(otherNumsS) sem(rightNumsS)],'-o','Color','k');
%     ylim([100 250])
    xlim([.5 2.5])
    xticks([1 2])
    xticklabels({'Left MS','Right MS'});
    title('Scotoma');

    figure;
    hold on
    for ii = 1:5
    plot([1 2],[(rightNumsC(ii) - otherNumsC(ii)) ...
        (rightNumsS(ii) - otherNumsS(ii))],...
       '-o','Color',[127 127 127]/255);
%     plot([2 2 2 2],(rightNumsS - otherNumsS),...
%        'o');
    end
     errorbar(1,mean(rightNumsC - otherNumsC),...
        sem(rightNumsC - otherNumsC),'o','Color','k','MarkerFaceColor','k');
   hold on
    errorbar(2,mean(rightNumsS - otherNumsS),...
        sem(rightNumsS - otherNumsS),'o','Color','k','MarkerFaceColor','k');
    ylim([-1.5 1.5])
    
    xlim([.5 2.5])
    line([0 3],[0 0]);
    xticks([1 2])
    xticklabels({'Left MS','Right MS'});
    ylabel('Difference in Normalized MS Direction');
    [h,p] = ttest(rightNumsC - otherNumsC,rightNumsS - otherNumsS);
    
 %% Offset Difference comparison
 for ii = 1:length(subjectsAll) 
    peaksLocControl{ii} = analysisTimeContour(tracesFix{ii}.Control.xAll,...
        tracesFix{ii}.Control.yAll, NaN, 20); 
    peaksLocScotoma{ii} = analysisTimeContour(tracesFix{ii}.Scotoma.xAll,...
        tracesFix{ii}.Scotoma.yAll, NaN, 20); 
    
 end
 
 symbs = {'o' 's' 'd' 'p' '^' 'v' '<' '>'};
 for ii = 1:length(subjectsAll)
     if ii == 3
         centeredPeak(ii,1:2) = peaksLocScotoma{ii}.contourPeak - peaksLocControl{ii}.mean;
         eucDist(ii,1) = hypot(peaksLocControl{ii}.mean(1),peaksLocControl{ii}.mean(2));
         eucDist(ii,2) = hypot(peaksLocScotoma{ii}.contourPeak(1),peaksLocScotoma{ii}.mean(2));
     else
         centeredPeak(ii,1:2) = peaksLocScotoma{ii}.contourPeak - peaksLocControl{ii}.contourPeak;
         eucDist(ii,1) = hypot(peaksLocControl{ii}.contourPeak(1),peaksLocControl{ii}.contourPeak(2));
         eucDist(ii,2) = hypot(peaksLocScotoma{ii}.contourPeak(1),peaksLocScotoma{ii}.contourPeak(2));
     end
 end
 
 figure;
 clrs = parula(5);
 subplot(1,2,1)
 for i = 1:length(subjectsAll)
     plot([1 2],[eucDist(i,1) eucDist(i,2)],'-o',...
         'Color',clrs(i,:),...
         'MarkerFaceColor',clrs(i,:),'MarkerSize',8);
     hold on
 end
 xticks([1 2])
 xticklabels({'Control','Scotoma'});
 ylabel('Euclidean Distance (arcmin)');
 xlim([0 3])
 errorbar([1.1 2.1],[mean(eucDist(:,1)) mean(eucDist(:,2))],...
     [sem(eucDist(:,1)) sem(eucDist(:,2))],'-o','Color','k',...
     'MarkerFaceColor','k','MarkerSize',10)
 [h,p1]=ttest( (eucDist(:,1)), (eucDist(:,2)))
title(sprintf('p = %.3f',p1));
 subplot(1,2,2)
rectangle('Position',[-10.5,-10.5,25,25],'FaceColor',[130 130 130]./255);
rectangle('Position',[-3.5,-3.5,7,7],'FaceColor',[0 0 0]);
pos = [-2.5 -2.5 5 5]; 
rectangle('Position',pos,'Curvature',[1 1],'FaceColor',[130 130 130]./255)
hold on
 for i = 1:length(subjectsAll)
     %      plot(peaksLocScotoma{ii}.contourPeak(1),peaksLocScotoma{ii}.contourPeak(2)
     plot(peaksLocScotoma{i}.contourPeak(1),peaksLocScotoma{i}.contourPeak(2), ...
         'Marker', symbs{i},'MarkerSize',13,'Color',clrs(i,:),...
         'MarkerFaceColor',clrs(i,:));
     hold on
 end
 axis ([-10 10 -10 10])
 line([0 0], [-10 10],'Color','r','LineStyle','--');
 line([-10 10],[0 0],'Color','r','LineStyle','--');
 
 title('Recentered Offsets')
 forleg(1) = plot(0,0,'k','Marker','*');
%  legend(forleg,{'Control Fixation'});

%%    % make figure in time
ylimsize = 10;
xlimsize = 30;
timeFromStart = 400;
for ii = 1:length(subjectsAll)
    figure;
    x1 = []; x2 = []; x3 = []; x4 = []; x5 = [];
    y1 = []; y2 = []; y3 = []; y4 = []; y5 = [];
    for i = 1:length(data{ii}.Control)
        if data{ii}.Control{i}.FixationTrial
            continue;
        end
        tempLocation = data{ii}.Control{i};
        %         fithVals = floor(length(data{ii}.Control{i}.x)/5);
        x1 = [x1 tempLocation.x(timeFromStart:tempLocation.responseTiming(1)-250)];
        x2 = [x2 tempLocation.x(tempLocation.responseTiming(1):...
            tempLocation.responseTiming(2)-250)];
        x3 = [x3 tempLocation.x(tempLocation.responseTiming(2):...
            tempLocation.responseTiming(3)-250)];
        x4 = [x4 tempLocation.x(tempLocation.responseTiming(3):...
            tempLocation.responseTiming(4)-250)];
        x5 = [x5 tempLocation.x(tempLocation.responseTiming(4):...
            tempLocation.responseTiming(5)-50)];
        y1 = [y1 tempLocation.y(timeFromStart:tempLocation.responseTiming(1)-250)];
        y2 = [y2 tempLocation.y(tempLocation.responseTiming(1):...
            tempLocation.responseTiming(2)-250)];
        y3 = [y3 tempLocation.y(tempLocation.responseTiming(2):...
            tempLocation.responseTiming(3)-250)];
        y4 = [y4 tempLocation.y(tempLocation.responseTiming(3):...
            tempLocation.responseTiming(4)-250)];
        y5 = [y5 tempLocation.y(tempLocation.responseTiming(4):...
            tempLocation.responseTiming(5)-50)];
        
        trialTasksCentered{ii}.Control{i} = [...
            tempLocation.x(timeFromStart:tempLocation.responseTiming(1)-250)+20 ...
            tempLocation.x(tempLocation.responseTiming(1):tempLocation.responseTiming(2)-250)+10 ...
            tempLocation.x(tempLocation.responseTiming(2):...
            tempLocation.responseTiming(3)-250) ...
            tempLocation.x(tempLocation.responseTiming(3):...
            tempLocation.responseTiming(4)-250)-10 ...
            tempLocation.x(tempLocation.responseTiming(4):...
            tempLocation.responseTiming(5)-50)-20;...
            ...
            tempLocation.y(timeFromStart:tempLocation.responseTiming(1)-250) ...
            tempLocation.y(tempLocation.responseTiming(1):...
            tempLocation.responseTiming(2)-250) ...
            tempLocation.y(tempLocation.responseTiming(2):...
            tempLocation.responseTiming(3)-250) ...
            tempLocation.y(tempLocation.responseTiming(3):...
            tempLocation.responseTiming(4)-250) ...
            tempLocation.y(tempLocation.responseTiming(4):...
            tempLocation.responseTiming(5)-50)];
        
            trialTasksCenteredStackedX{ii}.Control{i,1} = tempLocation.x(...
                timeFromStart:tempLocation.responseTiming(1)-250)+20; 
            trialTasksCenteredStackedX{ii}.Control{i,2} = tempLocation.x(...
                tempLocation.responseTiming(1):tempLocation.responseTiming(2)-250)+10;
            trialTasksCenteredStackedX{ii}.Control{i,3} = tempLocation.x(...
                tempLocation.responseTiming(2):...
                tempLocation.responseTiming(3)-250); 
            trialTasksCenteredStackedX{ii}.Control{i,4} = tempLocation.x(...
                tempLocation.responseTiming(3):...
                tempLocation.responseTiming(4)-250)-10;
            trialTasksCenteredStackedX{ii}.Control{i,5} = tempLocation.x(...
                tempLocation.responseTiming(4):...
                tempLocation.responseTiming(5)-50)-20;
            
            trialTasksCenteredStackedY{ii}.Control{i,1} = tempLocation.y(...
                timeFromStart:tempLocation.responseTiming(1)-250); 
            trialTasksCenteredStackedY{ii}.Control{i,2} = tempLocation.y(...
                tempLocation.responseTiming(1):...
                tempLocation.responseTiming(2)-250);
            trialTasksCenteredStackedY{ii}.Control{i,3} = tempLocation.y(...
                tempLocation.responseTiming(2):...
                tempLocation.responseTiming(3)-250); 
            trialTasksCenteredStackedY{ii}.Control{i,4} = tempLocation.y(...
                tempLocation.responseTiming(3):...
                tempLocation.responseTiming(4)-250);
            trialTasksCenteredStackedY{ii}.Control{i,5} = tempLocation.y(...
                tempLocation.responseTiming(4):...
                tempLocation.responseTiming(5)-50);
    end
    
    inTimeData{ii}.Control{1} = [x1' y1'];
    inTimeData{ii}.Control{2} = [x2' y2'];
    inTimeData{ii}.Control{3} = [x3' y3'];
    inTimeData{ii}.Control{4} = [x4' y4'];
    inTimeData{ii}.Control{5} = [x5' y5'];
    for nn = 1
        
        subplot(5,2,1)
        resultT = generateHeatMapSimple( ...
            x1, ...
            y1, ...
            'Bins', 100,...
            'StimulusSize', 10,...
            'AxisValue', xlimsize,...
            'Uncrowded', 0,...
            'Borders', 1);
        
        ylim([-ylimsize ylimsize])
        hold on
        plotRectangles2020Line(processedPPTrialsS{nn},0);
        title('Control');
        subplot(5,2,3)
        resultT = generateHeatMapSimple( ...
            x2, ...
            y2, ...
            'Bins', 100,...
            'StimulusSize', 10,...
            'AxisValue', xlimsize,...
            'Uncrowded', 0,...
            'Borders', 1);
        axis off
        ylim([-ylimsize ylimsize])
        hold on
        plotRectangles2020Line(processedPPTrialsS{nn},0);
        subplot(5,2,5)
        resultT = generateHeatMapSimple( ...
            x3, ...
            y3, ...
            'Bins', 100,...
            'StimulusSize', 10,...
            'AxisValue', xlimsize,...
            'Uncrowded', 0,...
            'Borders', 1);
        axis off
        ylim([-ylimsize ylimsize])
        hold on
        plotRectangles2020Line(processedPPTrialsS{nn},0);
        subplot(5,2,7)
        resultT = generateHeatMapSimple( ...
            x4, ...
            y4, ...
            'Bins', 100,...
            'StimulusSize', 10,...
            'AxisValue', xlimsize,...
            'Uncrowded', 0,...
            'Borders', 1);
        axis off
        ylim([-ylimsize ylimsize])
        hold on
        plotRectangles2020Line(processedPPTrialsS{nn},0);
        subplot(5,2,9)
        resultT = generateHeatMapSimple( ...
            x5, ...
            y5, ...
            'Bins', 100,...
            'StimulusSize', 10,...
            'AxisValue', xlimsize,...
            'Uncrowded', 0,...
            'Borders', 1);
        axis off
        ylim([-ylimsize ylimsize])
        hold on
        plotRectangles2020Line(processedPPTrialsS{nn},0);
        %     suptitle('Control')
    end
    x1 = []; x2 = []; x3 = []; x4 = []; x5 = [];
    y1 = []; y2 = []; y3 = []; y4 = []; y5 = [];
    for i = 1:length(data{ii}.Scotoma)
        if data{ii}.Scotoma{i}.FixationTrial
            continue;
        end
        tempLocation = data{ii}.Scotoma{i};
        %         fithVals = floor(length(data{ii}.Control{i}.x)/5);
        x1 = [x1 tempLocation.x(timeFromStart:tempLocation.responseTiming(1)-250)];
        x2 = [x2 tempLocation.x(tempLocation.responseTiming(1):...
            tempLocation.responseTiming(2)-250)];
        x3 = [x3 tempLocation.x(tempLocation.responseTiming(2):...
            tempLocation.responseTiming(3)-250)];
        x4 = [x4 tempLocation.x(tempLocation.responseTiming(3):...
            tempLocation.responseTiming(4)-250)];
        x5 = [x5 tempLocation.x(tempLocation.responseTiming(4):...
            tempLocation.responseTiming(5)-50)];
        y1 = [y1 tempLocation.y(timeFromStart:tempLocation.responseTiming(1)-250)];
        y2 = [y2 tempLocation.y(tempLocation.responseTiming(1):...
            tempLocation.responseTiming(2)-250)];
        y3 = [y3 tempLocation.y(tempLocation.responseTiming(2):...
            tempLocation.responseTiming(3)-250)];
        y4 = [y4 tempLocation.y(tempLocation.responseTiming(3):...
            tempLocation.responseTiming(4)-250)];
        y5 = [y5 tempLocation.y(tempLocation.responseTiming(4):...
            tempLocation.responseTiming(5)-50)];
        trialTasksCentered{ii}.Scotoma{i} = [...
            tempLocation.x(timeFromStart:tempLocation.responseTiming(1)-250)+20 ...
            tempLocation.x(tempLocation.responseTiming(1):tempLocation.responseTiming(2)-250)+10 ...
            tempLocation.x(tempLocation.responseTiming(2):...
            tempLocation.responseTiming(3)-250) ...
            tempLocation.x(tempLocation.responseTiming(3):...
            tempLocation.responseTiming(4)-250)-10 ...
            tempLocation.x(tempLocation.responseTiming(4):...
            tempLocation.responseTiming(5)-50)-20;...
            ...
            tempLocation.y(timeFromStart:tempLocation.responseTiming(1)-250) ...
            tempLocation.y(tempLocation.responseTiming(1):...
            tempLocation.responseTiming(2)-250) ...
            tempLocation.y(tempLocation.responseTiming(2):...
            tempLocation.responseTiming(3)-250) ...
            tempLocation.y(tempLocation.responseTiming(3):...
            tempLocation.responseTiming(4)-250) ...
            tempLocation.y(tempLocation.responseTiming(4):...
            tempLocation.responseTiming(5)-50)];
        
        
        trialTasksCenteredStackedX{ii}.Scotoma{i,1} = tempLocation.x(...
                timeFromStart:tempLocation.responseTiming(1)-250)+20; 
            trialTasksCenteredStackedX{ii}.Scotoma{i,2} = tempLocation.x(...
                tempLocation.responseTiming(1):tempLocation.responseTiming(2)-250)+10;
            trialTasksCenteredStackedX{ii}.Scotoma{i,3} = tempLocation.x(...
                tempLocation.responseTiming(2):...
                tempLocation.responseTiming(3)-250); 
            trialTasksCenteredStackedX{ii}.Scotoma{i,4} = tempLocation.x(...
                tempLocation.responseTiming(3):...
                tempLocation.responseTiming(4)-250)-10;
            trialTasksCenteredStackedX{ii}.Scotoma{i,5} = tempLocation.x(...
                tempLocation.responseTiming(4):...
                tempLocation.responseTiming(5)-50)-20;
            
            trialTasksCenteredStackedY{ii}.Scotoma{i,1} = tempLocation.y(...
                timeFromStart:tempLocation.responseTiming(1)-250); 
            trialTasksCenteredStackedY{ii}.Scotoma{i,2} = tempLocation.y(...
                tempLocation.responseTiming(1):...
                tempLocation.responseTiming(2)-250);
            trialTasksCenteredStackedY{ii}.Scotoma{i,3} = tempLocation.y(...
                tempLocation.responseTiming(2):...
                tempLocation.responseTiming(3)-250); 
            trialTasksCenteredStackedY{ii}.Scotoma{i,4} = tempLocation.y(...
                tempLocation.responseTiming(3):...
                tempLocation.responseTiming(4)-250);
            trialTasksCenteredStackedY{ii}.Scotoma{i,5} = tempLocation.y(...
                tempLocation.responseTiming(4):...
                tempLocation.responseTiming(5)-50);
    end
    inTimeData{ii}.Scotoma{1} = [x1' y1'];
    inTimeData{ii}.Scotoma{2} = [x2' y2'];
    inTimeData{ii}.Scotoma{3} = [x3' y3'];
    inTimeData{ii}.Scotoma{4} = [x4' y4'];
    inTimeData{ii}.Scotoma{5} = [x5' y5'];    
    for nn = 1
        %     figure;
        subplot(5,2,2)
        resultT = generateHeatMapSimple( ...
            x1, ...
            y1, ...
            'Bins', 100,...
            'StimulusSize', 10,...
            'AxisValue', xlimsize,...
            'Uncrowded', 0,...
            'Borders', 1);
        axis off
        ylim([-ylimsize ylimsize])
        hold on
        plotRectangles2020Line(processedPPTrialsS{nn},0);
        title('Scotoma');
        subplot(5,2,4)
        resultT = generateHeatMapSimple( ...
            x2, ...
            y2, ...
            'Bins', 100,...
            'StimulusSize', 10,...
            'AxisValue', xlimsize,...
            'Uncrowded', 0,...
            'Borders', 1);
        axis off
        ylim([-ylimsize ylimsize])
        hold on
        plotRectangles2020Line(processedPPTrialsS{nn},0);
        subplot(5,2,6)
        resultT = generateHeatMapSimple( ...
            x3, ...
            y3, ...
            'Bins', 100,...
            'StimulusSize', 10,...
            'AxisValue', xlimsize,...
            'Uncrowded', 0,...
            'Borders', 1);
        axis off
        ylim([-ylimsize ylimsize])
        hold on
        plotRectangles2020Line(processedPPTrialsS{nn},0);
        subplot(5,2,8)
        resultT = generateHeatMapSimple( ...
            x4, ...
            y4, ...
            'Bins', 100,...
            'StimulusSize', 10,...
            'AxisValue', xlimsize,...
            'Uncrowded', 0,...
            'Borders', 1);
        axis off
        ylim([-ylimsize ylimsize])
        hold on
        plotRectangles2020Line(processedPPTrialsS{nn},0);
        subplot(5,2,10)
        resultT = generateHeatMapSimple( ...
            x5, ...
            y5, ...
            'Bins', 100,...
            'StimulusSize', 10,...
            'AxisValue', xlimsize,...
            'Uncrowded', 0,...
            'Borders', 1);
        axis off
        ylim([-ylimsize ylimsize])
        hold on
        plotRectangles2020Line(processedPPTrialsS{nn},0);
        %     suptitle('Scotoma');
    end
    
    figure;
    %fixation
    x1 = []; x2 = []; x3 = []; x4 = []; x5 = [];
    y1 = []; y2 = []; y3 = []; y4 = []; y5 = [];
    for i = 1:length(data{ii}.Scotoma)
        if data{ii}.Scotoma{i}.FixationTrial
            tempLocation = data{ii}.Scotoma{i};
            fithVals = floor(length(tempLocation.x)/5);
            x1 = [x1 tempLocation.x(1:fithVals)];
            x2 = [x2 tempLocation.x(fithVals+1:fithVals*2)];
            x3 = [x3 tempLocation.x(fithVals*2+1:fithVals*3)];
            x4 = [x4 tempLocation.x(fithVals*3+1:fithVals*4)];
            x5 = [x5 tempLocation.x(fithVals*4+1:fithVals*5)];
            y1 = [y1 tempLocation.y(1:fithVals)];
            y2 = [y2 tempLocation.y(fithVals+1:fithVals*2)];
            y3 = [y3 tempLocation.y(fithVals*2+1:fithVals*3)];
            y4 = [y4 tempLocation.y(fithVals*3+1:fithVals*4)];
            y5 = [y5 tempLocation.y(fithVals*4+1:fithVals*5)];
        end
    end
    for nn = 1
        %     figure;
        subplot(5,2,2)
        resultT = generateHeatMapSimple( ...
            x1, ...
            y1, ...
            'Bins', 100,...
            'StimulusSize', 7,...
            'AxisValue', xlimsize,...
            'Uncrowded', 4,...
            'Borders', 1);
        axis off
        ylim([-ylimsize ylimsize])
        hold on
        %     plotRectangles2020Line(processedPPTrialsS{nn},0);
        title('Scotoma Fixation');
        subplot(5,2,4)
        resultT = generateHeatMapSimple( ...
            x2, ...
            y2, ...
            'Bins', 100,...
            'StimulusSize', 10,...
            'AxisValue', xlimsize,...
            'Uncrowded', 4,...
            'Borders', 1);
        axis off
        ylim([-ylimsize ylimsize])
        hold on
        %     plotRectangles2020Line(processedPPTrialsS{nn},0);
        subplot(5,2,6)
        resultT = generateHeatMapSimple( ...
            x3, ...
            y3, ...
            'Bins', 100,...
            'StimulusSize', 10,...
            'AxisValue', xlimsize,...
            'Uncrowded', 4,...
            'Borders', 1);
        axis off
        ylim([-ylimsize ylimsize])
        hold on
        %     plotRectangles2020Line(processedPPTrialsS{nn},0);
        subplot(5,2,8)
        resultT = generateHeatMapSimple( ...
            x4, ...
            y4, ...
            'Bins', 100,...
            'StimulusSize', 10,...
            'AxisValue', xlimsize,...
            'Uncrowded', 4,...
            'Borders', 1);
        axis off
        ylim([-ylimsize ylimsize])
        hold on
        %     plotRectangles2020Line(processedPPTrialsS{nn},0);
        subplot(5,2,10)
        resultT = generateHeatMapSimple( ...
            x5, ...
            y5, ...
            'Bins', 100,...
            'StimulusSize', 10,...
            'AxisValue', xlimsize,...
            'Uncrowded', 4,...
            'Borders', 1);
        axis off
        ylim([-ylimsize ylimsize])
        hold on
        %     plotRectangles2020Line(processedPPTrialsS{nn},0);
        %     suptitle('Scotoma');
    end
    
    for i = 1:length(data{ii}.Control)
        if data{ii}.Control{i}.FixationTrial
            tempLocation = data{ii}.Control{i};
            fithVals = floor(length(tempLocation.x)/5);
            x1 = [x1 tempLocation.x(1:fithVals)];
            x2 = [x2 tempLocation.x(fithVals+1:fithVals*2)];
            x3 = [x3 tempLocation.x(fithVals*2+1:fithVals*3)];
            x4 = [x4 tempLocation.x(fithVals*3+1:fithVals*4)];
            x5 = [x5 tempLocation.x(fithVals*4+1:fithVals*5)];
            y1 = [y1 tempLocation.y(1:fithVals)];
            y2 = [y2 tempLocation.y(fithVals+1:fithVals*2)];
            y3 = [y3 tempLocation.y(fithVals*2+1:fithVals*3)];
            y4 = [y4 tempLocation.y(fithVals*3+1:fithVals*4)];
            y5 = [y5 tempLocation.y(fithVals*4+1:fithVals*5)];
        end
    end
    for nn = 1
        %     figure;
        subplot(5,2,1)
        resultT = generateHeatMapSimple( ...
            x1, ...
            y1, ...
            'Bins', 100,...
            'StimulusSize', 7,...
            'AxisValue', xlimsize,...
            'Uncrowded', 4,...
            'Borders', 1);
        axis off
        ylim([-ylimsize ylimsize])
        hold on
        %     plotRectangles2020Line(processedPPTrialsS{nn},0);
        title('Fixation Control');
        subplot(5,2,3)
        resultT = generateHeatMapSimple( ...
            x2, ...
            y2, ...
            'Bins', 100,...
            'StimulusSize', 10,...
            'AxisValue', xlimsize,...
            'Uncrowded', 4,...
            'Borders', 1);
        axis off
        ylim([-ylimsize ylimsize])
        hold on
        %     plotRectangles2020Line(processedPPTrialsS{nn},0);
        subplot(5,2,5)
        resultT = generateHeatMapSimple( ...
            x3, ...
            y3, ...
            'Bins', 100,...
            'StimulusSize', 10,...
            'AxisValue', xlimsize,...
            'Uncrowded', 4,...
            'Borders', 1);
        axis off
        ylim([-ylimsize ylimsize])
        hold on
        %     plotRectangles2020Line(processedPPTrialsS{nn},0);
        subplot(5,2,7)
        resultT = generateHeatMapSimple( ...
            x4, ...
            y4, ...
            'Bins', 100,...
            'StimulusSize', 10,...
            'AxisValue', xlimsize,...
            'Uncrowded', 4,...
            'Borders', 1);
        axis off
        ylim([-ylimsize ylimsize])
        hold on
        %     plotRectangles2020Line(processedPPTrialsS{nn},0);
        subplot(5,2,9)
        resultT = generateHeatMapSimple( ...
            x5, ...
            y5, ...
            'Bins', 100,...
            'StimulusSize', 10,...
            'AxisValue', xlimsize,...
            'Uncrowded', 4,...
            'Borders', 1);
        axis off
        ylim([-ylimsize ylimsize])
        hold on
        %     plotRectangles2020Line(processedPPTrialsS{nn},0);
        %     suptitle('Scotoma');
    end
    
    %single optotype
    figure;
    suptitle('Single Snellen E')
    if ii == 3
        dataCompare.ecc0 = load('X:\Ashley\VisualAcuity_Ecc\A144\Uncrowded_Unstabilized_0ecc.mat');
        temp = dataCompare.ecc0;
        dateCollect = datetime(str2double(temp.eis_data.filename(5:8)),...
            str2double(temp.eis_data.filename(9:10)),...
            str2double(temp.eis_data.filename(11:12)));
        
        temppptrials = convertDataToPPtrialsTemp(temp.eis_data,dateCollect);
        processedPPTrials = newEyerisEISReadTemp(temppptrials);
        x1 = []; x2 = []; x3 = []; x4 = []; x5 = [];
        y1 = []; y2 = []; y3 = []; y4 = []; y5 = [];
        for i = 1:length(processedPPTrials)
            if processedPPTrials{i}.fixationTrial
                continue;
            end
            tempLocation = processedPPTrials{i};
            fithVals = floor(length(tempLocation.x)/5);
            x1 = [x1 tempLocation.x(1:fithVals)];
            x2 = [x2 tempLocation.x(fithVals+1:fithVals*2)];
            x3 = [x3 tempLocation.x(fithVals*2+1:fithVals*3)];
            x4 = [x4 tempLocation.x(fithVals*3+1:fithVals*4)];
            x5 = [x5 tempLocation.x(fithVals*4+1:fithVals*5)];
            y1 = [y1 tempLocation.y(1:fithVals)];
            y2 = [y2 tempLocation.y(fithVals+1:fithVals*2)];
            y3 = [y3 tempLocation.y(fithVals*2+1:fithVals*3)];
            y4 = [y4 tempLocation.y(fithVals*3+1:fithVals*4)];
            y5 = [y5 tempLocation.y(fithVals*4+1:fithVals*5)];
        end
        for nn = 1
            %     figure;
            %             subplot(3,2,1)
            resultT = generateHeatMapSimple( ...
                [x1 x2 x3 x4 x5], ...
                [y1 y2 y3 y4 y5], ...
                'Bins', 50,...
                'StimulusSize', 7,...
                'AxisValue', ylimsize,...
                'Uncrowded', 0,...
                'Borders', 1);
            axis off
            ylim([-ylimsize ylimsize])
            hold on
            rectangle('Position',[-2.5 -2.5 5 5]);
            title('Single Snellen E');
            %             subplot(3,2,2)
            %             resultT = generateHeatMapSimple( ...
            %                 x2, ...
            %                 y2, ...
            %                 'Bins', 50,...
            %                 'StimulusSize', 10,...
            %                 'AxisValue', xlimsize,...
            %                 'Uncrowded', 0,...
            %                 'Borders', 1);
            %             axis off
            %             ylim([-ylimsize ylimsize])
            %             hold on
            %             rectangle('Position',[-2.5 -2.5 5 5]);
            %             subplot(3,2,3)
            %             resultT = generateHeatMapSimple( ...
            %                 x3, ...
            %                 y3, ...
            %                 'Bins', 50,...
            %                 'StimulusSize', 10,...
            %                 'AxisValue', xlimsize,...
            %                 'Uncrowded', 0,...
            %                 'Borders', 1);
            %             axis off
            %             ylim([-ylimsize ylimsize])
            %             hold on
            %             rectangle('Position',[-2.5 -2.5 5 5]);
            %             subplot(3,2,4)
            %             resultT = generateHeatMapSimple( ...
            %                 x4, ...
            %                 y4, ...
            %                 'Bins', 50,...
            %                 'StimulusSize', 10,...
            %                 'AxisValue', xlimsize,...
            %                 'Uncrowded', 0,...
            %                 'Borders', 1);
            %             axis off
            %             ylim([-ylimsize ylimsize])
            %             hold on
            %             rectangle('Position',[-2.5 -2.5 5 5]);
            %             subplot(3,2,5)
            %             resultT = generateHeatMapSimple( ...
            %                 x5, ...
            %                 y5, ...
            %                 'Bins', 50,...
            %                 'StimulusSize', 10,...
            %                 'AxisValue', xlimsize,...
            %                 'Uncrowded', 0,...
            %                 'Borders', 1);
            %             axis off
            %             ylim([-ylimsize ylimsize])
            %             hold on
            %             rectangle('Position',[-2.5 -2.5 5 5]);
            %     suptitle('Scotoma');
        end
        
    end
    
    if ii == 5
        dataCompare.ecc0 = load('X:\Ashley\VisualAcuity_Ecc\Z055\tumE0ecc_1.mat');
        temp = dataCompare.ecc0;
        dateCollect = datetime(str2double(temp.eis_data.filename(5:8)),...
            str2double(temp.eis_data.filename(9:10)),...
            str2double(temp.eis_data.filename(11:12)));
        
        temppptrials = convertDataToPPtrialsTemp(temp.eis_data,dateCollect);
        processedPPTrials = newEyerisEISReadTemp(temppptrials);
        x1 = []; x2 = []; x3 = []; x4 = []; x5 = [];
        y1 = []; y2 = []; y3 = []; y4 = []; y5 = [];
        for i = 1:length(processedPPTrials)
            if processedPPTrials{i}.fixationTrial
                continue;
            end
            tempLocation = processedPPTrials{i};
            fithVals = floor(length(tempLocation.x)/5);
            x1 = [x1 tempLocation.x(1:fithVals)];
            x2 = [x2 tempLocation.x(fithVals+1:fithVals*2)];
            x3 = [x3 tempLocation.x(fithVals*2+1:fithVals*3)];
            x4 = [x4 tempLocation.x(fithVals*3+1:fithVals*4)];
            x5 = [x5 tempLocation.x(fithVals*4+1:fithVals*5)];
            y1 = [y1 tempLocation.y(1:fithVals)];
            y2 = [y2 tempLocation.y(fithVals+1:fithVals*2)];
            y3 = [y3 tempLocation.y(fithVals*2+1:fithVals*3)];
            y4 = [y4 tempLocation.y(fithVals*3+1:fithVals*4)];
            y5 = [y5 tempLocation.y(fithVals*4+1:fithVals*5)];
        end
        for nn = 1
            %     figure;
            %             subplot(3,2,1)
            resultT = generateHeatMapSimple( ...
                [x1 x2 x3 x4 x5], ...
                [y1 y2 y3 y4 y5], ...
                'Bins', 50,...
                'StimulusSize', 7,...
                'AxisValue', ylimsize,...
                'Uncrowded', 0,...
                'Borders', 1);
            axis off
            ylim([-ylimsize ylimsize])
            hold on
            rectangle('Position',[-2.5 -2.5 5 5]);
            title('Single Snellen E');
            
        end
        
    end
    
end
close all

%% one combined task heat map
figure;
for ii = 1:length(subjectsAll)
    x = ([inTimeData{ii}.Control{1}(:,1)'+20,...
        inTimeData{ii}.Control{2}(:,1)'+10, ...
        inTimeData{ii}.Control{3}(:,1)'+0, ...
        inTimeData{ii}.Control{4}(:,1)'-10, ...
        inTimeData{ii}.Control{5}(:,1)'-20]);
    
    y = ([inTimeData{ii}.Control{1}(:,2)',...
        inTimeData{ii}.Control{2}(:,2)', ...
        inTimeData{ii}.Control{3}(:,2)', ...
        inTimeData{ii}.Control{4}(:,2)', ...
        inTimeData{ii}.Control{5}(:,2)']);
    
    idxAll = find(x < 100 & y < 100);
    
    subplot(2,3,ii)
%     scatter(x(idxAll),y(idxAll),'o','MarkerFaceColor','b','MarkerEdgeColor','b',...
%     'MarkerFaceAlpha',.2,'MarkerEdgeAlpha',.2);
      generateHeatMapSimple( ...
           x(idxAll),y(idxAll), ...
            'Bins', 30,...
            'StimulusSize', 15,...
            'AxisValue', 15,...
            'Uncrowded', 0,...
            'Borders', 1);
%         axis([0 20 0 20])
        axis square
     rectangle('Position',[-2.5 -2.5 5 5]);   
     title(subjectsAll{ii})
     xlabel(' X')
     ylabel(' Y');
     
end
suptitle('Control');

%  figure;
for ii = 1:length(subjectsAll)
    x = ([inTimeData{ii}.Scotoma{1}(:,1)'+20,...
        inTimeData{ii}.Scotoma{2}(:,1)'+10, ...
        inTimeData{ii}.Scotoma{3}(:,1)'+0, ...
        inTimeData{ii}.Scotoma{4}(:,1)'-10, ...
        inTimeData{ii}.Scotoma{5}(:,1)'-20]);
    
    y = ([inTimeData{ii}.Scotoma{1}(:,2)',...
        inTimeData{ii}.Scotoma{2}(:,2)', ...
        inTimeData{ii}.Scotoma{3}(:,2)', ...
        inTimeData{ii}.Scotoma{4}(:,2)', ...
        inTimeData{ii}.Scotoma{5}(:,2)']);
    
    idxAll = find(x < 100 & y < 100);
    
    subplot(2,3,2)
%     scatter(x(idxAll),y(idxAll),'o','MarkerFaceColor','b','MarkerEdgeColor','b',...
%     'MarkerFaceAlpha',.2,'MarkerEdgeAlpha',.2);
      generateHeatMapSimple( ...
           x(idxAll),y(idxAll), ...
            'Bins', 20,...
            'StimulusSize', 10,...
            'AxisValue', 15,...
            'Uncrowded', 0,...
            'Borders', 1);
%         axis([0 20 0 20])
        axis square
     rectangle('Position',[-2.5 -2.5 5 5]);   
     title(subjectsAll{ii})
     xlabel(' X')
     ylabel(' Y');
     
end
suptitle('Scotoma');

%% make figure in time ABSOLUTE VALUE
figure;
for ii = 1:length(subjectsAll)
    x = abs([inTimeData{ii}.Control{1}(:,1)'+20,...
        inTimeData{ii}.Control{2}(:,1)'+10, ...
        inTimeData{ii}.Control{3}(:,1)'+0, ...
        inTimeData{ii}.Control{4}(:,1)'-10, ...
        inTimeData{ii}.Control{5}(:,1)'-20]);
    
    y = abs([inTimeData{ii}.Control{1}(:,2)',...
        inTimeData{ii}.Control{2}(:,2)', ...
        inTimeData{ii}.Control{3}(:,2)', ...
        inTimeData{ii}.Control{4}(:,2)', ...
        inTimeData{ii}.Control{5}(:,2)']);
    
    idxAll = find(x < 100 & y < 100);
    
    subplot(2,3,ii)
%     scatter(x(idxAll),y(idxAll),'o','MarkerFaceColor','b','MarkerEdgeColor','b',...
%     'MarkerFaceAlpha',.2,'MarkerEdgeAlpha',.2);
      generateHeatMapSimple( ...
           x(idxAll),y(idxAll), ...
            'Bins', 100,...
            'StimulusSize', 10,...
            'AxisValue', xlimsize,...
            'Uncrowded', 0,...
            'Borders', 1);
        axis([0 20 0 20])
        axis square
     rectangle('Position',[0 0 2.5 2.5]);   
     title(subjectsAll{ii})
     xlabel('Abs X')
     ylabel('Abs Y');
     
end
suptitle('Control');

 figure;
for ii = 1:length(subjectsAll)
    x = abs([inTimeData{ii}.Scotoma{1}(:,1)'+20,...
        inTimeData{ii}.Scotoma{2}(:,1)'+10, ...
        inTimeData{ii}.Scotoma{3}(:,1)'+0, ...
        inTimeData{ii}.Scotoma{4}(:,1)'-10, ...
        inTimeData{ii}.Scotoma{5}(:,1)'-20]);
    
    y = abs([inTimeData{ii}.Scotoma{1}(:,2)',...
        inTimeData{ii}.Scotoma{2}(:,2)', ...
        inTimeData{ii}.Scotoma{3}(:,2)', ...
        inTimeData{ii}.Scotoma{4}(:,2)', ...
        inTimeData{ii}.Scotoma{5}(:,2)']);
    
    idxAll = find(x < 100 & y < 100);
    
    subplot(2,3,ii)
%     scatter(x(idxAll),y(idxAll),'o','MarkerFaceColor','b','MarkerEdgeColor','b',...
%     'MarkerFaceAlpha',.2,'MarkerEdgeAlpha',.2);
      generateHeatMapSimple( ...
           x(idxAll),y(idxAll), ...
            'Bins', 100,...
            'StimulusSize', 10,...
            'AxisValue', xlimsize,...
            'Uncrowded', 0,...
            'Borders', 1);
        axis([0 20 0 20])
        axis square
     rectangle('Position',[0 0 2.5 2.5]);   
     title(subjectsAll{ii})
     xlabel('Abs X')
     ylabel('Abs Y');
     
end
suptitle('Scotoma');

%% figure in time across TRIALS (start-end of trial), but centered for optotypes
close all
for ii = 1:length(subjectsAll)
    xlimsize = 10;
%     if ii == 1
%         chunks = [150 300 550 700 950 1200];
% %     elseif ii == 3
% %         chunks = [150 300 550 700 950 1200];
%     elseif ii == 4
%         xlimsize = 20;
%         chunks = [0 150 300 550 700 950];
%     else
        chunks = [0 150 300 550 700 950];
%     end
    C = trialTasksCenteredStackedX{ii}.Control;
    [s,d] = cellfun(@size,C);
    allTrialsC{ii}.x = NaN(length(trialTasksCenteredStackedX{ii}.Control)*5,max(max(d)));
    allTrialsC{ii}.y = NaN(length(trialTasksCenteredStackedX{ii}.Control)*5,max(max(d)));
    counter = 1;
    for i = 1:5
        for c = 1:length(trialTasksCenteredStackedX{ii}.Control)
            if isempty(trialTasksCenteredStackedX{ii}.Control{c,i})
                continue
            else
                allTrialsC{ii}.x(counter,1:length(trialTasksCenteredStackedX{ii}.Control{c,i})) = ...
                    trialTasksCenteredStackedX{ii}.Control{c,i};
                allTrialsC{ii}.y(counter,1:length(trialTasksCenteredStackedY{ii}.Control{c,i})) = ...
                    trialTasksCenteredStackedY{ii}.Control{c,i};
                counter = counter + 1;
            end
        end
    end
    S = trialTasksCenteredStackedX{ii}.Scotoma;
    [s,d] = cellfun(@size,S);
    allTrialsS{ii}.x = NaN(length(trialTasksCenteredStackedX{ii}.Scotoma)*5,max(max(d)));
    allTrialsS{ii}.y = NaN(length(trialTasksCenteredStackedX{ii}.Scotoma)*5,max(max(d)));
    counter = 1;
    for i = 1:5
        for c = 1:length(trialTasksCenteredStackedX{ii}.Scotoma)
            if isempty(trialTasksCenteredStackedX{ii}.Scotoma{c,i})
                continue
            else
                allTrialsS{ii}.x(counter,1:length(trialTasksCenteredStackedX{ii}.Scotoma{c,i})) = ...
                    trialTasksCenteredStackedX{ii}.Scotoma{c,i};
                allTrialsS{ii}.y(counter,1:length(trialTasksCenteredStackedY{ii}.Scotoma{c,i})) = ...
                    trialTasksCenteredStackedY{ii}.Scotoma{c,i};
                counter = counter + 1;
            end
        end
    end
    figure('Position',[100 100 1600 800]);
%     chunks = [0 200 400 600 800 1000];
    for i = 1:5
        subplot(2,5,i)
        resultT = generateHeatMapSimple( ...
            reshape(allTrialsC{ii}.x(:,chunks(i)+1:chunks(i+1)),1,[]), ...
            reshape(allTrialsC{ii}.y(:,chunks(i)+1:chunks(i+1)),1,[]), ...
            'Bins', 10,...
            'StimulusSize', 7,...
            'AxisValue', xlimsize,...
            'Uncrowded', 0,...
            'Borders', 1);
        rectangle('Position',[-2.5 -2.5 5 5])
        axis square
        axis([-10 10 -10 10])
        title(sprintf('Time %i to %ims',chunks(i)+1,chunks(i+1)));
        %         for ii = 1:length(subjectsAll)
        temp = [];
        temp = analysisTimeContour(reshape(allTrialsC{ii}.x(:,chunks(i)+1:chunks(i+1)),1,[]),...
            reshape(allTrialsC{ii}.y(:,chunks(i)+1:chunks(i+1)),1,[]), NaN, 20, 0);
        %         allHyps = %hypot(reshape(allTrialsC{ii}.x(:,chunks(i)+1:chunks(i+1)),1,[]),...
        %             reshape(allTrialsC{ii}.y(:,chunks(i)+1:chunks(i+1)),1,[]));
%         eDistCont(ii,i) = hypot(temp.contourPeak(1), temp.contourPeak(2));%mode(allHyps(allHyps<50));
        eDistCont(ii,i) = hypot(temp.wCentroid(1), temp.wCentroid(2));
        if ii == 2 && i == 3
            temp = [];
            temp = hypot(reshape(allTrialsC{ii}.x(:,chunks(i)+1:chunks(i+1)),1,[]),...
                    reshape(allTrialsC{ii}.y(:,chunks(i)+1:chunks(i+1)),1,[]));
            eDistCont(ii,i) = NaN;%mean(temp(find(temp < 50)));
        end
    end
    ylabel('Control');
    
    % figure;
%     chunks = [0 200 400 600 800 1000];
    for i = 1:5
        subplot(2,5,i+5)
        resultT = generateHeatMapSimple( ...
            reshape(allTrialsS{ii}.x(:,chunks(i)+1:chunks(i+1)),1,[]), ...
            reshape(allTrialsS{ii}.y(:,chunks(i)+1:chunks(i+1)),1,[]), ...
            'Bins', 10,...
            'StimulusSize', 7,...
            'AxisValue', xlimsize,...
            'Uncrowded', 0,...
            'Borders', 1);
        rectangle('Position',[-2.5 -2.5 5 5])
        axis square
        axis([-10 10 -10 10])
        title(sprintf('Time %i to %ims',chunks(i)+1,chunks(i+1)));
        temp = [];
        temp = analysisTimeContour(reshape(allTrialsS{ii}.x(:,chunks(i)+1:chunks(i+1)),1,[]),...
            reshape(allTrialsS{ii}.y(:,chunks(i)+1:chunks(i+1)),1,[]), NaN, 20, 0);
%         allHyps = hypot(reshape(allTrialsS{ii}.x(:,chunks(i)+1:chunks(i+1)),1,[]),...
%             reshape(allTrialsS{ii}.y(:,chunks(i)+1:chunks(i+1)),1,[]));
%         eDistScot(ii,i) = hypot(temp.contourPeak(1), temp.contourPeak(2));
        eDistScot(ii,i) = hypot(temp.wCentroid(1), temp.wCentroid(2));
    end
    ylabel('Scotoma');
    suptitle(subjectsAll{ii})
    saveas(gcf,sprintf('../../ChangeInPLFwithTimeInTrial%s.png',subjectsAll{ii}));

end

for ii = 1:length(subjectsAll)
    largestOffset(ii,2) = max(eDistScot(ii,:));
%     idxTemp(ii) = find(eDistScot(ii,:) == max(eDistScot(ii,:)));
%     largestOffset(ii,1) = max(eDistCont(ii,idxTemp(ii)));
    largestOffset(ii,1) = max(eDistCont(ii,:));

end

eDistScot
eDistCont
[h,p] = ttest(reshape(eDistScot,1,[]),reshape(eDistCont,1,[]))
[h,p] = ttest(eDistCont(:,1),largestOffset(:,1))
[h,p] = ttest(eDistScot(:,1),largestOffset(:,2))

% 
figure
[p,tbl,stats] = anova1(eDistCont);
results = multcompare(stats);


figure
[p,tbl,stats] = anova1(eDistScot);
results = multcompare(stats);


figure;
for ii = 1:length(subjectsAll)
    subplot(2,3,[1 2])
    plot([1:5],(eDistCont(ii,:)),'-o','Color',clrs(ii,:));
    hold on
    subplot(2,3,[4 5])
    plot([1:5],(eDistScot(ii,:)),'-o','Color',clrs(ii,:));
    hold on
end

subplot(2,3,[1 2])
errorbar([1:5],nanmean(eDistCont),nanstd(eDistCont),'-o','Color','k','MarkerFaceColor','k');
ylabel('Control')
xlabel('Time')
xticks([1:5])
xticklabels(chunks(2:end))

subplot(2,3,[4 5])
errorbar([1:5],mean(eDistScot),std(eDistScot),'-o','Color','k','MarkerFaceColor','k');
xlabel('Time')
xticks([1:5])
ylabel('Scotoma')
xticklabels(chunks(2:end))

subplot(2,3,3)
errorbar([1 2],[mean(eDistCont(:,1)) mean(eDistCont(:,5))],...
    [sem(eDistCont(:,1)) sem(eDistCont(:,5))],'-o','Color','k');
axis([.5 2.5 0 10])
xticks([1:2])
xticklabels({'Start','Mid-End'})
ylabel('Euclidean Distance (arcmin)')
subplot(2,3,6)
errorbar([1 2],[mean(eDistScot(:,1)) mean(eDistScot(:,5))],...
    [sem(eDistScot(:,1)) sem(eDistScot(:,5))],'-o','Color','k');
axis([.5 2.5 0 10])
xticks([1:2])
xticklabels({'Start','Mid-End'})
ylabel('Euclidean Distance (arcmin)')

%% task trial in time from beginning of session
% figure;
for ii = 1:length(subjectsAll)
    figure('Position', [10 10 1800 900]);
    counter = 1;

    idxs = [1:floor(length(trialTasksCentered{ii}.Control)/5):...
        length(trialTasksCentered{ii}.Control)];
    for j = 1:length(idxs)-1
        xtimeBin = []; ytimeBin = [];
        for i = idxs(j):idxs(j+1)
            if isempty(trialTasksCentered{ii}.Control{i})
                continue
            end
            xtimeBin = [xtimeBin trialTasksCentered{ii}.Control{i}(1,:)];
            ytimeBin = [ytimeBin trialTasksCentered{ii}.Control{i}(2,:)];
        end
        %     xTimeBin = trialTasksCentered{ii}.Control{idxs(i):idxs(i+1)};
        subplot(3,length(idxs)-1,j)
        resultT = generateHeatMapSimple( ...
            xtimeBin, ...
            ytimeBin, ...
            'Bins', 100,...
            'StimulusSize', 7,...
            'AxisValue', xlimsize,...
            'Uncrowded', 4,...
            'Borders', 1);
        axis square
        counter = counter + 1;
        %     end
        %     trialTasksCentered{ii}.Control
        title(sprintf('Trials %i - %i', idxs(j), idxs(j+1)))
        if j == 1
            ylabel(subjectsAll{ii})
            xlabel('Control')
        end
    end
%     suptitle('Control, Task')
% end

% % for ii = 1:length(subjectsAll)
%     figure('Position', [10 10 1800 600]);
    idxs = [1:floor(length(trialTasksCentered{ii}.Scotoma)/5):...
        length(trialTasksCentered{ii}.Scotoma)];
    for j = 1:length(idxs)-1
        xtimeBin = []; ytimeBin = [];
        for i = idxs(j):idxs(j+1)
            if isempty(trialTasksCentered{ii}.Scotoma{i})
                continue
            end
            xtimeBin = [xtimeBin trialTasksCentered{ii}.Scotoma{i}(1,:)];
            ytimeBin = [ytimeBin trialTasksCentered{ii}.Scotoma{i}(2,:)];
        end
        %     xTimeBin = trialTasksCentered{ii}.Control{idxs(i):idxs(i+1)};
        subplot(3,length(idxs)-1,counter)
        resultT = generateHeatMapSimple( ...
            xtimeBin, ...
            ytimeBin, ...
            'Bins', 100,...
            'StimulusSize', 7,...
            'AxisValue', xlimsize,...
            'Uncrowded', 4,...
            'Borders', 1);
        counter = counter + 1;
        axis square
        %     end
        %     trialTasksCentered{ii}.Control
        title(sprintf('Trials %i - %i', idxs(j), idxs(j+1)))
        if j == 1
            ylabel(subjectsAll{ii})
            xlabel('Scotoma')
        end
    end
    suptitle('Task')
    saveas(gcf,sprintf('../../ChangeInPLF%s.png',subjectsAll{ii}));
    

end

%% task offset analysis

addEcc = [20 10 0 -10 -20];
for ii = 1:length(subjectsAll)
    for i = 1:5
        temp = analysisTimeContour(inTimeData{ii}.Control{i}(:,1)',...
            inTimeData{ii}.Control{i}(:,2)', NaN, 20);
        distFromOpto.Control(ii,i) = hypot(temp.contourPeak(1)+addEcc(i),temp.contourPeak(2));
        xyCoordCont(ii,1) = temp.contourPeak(1)+addEcc(i);
        xyCoordCont(ii,2) = temp.contourPeak(2);
        
    end
    for i = 1:5
        temp = analysisTimeContour(inTimeData{ii}.Scotoma{i}(:,1)',...
            inTimeData{ii}.Scotoma{i}(:,2)', NaN, 20);
        distFromOpto.Scotoma(ii,i) = hypot(temp.contourPeak(1)+addEcc(i),temp.contourPeak(2));
        xyCoordScot(ii,1) = temp.contourPeak(1)+addEcc(i);
        xyCoordScot(ii,2) = temp.contourPeak(2);
    end
end
 close all
 
 counter1 = [1:5];
 counter2 = [6:10];
 xlimsize = 15;
 figure;
 for ii = 3%1:length(subjectsAll)
    subplot(2, length(subjectsAll),counter1(ii))
    if ii == 1
        ylabel('Control');
    end
      generateHeatMapSimple( ...
            [inTimeData{ii}.Control{1}(:,1)'+20,...
             inTimeData{ii}.Control{2}(:,1)'+10, ...
             inTimeData{ii}.Control{3}(:,1)'+0, ...
             inTimeData{ii}.Control{4}(:,1)'-10, ...
             inTimeData{ii}.Control{5}(:,1)'-20], ...
             [inTimeData{ii}.Control{1}(:,2)',...
             inTimeData{ii}.Control{2}(:,2)', ...
             inTimeData{ii}.Control{3}(:,2)', ...
             inTimeData{ii}.Control{4}(:,2)', ...
             inTimeData{ii}.Control{5}(:,2)'], ...
            'Bins', 100,...
            'StimulusSize', 10,...
            'AxisValue', xlimsize,...
            'Uncrowded', 0,...
            'Borders', 1);
        rectangle('Position',[-2.5 -2.5 5 5 ]);
        axis square
        title(subjectsAll{ii});
        subplot(2,length(subjectsAll),counter2(ii))
        if ii == 1
            ylabel('Scotoma');
        end
        generateHeatMapSimple( ...
            [inTimeData{ii}.Scotoma{1}(:,1)'+20,...
            inTimeData{ii}.Scotoma{2}(:,1)'+10, ...
            inTimeData{ii}.Scotoma{3}(:,1)'+0, ...
            inTimeData{ii}.Scotoma{4}(:,1)'-10, ...
             inTimeData{ii}.Scotoma{5}(:,1)'-20], ...
             [inTimeData{ii}.Scotoma{1}(:,2)',...
             inTimeData{ii}.Scotoma{2}(:,2)', ...
             inTimeData{ii}.Scotoma{3}(:,2)', ...
             inTimeData{ii}.Scotoma{4}(:,2)', ...
             inTimeData{ii}.Scotoma{5}(:,2)'], ...
            'Bins', 100,...
            'StimulusSize', 5,...
            'AxisValue', xlimsize,...
            'Uncrowded', 0,...
            'Borders', 1);
        rectangle('Position',[-2.5 -2.5 5 5 ]);
        axis square
%        suptitle(subjectsAll{ii})
 end
 
 figure;
 subplot(2,1,1)
 for ii = 1:length(subjectsAll)
     plot([1 2 3 4 5],distFromOpto.Control(ii,:),'-o')
     hold on
 end
 title('Control')
 ylim([0 15])
 ylabel('Eucliden Distance (arcmin)');
 xlabel('Optoptype');
 xticks([1 2 3 4 5]);
 subplot(2,1,2)
 for ii = 1:length(subjectsAll)
     plot([1 2 3 4 5],distFromOpto.Scotoma(ii,:),'-o')
     hold on
 end
 title('Scotoma')
  ylim([0 15])
  ylabel('Eucliden Distance (arcmin)');
 xlabel('Optoptype');
 xticks([1 2 3 4 5]);
 
 
 
 figure;
 clrs = parula(5);
 for i = 1:length(subjectsAll)
     plot([1 2],[mean(distFromOpto.Control(i,:)) mean(distFromOpto.Scotoma(i,:))],'-o',...
         'Color',clrs(i,:),...
         'MarkerFaceColor',clrs(i,:),'MarkerSize',8);
     hold on
 end
 xticks([1 2])
 xticklabels({'Control','Scotoma'});
 ylabel('Euclidean Distance (arcmin)');
 xlim([0 3])
 errorbar([1.1 2.1],[mean(mean(distFromOpto.Control)) mean(mean(distFromOpto.Scotoma))],...
    [sem(mean(distFromOpto.Control)) sem(mean(distFromOpto.Scotoma))],'-o','Color','k',...
     'MarkerFaceColor','k','MarkerSize',10)
 [h,p1]=ttest(mean(distFromOpto.Control),mean(distFromOpto.Scotoma));
title(sprintf('p = %.3f',p1));
 
 
 
 
 
 figure;
 subplot(1,2,1)
 for ii = 1:length(subjectsAll)
     forLeg(ii) = plot([1 2],[mean(distFromOpto.Control(ii,:)) mean(distFromOpto.Scotoma(ii,:))],'-o');
     hold on
 end
 xlim([0 3])
 xticks([1 2])
 xticklabels({'Control','Scotoma'});
 ylabel('Euc Distance from Optotype');
errorbar([1.1 2.1],[mean(mean(distFromOpto.Control)) mean(mean(distFromOpto.Scotoma))],...
    [sem(mean(distFromOpto.Control)) sem(mean(distFromOpto.Scotoma))],'-o','Color','k',...
    'MarkerFaceColor','k');
legend(forLeg,subjectsAll);
[h,p]=ttest(mean(distFromOpto.Control),mean(distFromOpto.Scotoma));
title(sprintf('p = %.3f',p))

subplot(1,2,2)
 symbs = {'o' 's' 'd' 'p' '^' 'v' '<' '>'};

for i = 1:length(subjectsAll)
    plot(xyCoordScot(i,1)-xyCoordCont(i,1),xyCoordScot(i,2)-xyCoordCont(i,2), 'Marker', symbs{i});
    hold on
end
 axis ([-10 10 -10 10])
 line([0 0], [-5 5]);
 line([-5 5],[0 0]);
 title('Recentered Offsets')
 forleg(1) = plot(0,0,'k','Marker','*');

 %% MS during the task
 
 for ii = 1:length(subjectsAll)
     figure;
     subplot(2,1,1)
     plotRectangles2020Line(processedPPTrialsS{1},0);
     scatter1 = scatter(out{ii}{1}.outLandLocsX,out{ii}{1}.outLandLocsY,...
         'MarkerFaceColor','r',...
         'MarkerFaceColor','r');
     scatter1.MarkerFaceAlpha = .5;
    scatter1.MarkerEdgeAlpha = .5;
     axis([-30 30 -10 10])
     ylabel('Control');
     

     subplot(2,1,2)
     plotRectangles2020Line(processedPPTrialsS{1},0);
      scatter2 = scatter(out{ii}{2}.outLandLocsX,out{ii}{2}.outLandLocsY,...
         'MarkerFaceColor','r',...
         'MarkerFaceColor','r');
     scatter2.MarkerFaceAlpha = .5;
    scatter2.MarkerEdgeAlpha = .5;
     axis([-30 30 -10 10])
     ylabel('Scotoma');
     
     suptitle(subjectsAll{ii})
 end
 
 for ii = 1:length(subjectsAll)
     msIdxCont =  (hypot(recenteredX{ii}{1}.outLandLocsX, ...
         out{ii}{1}.outLandLocsY) < 30);
     figure;
     subplot(1,2,1)
     %      plotRectangles2020Line(processedPPTrialsS{1},0);
     centerXRect = [-20 -10 0 10 20];
     closestXLocation = (out{ii}{1}.outLandLocsX...
         - centerXRect');
     conter = 1;
     for i = msIdxCont%1:length(closestXLocation)
         idxT = find(abs(closestXLocation(:,i)) == min(abs(closestXLocation(:,i))));
         recenteredX{ii}{1}.outLandLocsX(i) = closestXLocation(idxT,i);
         counter = counter + 1;
     end
     
     scatter(recenteredX{ii}{1}.outLandLocsX(msIdxCont), ...
         out{ii}{1}.outLandLocsY(msIdxCont),'o','b');
     hypoCont = hypot(recenteredX{ii}{1}.outLandLocsX(msIdxCont), ...
         out{ii}{1}.outLandLocsY(msIdxCont));
     title(sprintf('Average Dist Landing %.2f',mode(hypoCont)));
     modeMSLand(1,ii) = mode(hypoCont);
     ylabel('Control');
     rectangle('Position',[-2.5 -2.5 5 5])
     axis([-20 20 -20 20])
     axis square
    
     subplot(1,2,2)
     msIdxScot = (hypot(recenteredX{ii}{2}.outLandLocsX, ...
         out{ii}{2}.outLandLocsY) < 30);
     closestXLocation = (out{ii}{2}.outLandLocsX(msIdxScot)...
         - centerXRect');
     counter = 1;
     for i = msIdxScot%1:length(closestXLocation)
         idxT = find(abs(closestXLocation(:,i)) == min(abs(closestXLocation(:,i))));
         recenteredX{ii}{2}.outLandLocsX(i) = closestXLocation(idxT,i);
         counter = counter + 1;
     end
     
     scatter(recenteredX{ii}{2}.outLandLocsX(msIdxScot), ...
         out{ii}{2}.outLandLocsY(msIdxScot),'o','r')
     hypoScot = hypot(recenteredX{ii}{2}.outLandLocsX(msIdxScot), ...
         out{ii}{2}.outLandLocsY(msIdxScot));
     title(sprintf('Average Dist Landing %.2f',mode(hypoScot)));
     modeMSLand(2,ii) = mode(hypoScot);
     ylabel('Scotoma');
     rectangle('Position',[-2.5 -2.5 5 5])
     suptitle(subjectsAll{ii})
     axis([-20 20 -20 20])
     axis square
 end
 
 figure;
 for ii = 1:length(subjectsAll)
     plot([1 2],[(modeMSLand(1,ii) ) (modeMSLand(2,ii) )],'-o','Color',clrs(ii,:));
     hold on
 end
 
%% Peformance Analysis
for ii = 1:length(subjectsAll)
    RTControl(ii) = mean(RT{ii}{1});
    RTScotoma(ii) = mean(RT{ii}{2});
    
    PerfControl(ii) = mean(Perf{ii}{1});
    PerfScotoma(ii) = mean(Perf{ii}{2});
    
    ERTControl(ii) = sem(RT{ii}{1});
    ERTScotoma(ii) = sem(RT{ii}{2});
    
    EPerfControl(ii) = sem(Perf{ii}{1});
    EPerfScotoma(ii) = sem(Perf{ii}{2});
    
end

figure;
subplot(2,1,1)
forleg(1) = errorbar([1:3,5],RTControl([1:3,5]),ERTControl([1:3,5]),'o');
hold on
forleg(2) = errorbar([1:3,5],RTScotoma,ERTScotoma,'o');
% ylim([0 2000])
xticks([1:5]);
xticklabels(subjectsAll)
xlim([.5 5.5])
legend(forleg,{'Control','Scotoma'})
ylabel('Reaction Time');
subplot(2,1,2);
errorbar([1:5],PerfControl*100,EPerfControl*100,'o');
hold on
errorbar([1:5],PerfScotoma*100,EPerfScotoma*100,'o');
xticks([1:5])
xticklabels(subjectsAll)
xlim([.5 5.5])
ylabel('Performance');

figure;
for ii = [1:3,5]
    forleg(1) = errorbar([1 2],[PerfControl(ii) PerfScotoma(ii)],...
        [EPerfControl(ii) EPerfScotoma(ii)],'-o');
hold on
end
ylim([0 1])
xlim([0.5 2.5])
errorbar([1.1 2.1],[mean(PerfControl) mean(PerfScotoma)],...
        [mean(EPerfControl) mean(EPerfScotoma)],'-o','Color','k',...
        'MarkerFaceColor','k');
xticks([1 2])
xticklabels({'Control','Scotoma'})
ylabel('Proportion Correct');


[h,p]=ttest2(RTControl,RTScotoma);
[h,p]=ttest(PerfControl([1:3,5]),PerfScotoma([1:3,5]));

RT{ii}{c}(pp)
%     figu
%         nn=39;
nn = 1;
figure;
plotRectangles2020Line(processedPPTrialsC{nn},0);
x =  processedPPTrialsC{nn}.x;
y =  processedPPTrialsC{nn}.y;
idxT = find(x < 40 & x > -40 &...
    y < 40 & y > -40);
z = zeros(size(x));
clrx = linspace(1,10,length(x(idxT)));
hold on
scatter1 = scatter(x(idxT),y(idxT),[],clrx);%...
scatter1.MarkerFaceAlpha = .3;
scatter1.MarkerEdgeAlpha = .3;
axis([-40 40 -40 40])
axis square
colormap(jet)
colorbar
title(condition{1})
nn = nn + 1;
%%%%%%%
nn = 1;
figure;
plotRectangles2020Line(processedPPTrialsS{nn});
x =  processedPPTrialsS{nn}.x;
y =  processedPPTrialsS{nn}.y;
idxT = find(x < 40 & x > -40 &...
    y < 40 & y > -40);
z = zeros(size(x));
clrx = linspace(1,10,length(x(idxT)));
hold on
scatter1 = scatter(x(idxT),y(idxT),[],clrx);%...
scatter1.MarkerFaceAlpha = .3;
scatter1.MarkerEdgeAlpha = .3;
axis([-40 40 -40 40])
axis square
colormap(jet)
colorbar
title(condition{2})
nn = nn + 1;



    %     [numTrials,~] = size(timeTraces{ii}.Scotoma.xAllTime);
%     timeTracesScotoma_hist = [];
%     bins_Time = 10;
%     steps = 1:400:2000;
%     for t = 1:length(steps) - 1
%         x = reshape(timeTraces{ii}.Scotoma.xAllTime(:,steps(t):steps(t+1)),1,[]);
%         y = reshape(timeTraces{ii}.Scotoma.yAllTime(:,steps(t):steps(t+1)),1,[]);
%         result = MyHistogram2(x,y, ...
%             [-40,40,bins_Time;-40,40,bins_Time]);
%         timeTracesScotoma_hist(:,:,t) = result';
%     end
%     fig = uifigure();
%     uip = uipanel(fig,'Position', [20 100 500 300]);
%     heatObj = heatmap(uip, timeTracesScotoma_hist(:,:,1));
%     title(heatObj, 'Frame #1, Scotoma');
%     n = length(steps);
%     uis = uislider(fig,'Position',[50 50 450 3],...
%         'Value',1, ...
%         'Limits', [1,n], ...
%         'MajorTicks', [1, length(steps):length(steps):n], ...
%         'MinorTicks', []);
%     uis.ValueChangingFcn = {@sliderChangingFcn, timeTracesScotoma_hist, heatObj};

% end


%%
% temp1 = load('X:\Ashley\VisualAcuity\Z184\cont_ses1.mat');
% temp2 = load('X:\Ashley\VisualAcuity\Z184\scot_ses1.mat');
% 
% 
% 
% temp1 = load('X:\Ashley\VisualAcuity\Z091\cont_ses2.mat');
% temp2 = load('X:\Ashley\VisualAcuity\Z091\scot_ses2.mat');
% 
% 
% 
% temp1 = load('X:\Ashley\VisualAcuity\A144\cont_ses1.mat');
% temp2 = load('X:\Ashley\VisualAcuity\A144\scot_ses1.mat');
% temp2 = load('X:\Ashley\VisualAcuity\A144\scot_ses2.mat');
% 
% 
% % temp1 = load('X:\Ashley\FacesScotoma\Snellen\Z171\control_ses1.mat')
% temp1 = load('X:\Ashley\VisualAcuity\Z171\control_ses3.mat')
% % temp3 = load('X:\Ashley\FacesScotoma\Snellen\Z171\scot_ses1.mat')
% temp2 = load('X:\Ashley\VisualAcuity\Z171\scot_ses3.mat')
% 
% % temp1 = load('X:\Ashley\VisualAcuity\Z055\control_ses1.mat')
% % temp2 = load('X:\Ashley\VisualAcuity\Z055\control_ses2.mat')
% % temp3 = load('X:\Ashley\VisualAcuity\Z055\scot_ses1.mat')
% % temp4 = load('X:\Ashley\VisualAcuity\Z055\scot_ses2.mat')
% 
% % temp1 = load('X:\Ashley\VisualAcuity\Z055\test1.mat')
% 
% % figure;
% 
% dataSet = temp1;
% % plot(temp2.eis_data.eye_data.eye_1.calibrated_x,...
% %     temp2.eis_data.eye_data.eye_1.calibrated_y,...
% %     '.');
% % axis([-40 40 -40 40])
% % rectangle('Position',[-2.5,-2.5,5,5]);
% % rectangle('Position',[-2.5+10,-2.5,5,5]);
% % rectangle('Position',[-2.5+20,-2.5,5,5]);
% % rectangle('Position',[-2.5-10,-2.5,5,5]);
% % rectangle('Position',[-2.5-20,-2.5,5,5]);
% 
% dateCollect = datetime(str2double(dataSet.eis_data.filename(5:8)),...
%     str2double(dataSet.eis_data.filename(9:10)),...
%     str2double(dataSet.eis_data.filename(11:12)));
% 
% temppptrials = convertDataToPPtrialsSnellen(dataSet.eis_data,dateCollect);
% processedPPTrialsS = newEyerisEISRead(temppptrials);
% 
% %
% % ii = 1;
% % idx = find(processedPPTrialsS{ii}.x < 120 & processedPPTrialsS{ii}.x > -120&...
% %     processedPPTrialsS{ii}.y < 120 & processedPPTrialsS{ii}.y > -120);
% % figure;
% % plot(processedPPTrialsS{ii}.x(idx),processedPPTrialsS{ii}.y(idx),'-o')
% % axis([-40 40 -40 40])
% % rectangle('Position',[-2.5,-2.5,5,5]);
% % rectangle('Position',[-2.5+10,-2.5,5,5]);
% % rectangle('Position',[-2.5+20,-2.5,5,5]);
% % rectangle('Position',[-2.5-10,-2.5,5,5]);
% % rectangle('Position',[-2.5-20,-2.5,5,5]);
% % ii = ii + 1;
% %
% counter = 1;
% xAll = [];
% yAll = [];
% for pp = 1:length(processedPPTrialsS)
%     if counter == 3
%         counter = 1;
%         continue
%     else
%         counter = counter + 1;
%         %         for i = 1:length(processedPPTrialsS{pp}.drifts.start)
%         %             xAll = [xAll processedPPTrialsS{pp}.x(...
%         %                 processedPPTrialsS{pp}.drifts.start(i):...
%         %                 processedPPTrialsS{pp}.drifts.start(i) + ...
%         %                 processedPPTrialsS{pp}.drifts.duration(i)-1)];
%         %             yAll = [yAll processedPPTrialsS{pp}.y(...
%         %                 processedPPTrialsS{pp}.drifts.start(i):...
%         %                 processedPPTrialsS{pp}.drifts.start(i) + ...
%         %                 processedPPTrialsS{pp}.drifts.duration(i)-1)];
%         %         end
%         
%         xAll = [xAll processedPPTrialsS{pp}.x];
%         yAll = [yAll processedPPTrialsS{pp}.y];
%     end
%     
% end
% 
% figure;
% result = generateHeatMapSimple( ...
%     xAll, ...
%     yAll, ...
%     'Bins', 200,...
%     'StimulusSize', 10,...
%     'AxisValue', 40,...
%     'Uncrowded', 0,...
%     'Borders', 1);
% 
% plot(xAll,yAll,'.')
% axis([-40 40 -40 40])
% rectangle('Position',[-2.5,-2.5,5,5]);
% rectangle('Position',[-2.5+10,-2.5,5,5]);
% rectangle('Position',[-2.5+20,-2.5,5,5]);
% rectangle('Position',[-2.5-10,-2.5,5,5]);
% rectangle('Position',[-2.5-20,-2.5,5,5]);
% 
% title('Control');
% 
% 
% dataSet = temp2;
% % rectangle('Position',[-2.5-20,-2.5,5,5]);
% 
% dateCollect = datetime(str2double(dataSet.eis_data.filename(5:8)),...
%     str2double(dataSet.eis_data.filename(9:10)),...
%     str2double(dataSet.eis_data.filename(11:12)));
% 
% temppptrials = convertDataToPPtrialsSnellen(dataSet.eis_data,dateCollect);
% processedPPTrialsS = newEyerisEISRead(temppptrials);
% 
% counter = 1;
% xAll = [];
% yAll = [];
% for pp = 1:length(processedPPTrialsS)
%     if counter == 3
%         counter = 1;
%         continue
%     else
%         counter = counter + 1;
%         %
%         
%         xAll = [xAll processedPPTrialsS{pp}.x];
%         yAll = [yAll processedPPTrialsS{pp}.y];
%     end
%     
% end
% 
% figure;
% result = generateHeatMapSimple( ...
%     xAll, ...
%     yAll, ...
%     'Bins', 200,...
%     'StimulusSize', 10,...
%     'AxisValue', 40,...
%     'Uncrowded', 0,...
%     'Borders', 1);
% 
% % plot(xAll,yAll,'.')
% axis([-40 40 -40 40])
% rectangle('Position',[-2.5,-2.5,5,5]);
% rectangle('Position',[-2.5+10,-2.5,5,5]);
% rectangle('Position',[-2.5+20,-2.5,5,5]);
% rectangle('Position',[-2.5-10,-2.5,5,5]);
% rectangle('Position',[-2.5-20,-2.5,5,5]);
% 
% title('Scotoma');
% 
