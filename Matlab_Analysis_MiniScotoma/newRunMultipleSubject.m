clear
clc
close all

%% Subject Notes
%   AMC = New eyeris, Ashley = old eyeris
%   Z091 = Zac
%  'Sanjana' = could not present scotoma accurately because looking
%  binocularly
%% First look at scotoma properties
figure;
% A = imshow('scotomaBlur.png');
   
Image = imread('scotomaBlur.png');
A = (rgb2gray(Image));
figure;plot(x,count,'-')

xi = [1 size(A,2) size(A,2) 1]; 
yi = [ceil(size(A,1)/2), ceil(size(A,1)/2), ceil(size(A,1)/2 ),ceil(size(A,1)/2)] %//These x and y coordinates is the red line which you drew with a width of 1 pixel
improfile(((A)),xi,yi),grid on;
view(0,360)
zlabel('Gray Intensity (RGB)')
xlabel('X (pixels)')
rgbColor = impixel(I.CData{3}, 158/2, 158/2);

% 103.335-51.666 = 51.669; %values above 120
% 158*5 = 790;
% 790/51.669 = 15.2896;
%% Params
figures = struct(...
    'FIGURE_ON', 0,... %Show individual trial with traces in 2D space
    'VIDEO', 0,... %Wills save video of FIGURE_ON figures
    'FIXATION_ANALYSIS',0,... %Plots psychometric curves
    'QUICK_ANALYSIS',0,... %Only plots psychometric curves
    'CHECK_TRACES',0,... %will plot traces
    'COMPARE_SPANS',0); %0 = all spans, 1 = small spans, 2 = large spans

machine = {'D'}; %(D = DDPI, A = DPI),
%
subjectsAll = {'Z162', 'Z124', 'A144', 'A188','Z091','Z055','Z151'};
oldEyeris = [0];
conditionsAll = {'Control','Scotoma'}; %'Control',
imageSize = [72/2];
session = {'ALL'}; %%ALL
checkTraces = 0;
makemovie = 1;

for ii = 1:length(subjectsAll)
    axisVal = imageSize(1);
    %% Load in Data
    subjFigPath = strcat('../../../Documents/Overleaf_MiniScotoma/figures');
    %     if oldEyeris(ii)
    %         [dataAll] = loadInData(ii, conditionsAll, session, figures, subjectsAll{ii});
    %     else
    subjDataPath = strcat(sprintf('X:/Ashley/FacesScotoma/%s', subjectsAll{ii}));
    allfiles = dir(subjDataPath);
    data{ii}.Scotoma = [];
    data{ii}.Control = [];
    for ss = find(~[allfiles.isdir])
        clear temp
        if startsWith(string(allfiles(ss).name),["scot"]) %scotoma condition
            if exist(sprintf('ScotomaCleaned%s.mat',subjectsAll{ii}),'file') == 2
                temp = load(sprintf('ScotomaCleaned%s.mat',subjectsAll{ii}));
                data{ii}.Scotoma = temp.pptrials;
            else
            temp = load(sprintf('%s/%s',subjDataPath,allfiles(ss).name));
            dateCollect = datetime(str2double(temp.eis_data.filename(5:8)),...
                str2double(temp.eis_data.filename(9:10)),...
                str2double(temp.eis_data.filename(11:12)));
            if temp.eis_data.user_data.variables.trial2Data.m_scotomaPresent
                temppptrials = convertDataToPPtrials(temp.eis_data,dateCollect);
                processedPPTrialsS = newEyerisEISRead(temppptrials);
                data{ii}.Scotoma = [data{ii}.Scotoma processedPPTrialsS];
            else
                error('Mislabelled File');
            end
            end
        elseif startsWith(string(allfiles(ss).name),["cont"])
            if exist(sprintf('ControlCleaned%s.mat',subjectsAll{ii}),'file') == 2
                temp = load(sprintf('ControlCleaned%s.mat',subjectsAll{ii}));
                data{ii}.Control = temp.pptrials;
            else
            temp = load(sprintf('%s/%s',subjDataPath,allfiles(ss).name));
            dateCollect = datetime(str2double(temp.eis_data.filename(5:8)),...
                str2double(temp.eis_data.filename(9:10)),...
                str2double(temp.eis_data.filename(11:12)));
            
            temppptrials = convertDataToPPtrials(temp.eis_data,dateCollect);
            processedPPTrialsC = newEyerisEISRead(temppptrials);
            data{ii}.Control = [data{ii}.Control processedPPTrialsC];
            end
        else
            error('Change name of File');
        end
    end
    if checkTraces
        if makemovie
%             for z = 1:length(data{ii}.Control)
%                 figure;
%                 if data{ii}.Control{z}.FixationTrial
% %                     title('Fixation Control')
%                     videoPlotting(data{ii}.Control{z}.x,data{ii}.Control{z}.y,z,'Fixation Control');
%                 else
%                     title('Task Control')
%                     videoPlotting(data{ii}.Control{z}.x,data{ii}.Control{z}.y,z,'Task Control');
%                 end
%             end
            for z = 1:length(data{ii}.Scotoma)
                figure;
                if data{ii}.Scotoma{z}.FixationTrial
                    title('Fixation Scotoma')
                    videoPlotting(data{ii}.Scotoma{z}.x,data{ii}.Control{z}.y,z,'Fixation Scotoma');
                else
                    title('Task Scotoma')
                    videoPlotting(data{ii}.Scotoma{z}.x,data{ii}.Control{z}.y,z,'Task Scotoma');
                end
            end
        else
        data{ii}.Control = checkMSTraces_new([], data{ii}.Control, cd, sprintf(...
            'ControlCleaned%s.mat',subjectsAll{ii}));
        data{ii}.Scotoma = checkMSTraces_new([], data{ii}.Scotoma, cd, sprintf(...
            'ScotomaCleaned%s.mat',subjectsAll{ii}));
        end
    end
    dataAll{ii}.Scotoma.allXClean = [];
    dataAll{ii}.Scotoma.allYClean = [];
    dataAll{ii}.Control.allXClean = [];
    dataAll{ii}.Control.allYClean = [];
    dataAll{ii}.Scotoma.allXCleanFIX = [];
    dataAll{ii}.Scotoma.allYCleanFIX = [];
    dataAll{ii}.Control.allXCleanFIX = [];
    dataAll{ii}.Control.allYCleanFIX = [];
    clear msAll
    msAll.startTime = [];
    msAll.endTime = [];
    msAll.angle = [];
    msAll.start = [0;0];
    msAll.end = [0;0];
    msAll.trial = [];
    msAll.amplitude = [];
    msAll.rate = [];
    for c = 1:length(conditionsAll)
        currentCond = conditionsAll{c};
        counter = 1;
        for ss = 1:length(data{ii}.(currentCond))
            if ~isempty(find(data{ii}.(currentCond){ss}.microsaccades.start == 0))
                data{ii}.(currentCond){ss}.microsaccades.start =  data{ii}.(currentCond){ss}.microsaccades.start(...
                    find( data{ii}.(currentCond){ss}.microsaccades.start));
                data{ii}.(currentCond){ss}.microsaccades.duration =  data{ii}.(currentCond){ss}.microsaccades.duration(...
                    find( data{ii}.(currentCond){ss}.microsaccades.start));
                if isempty(data{ii}.(currentCond){ss}.microsaccades.angle)
                    data{ii}.(currentCond){ss}.microsaccades.angle = ones(1,length(...
                        data{ii}.(currentCond){ss}.microsaccades.start))*NaN;
                    data{ii}.(currentCond){ss}.microsaccades.amplitude = ones(1,length(...
                        data{ii}.(currentCond){ss}.microsaccades.start))*NaN;
                else
                    data{ii}.(currentCond){ss}.microsaccades.angle =  ...
                        data{ii}.(currentCond){ss}.microsaccades.angle(...
                        find( data{ii}.(currentCond){ss}.microsaccades.start));
                    data{ii}.(currentCond){ss}.microsaccades.amplitude =  ...
                        data{ii}.(currentCond){ss}.microsaccades.amplitude(...
                        find( data{ii}.(currentCond){ss}.microsaccades.start));
                    if data{ii}.(currentCond){ss}.microsaccades.amplitude < 5
                        data{ii}.(currentCond){ss}.microsaccades.amplitude = NaN;
                    end
                end
            end
            fixationTrials.(currentCond)(ss) = data{ii}.(currentCond){ss}.FixationTrial;
            dataAll{ii}.(currentCond).correct(ss) = data{ii}.(currentCond){ss}.Correct;
            dataAll{ii}.(currentCond).fixation(ss) = fixationTrials.(currentCond)(ss);
            dataAll{ii}.(currentCond).rt(ss) = data{ii}.(currentCond){ss}.TimeResponse-data{ii}.(currentCond){ss}.TimeImageOn;
            msAll.rate(ss) = length(data{ii}.(currentCond){ss}.microsaccades.start)/1.5;
            msAll.trial = [msAll.trial ones(1,...
                length(data{ii}.(currentCond){ss}.microsaccades.start))*ss];
            msAll.startTime =  [msAll.startTime data{ii}.(currentCond){ss}.microsaccades.start];
            msAll.endTime =  [msAll.endTime data{ii}.(currentCond){ss}.microsaccades.start+...
                data{ii}.(currentCond){ss}.microsaccades.duration];
            
%             msAll.angle = [msAll.angle data{ii}.(currentCond){ss}.microsaccades.angle];
%             msAll.amplitude = [msAll.amplitude ...
%                 data{ii}.(currentCond){ss}.microsaccades.amplitude(find(...
%                 data{ii}.(currentCond){ss}.microsaccades.amplitude < 30))];
            msAll.start = [msAll.start ...
                [data{ii}.(currentCond){ss}.x(data{ii}.(currentCond){ss}.microsaccades.start);...
                data{ii}.(currentCond){ss}.y(data{ii}.(currentCond){ss}.microsaccades.start)]];
            
            if sum(data{ii}.(currentCond){ss}.microsaccades.start+...
                    data{ii}.(currentCond){ss}.microsaccades.duration > length(data{ii}.(currentCond){ss}.x))
                removeEnd = -1;
            else
                removeEnd = 0;
            end
            msAll.end = [msAll.end ...
                [data{ii}.(currentCond){ss}.x(data{ii}.(currentCond){ss}.microsaccades.start+...
                data{ii}.(currentCond){ss}.microsaccades.duration+removeEnd);...
                data{ii}.(currentCond){ss}.y(data{ii}.(currentCond){ss}.microsaccades.start+...
                data{ii}.(currentCond){ss}.microsaccades.duration+removeEnd)]];
%              msAll.angle = [msAll.angle atan2d()

            cleanX = [];
            cleanX = data{ii}.(currentCond){ss}.x(...
                find( data{ii}.(currentCond){ss}.x < axisVal &  data{ii}.(currentCond){ss}.x > -axisVal ...
                &  data{ii}.(currentCond){ss}.y < axisVal &  data{ii}.(currentCond){ss}.y > -axisVal));
            cleanY = [];
            cleanY = data{ii}.(currentCond){ss}.y(...
                find( data{ii}.(currentCond){ss}.x < axisVal &  data{ii}.(currentCond){ss}.x > -axisVal ...
                &  data{ii}.(currentCond){ss}.y < axisVal &  data{ii}.(currentCond){ss}.y > -axisVal));
            if length(cleanY) < 1440
                continue;
            end
            if fixationTrials.(currentCond)(ss) == 1
                dataAll{ii}.(currentCond).allXCleanFIX = [dataAll{ii}.(currentCond).allXCleanFIX ...
                    cleanX];
                dataAll{ii}.(currentCond).allYCleanFIX = [dataAll{ii}.(currentCond).allYCleanFIX ...
                    cleanY];
            else
                dataAll{ii}.(currentCond).allXClean = [dataAll{ii}.(currentCond).allXClean ...
                    cleanX];
                dataAll{ii}.(currentCond).allYClean = [dataAll{ii}.(currentCond).allYClean ...
                    cleanY];
            end
            dataAll{ii}.(currentCond).allXCleanFIXmat(counter,:) = cleanX(1:1440);
            dataAll{ii}.(currentCond).allYCleanFIXmat(counter,:) = cleanY(1:1440);
            dataAll{ii}.(currentCond).cleanFixation(counter,:) = dataAll{ii}.(currentCond).fixation(ss);
            
            counter = counter + 1;

            %             else
            %                 dataAll{ii}.(currentCond).allXClean= [dataAll{ii}.(currentCond).allXClean ...
            %                     data{ii}.(currentCond){i}.x(...
            %                     find( data{ii}.(currentCond){i}.x < axisVal &  data{ii}.(currentCond){i}.x > -axisVal ...
            %                     &  data{ii}.(currentCond){i}.y < axisVal &  data{ii}.(currentCond){i}.y > -axisVal));];
            %                 dataAll{ii}.(currentCond).allYClean = [dataAll{ii}.(currentCond).allYClean ...
            %                     data{ii}.(currentCond){i}.y(...
            %                     find( data{ii}.(currentCond){i}.x < axisVal &  data{ii}.(currentCond){i}.x > -axisVal ...
            %                     &  data{ii}.(currentCond){i}.y < axisVal &  data{ii}.(currentCond){i}.y > -axisVal));];
            %             end
        end
        msAll.angle = deg2rad(atan2d(msAll.end(1,:), msAll.end(2,:)));
        X = msAll.start - msAll.end;
        for a = 1:length(X)
            msAll.amplitude(a) = eucDist(X(1,a),X(2,a));
        end
        dataAll{ii}.(currentCond).msAll = msAll;
    end
    
    %% Performance Figure per Subject
    %   Overall performance; cumalitive.
    performanceC(ii) = (sum(dataAll{ii}.Control.correct)/...
        length(dataAll{ii}.Control.correct))*100;
    performanceS(ii) = sum(dataAll{ii}.Scotoma.correct)/...
        length(dataAll{ii}.Scotoma.correct)*100;
    %     end
    figure;
    errorbar([0 1], [performanceC(ii) performanceS(ii)], ...
        [sem(dataAll{ii}.Control.correct * 100) sem(dataAll{ii}.Scotoma.correct*100)],...
        '-o','Color','k','MarkerSize',10,...
        'MarkerEdgeColor','k','MarkerFaceColor','k');
    ylim([40 100])
    xlim([-.5 1.5])
    xticks([0 1])
    xticklabels({'Control','Scotoma'});
    ylabel('Performance');
    % line([-.5 1.5],[50 50],'Color','red','LineStyle','--')
    yticks([50 60 70 80 90 100])
    title(subjectsAll{ii});
    saveas(gcf,strcat(subjFigPath,'/',sprintf('Performance%s',subjectsAll{ii}),'.png'));
    saveas(gcf,strcat(subjFigPath,'/',sprintf('Performance%s',subjectsAll{ii}),'.epsc'));
    
    %% Response Time
    % Response time from start of image presentation (only in task trials)
    
    figure;
    errorbar([0 1], [mean(dataAll{ii}.Control.rt(find(~dataAll{ii}.Control.fixation))) ,...
        mean(dataAll{ii}.Scotoma.rt(find(~dataAll{ii}.Scotoma.fixation)))] ,...
        [sem(dataAll{ii}.Control.rt(find(~dataAll{ii}.Control.fixation))) ,...
        sem(dataAll{ii}.Scotoma.rt(find(~dataAll{ii}.Scotoma.fixation)))] ,...
        '-o','Color','k','MarkerSize',10,...
        'MarkerEdgeColor','k','MarkerFaceColor','k');
    xlim([-.5 1.5])
    xticks([0 1])
    xticklabels({'Control','Scotoma'});
    ylabel('Reaction Time');
    [~,P] = ttest2((dataAll{ii}.Control.rt(find(~dataAll{ii}.Control.fixation))) ,...
        (dataAll{ii}.Scotoma.rt(find(~dataAll{ii}.Scotoma.fixation))));
    
    title(subjectsAll{ii})
    saveas(gcf,strcat(subjFigPath,'/',sprintf('ReactionTime%s',subjectsAll{ii}),'.png'));
    saveas(gcf,strcat(subjFigPath,'/',sprintf('ReactionTime%s',subjectsAll{ii}),'.epsc'));
    
    %% Heat Map of All X and Y Over Face
    
    h2 = figure;
    subplot(2,2,1)
    I=imread('1_Neutral_f_03_176px.png');
    image(-axisVal:axisVal, -axisVal:axisVal, flipud(I), 'CDataMapping', 'scaled');
    axis('xy')
    axis image
    hold on
    result = generateHeatMapSimple( ...
        dataAll{ii}.Control.allXClean, ...
        dataAll{ii}.Control.allYClean, ...
        'Bins', 21,...
        'StimulusSize', 10,...
        'AxisValue', axisVal,...
        'Uncrowded', 0,...
        'Borders', 1);
    [row, col] = find(ismember(result, max(result(:))));
    axis square
    caxis([-.02 1])
    title('Control')
    hold on
    binToArcmin = axisVal/20;
    circle(mean(dataAll{ii}.Control.allXClean),...
        mean(dataAll{ii}.Control.allYClean), 5/2);
    line([-axisVal,axisVal],[0,0])
    line([0,0],[-axisVal,axisVal])
    
    subplot(2,2,2)
    I=imread('1_Neutral_f_03_176px.png');
    image(-axisVal:axisVal, -axisVal:axisVal, flipud(I), 'CDataMapping', 'scaled');
    axis('xy')
    axis image
    hold on
    result = generateHeatMapSimple( ...
        dataAll{ii}.Scotoma.allXClean, ...
        dataAll{ii}.Scotoma.allYClean, ...
        'Bins', 20,...
        'StimulusSize', 10,...
        'AxisValue', axisVal,...
        'Uncrowded', 0,...
        'Borders', 1);
    axis square
    caxis([-.02 1])
    title('Scotoma')
    hold on
    [row, col] = find(ismember(result, max(result(:))));
    binToArcmin = axisVal/20;
      circle(mean(dataAll{ii}.Scotoma.allXClean),...
        mean(dataAll{ii}.Scotoma.allYClean), 5/2);
    line([-axisVal,axisVal],[0,0])
    line([0,0],[-axisVal,axisVal])
    
    subplot(2,2,3)
    result = generateHeatMapSimple( ...
        dataAll{ii}.Control.allXCleanFIX, ...
        dataAll{ii}.Control.allYCleanFIX, ...
        'Bins', 20,...
        'StimulusSize', 5,...
        'AxisValue', axisVal,...
        'Uncrowded', 4,...
        'Borders', 1);
    axis square
    caxis([-.02 1])
    title('Control, Fixation')
    hold on
    [row, col] = find(ismember(result, max(result(:))));
    binToArcmin = axisVal/20;
       circle(mean(dataAll{ii}.Control.allXCleanFIX),...
        mean(dataAll{ii}.Control.allYCleanFIX), 5/2);
    line([-axisVal,axisVal],[0,0])
    line([0,0],[-axisVal,axisVal])
    
    subplot(2,2,4)
    result = generateHeatMapSimple( ...
        dataAll{ii}.Scotoma.allXCleanFIX, ...
        dataAll{ii}.Scotoma.allYCleanFIX, ...
        'Bins', 20,...
        'StimulusSize', 5,...
        'AxisValue', axisVal,...
        'Uncrowded', 4,...
        'Borders', 1);
    axis square
    caxis([-.02 1])
    title('Scotoma, Fixation')
    suptitle(subjectsAll{ii});
    hold on
    [row, col] = find(ismember(result, max(result(:))));
    binToArcmin = axisVal/20;
     circle(mean(dataAll{ii}.Scotoma.allXCleanFIX),...
        mean(dataAll{ii}.Scotoma.allYCleanFIX), 5/2);
    line([-axisVal,axisVal],[0,0])
    line([0,0],[-axisVal,axisVal])
    saveas(gcf,strcat(subjFigPath,'/',sprintf('PatternHeatMapAll%s',subjectsAll{ii}),'.png'));
    saveas(gcf,strcat(subjFigPath,'/',sprintf('PatternHeatMapAll%s',subjectsAll{ii}),'.epsc'));
    
    %% Load in AO Image and plot on top
    
    
%         if strcmp(subjectsAll{ii},'Z091') || strcmp(subjectsAll{ii},'Z151') ||...
%              strcmp(subjectsAll{ii},'A188')%|| strcmp(subjectsAll{ii},'Z055')
%         if  strcmp(subjectsAll{ii},'Z091')
%             pathName = 'C:\Users\Ruccilab\Downloads\Z190_R_2022_06_03_CD_data.mat';
%         elseif strcmp(subjectsAll{ii},'Z151')
%             pathName = 'C:\Users\Ruccilab\Downloads\Z151_R_2022_07_29_CD_data (1).mat';
%         elseif strcmp(subjectsAll{ii},'A188')
%             continue;
%             coneMap = load('C:\Users\Ruccilab\Downloads\A188_R_2022_10_17_CD_data.mat');
%         end
%         plotAOImages(pathName,dataAll{ii});
%         saveas(gcf,sprintf('../../../Documents/Overleaf_MiniScotoma/figures/AOEM%s.png',subjectsAll{ii}));
%         saveas(gcf,sprintf('../../../Documents/Overleaf_MiniScotoma/figures/AOEM%s.epsc',subjectsAll{ii}));
        save(sprintf('ForBen/%s_EyeData_MiniScot.mat',subjectsAll{1}),'dataAll');
        
        
% % %         [h,w] = size(coneMap.refImage);
% % %         arcminWidthImage = (w/coneMap.pixPerdeg)*60;
% % %         arcminHeightImage = (h/coneMap.pixPerdeg)*60;
% % %         imageCenterX = w/2;
% % %         imageCenterY = h/2;
% % %         PRL_Xarcmin = ((coneMap.PRL_X - imageCenterX)/coneMap.pixPerdeg)*60;
% % %         PRL_Yarcmin = ((coneMap.PRL_Y - imageCenterY)/coneMap.pixPerdeg)*60;
% % %         CDC_Xarcmin = ((coneMap.oursCDC_x - imageCenterX)/coneMap.pixPerdeg)*60;
% % %         CDC_Yarcmin = ((coneMap.oursCDC_y - imageCenterY)/coneMap.pixPerdeg)*60;
% % %         PCD_Xarcmin = ((coneMap.oursPCD_x - imageCenterX)/coneMap.pixPerdeg)*60;
% % %         PCD_Yarcmin = ((coneMap.oursPCD_y - imageCenterY)/coneMap.pixPerdeg)*60;
% % %         figure;
% % %         t1 = image(-arcminWidthImage/2:arcminWidthImage/2,...
% % %             -arcminHeightImage/2:arcminHeightImage/2, (coneMap.refImage), 'CDataMapping', 'scaled');
% % %         hold on
% % %         
% % %         result = generateHeatMapSimple( ...
% % %             (dataAll{ii}.Scotoma.allXCleanFIX+PRL_Xarcmin), ... %look at scotoma on retina
% % %             (-dataAll{ii}.Scotoma.allYCleanFIX+PRL_Yarcmin), ...
% % %             'Bins', 21,...
% % %             'StimulusSize', 5,...
% % %             'AxisValue', 40,...
% % %             'Uncrowded', 0,...
% % %             'Borders', 1);
% % % %         close figure 100
% % %         figure;
% % %         t1 = image(-arcminWidthImage/2:arcminWidthImage/2,...
% % %             -arcminHeightImage/2:arcminHeightImage/2, (coneMap.refImage), 'CDataMapping', 'scaled');
% % %         hold on
% % %         generateHeatMapSimple( ... % look at cones stimulated by fixation marker
% % %             (dataAll{ii}.Scotoma.allXCleanFIX-mean(dataAll{ii}.Scotoma.allXCleanFIX)), ...
% % %             (-dataAll{ii}.Scotoma.allYCleanFIX-mean(dataAll{ii}.Scotoma.allYCleanFIX)), ...
% % %             'Bins', 21,...
% % %             'StimulusSize', 5,...
% % %             'AxisValue', 40,...
% % %             'Uncrowded', 0,...
% % %             'Borders', 1);
% % %         
% % %         axis([-40 40 -40 40])
% % %         axis square
% % %         title('Scotoma, Fixation')
% % %         suptitle(subjectsAll{ii});
% % %         hold on
% % % %         [r,c] = find(ismember(result, max(result(:))));
% % % %         row = (r-(11)); %half bin round up
% % % %         col = (c-(11));
% % % %         binToArcmin = 40/21;
% % %         xCenter = PRL_Xarcmin;%mean(dataAll{ii}.Scotoma.allXCleanFIX);%(-col*binToArcmin);
% % %         yCenter = PRL_Yarcmin;%mean(dataAll{ii}.Scotoma.allYCleanFIX);%(row*binToArcmin);
% % %         rectangle('Position',[-7/2,-7/2,7,7]);
% % %         circle(xCenter,yCenter, 5/2);
% % % %         line([-40,40],[yCenter yCenter])
% % % %         line([xCenter xCenter],[-40,40])
% % %         hold on
% % %         legS(1) = plot(PRL_Xarcmin,PRL_Yarcmin,'*','Color','b');
% % %         hold on
% % %         legS(2) =plot(PCD_Xarcmin,PCD_Yarcmin,'^','Color','k');
% % %         legS(3) =plot(CDC_Xarcmin,CDC_Xarcmin,'d','Color','m');
% % %         legend(legS,{'PRL','PCD','CDC'});
% % %         saveas(gcf,sprintf('../../../Documents/Overleaf_MiniScotoma/figures/AOEM%s.png',subjectsAll{ii}));
% % %         saveas(gcf,sprintf('../../../Documents/Overleaf_MiniScotoma/figures/AOEM%s.epsc',subjectsAll{ii}));
%     end
    %% Performance in Time Figure per Subject
    %   Divided the session into 3 different times. Each point has the number
    %   of trials indicated in the title (ie XX#C means there were XX trials
    %   for each Control point in all 3 figures).
    
    divideBy = 4;
    
    timeAcrossSessionC = floor(length(dataAll{ii}.Control.correct)/divideBy-1);
    timeAcrossSessionS = floor(length(dataAll{ii}.Scotoma.correct)/divideBy-1);
    timeDivideC = 1:timeAcrossSessionC:floor(length(dataAll{ii}.Control.correct));
    timeDivideS = 1:timeAcrossSessionS:floor(length(dataAll{ii}.Scotoma.correct));
    
    figure;
    for t = 1:divideBy
        performanceC = [];
        performanceS = [];
        %         for ii = 1:length(subjectsAll)
        performanceC = nansum(dataAll{ii}.Control.correct(timeDivideC(t):timeDivideC(t+1)-1))/...
            sum(~isnan(dataAll{ii}.Control.correct(timeDivideC(t):timeDivideC(t+1)-1)))*100;
        valC = sum(~isnan(dataAll{ii}.Control.correct(timeDivideC(t):timeDivideC(t+1)-1)));
        performanceS = nansum(dataAll{ii}.Scotoma.correct(timeDivideS(t):timeDivideS(t+1)-1))/...
            sum(~isnan(dataAll{ii}.Scotoma.correct(timeDivideS(t):timeDivideS(t+1)-1)))*100;
        valS = sum(~isnan(dataAll{ii}.Scotoma.correct(timeDivideS(t):timeDivideS(t+1)-1)));
        %         end
        
        %         subplot(2,3,t)
        leg(1) = errorbar([t ], [ performanceS], ...
            [sem(dataAll{ii}.Scotoma.correct*100)],...
            'o','Color','k','MarkerSize',10,...
            'MarkerEdgeColor','k','MarkerFaceColor','k');
        hold on
        leg(2) = errorbar([t ], [performanceC], ...
            [sem(dataAll{ii}.Control.correct * 100) ],...
            'o','Color','b','MarkerSize',10,...
            'MarkerEdgeColor','b','MarkerFaceColor','b');
        ylim([30 100])
        %         xlim([-.5 1.5])
        %         xticks([0 1])
        xlabel('Time Samples (non-cumal)');
        ylabel('Performance');
        % line([-.5 1.5],[50 50],'Color','red','LineStyle','--')
        yticks([50 60 70 80 90 100])
        title(sprintf('Time Section %i',t));
    end
    %     suptitle(sprintf('%i#C, %i#S',valC,valS))
    legend(leg,{'Scotoma','Control'});
    title(subjectsAll{ii});
    saveas(gcf,sprintf('../../../Documents/Overleaf_MiniScotoma/figures/%sPerformanceInTime.png',...
        subjectsAll{ii}));
    saveas(gcf,sprintf('../../../Documents/Overleaf_MiniScotoma/figures/%sPerformanceInTime.epsc',...
        subjectsAll{ii}));
    
    
    %% Heatmaps for All X Y Over Time
    %   Heatmap for all x y position across time within 1x1 deg region. IE Section
    %   1 is each trials 1st 330 ms of traces. Full stimulus duration is 1
    %   second.
    
    if makemovie
        timeBins = 1:20:1440;
    else
        timeBins = 1:470:1440;
    end
    counter = 0;
    for t = 1:length(timeBins) - 1
        counter = counter + 1;
        time{ii}.XC{counter} = dataAll{ii}.Control.allXCleanFIXmat(...
            find(~dataAll{ii}.Control.cleanFixation),...
            timeBins(t):(timeBins(t+1)-1));
        time{ii}.YC{counter} = dataAll{ii}.Control.allYCleanFIXmat(...
            find(~dataAll{ii}.Control.cleanFixation),...
            timeBins(t):(timeBins(t+1)-1));
        time{ii}.XS{counter} = dataAll{ii}.Scotoma.allXCleanFIXmat(...
            find(~dataAll{ii}.Scotoma.cleanFixation),...
            timeBins(t):(timeBins(t+1)-1));
        time{ii}.YS{counter} = dataAll{ii}.Scotoma.allYCleanFIXmat(...
            find(~dataAll{ii}.Scotoma.cleanFixation),...
            timeBins(t):(timeBins(t+1)-1));
        time{ii}.XFC{counter} = dataAll{ii}.Control.allXCleanFIXmat(...
            find(dataAll{ii}.Control.cleanFixation),...
            timeBins(t):(timeBins(t+1)-1));
        time{ii}.YFC{counter} =  dataAll{ii}.Control.allYCleanFIXmat(...
            find(dataAll{ii}.Control.cleanFixation),...
            timeBins(t):(timeBins(t+1)-1));
        time{ii}.XFS{counter} = dataAll{ii}.Scotoma.allXCleanFIXmat(...
            find(dataAll{ii}.Scotoma.cleanFixation),...
            timeBins(t):(timeBins(t+1)-1));
        time{ii}.YFS{counter} = dataAll{ii}.Scotoma.allYCleanFIXmat(...
            find(dataAll{ii}.Scotoma.cleanFixation),...
            timeBins(t):(timeBins(t+1)-1));
    end
    
    stringBinsX = {'XC','XS','XFC','XFS'};
    stringBinsY = {'YC','YS','YFC','YFS'};
    fourConds = {'Control','Scotoma','Control Fix', 'Scotoma Fix'};
    if makemovie
        for ss = 1:4
            
            figure
            
            axes;
            % Create and open the video object
            vidObj = VideoWriter(sprintf('%s_%iSIN_X_COS_X.avi',subjectsAll{ii}, ss));
            open(vidObj);
            %
            % Loop over the data to create the video
            for t=1:length(timeBins)-1
                % Plot the data
                sizeVec = (find(time{ii}.(stringBinsX{ss}){t}));
                generateHeatMapSimple( ...
                    reshape(time{ii}.(stringBinsX{ss}){t},[1, sizeVec(end)]), ...
                    reshape(time{ii}.(stringBinsY{ss}){t},[1, sizeVec(end)]), ...
                    'Bins', 10,...
                    'StimulusSize', 5,...
                    'AxisValue', axisVal,...
                    'Uncrowded', 4,...
                    'Borders', 1);
                
                % Get the current frame
                currFrame = getframe;
                
                % Write the current frame
                writeVideo(vidObj,currFrame);
                
                clf
                text(-30,30,sprintf('%i ms',timeBins(t)));
                title(sprintf('%s,%s',subjectsAll{ii},fourConds{ss}))
                if ss < 3
                    a = circleAMC(10,10,5);
                    uistack(a,'top')
                    b = circleAMC(-10,10,5);
                    uistack(b,'top')
                end
                %             delete(k)
            end
            
            % Close (and save) the video object
            close(vidObj);
        end
    else
        figure('Position', [10 10 900 1200]);
        counter = 1;
        for ss = 1:4
            for tB = 1:length(timeBins) - 1
                subplot(4,3,counter)
                if ss < 3
                    I=imread('1_Neutral_f_03_176px.png');
                    image(-axisVal:axisVal, -axisVal:axisVal, flipud(I), 'CDataMapping', 'scaled');
                    axis('xy')
                    axis image
                    hold on
                end
                sizeVec = (find(time{ii}.(stringBinsX{ss}){tB}));
                generateHeatMapSimple( ...
                    reshape(time{ii}.(stringBinsX{ss}){tB},[1, sizeVec(end)]), ...
                    reshape(time{ii}.(stringBinsY{ss}){tB},[1, sizeVec(end)]), ...
                    'Bins', 10,...
                    'StimulusSize', 5,...
                    'AxisValue', axisVal,...
                    'Uncrowded', 4,...
                    'Borders', 1);
                axis square
                line([-axisVal,axisVal],[0,0])
                line([0,0],[-axisVal,axisVal])
                ylabel(fourConds{ss})
                title(sprintf('%i-%ims',timeBins(tB),timeBins(tB+1)-1));
                
                counter = counter + 1;
            end
        end
        suptitle(subjectsAll{ii});
    end
    %% Microsaccade Analysis
    figure('Position', [10 10 900 1200]);
    counter = 0;
    for i = 1:2
        counter = counter + 1;
        subplot(4,2,counter)
        I=imread('1_Neutral_f_03_176px.png');
        image(-axisVal:axisVal, -axisVal:axisVal, flipud(I), 'CDataMapping', 'scaled');
        axis('xy')
        axis image
        hold on
        x1Idx = (...
            find(ismember(dataAll{ii}.(conditionsAll{i}).msAll.trial, ...
            find(~dataAll{ii}.(conditionsAll{i}).fixation))));
        x2Idx = find(ismember(x1Idx, find(dataAll{ii}.(conditionsAll{i}).msAll.startTime > 400 &...
            dataAll{ii}.(conditionsAll{i}).msAll.endTime < 1200 & ...
            dataAll{ii}.(conditionsAll{i}).msAll.amplitude(2:end) > 5)));
        
        x2 = dataAll{ii}.(conditionsAll{i}).msAll.start(2,x2Idx);
        x1 = dataAll{ii}.(conditionsAll{i}).msAll.start(1,x2Idx);
        
        scatter1 = scatter(x1, ...
            x2,...
            '*','MarkerFaceColor','r','MarkerEdgeColor','r');
        title('Start MS');
        ylabel(conditionsAll{i});
        axis([-axisVal axisVal -axisVal axisVal])
        scatter1.MarkerFaceAlpha = .1;
        scatter1.MarkerEdgeAlpha = .1;
        line([0 0],[-axisVal axisVal]);
        line([-axisVal axisVal],[0 0]);
        
        counter = counter + 1;
        subplot(4,2,counter)
        I=imread('1_Neutral_f_03_176px.png');
        image(-axisVal:axisVal, -axisVal:axisVal, flipud(I), 'CDataMapping', 'scaled');
        axis('xy')
        axis image
        hold on
        x2 = dataAll{ii}.(conditionsAll{i}).msAll.end(2,x2Idx);
        x1 = dataAll{ii}.(conditionsAll{i}).msAll.end(1,x2Idx);
        scatter1 = scatter(x1,x2,...
            '*','MarkerFaceColor','b','MarkerEdgeColor','b');
        title('End MS');
        averageMSEndC(ii,1) = mean(x1);
        averageMSEndC(ii,2)= mean(x2);
        hold on
        plot(averageMSEndC(ii,1),averageMSEndC(ii,2),'d','Color','k','MarkerFaceColor','k');
        
        axis([-axisVal axisVal -axisVal axisVal])
        scatter1.MarkerFaceAlpha = .1;
        scatter1.MarkerEdgeAlpha = .1;
        line([0 0],[-axisVal axisVal]);
        line([-axisVal axisVal],[0 0]);
    end

    %%%%%% fixation %%%%%%%
   for i = 1:2
        counter = counter + 1;
        subplot(4,2,counter)
%         I=imread('1_Neutral_f_03_176px.png');
%         image(-axisVal:axisVal, -axisVal:axisVal, flipud(I), 'CDataMapping', 'scaled');
%         axis('xy')
%         axis image
        axis square
        hold on
        x1Idx = (...
            find(ismember(dataAll{ii}.(conditionsAll{i}).msAll.trial, ...
            find(dataAll{ii}.(conditionsAll{i}).fixation))));
        x2Idx = find(ismember(x1Idx, find(dataAll{ii}.(conditionsAll{i}).msAll.startTime > 400 &...
            dataAll{ii}.(conditionsAll{i}).msAll.endTime < 1200 & ...
            dataAll{ii}.(conditionsAll{i}).msAll.amplitude(2:end) > 5)));
        
        x2 = dataAll{ii}.(conditionsAll{i}).msAll.start(2,x2Idx);
        x1 = dataAll{ii}.(conditionsAll{i}).msAll.start(1,x2Idx);
        
        scatter1 = scatter(x1, ...
            x2,...
            '*','MarkerFaceColor','r','MarkerEdgeColor','r');
        title('Start MS');
        ylabel(conditionsAll{i});
        axis([-axisVal axisVal -axisVal axisVal])
        scatter1.MarkerFaceAlpha = .1;
        scatter1.MarkerEdgeAlpha = .1;
        line([0 0],[-axisVal axisVal]);
        line([-axisVal axisVal],[0 0]);
        
        counter = counter + 1;
        subplot(4,2,counter)
%         I=imread('1_Neutral_f_03_176px.png');
%         image(-axisVal:axisVal, -axisVal:axisVal, flipud(I), 'CDataMapping', 'scaled');
%         axis('xy')
%         axis image
        axis square
        hold on
        x2 = dataAll{ii}.(conditionsAll{i}).msAll.end(2,x2Idx);
        x1 = dataAll{ii}.(conditionsAll{i}).msAll.end(1,x2Idx);
        scatter1 = scatter(x1,x2,...
            '*','MarkerFaceColor','b','MarkerEdgeColor','b');
        title('End MS');
        averageMSEndC(ii,1) = mean(x1);
        averageMSEndC(ii,2)= mean(x2);
        hold on
        plot(averageMSEndC(ii,1),averageMSEndC(ii,2),'d','Color','k','MarkerFaceColor','k');
        
        axis([-axisVal axisVal -axisVal axisVal])
        scatter1.MarkerFaceAlpha = .1;
        scatter1.MarkerEdgeAlpha = .1;
        line([0 0],[-axisVal axisVal]);
        line([-axisVal axisVal],[0 0]);
    end
  %% Rate of MS
    figure;
    errorbar([0 1 2 3],...
        [mean(dataAll{ii}.Control.msAll.rate(find(~dataAll{ii}.Control.fixation))),...
        mean(dataAll{ii}.Scotoma.msAll.rate(find(~dataAll{ii}.Scotoma.fixation))),...
        mean(dataAll{ii}.Control.msAll.rate(find(dataAll{ii}.Control.fixation))),...
        mean(dataAll{ii}.Scotoma.msAll.rate(find(dataAll{ii}.Scotoma.fixation)))],...
        [sem(dataAll{ii}.Control.msAll.rate(find(~dataAll{ii}.Control.fixation))),...
        sem(dataAll{ii}.Scotoma.msAll.rate(find(~dataAll{ii}.Scotoma.fixation))),...
        sem(dataAll{ii}.Control.msAll.rate(find(dataAll{ii}.Control.fixation))),...
        sem(dataAll{ii}.Scotoma.msAll.rate(find(dataAll{ii}.Scotoma.fixation)))],'o');
    ylabel('MS Rate')
    xticks([0 1 2 3])
    xlim([-1 4])
    xticklabels({'Control','Scotoma','Control Fix','Scotoma Fix'});
    title(subjectsAll{ii});
    
    %% Direction of MS
    clear h;
    figure;
    subplot(1,2,1)
    [val,pos]=intersect(find(~dataAll{ii}.Control.fixation),dataAll{ii}.Control.msAll.trial);
    pos1 = pos(pos'<length(dataAll{ii}.Control.msAll.angle))';
    theta = dataAll{ii}.Control.msAll.angle(pos1);
    % rho = dataAll{ii}.Control.msAll.amplitude(find(~dataAll{ii}.Control.fixation));
    [vals,idxs] = sort(round(theta,2));
    h(1) = polarhistogram(vals,'EdgeColor','b');
    h(1).DisplayStyle = 'stairs';
    
    hold on
    [val,pos2]=intersect(find(~dataAll{ii}.Scotoma.fixation),dataAll{ii}.Scotoma.msAll.trial);
    theta = dataAll{ii}.Scotoma.msAll.angle(pos2');
    % rho = dataAll{ii}.Scotoma.msAll.amplitude(find(~dataAll{ii}.Scotoma.fixation));
    [vals,idxs] = sort(round(theta,2));
    h(2) = polarhistogram(vals,'EdgeColor','r');
    h(2).DisplayStyle = 'stairs';
    legend(h,{'Control','Scotoma'},'Location','south');
    title('Faces');
    
    subplot(1,2,2)
    [val,pos3]=intersect(find(dataAll{ii}.Control.fixation),dataAll{ii}.Control.msAll.trial);
    theta = dataAll{ii}.Control.msAll.angle(pos3');
    [vals,idxs] = sort(round(theta,2));
    h(1) = polarhistogram(vals,'EdgeColor','b');
    h(1).DisplayStyle = 'stairs';
    hold on
    [val,pos4]=intersect(find(dataAll{ii}.Scotoma.fixation),dataAll{ii}.Scotoma.msAll.trial);
    theta = dataAll{ii}.Scotoma.msAll.angle(pos4');
    % rho = dataAll{ii}.Scotoma.msAll.amplitude(find(dataAll{ii}.Scotoma.fixation));
    [vals,idxs] = sort(round(theta,2));
    h(2) = polarhistogram(vals,'EdgeColor','r');
    h(2).DisplayStyle = 'stairs';
    legend(h,{'Control','Scotoma'},'Location','south');
    title('Fixation')
    suptitle(sprintf('Angle,%s', subjectsAll{ii}));
    
    %% Microsaccade Amplitude
    rho{1} = dataAll{ii}.Control.msAll.amplitude(pos1');
    rho{2} = dataAll{ii}.Scotoma.msAll.amplitude(pos2');
    rho{3} = dataAll{ii}.Control.msAll.amplitude(pos3');
    rho{4} = dataAll{ii}.Scotoma.msAll.amplitude(pos4');
    
    temp = struct2table(cell2struct(rho,...
        {'Control','Scotoma','ControlFix', 'ScotomaFix'},2));
    figure;
    violinplot(temp);
    ylabel('MS Amplitude')
    
end

%% AcrossSubject


%%% MS Rate
figure;
for ii = 1:length(subjectsAll)
    plot([0 1 2 3],...
        [mean(dataAll{ii}.Control.msAll.rate(find(~dataAll{ii}.Control.fixation))),...
        mean(dataAll{ii}.Scotoma.msAll.rate(find(~dataAll{ii}.Scotoma.fixation))),...
        mean(dataAll{ii}.Control.msAll.rate(find(dataAll{ii}.Control.fixation))),...
        mean(dataAll{ii}.Scotoma.msAll.rate(find(dataAll{ii}.Scotoma.fixation)))],'o');
    msInfo(ii,:) = [mean(dataAll{ii}.Control.msAll.rate(find(~dataAll{ii}.Control.fixation))),...
        mean(dataAll{ii}.Scotoma.msAll.rate(find(~dataAll{ii}.Scotoma.fixation))),...
        mean(dataAll{ii}.Control.msAll.rate(find(dataAll{ii}.Control.fixation))),...
        mean(dataAll{ii}.Scotoma.msAll.rate(find(dataAll{ii}.Scotoma.fixation)))];
    
    hold on
end
hold on
errorbar([0.1 1.1 2.1 3.1],...
    [mean(msInfo(:,1)),...
    mean(msInfo(:,2)),...
    mean(msInfo(:,3)),...
    mean(msInfo(:,4))],...
    [sem(msInfo(:,1)),...
    sem(msInfo(:,2)),...
    sem(msInfo(:,3)),...
    sem(msInfo(:,4))],'o','Color','k','MarkerFaceColor','k');

xticks([0 1 2 3])
xlim([-1 4])
xticklabels({'Control','Scotoma','Control Fix','Scotoma Fix'});
ylabel('MS Rate');
xlabel('Condition');
[~,p1] = ttest(msInfo(:,1), msInfo(:,2));
[~,p2] = ttest(msInfo(:,3), msInfo(:,4));

%% Performance Across All
figure;
for ii = 1:length(subjectsAll)-1
    performanceC(ii) = (sum(dataAll{1,ii}.Control.correct)/...
        length(dataAll{1,ii}.Control.correct))*100;
    performanceS(ii) = sum(dataAll{1,ii}.Scotoma.correct)/...
        length(dataAll{1,ii}.Scotoma.correct)*100;
    plot([0 1], [performanceC(ii) performanceS(ii)],...
        '-o');
    hold on
end
errorbar([0.1 1.1],[mean(performanceC) mean(performanceS)],...
    [sem(performanceC) sem(performanceS)],'-o','Color','k');
ylim([40 100])
xlim([-.5 1.5])
xticks([0 1])
xticklabels({'Control','Scotoma'});
ylabel('Performance');
% line([-.5 1.5],[50 50],'Color','red','LineStyle','--')
yticks([50 60 70 80 90 100])
[h,p] = ttest(performanceC, performanceS);
title(sprintf('p = %.4f',p));

figure;
for ii = 1:length(subjectsAll)
    xAv.FixC(ii) = abs(mean(dataAll{ii}.Control.allXCleanFIX));
    xAv.FixS(ii) =  abs(mean(dataAll{ii}.Scotoma.allXCleanFIX));
    yAv.FixC(ii) = abs( mean(dataAll{ii}.Control.allYCleanFIX));
    yAv.FixS(ii) = abs( mean(dataAll{ii}.Scotoma.allYCleanFIX));
    %     eAv.FixC(ii) = eucDist(mean(dataAll{ii}.Control.allXCleanFIX), mean(dataAll{ii}.Control.allYCleanFIX));
    %     eAv.FixS(ii) = eucDist(mean(dataAll{ii}.Scotoma.allXCleanFIX), mean(dataAll{ii}.Scotoma.allYCleanFIX));
    eAv.FixC(ii) = mean(eucDist((dataAll{ii}.Control.allXCleanFIX), (dataAll{ii}.Control.allYCleanFIX)));
    eAv.FixS(ii) = mean(eucDist((dataAll{ii}.Scotoma.allXCleanFIX), (dataAll{ii}.Scotoma.allYCleanFIX)));
    
    %     generateHeatMapSimple( ...
    %         dataAll{ii}.Control.allXCleanFIX, ...
    %         dataAll{ii}.Control.allYCleanFIX, ...
    %         'Bins', 20,...
    %         'StimulusSize', 5,...
    %         'AxisValue', axisVal,...
    %         'Uncrowded', 4,...
    %         'Borders', 1);
    %     axis square
    %     caxis([-.02 1])
    %     title('Control, Fixation')
    %     hold on
    %     line([-axisVal,axisVal],[0,0])
    %     line([0,0],[-axisVal,axisVal])
    subplot(1,3,1)
    plot([0 1],[xAv.FixC(ii) xAv.FixS(ii)],'-o');
    hold on
    subplot(1,3,2)
    plot([0 1],[yAv.FixC(ii) yAv.FixS(ii)],'-o');
    hold on
    subplot(1,3,3)
    plot([0 1],[eAv.FixC(ii) eAv.FixS(ii)],'-o');
    hold on
end
subplot(1,3,1)
xticks([0 1])
xticklabels({'Control','Scotoma'});
ylabel('Average X Pos(arcmin)');
[h,p1] = ttest(xAv.FixC, xAv.FixS);
title(p1);
subplot(1,3,2)
xticks([0 1])
xticklabels({'Control','Scotoma'});
ylabel('Average Y Pos(arcmin)')
[h,p2] = ttest(yAv.FixC, yAv.FixS);
title(p2);
subplot(1,3,3)
xticks([0 1])
xticklabels({'Control','Scotoma'});
ylabel('Average Euc. Pos(arcmin)')
[h,p3] = ttest(eAv.FixC, eAv.FixS);
title(p3);
suptitle('Fixation');

% flags = [1 0 0 0 1]; %subjects who show no offset
% figure;
% for ii = find(~flags)
%     subplot(1,2,1)
%     plot([0 1], [eAv.FixC(ii) eAv.FixS(ii)], '-o','Color','r');
%     hold on
%     subplot(1,2,2)
%     plot([2 3],[performanceC(ii) performanceS(ii)],'-o','Color','r');
%     hold on
% end
% for ii = find(flags)
%     subplot(1,2,1)
%     plot([0 1], [eAv.FixC(ii) eAv.FixS(ii)], '-o','Color','b');
%     hold on
%     subplot(1,2,2)
%     plot([2 3],[performanceC(ii) performanceS(ii)],'-o','Color','b');
%     hold on
% end

%% Drift Char

DC = []; Amp = []; Ang = []; Spn = [];
AreaF = [];
AreaT = [];
clear meanXFixTrial; clear meanYFixTrial;
limFix = 72*2;

for ss = 1:length(subjectsAll)%:4
    for cc = 1:2
        currentIdxName = conditionsAll{cc};
        dataAll{ss,cc}.pptrials = [];
        dataAll{ss,cc}.pptrials = data{ss}.(currentIdxName);
        dataAll{ss,cc}.trialsFix = [];
        xAll = [];
        yAll = [];
        Spn = [];
        instSpX = [];
        instSpY = [];
        instSpXF = [];
        instSpYF = [];
        xAllFixation = [];
        yAllFixation = [];
        dataAll{ss,cc}.trials  = [];
        counter = 1;
        counter2 = 1;
        counterF = 1;
%         AreaFInd = [];
        for ii = 1:length(dataAll{ss,cc}.pptrials)
            if ~isfield(dataAll{ss,cc}.pptrials{ii},'drifts')
                continue;
            end
            if dataAll{ss,cc}.pptrials{ii}.FixationTrial
                currentIdx = find(dataAll{ss,cc}.pptrials{ii}.x > -limFix & ...
                    dataAll{ss,cc}.pptrials{ii}.x < limFix & ...
                    dataAll{ss,cc}.pptrials{ii}.y > -limFix & ...
                    dataAll{ss,cc}.pptrials{ii}.y < limFix & ...
                    ~isinf(dataAll{ss,cc}.pptrials{ii}.y) & ...
                    ~isinf(dataAll{ss,cc}.pptrials{ii}.x) & ...
                    ~isnan(dataAll{ss,cc}.pptrials{ii}.y) & ...
                    ~isnan(dataAll{ss,cc}.pptrials{ii}.x));
                
                instStructF = instandSpeed(dataAll{ss,cc}.pptrials{ii}.x(currentIdx),...
                    dataAll{ss,cc}.pptrials{ii}.y(currentIdx));
                instSpXF = [instSpXF instStructF.instSpX];
                instSpYF = [instSpYF instStructF.instSpY];
                
%                 AreaFInd(counterF) = polyarea(dataAll{ss,cc}.pptrials{ii}.x(currentIdx),...
%                     dataAll{ss,cc}.pptrials{ii}.y(currentIdx));
                
                xAllFixation = [xAllFixation dataAll{ss,cc}.pptrials{ii}.x(currentIdx)];
                yAllFixation = [yAllFixation dataAll{ss,cc}.pptrials{ii}.y(currentIdx)];
                meanXFixTrial{ss,cc}(counterF) = mean(dataAll{ss,cc}.pptrials{ii}.x(currentIdx));
                meanYFixTrial{ss,cc}(counterF) = mean(dataAll{ss,cc}.pptrials{ii}.y(currentIdx));
                counterF = counterF + 1;
            end
            for d = 1:length(dataAll{ss,cc}.pptrials{ii}.drifts.start)
                driftStart = dataAll{ss,cc}.pptrials{ii}.drifts.start(d);
                driftEnd = dataAll{ss,cc}.pptrials{ii}.drifts.duration(d) + ...
                    driftStart;
                if driftStart >= length(dataAll{ss,cc}.pptrials{ii}.x)
                    continue;
                elseif driftStart == 0
                    continue;
                elseif driftEnd >=length(dataAll{ss,cc}.pptrials{ii}.x)
                    driftEnd = length(dataAll{ss,cc}.pptrials{ii}.x);
                end
                if ~dataAll{ss,cc}.pptrials{ii}.FixationTrial
                    tempX = [];
                    tempY = [];
                    tempx = dataAll{ss,cc}.pptrials{ii}.x(driftStart:driftEnd);
                    tempy = dataAll{ss,cc}.pptrials{ii}.y(driftStart:driftEnd);
                    tempIdx = find(~isnan(tempx) | (~isnan(tempy)));
                    
                    dataAll{ss,cc}.position(counter).x = tempx(tempIdx);
                    dataAll{ss,cc}.position(counter).y = tempy(tempIdx);
                    
                    x = dataAll{ss,cc}.position(counter).x;
                    y = dataAll{ss,cc}.position(counter).y;
%                     Spn(counter) = quantile(sqrt(x-mean(x).^2 +...
%                               y-mean(y).^2), .95);
                    
                    xAll = [xAll dataAll{ss,cc}.position(counter).x];
                    yAll = [yAll dataAll{ss,cc}.position(counter).y];
                    
                    meanXTrial{ss,cc}(counter) = mean(dataAll{ss,cc}.position(counter).x);
                    meanYTrial{ss,cc}(counter) = mean(dataAll{ss,cc}.position(counter).y);
                    
                  
       
%                     if d > length(dataAll{ss,cc}.pptrials{ii}.drifts.angle)
%                         dataAll{ss,cc}.driftAngle(counter) = NaN;
%                         dataAll{ss,cc}.driftAmp(counter) = NaN;
%                         dataAll{ss,cc}.trials = [dataAll{ss,cc}.trials ii];
%                         continue
%                     end
                    dataAll{ss,cc}.driftAngle(counter) = atan2d(mean(tempy(tempIdx)),mean(tempx(tempIdx)));
                    dataAll{ss,cc}.driftAmp(counter) = eucDist(mean(tempx(tempIdx)),mean(tempy(tempIdx)));
                    dataAll{ss,cc}.trials = [dataAll{ss,cc}.trials ii];

                    if length(dataAll{ss,cc}.pptrials{ii}.x(driftStart:driftEnd)) >= 256
                        tempX = dataAll{ss,cc}.pptrials{ii}.x(driftStart:driftStart+255);
                        tempY = dataAll{ss,cc}.pptrials{ii}.y(driftStart:driftStart+255);
                        dataAll{ss,cc}.positionDC(counter2).x = ...
                            tempX(find(~isnan(tempX)));
                        dataAll{ss,cc}.positionDC(counter2).y = ...
                            tempY(find(~isnan(tempY)));
                        instStruct = instandSpeed(tempx(tempIdx),...
                            tempy(tempIdx));
                        
%                         AreaT{counter2} = CalculateTheArea(...
%                             tempX,...
%                             tempY,...
%                             0.68,70, 20);
                        
                        instSpX = [instSpX instStruct.instSpX];
                        instSpY = [instSpY instStruct.instSpY];
                        
                        counter2 = counter2 + 1;
                    end
                    counter = counter + 1;
                else
                     dataAll{ss,cc}.trialsFix = [ dataAll{ss,cc}.trialsFix ii];
                end
            end
        end
        tempDX = {dataAll{ss,cc}.positionDC.x};
        tempDY = {dataAll{ss,cc}.positionDC.y};
        forDSQCalc.x = tempDX;
        forDSQCalc.y = tempDY;
        xAllSaved{ss,cc} = xAll;
        yAllSaved{ss,cc} = yAll;
        axisVal = 15;
        tempIdx = find(xAllFixation > -axisVal & xAllFixation < axisVal &...
            yAllFixation > -axisVal & yAllFixation < axisVal);
        xAllSavedF{ss,cc} = xAllFixation(tempIdx);
        yAllSavedF{ss,cc} = yAllFixation(tempIdx);

%         xAllSavedF{ss,cc} = xAllFixation;
%         yAllSavedF{ss,cc} = yAllFixation;
        tempIdx = [];
        instSavedXF{ss,cc} = instSpXF;
        instSavedYF{ss,cc} = instSpYF;
        [~,~,~, ~, ~, ...
            tempSingleSegmentDsq,~,~] = ...
            CalculateDiffusionCoef(struct('x',forDSQCalc.x, 'y', forDSQCalc.y));
        idx2 = [];
        for s = 4:size(tempSingleSegmentDsq)
            %         if isempty(find(tempSingleSegmentDsq(s,:) > 65 | tempSingleSegmentDsq(s,100) > 40)) %Janis
            if strcmp(subjectsAll{1},'A188')
                if isempty(find(tempSingleSegmentDsq(s,:) > 120 | tempSingleSegmentDsq(s,40) > 15)) %Sanjana
                    idx2(s) = 1;
                else
                    idx2(s) = 0;
                end
            elseif  strcmp(subjectsAll{1},'Sanjana')
                if isempty(find(tempSingleSegmentDsq(s,:) > 120 | tempSingleSegmentDsq(s,50) > 20)) %Sanjana
                    idx2(s) = 1;
                else
                    idx2(s) = 0;
                end
            else
                if isempty(find(isnan(tempSingleSegmentDsq(s,:)))) %Sanjana
                    idx2(s) = 1;
                    if isempty(find(tempSingleSegmentDsq(s,:) > 120))
                        idx2(s) = 1;
                    else
                        idx2(s) = 0;
                    end
                else
                    idx2(s) = 0;
                end
            end
        end
        forDSQCalc.x = tempDX(find(idx2));
        forDSQCalc.y = tempDY(find(idx2));
        
        [~,dataAll{ss,cc}.Bias,dataAll{ss,cc}.dCoefDsq, ~, dataAll{ss,cc}.Dsq, ...
            dataAll{ss,cc}.SingleSegmentDsq,~,~] = ...
            CalculateDiffusionCoef(struct('x',forDSQCalc.x, 'y', forDSQCalc.y));
        temp = [];

        dataAll{ss,cc}.driftInstSpX = instSpX;
        dataAll{ss,cc}.driftInstSpY = instSpY;
        DC{cc,ss} = [dataAll{ss,cc}.dCoefDsq];
        Amp{cc,ss} =  dataAll{ss,cc}.driftAmp(find(dataAll{ss,cc}.driftAmp < 25));
        Ang{cc,ss} =  dataAll{ss,cc}.driftAngle(find(dataAll{ss,cc}.driftAmp < 25));
        val1 = xAll; val2 = yAll;
        smallerIdx = find(val1 < 200 & val1 > -200 & val2 < 200 & val2 > -200);
        AreaT{ss,cc} = (CalculateTheArea(xAll(smallerIdx),yAll(smallerIdx),...
                            0.30, 72/2, 20));
        val1 = xAllFixation; val2 = yAllFixation;
        smallerIdx = find(val1 < 200 & val1 > -200 & val2 < 200 & val2 > -200);
        AreaF{ss,cc} = CalculateTheArea(xAllFixation(smallerIdx),...
            yAllFixation(smallerIdx),...
            0.30, 72/2, 20);
        
%         figure;
%         plot(xAll(smallerIdx),yAll(smallerIdx))
        forNDHistIdx = find(~isnan(dataAll{ss, cc}.driftInstSpX) &...
            dataAll{ss, cc}.driftInstSpX < 200 & dataAll{ss, cc}.driftInstSpX > -200 &...
            dataAll{ss, cc}.driftInstSpY < 200 & dataAll{ss, cc}.driftInstSpY > -200);
        forNDHistX = dataAll{ss, cc}.driftInstSpX(forNDHistIdx);
        forNDHistY = dataAll{ss, cc}.driftInstSpY(forNDHistIdx);
        figure;
        subplot(1,2,1)
        ndhist(forNDHistX, forNDHistY,...
            'bins',2, 'radial','axis',[-200 200 -200 200],'nr','filt');
        title(sprintf('%s %s - Task, Drift',subjectsAll{ss},  conditionsAll{cc}))
        
        forNDHistIdxF = find(~isnan(instSavedXF{ss,cc}) &...
            instSavedYF{ss,cc} < 200 & instSavedYF{ss,cc} > -200 &...
            instSavedXF{ss,cc} < 200 & instSavedXF{ss,cc} > -200);
        forNDHistXF = instSavedXF{ss,cc}(forNDHistIdxF);
        forNDHistYF = instSavedYF{ss,cc}(forNDHistIdxF);
        %     figure;
        subplot(1,2,2)
        ndhist(forNDHistXF, forNDHistYF,...
            'bins',2, 'radial','axis',[-200 200 -200 200],'nr','filt');
        title(sprintf('%s %s - Fixation, Drift',subjectsAll{ss},  conditionsAll{cc}))
        
    end
end

[h,p] = ttest2([DC{1,:}],[DC{2,:}]);
[h,pAT] = ttest([AreaT{:,1}],[AreaT{:,2}]);
[h,pAF] = ttest([AreaF{:,1}],[AreaF{:,2}]);
figure;
errorbar([0 1 2 3],[mean([AreaT{:,1}]),mean([AreaT{:,2}]),...
    mean([AreaF{:,1}]),mean([AreaF{:,2}])],...
    [sem([AreaT{:,1}]),sem([AreaT{:,2}]),...
    sem([AreaF{:,1}]),sem([AreaF{:,2}])],'-o');
xlim([-.5 3.5])
xticks([0 1 2 3]);
xticklabels({'Task Normal','Task Scotoma','Fix Normal','Fix Scotoma'});
ylabel('30/% Area')
%% Amp and Anlge
% figure;
% for ss = 1:length(subjectsAll)
%     subplot(2,3,ss)
%     histogram(Amp{1,ss});
%     hold on
%     histogram(Amp{2,ss});
%     %     plot([0 1], [nanmean(Amp{1,cc}) nanmean(Amp{2,cc})],'-o');
%     %     hold on
%     %     AmpAll(1,cc) = nanmean(Amp{1,cc}) ;
%     %     AmpAll(2,cc) = nanmean(Amp{2,cc}) ;
% end
% title('Amplitude of Drift');
% [h,p] = ttest(AmpAll(1,:),AmpAll(2,:));
% 

for ss = 1:length(subjectsAll)
    figure;
    subplot(1,2,1)
    idx = intersect(...
        find(dataAll{1,ss}.Scotoma.fixation),dataAll{ss,1}.trialsFix);
    polarhistogram(dataAll{ss,2}.driftAngle(idx < length(dataAll{ss,2}.driftAngle)));
    hold on
    
    idx = intersect(...
        find(dataAll{1,ss}.Scotoma.fixation),dataAll{ss,1}.trialsFix);
    polarhistogram(dataAll{ss,1}.driftAngle(idx < length(dataAll{ss,1}.driftAngle)));
    
    title('Drift')
   
    subplot(1,2,2)

     [val,pos2]=intersect(find(dataAll{1,ss}.Scotoma.fixation),...
         dataAll{1,ss}.Scotoma.msAll.trial);
    theta2 = dataAll{1,ss}.Scotoma.msAll.angle(pos2');
    [vals2,idxs] = sort(round(theta2,2));
    polarhistogram(vals2,'EdgeColor','b');
    
    hold on
    [val,pos1]=intersect(find(dataAll{1,ss}.Control.fixation),dataAll{1,ss}.Control.msAll.trial);
    theta = dataAll{1,ss}.Control.msAll.angle(pos1');
    [vals,idxs] = sort(round(theta,2));
    polarhistogram(vals,'EdgeColor','r');
    title('MS')
    %     plot([0 1], [nanmean(Amp{1,cc}) nanmean(Amp{2,cc})],'-o');
    %     hold on
    %     AmpAll(1,cc) = nanmean(Amp{1,cc}) ;
    %     AmpAll(2,cc) = nanmean(Amp{2,cc}) ;
    suptitle(subjectsAll{ss})

    saveas(gcf,sprintf('../../../Documents/Overleaf_MiniScotoma/figures/AngleMSandDrift%s.png',subjectsAll{ss}));
    saveas(gcf,sprintf('../../../Documents/Overleaf_MiniScotoma/figures/AngleMSandDrift%s.epsc',subjectsAll{ss}));
end
% title('Angle of Drift');

% figure;
% for ss = 1:length(subjectsAll)
%     subplot(2,3,ss)
%     polarhistogram(Ang{1,ss});
%     hold on
%     polarhistogram(Ang{2,ss});
%     %     plot([0 1], [nanmean(Amp{1,cc}) nanmean(Amp{2,cc})],'-o');
%     %     hold on
%     %     AmpAll(1,cc) = nanmean(Amp{1,cc}) ;
%     %     AmpAll(2,cc) = nanmean(Amp{2,cc}) ;
% end

%%
[pAll, allValsC, allValsS, K] = distributionMapsEYX(subjectsAll, yAllSavedF, xAllSavedF,...
    meanYFixTrial, meanXFixTrial);

allValsC;
% % % [pF,DF] = kstest2d([mean(K.meanBinNormCX);mean(K.meanBinNormCY)]',...
% % %     [mean(K.meanBinNormSX);mean(K.meanBinNormSY)]');
% % % 
% % % 
[pF,DF] = hotell2([mean(K.meanBinNormCX);mean(K.meanBinNormSX)]',...
    [mean(K.meanBinNormCY);mean(K.meanBinNormSY)]');
% % % 
for pp = 1:length(subjectsAll)
    temp1(pp) = median(eucDist((xAllSavedF{pp,1}),(yAllSavedF{pp,1})));
    temp2(pp) = median(eucDist((xAllSavedF{pp,2}),(yAllSavedF{pp,2})));
end
[h,p] = ttest(temp1,temp2);

% [pAll, allValsC, allValsS, S] = distributionMapsEYX(subjectsAll, yAllSaved, xAllSaved, ...
%     meanYTrial, meanXTrial);

eucDistC = [];
eucDistS = [];
for ii = 1:length(subjectsAll)
    DistCX(ii) = (mean(xAllSavedF{ii,1}));
    DistSX(ii) = (mean(xAllSavedF{ii,2}));
    DistCY(ii) = (mean(yAllSavedF{ii,1}));
    DistSY(ii) = (mean(yAllSavedF{ii,2}));
    
%     polyin = polyshape({xAllSavedF{ii,1},xAllSavedF{ii,2}},...
%         {yAllSavedF{ii,1},yAllSavedF{ii,2}});
%     [x,y] = centroid(polyin,[1 2]);
%     tempE(ii) = eucDist(x(2),y(2));
end


eucDistC = eucDist(DistCX, DistCY);
eucDistS = eucDist(DistSX, DistSY);

[h,p] = ttest(eucDistC,eucDistS);

[pF,DF] = hotell2([DistCX;DistCY]',...
    [DistSX;DistSY]');

figure;
bar([mean(eucDistC);mean(eucDistS)]);
% ylim([6 12]);
hold on;
errorbar([1 2],[mean(eucDistC) mean(eucDistS)],[sem(eucDistC) sem(eucDistS)],...
    'Color','k','LineStyle','none')
ylabel('Euclidean Offset (arcmin)');
xticklabels({'Control','Scotoma'});
xlabel('Fixation Condition')

% % % % % % %  xcAllInd = [];
% % % % % % %     ycAllInd = [];
% % % % % % %     xsAllInd = [];
% % % % % % %     ysAllInd = [];
% % % % % % %     [~,numCols] = size(time{ii}.XFC{1});
% % % % % % %     for t = 1:numCols
% % % % % % %         xcAll = []; ycAll = [];
% % % % % % %         for i = 1:length(time{ii}.XFC)
% % % % % % %             xcAll = [xcAll time{ii}.XFC{1,i}(:,t)'];
% % % % % % %             ycAll = [ycAll time{ii}.YFC{1,i}(:,t)'];
% % % % % % %         end
% % % % % % %         xcAllInd(t,:) = xcAll;
% % % % % % %         ycAllInd(t,:) = ycAll;
% % % % % % %     end
% % % % % % %     [~,numCols] = size(time{ii}.XFS{1});
% % % % % % %     for t = 1:numCols
% % % % % % %         xsAll = []; ysAll = [];
% % % % % % %         for i = 1:length(time{ii}.XFC)
% % % % % % %             xsAll = [xsAll time{ii}.XFS{1,i}(:,t)'];
% % % % % % %             ysAll = [ysAll time{ii}.YFS{1,i}(:,t)'];
% % % % % % %         end
% % % % % % %         xsAllInd(t,:) = xsAll;
% % % % % % %         ysAllInd(t,:) = ysAll;
% % % % % % %     end
%%% Check to see which bins have the largest difference.
meansEucS=[];
meansEucC = [];

figure;
counter = 0;
for ii = 1:length(subjectsAll)
    counter = counter + 1;
    subplot(7,2,counter)
    legs(1) = plot(1:size(dataAll{1,ii}.Control.allXCleanFIXmat'), ...
        mean(dataAll{1,ii}.Control.allXCleanFIXmat),...
        '-');
    hold on
    legs(2) = plot(1:size(dataAll{1,ii}.Control.allYCleanFIXmat'), ...
        mean(dataAll{1,ii}.Control.allYCleanFIXmat),...
        '-');
%     title('Control')
%     legend(legs,{'X','Y'},'Location','northwest');
    ylim([-7 7])
    hold on
    counter = counter + 1;
    subplot(7,2,counter)
   legs(1) = plot(1:size(dataAll{1,ii}.Scotoma.allXCleanFIXmat'), ...
        mean(dataAll{1,ii}.Scotoma.allXCleanFIXmat),...
        '-');
    hold on
    legs(2) = plot(1:size(dataAll{1,ii}.Scotoma.allYCleanFIXmat'), ...
        mean(dataAll{1,ii}.Scotoma.allYCleanFIXmat),...
        '-');
     ylim([-7 7])

%     title('Scotoma')
%     suptitle(sprintf('%s',subjectsAll{ii}))
end
subplot(7,2,1)
title('Control')
legend(legs,{'X','Y'},'Location','northwest');
xlabel('Samples');
ylabel('Average Position across Trials (arcmin)')
subplot(7,2,2)
title('Scotoma')


for ii = 1:length(subjectsAll)
    for i = 1:length(time{ii}.XFS)
        meansEucS(ii,i) = mean(eucDist(time{ii}.XFS{i}(:)', time{ii}.YFS{i}(:)'));
        meansEucC(ii,i) = mean(eucDist(time{ii}.XFC{i}(:)', time{ii}.YFC{i}(:)'));
    end
    temp = meansEucS(ii,:)-meansEucC(ii,:);
    biggestVals = maxk(temp,16);
    for k = 1:16
        bigestTimeBin(ii,k) = find(temp == biggestVals(k));
    end
end


for ii=1:length(subjectsAll)
    timeSXX = []; timeSYY = [];
    timeCXX = []; timeCYY = [];
    for combsTime = sort(bigestTimeBin(ii,:))
        timeSXX = [timeSXX time{ii}.XFS{combsTime}(:)'];
        timeSYY = [timeSYY time{ii}.YFS{combsTime}(:)'];
        timeCXX = [timeCXX time{ii}.XFC{combsTime}(:)'];
        timeCYY = [timeCYY time{ii}.YFC{combsTime}(:)'];
    end
    timeSX{ii} =  timeSXX;
    timeSY{ii} =  timeSYY;
    timeCX{ii} =  timeCXX;
    timeCY{ii} =  timeCYY;
    
end

for ii = 1:length(subjectsAll)
    x=[];y=[]; x2=[];y2=[];
    tempC = [];
    tempS = [];
    figure;
    subplot(2,2,1)
    
    sizeNVec = size(time{ii}.XFS{71});
%   timeCX = xAllSavedF{ii,1};
%   timeCY = yAllSavedF{ii,1};%, ... , ...
%   timeSX = xAllSavedF{ii,2};
%   timeSY = yAllSavedF{ii,2};%, ... , ...
%    , ...
    tempC = generateHeatMapSimple( ...
         timeCX{ii}, ...
         timeCY{ii}, ...
        'Bins', 10*2,...
        'StimulusSize', 7,...
        'AxisValue', 14,...
        'Uncrowded', 4,...
        'Borders', 1);
%     tempC = generateHeatMapSimple( ...
%         xAllSavedF{ii,1}, ...
%         yAllSavedF{ii,1}, ...
%         'Bins', 10*2,...
%         'StimulusSize', 7,...
%         'AxisValue', 14,...
%         'Uncrowded', 4,...
%         'Borders', 1);
    title('Control')
    [xC,yC]=find(tempC==1);
    distHMP_C(ii) = eucDist(xC(1)-7,yC(1)-7)/2;
    text(5,-5,sprintf('HM = %.2f',distHMP_C(ii)));
    text(5,-8,sprintf('Euc = %.2f',mean(eucDist(timeCX{ii},timeCY{ii}))));
    
    subplot(2,2,2)
    tempS = generateHeatMapSimple( ...
         timeSX{ii}, ...
         timeSY{ii}, ...
        'Bins', 10*2,...
        'StimulusSize', 7,...
        'AxisValue', 14,...
        'Uncrowded', 4,...
        'Borders', 1);
%     tempS = generateHeatMapSimple( ...
%         xAllSavedF{ii,2}, ...
%         yAllSavedF{ii,2}, ...
%         'Bins', 10*2,...
%         'StimulusSize', 7,...
%         'AxisValue', 14,...
%         'Uncrowded', 4,...
%         'Borders', 1);
    title('Scotoma')
    [xS,yS]=find(tempS==1);
    distHMP_S(ii) = eucDist(xS(1)-7,yS(1)-7)/2;
    text(5,-5,sprintf('HM = %.2f',distHMP_S(ii)));
    text(5,-8,sprintf('Euc = %.2f',mean(eucDist(timeSX{ii},timeSY{ii}))));
    [p(ii),DF] = hotell2(tempC,tempS);
    
    x1 = [min(timeCX{ii}) max(timeCX{ii})];
    x2 = [min(timeSX{ii}) max(timeSX{ii})];
    y1 = [min(timeCY{ii}) max(timeCY{ii})];
    y2 = [min(timeSY{ii}) max(timeSY{ii})];
    
    subplot(2,2,[3 4])
    [tempC]=ellipseXY(timeCX{ii}', timeCY{ii}', 68, [150 150 150]/255,0);
    [tempS]=ellipseXY(timeSX{ii}', timeSY{ii}', 68, [150 150 150]/255,0);
    
    x1 = [tempC.Position(1) tempC.Position(1) ...
        tempC.Position(1)+tempC.Position(3) tempC.Position(1)+tempC.Position(3)];
    x2= [tempS.Position(1) tempS.Position(1) ...
        tempS.Position(1)+tempS.Position(3) tempS.Position(1)+tempS.Position(3)];
    y1= [tempC.Position(2)+tempC.Position(4) tempC.Position(2)  ...
        tempC.Position(2) tempC.Position(2)+tempC.Position(4)];
    y2= [tempS.Position(2)+tempS.Position(4) tempS.Position(2)  ...
        tempS.Position(2) tempS.Position(2)+tempS.Position(4)];
    
    % figure;
    polyin = polyshape(x1,y1);
    [x,y] = centroid(polyin);
    hold on
    legs(1) = plot(x,y,'r*')
    centroidDistC(ii) = eucDist(x,y);
    
    polyin = polyshape(x2,y2);
    [x,y] = centroid(polyin);
    hold on
    legs(2) = plot(x,y,'b*')
    centroidDistS(ii) = eucDist(x,y);
    axis([-15 15 -15 15]);
    line([-0 0],[-15 15]);
    line([-15 15], [-0 0]);
    axis square
    legend(legs,{'Control','Scotoma'});
    %     subplot(2,2,3)
    %     h3(1) = histogram(xAllSavedF{ii,1});
    %     hold on
    %     h3(2) = histogram(xAllSavedF{ii,2});
    %     legend(h3,{'Control','Scotoma'});
    %     title('X');
    %     xlim([-axisVal axisVal])
    %     subplot(2,2,4)
    %     histogram(yAllSavedF{ii,1})
    %     hold on
    %     histogram(yAllSavedF{ii,2})
    %     xlim([-axisVal axisVal])
    %     title('Y');
    suptitle(subjectsAll{ii});
end

[h,pDC] = ttest(centroidDistC([1:4,6:7]),centroidDistS([1:4,6:7]))

[h,p] = ttest(distHMP_C,distHMP_S)

figure;
bar([mean(centroidDistC);mean(centroidDistS)]');
hold on
er = errorbar([1 2],[mean(centroidDistC) mean(centroidDistS)],...
    [sem(centroidDistC) sem(centroidDistS)],...
    'Color','k','LineStyle','none')
% ylim([0 20])
 xticklabels({'Control','Scotoma'});
     ylabel({'Euclidean Distance from 0,0 (arcmin)','for 68% centroid of gaze'});
 xlabel('Fixation Condition')
title(sprintf('p = %.3f',pDC));
 
slectedCentralRegion = [];
slectedCentralRegion(1,:) = [-40:0];
slectedCentralRegion(2,:) = [0:40];
slectedCentralRegion(3,:) = [0:40];
slectedCentralRegion(4,:) = [0:40];
slectedCentralRegion(5,:) = [0:40];
slectedCentralRegion(6,:) = [-40:0];
slectedCentralRegion(7,:) = [0:40];
slectedCentralRegion = slectedCentralRegion + 120;
counter = 1;
for ii = [1:5,length(subjectsAll)]
    xScot(counter) = mean(K.meanBinNormSX(ii,slectedCentralRegion(ii,:)));
    yScot(counter) = mean(K.meanBinNormSY(ii,slectedCentralRegion(ii,:)));
    xCont(counter) = mean(K.meanBinNormCX(ii,slectedCentralRegion(ii,:)));
    yCont(counter) = mean(K.meanBinNormCY(ii,slectedCentralRegion(ii,:)));
    counter = counter + 1;
end
eDistS = eucDist(xScot,yScot)%*4; %%multiplication factor from 240 bins to 60arcmin
eDistC = eucDist(xCont,yCont)%*4;

figure;
forleg(1) = plot(eDistS,eDistC,'o','MarkerFaceColor','b');
hold on
line([0,1],[0,1])
forleg(2) = plot(mean(eDistS),mean(eDistC),'d','MarkerSize',10,'MarkerFaceColor','r');
[h,p] = ttest(eDistS,eDistC);
xlabel({'Probability of Gaze Distance from 0','Scotoma'});
ylabel({'Probability of Gaze Distance from 0','Control'});

text(0.4,0.1,'gaze further away from 0 in Scotoma');
text(0.1,0.8,'gaze further away from 0 in Control');
title(sprintf('p = %.2f',p));
axis square
legend(forleg,{'Individual Subject','Average'});

axis([0 1 0 1]);

figure;
bar([median(eDistC);median(eDistS)]');
hold on
er = errorbar([1 2],[median(eDistC) median(eDistS)],[sem(eDistC) sem(eDistS)],...
    'Color','k','LineStyle','none')
ylim([2 2.8])
 xticklabels({'Control','Scotoma'});
 ylabel('Normalized Euclidean Distance from 0,0 (arcmin)');
 yticks([2 2.2 2.4 2.6 2.8])
 xlabel('Fixation Condition')
% [p,D] = hotell2([xCont,yCont]',[xScot,yScot]')
% 
% [p,D] = hotell2([mean(S.meanBinNormCX(:,slectedCentralRegion)');...
%     mean(S.meanBinNormSX(:,slectedCentralRegion)')]',...
%     [mean(S.meanBinNormCY(:,slectedCentralRegion)');...
%     mean(S.meanBinNormSY(:,slectedCentralRegion)')]');

% end
% for pp = 1:6
% dif(pp) = abs(find(K.meanBinNormCE(pp,:) == 1)-120)*4;
% 
% end
