function [newVals] = selectWindowFromVectorAverage(vector,val,windowSize)

if (val + windowSize) > length(vector) 
    extreme = length(vector) ;
    newVals = val-windowSize:extreme;
    
elseif val - windowSize < 0
    extreme = 1;
    newVals = extreme:val+windowSize;
    
else
    newVals = val-windowSize:val+windowSize;
end