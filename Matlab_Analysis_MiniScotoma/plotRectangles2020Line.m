function plotRectangles2020Line(ppTrial,colorsAdd)

orientations = [ppTrial.Responses.orientation1,...
    ppTrial.Responses.orientation2,...
    ppTrial.Responses.orientation3,...
    ppTrial.Responses.orientation4,...
    ppTrial.Responses.orientation5];

answers = [ppTrial.Responses.response1,...
    ppTrial.Responses.response2,...
    ppTrial.Responses.response3,...
    ppTrial.Responses.response4,...
    ppTrial.Responses.response5];

timeLengthTrial = ppTrial.TimeStimulusOFF - ppTrial.TimeStimulusON;
% text(-30,30,sprintf('Time = %i ms',timeLengthTrial));

hold on;
if ppTrial.FixationTrial
    rectangle('Position',[-3.5,-3.5,7,7]);
else
    for ii = 1:5
        if ii == 1
            if orientations(ii) == 0
                if colorsAdd
                    if answers(ii) == 1
                        colorTemp = [0 1 0];
                    else
                        colorTemp = [1 0 0];
                    end
                else
                    colorTemp = [0 0 0];
                end
                rectangle('Position',[-2.5-20,-2.5,1,5],'FaceColor',colorTemp,'EdgeColor',colorTemp)
                rectangle('Position',[-2.5-20,-2.5,5,1],'FaceColor',colorTemp,'EdgeColor',colorTemp)
                rectangle('Position',[-2.5-20,-2.5+2,5,1],'FaceColor',colorTemp,'EdgeColor',colorTemp)
                rectangle('Position',[-2.5-20,-2.5+4,5,1],'FaceColor',colorTemp,'EdgeColor',colorTemp)
            elseif orientations(ii) == 90
                if colorsAdd
                    if answers(ii) == 1
                        colorTemp = [0 1 0];
                    else
                        colorTemp = [1 0 0];
                    end
                else
                    colorTemp = [0 0 0];
                end
                rectangle('Position',[-2.5-20,-2.5,5,1],'FaceColor',colorTemp,'EdgeColor',colorTemp)
                rectangle('Position',[-2.5-20,-2.5,1,5],'FaceColor',colorTemp,'EdgeColor',colorTemp)
                rectangle('Position',[-2.5-20+2,-2.5,1,5],'FaceColor',colorTemp,'EdgeColor',colorTemp)
                rectangle('Position',[-2.5-20+4,-2.5,1,5],'FaceColor',colorTemp,'EdgeColor',colorTemp)
            elseif orientations(ii) == 180
                if colorsAdd
                    if answers(ii) == 1
                        colorTemp = [0 1 0];
                    else
                        colorTemp = [1 0 0];
                    end
                else
                    colorTemp = [0 0 0];
                end
                rectangle('Position',[-2.5-20+4,-2.5,1,5],'FaceColor',colorTemp,'EdgeColor',colorTemp)
                rectangle('Position',[-2.5-20,-2.5,5,1],'FaceColor',colorTemp,'EdgeColor',colorTemp)
                rectangle('Position',[-2.5-20,-2.5+2,5,1],'FaceColor',colorTemp,'EdgeColor',colorTemp)
                rectangle('Position',[-2.5-20,-2.5+4,5,1],'FaceColor',colorTemp,'EdgeColor',colorTemp)
            elseif orientations(ii) == 270
                if colorsAdd
                    if answers(ii) == 1
                        colorTemp = [0 1 0];
                    else
                        colorTemp = [1 0 0];
                    end
                else
                    colorTemp = [0 0 0];
                end
                rectangle('Position',[-2.5-20,-2.5+4,5,1],'FaceColor',colorTemp,'EdgeColor',colorTemp)
                rectangle('Position',[-2.5-20,-2.5,1,5],'FaceColor',colorTemp,'EdgeColor',colorTemp)
                rectangle('Position',[-2.5-20+2,-2.5,1,5],'FaceColor',colorTemp,'EdgeColor',colorTemp)
                rectangle('Position',[-2.5-20+4,-2.5,1,5],'FaceColor',colorTemp,'EdgeColor',colorTemp)
            end
        elseif ii == 2
            if orientations(ii) == 0
                if colorsAdd
                    if answers(ii) == 1
                        colorTemp = [0 1 0];
                    else
                        colorTemp = [1 0 0];
                    end
                else
                    colorTemp = [0 0 0];
                end
                rectangle('Position',[-2.5-10,-2.5,1,5],'FaceColor',colorTemp,'EdgeColor',colorTemp)
                rectangle('Position',[-2.5-10,-2.5,5,1],'FaceColor',colorTemp,'EdgeColor',colorTemp)
                rectangle('Position',[-2.5-10,-2.5+2,5,1],'FaceColor',colorTemp,'EdgeColor',colorTemp)
                rectangle('Position',[-2.5-10,-2.5+4,5,1],'FaceColor',colorTemp,'EdgeColor',colorTemp)
            elseif orientations(ii) == 90
                if colorsAdd
                    if answers(ii) == 1
                        colorTemp = [0 1 0];
                    else
                        colorTemp = [1 0 0];
                    end
                else
                    colorTemp = [0 0 0];
                end
                rectangle('Position',[-2.5-10,-2.5,5,1],'FaceColor',colorTemp,'EdgeColor',colorTemp)
                rectangle('Position',[-2.5-10,-2.5,1,5],'FaceColor',colorTemp,'EdgeColor',colorTemp)
                rectangle('Position',[-2.5-10+2,-2.5,1,5],'FaceColor',colorTemp,'EdgeColor',colorTemp)
                rectangle('Position',[-2.5-10+4,-2.5,1,5],'FaceColor',colorTemp,'EdgeColor',colorTemp)
            elseif orientations(ii) == 180
                if colorsAdd
                    if answers(ii) == 1
                        colorTemp = [0 1 0];
                    else
                        colorTemp = [1 0 0];
                    end
                else
                    colorTemp = [0 0 0];
                end
                
                rectangle('Position',[-2.5-10+4,-2.5,1,5],'FaceColor',colorTemp,'EdgeColor',colorTemp)
                rectangle('Position',[-2.5-10,-2.5,5,1],'FaceColor',colorTemp,'EdgeColor',colorTemp)
                rectangle('Position',[-2.5-10,-2.5+2,5,1],'FaceColor',colorTemp,'EdgeColor',colorTemp)
                rectangle('Position',[-2.5-10,-2.5+4,5,1],'FaceColor',colorTemp,'EdgeColor',colorTemp)
            elseif orientations(ii) == 270
                if colorsAdd
                    if answers(ii) == 1
                        colorTemp = [0 1 0];
                    else
                        colorTemp = [1 0 0];
                    end
                else
                    colorTemp = [0 0 0];
                end
                rectangle('Position',[-2.5-10,-2.5+4,5,1],'FaceColor',colorTemp,'EdgeColor',colorTemp)
                rectangle('Position',[-2.5-10,-2.5,1,5],'FaceColor',colorTemp,'EdgeColor',colorTemp)
                rectangle('Position',[-2.5-10+2,-2.5,1,5],'FaceColor',colorTemp,'EdgeColor',colorTemp)
                rectangle('Position',[-2.5-10+4,-2.5,1,5],'FaceColor',colorTemp,'EdgeColor',colorTemp)
            end
            
        elseif ii == 3
            if orientations(ii) == 0
                if colorsAdd
                    if answers(ii) == 1
                        colorTemp = [0 1 0];
                    else
                        colorTemp = [1 0 0];
                    end
                else
                    colorTemp = [0 0 0];
                end
                rectangle('Position',[-2.5,-2.5,1,5],'FaceColor',colorTemp,'EdgeColor',colorTemp)
                rectangle('Position',[-2.5,-2.5,5,1],'FaceColor',colorTemp,'EdgeColor',colorTemp)
                rectangle('Position',[-2.5,-2.5+2,5,1],'FaceColor',colorTemp,'EdgeColor',colorTemp)
                rectangle('Position',[-2.5,-2.5+4,5,1],'FaceColor',colorTemp,'EdgeColor',colorTemp)
            elseif orientations(ii) == 90
                if colorsAdd
                    if answers(ii) == 1
                        colorTemp = [0 1 0];
                    else
                        colorTemp = [1 0 0];
                    end
                else
                    colorTemp = [0 0 0];
                end
                rectangle('Position',[-2.5,-2.5,5,1],'FaceColor',colorTemp,'EdgeColor',colorTemp)
                rectangle('Position',[-2.5,-2.5,1,5],'FaceColor',colorTemp,'EdgeColor',colorTemp)
                rectangle('Position',[-2.5+2,-2.5,1,5],'FaceColor',colorTemp,'EdgeColor',colorTemp)
                rectangle('Position',[-2.5+4,-2.5,1,5],'FaceColor',colorTemp,'EdgeColor',colorTemp)
            elseif orientations(ii) == 180
                if colorsAdd
                    if answers(ii) == 1
                        colorTemp = [0 1 0];
                    else
                        colorTemp = [1 0 0];
                    end
                else
                    colorTemp = [0 0 0];
                end
                
                rectangle('Position',[-2.5+4,-2.5,1,5],'FaceColor',colorTemp,'EdgeColor',colorTemp)
                rectangle('Position',[-2.5,-2.5,5,1],'FaceColor',colorTemp,'EdgeColor',colorTemp)
                rectangle('Position',[-2.5,-2.5+2,5,1],'FaceColor',colorTemp,'EdgeColor',colorTemp)
                rectangle('Position',[-2.5,-2.5+4,5,1],'FaceColor',colorTemp,'EdgeColor',colorTemp)
            elseif orientations(ii) == 270
                if colorsAdd
                    if answers(ii) == 1
                        colorTemp = [0 1 0];
                    else
                        colorTemp = [1 0 0];
                    end
                else
                    colorTemp = [0 0 0];
                end
                
                rectangle('Position',[-2.5,-2.5+4,5,1],'FaceColor',colorTemp,'EdgeColor',colorTemp)
                rectangle('Position',[-2.5,-2.5,1,5],'FaceColor',colorTemp,'EdgeColor',colorTemp)
                rectangle('Position',[-2.5+2,-2.5,1,5],'FaceColor',colorTemp,'EdgeColor',colorTemp)
                rectangle('Position',[-2.5+4,-2.5,1,5],'FaceColor',colorTemp,'EdgeColor',colorTemp)
            end
        elseif ii == 4
            if orientations(ii) == 0
                if colorsAdd
                    if answers(ii) == 1
                        colorTemp = [0 1 0];
                    else
                        colorTemp = [1 0 0];
                    end
                else
                    colorTemp = [0 0 0];
                end
                rectangle('Position',[-2.5+10,-2.5,1,5],'FaceColor',colorTemp,'EdgeColor',colorTemp)
                rectangle('Position',[-2.5+10,-2.5,5,1],'FaceColor',colorTemp,'EdgeColor',colorTemp)
                rectangle('Position',[-2.5+10,-2.5+2,5,1],'FaceColor',colorTemp,'EdgeColor',colorTemp)
                rectangle('Position',[-2.5+10,-2.5+4,5,1],'FaceColor',colorTemp,'EdgeColor',colorTemp)
            elseif orientations(ii) == 90
                if colorsAdd
                    if answers(ii) == 1
                        colorTemp = [0 1 0];
                    else
                        colorTemp = [1 0 0];
                    end
                else
                    colorTemp = [0 0 0];
                end
                rectangle('Position',[-2.5+10,-2.5,5,1],'FaceColor',colorTemp,'EdgeColor',colorTemp)
                rectangle('Position',[-2.5+10,-2.5,1,5],'FaceColor',colorTemp,'EdgeColor',colorTemp)
                rectangle('Position',[-2.5+10+2,-2.5,1,5],'FaceColor',colorTemp,'EdgeColor',colorTemp)
                rectangle('Position',[-2.5+10+4,-2.5,1,5],'FaceColor',colorTemp,'EdgeColor',colorTemp)
            elseif orientations(ii) == 180
                if colorsAdd
                    if answers(ii) == 1
                        colorTemp = [0 1 0];
                    else
                        colorTemp = [1 0 0];
                    end
                else
                    colorTemp = [0 0 0];
                end
                
                rectangle('Position',[-2.5+10+4,-2.5,1,5],'FaceColor',colorTemp,'EdgeColor',colorTemp)
                rectangle('Position',[-2.5+10,-2.5,5,1],'FaceColor',colorTemp,'EdgeColor',colorTemp)
                rectangle('Position',[-2.5+10,-2.5+2,5,1],'FaceColor',colorTemp,'EdgeColor',colorTemp)
                rectangle('Position',[-2.5+10,-2.5+4,5,1],'FaceColor',colorTemp,'EdgeColor',colorTemp)
            elseif orientations(ii) == 270
                if colorsAdd
                    if answers(ii) == 1
                        colorTemp = [0 1 0];
                    else
                        colorTemp = [1 0 0];
                    end
                else
                    colorTemp = [0 0 0];
                end
                
                rectangle('Position',[-2.5+10,-2.5+4,5,1],'FaceColor',colorTemp,'EdgeColor',colorTemp)
                rectangle('Position',[-2.5+10,-2.5,1,5],'FaceColor',colorTemp,'EdgeColor',colorTemp)
                rectangle('Position',[-2.5+10+2,-2.5,1,5],'FaceColor',colorTemp,'EdgeColor',colorTemp)
                rectangle('Position',[-2.5+10+4,-2.5,1,5],'FaceColor',colorTemp,'EdgeColor',colorTemp)
            end
        elseif ii == 5
            if orientations(ii) == 0
                if colorsAdd
                    if answers(ii) == 1
                        colorTemp = [0 1 0];
                    else
                        colorTemp = [1 0 0];
                    end
                else
                    colorTemp = [0 0 0];
                end
                rectangle('Position',[-2.5+20,-2.5,1,5],'FaceColor',colorTemp,'EdgeColor',colorTemp)
                rectangle('Position',[-2.5+20,-2.5,5,1],'FaceColor',colorTemp,'EdgeColor',colorTemp)
                rectangle('Position',[-2.5+20,-2.5+2,5,1],'FaceColor',colorTemp,'EdgeColor',colorTemp)
                rectangle('Position',[-2.5+20,-2.5+4,5,1],'FaceColor',colorTemp,'EdgeColor',colorTemp)
            elseif orientations(ii) == 90
                if answers(ii) == 1
                    colorTemp = [0 1 0];
                else
                    colorTemp = [1 0 0];
                end
                rectangle('Position',[-2.5+20,-2.5,5,1],'FaceColor',colorTemp,'EdgeColor',colorTemp)
                rectangle('Position',[-2.5+20,-2.5,1,5],'FaceColor',colorTemp,'EdgeColor',colorTemp)
                rectangle('Position',[-2.5+20+2,-2.5,1,5],'FaceColor',colorTemp,'EdgeColor',colorTemp)
                rectangle('Position',[-2.5+20+4,-2.5,1,5],'FaceColor',colorTemp,'EdgeColor',colorTemp)
            elseif orientations(ii) == 180
                if colorsAdd
                    if answers(ii) == 1
                        colorTemp = [0 1 0];
                    else
                        colorTemp = [1 0 0];
                    end
                else
                    colorTemp = [0 0 0];
                end
                
                rectangle('Position',[-2.5+20+4,-2.5,1,5],'FaceColor',colorTemp,'EdgeColor',colorTemp)
                rectangle('Position',[-2.5+20,-2.5,5,1],'FaceColor',colorTemp,'EdgeColor',colorTemp)
                rectangle('Position',[-2.5+20,-2.5+2,5,1],'FaceColor',colorTemp,'EdgeColor',colorTemp)
                rectangle('Position',[-2.5+20,-2.5+4,5,1],'FaceColor',colorTemp,'EdgeColor',colorTemp)
            elseif orientations(ii) == 270
                if colorsAdd
                    if answers(ii) == 1
                        colorTemp = [0 1 0];
                    else
                        colorTemp = [1 0 0];
                    end
                else
                    colorTemp = [0 0 0];
                end
                
                rectangle('Position',[-2.5+20,-2.5+4,5,1],'FaceColor',colorTemp,'EdgeColor',colorTemp)
                rectangle('Position',[-2.5+20,-2.5,1,5],'FaceColor',colorTemp,'EdgeColor',colorTemp)
                rectangle('Position',[-2.5+20+2,-2.5,1,5],'FaceColor',colorTemp,'EdgeColor',colorTemp)
                rectangle('Position',[-2.5+20+4,-2.5,1,5],'FaceColor',colorTemp,'EdgeColor',colorTemp)
            end
        end
    end
end
% rectangle('Position',[-2.5,-2.5,5,5]); C
% rectangle('Position',[-2.5+10,-2.5,5,5]); R
% rectangle('Position',[-2.5+20,-2.5,5,5]); FR
% rectangle('Position',[-2.5-10,-2.5,5,5]); L
% rectangle('Position',[-2.5-20,-2.5,5,5]); Fl