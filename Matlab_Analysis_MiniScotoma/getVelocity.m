function vel = getVelocity(x,y,t)

dt = t(2:size(t,1),1)-t(1:size(t,1)-1,1);
dx = x(2:size(x,1),2)-x(1:size(x,1)-1,2);
dy = y(2:size(y,1),3)-y(1:size(y,1)-1,3);
dist = sqrt(dx.^2+dy.^2);
vel = dist./dt;
%    velx = abs(dx)./dt;
%    vely = abs(dy)./dt;