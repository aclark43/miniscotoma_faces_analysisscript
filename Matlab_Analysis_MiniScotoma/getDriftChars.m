function [spanV1, ...
    instSpX, ...
    instSpY, ...
    mn_speed, ...
    driftAngle, ...
    mn_cur, ...
    varx, ...
    vary, ...
    bcea,...
    span, ...
    amplitude, ...
    prlDistance,...
    prlDistanceX, ...
    prlDistanceY] = getDriftChars(x, y, smoothing, cutseg, maxSpeed, sRate)
% smoothing: 41
% cutseg: how much of the drift do you wanna cut from the beginning and end
% maxSpeed (arcmin): 180 

% compute drift span
mx = mean(x(cutseg:end-cutseg));
my = mean(y(cutseg:end-cutseg));
spanV1 = max(sqrt((x - mx).^2 + (y - my).^2));

x2 = 2.291; %chi-square variable with two degrees of freedom, encompasing 68% of the highest density points
bceaMatrix =  2*x2*pi * std(x) * std(y) * (1-corrcoef(x,y).^2).^0.5; %smaller BCEA correlates to more stable fixation 
bcea = abs(bceaMatrix(1,2));
if any(bcea > maxSpeed)
    bcea = nan;
end
% s-golay filtering
smx = sgfilt(x, 3, smoothing, 0);
smy = sgfilt(y, 3, smoothing, 0);
smx1 = sgfilt(x, 3, smoothing, 1);
smy1 = sgfilt(y, 3, smoothing, 1);

instSpX = smx1*sRate; instSpY = smy1*sRate;%transformed to acrmin/sec
% compute drift speed
sp = 100*sqrt(smx1(cutseg:end-cutseg).^2 + smy1(cutseg:end-cutseg).^2);

if any(sp > maxSpeed)
    mn_speed = nan;
else
    mn_speed = mean(sp);
end
% 
% compute drift curvature
tmpCur = abs(cur(smx(cutseg:end-cutseg), smy(cutseg:end-cutseg)));
tmpCur(tmpCur > 300) = NaN;
mn_cur = nanmean(tmpCur);

varx = var(x(cutseg:end-cutseg));
vary = var(y(cutseg:end-cutseg));

% basisVec = [1,0];
% driftAngle = arrayfun(@(x,y) acos(dot([x, y], basisVec)/(norm([x, y])*norm(basisVec))), instSpX, instSpY);
% driftAngle(instSpY < 0) = 2*pi - driftAngle(instSpY < 0);

driftAngle = arrayfun(@(x,y) getAngle(x,y), instSpX, instSpY);
% driftAngle = [];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
span = quantile(sqrt((x-mean(x)).^2 + (y-mean(y)).^2), .95); %Caclulates Span

amplitude = sqrt((x(end) - x(1))^2 + (y(end) - y(1))^2);

meanSpan = mean(span);
stdSpan= std(span)/sqrt(length(span));

meanAmplitude = mean(amplitude);stdAmplitude = std(amplitude)/sqrt(length(amplitude));

prlDistance = mean(sqrt(x.^2 + y.^2));
prlDistanceX = mean(abs(x));
prlDistanceY = mean(abs(y));

end