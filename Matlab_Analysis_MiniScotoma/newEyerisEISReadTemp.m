function pptrials = newEyerisEISReadTemp(pptrials)

for ii = 1:length(pptrials)
    if ~isfield(pptrials{ii},'x') 
        continue;
    elseif length(pptrials{ii}.x) < 450
        continue;
    end
    x = pptrials{ii}.x;
    y = pptrials{ii}.y;
    
    %%
    MinSaccSpeed = 180;
    MinSaccAmp = 30;
    MinMSaccSpeed = 60;
    MinMSaccAmp = 2;
    MaxMSaccAmp = 60;
    MinVelocity = 50;
    
    Blink = zeros(1,length(x));
    NoTrack = zeros(1,length(x));
    DataValid = ones(1,length(x));
    DataInvalid = zeros(1,length(x));
    Trial = createTrial(ii, x, y, Blink, NoTrack, DataValid, DataInvalid, pptrials{ii}.sRate);
    Trial = preprocessSignals(Trial, 'minvel', MinVelocity, 'noanalysis');
    %%%% 'noanalysis' keeps the saccades at the very onset of the trial
    
    % Find all valid saccades with speed greater than 3 deg/sec
    % and bigger than 30 arcmin
    Trial = findSaccades(Trial, 'minvel',MinSaccSpeed, 'minsa',MinSaccAmp);
    Trial = findMicrosaccades(Trial, 'minvel',MinMSaccSpeed,'minmsa', MinMSaccAmp,'maxmsa', MaxMSaccAmp);
    Trial = findDrifts(Trial);
    fn=fieldnames(Trial);
    
    for ff=1:size(fn,1)-2
        entry = char(fn{ff});
        pptrials{ii}.(entry) = ...
            Trial.(entry);
    end
end

end

