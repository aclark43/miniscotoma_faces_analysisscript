function [ allPPTrials ] = reBuildPPtrials (filepath, params)
%takes pptrials from all sessions and combine them for pptrial
%   This allows you not to rerun basicEISread on new data

%   filepath = filepath where your data lives - where your graphs folder is
%   located
if sum(strcmp((fieldnames(params)),'practice') == 1)
    remFirst = params.practice;
else
    remFirst = 1;
end

S = dir(filepath);
allPPTrials = [];
numSessions = sum([S(~ismember({S.name},{'.','..'})).isdir]);

if strcmp('ALL',params.session)
    for ii = 1:numSessions
        sesName = sprintf('ses%i',ii);
        sessionFilePath = sprintf('%s/%s',filepath,sesName);
        
        load(fullfile(sessionFilePath, 'pptrials.mat'), 'pptrials');
        for ii = 1:length(pptrials)
            pptrials{ii}.ses = sesName;
        end
        allPPTrials = [allPPTrials pptrials(remFirst:end)];
    end
else
    for ii = str2double(string(regexp(params.session,'\d*','Match')))
        sesName = sprintf('ses%i',ii);
        sessionFilePath = sprintf('%s/%s',filepath,sesName);
        
        load(fullfile(sessionFilePath, 'pptrials.mat'), 'pptrials');
        for ii = 1:length(pptrials)
            pptrials{ii}.ses = sesName;
        end
        allPPTrials = [allPPTrials pptrials(remFirst:end)];
    end
end
%%%Change samples to miliseconds

conversionFactor = (1000/330);
% [allPPTrials, ~] = convertEverythingInpptrialsToMs(pptrials, conversionFactor);
%allPPTrials = convertSamplesToMs(pptrials, conversionFactor);


end

