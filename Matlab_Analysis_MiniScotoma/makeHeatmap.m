function [contourPts,figH] = makeHeatmap(X,Y,histData,lims,titleStr,drawScot,level)
% plots the stimulus position heatmap with a contour drawn to encompass 68%
% of the data. Returns the vertices of the contour (column 1 is x, column 2
% is y, column 3 is z: uniform height just for plotting purposes)
contourPts = [];

% keep top 99% of data for plotting
clipLevel = getLevel(histData,99);
loops = 0;  % counter for decreasing threshold if necessary
while isempty(clipLevel)   % adjust threshold if necessary (need to find a valid endpoint)
    loops = loops+1;    % increment counter
    clipLevel = getLevel(histData,99-1*loops);
end
histDataPlot = histData;    % make copy
% histDataPlot(histDataPlot<clipLevel) = NaN; % set values below clip level to NaN

% find 68% contour line
contour68 = getContour(X,Y,histDataPlot,level);
numVertices = contour68(2,1);   % number of vertices to include
if numVertices < 9  % first entry may just be a single point, not the whole contour
    startIndex = 1;
    while numVertices < 9
        startIndex = startIndex+numVertices+1;    % new index to start at for getting all vertices
        numVertices = contour68(2,startIndex);      % update the number of vertices
    end
    contourPts(:,1) = contour68(1,startIndex+1:startIndex+numVertices)';    % x-values for contour vertices
    contourPts(:,2) = contour68(2,startIndex+1:startIndex+numVertices)';    % y-values for contour vertices
    contourPts(:,3) = ones(numVertices,1);              % z-values for contour vertices (put at 1 to make visible on top of surface plot)
else
    contourPts(:,1) = contour68(1,2:numVertices+1)';    % x-values for contour vertices
    contourPts(:,2) = contour68(2,2:numVertices+1)';    % y-values for contour vertices
    contourPts(:,3) = ones(numVertices,1);              % z-values for contour vertices  (put at 1 to make visible on top of surface plot)
end

% figH = figure;      % figure for heatmap plus contour
% surf(X,Y,histDataPlot)
% colormap('jet')
% shading('interp')
% view(2)
% set(gca,'YDir','reverse');  % reverse y-axis direction so it matches with how the data is displayed on an image
% daspect([1 1 1]);
hold on
if drawScot == 1
    plot3(contourPts(:,1),contourPts(:,2),contourPts(:,3),'m','LineWidth', 2,'LineStyle','--')
else
    plot3(contourPts(:,1),contourPts(:,2),contourPts(:,3),'b','LineWidth', 2)
end
% draw x- and y-axes to locate origin
line([0,0],[lims(3),lims(4)],[1,1],'Color','k')
line([lims(1),lims(2)],[0,0],[1,1],'Color','k')

% if drawScot == 1
%     % add rectangle for scotoma
%     plot3(scotRect(:,1),scotRect(:,2),scotRect(:,3),...
%         'LineWidth',2,'LineStyle','--','Color','k');
% end

hold off
axis(lims)
xticks([lims(1),lims(1)/2,0,lims(2)/2,lims(2)])
yticks([lims(3),lims(3)/2,0,lims(4)/2,lims(4)])
set(gca,'FontSize',10)
xlabel('X (arcmin)')
ylabel('Y (arcmin)')
title(titleStr)


end