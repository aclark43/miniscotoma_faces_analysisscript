function list = CalListSnellen()

% stimulus duration: stimOff-saccOn.
% this is because the stimulus duration 
% is calculated from the moment the eye lands.

list = eis_readData([], 'x');
list = eis_readData(list, 'y');
%list = eis_readData(list, 'trigger', 'frame'); %% just added for test
list = eis_readData(list, 'trigger', 'blink');
list = eis_readData(list, 'trigger', 'notrack');
list = eis_readData(list, 'stream', 0, 'double');
list = eis_readData(list, 'stream', 1, 'double');
list = eis_readData(list, 'stream', 2, 'double');
list = eis_readData(list, 'stream', 3, 'double');
%list = eis_readData(list, 'joypad', 'r1');

% % user variables
list = eis_readData(list, 'uservar', 'Subject_Name');
list = eis_readData(list, 'uservar', 'xoffset1');
list = eis_readData(list, 'uservar', 'yoffset1');

list = eis_readData(list, 'uservar', 'contrast');
list = eis_readData(list, 'uservar', 'pixelAngle');
list = eis_readData(list, 'uservar', 'pixelAngle0');
list = eis_readData(list, 'uservar', 'screenDist');
list = eis_readData(list, 'uservar', 'ogScreen');
list = eis_readData(list, 'uservar', 'measuredAcuity');

list = eis_readData(list, 'uservar', 'magnificationFactor');
list = eis_readData(list, 'uservar', 'letterSize');
list = eis_readData(list, 'uservar', 'strokewidthAcmin'); % this was mis-spelled in original code!
list = eis_readData(list, 'uservar', 'trialLength');
list = eis_readData(list, 'uservar', 'userexit');
list = eis_readData(list, 'uservar', 'timeUntilFixation');
list = eis_readData(list, 'uservar', 'timeRampStart');
list = eis_readData(list, 'uservar', 'timeRampStop');
list = eis_readData(list, 'uservar', 'doStabilize');
list = eis_readData(list, 'uservar', 'contrastBlock');
list = eis_readData(list, 'uservar', 'control');

list = eis_readData(list, 'uservar', 'CorrectList1');
list = eis_readData(list, 'uservar', 'CorrectList2');
list = eis_readData(list, 'uservar', 'CorrectList3');
list = eis_readData(list, 'uservar', 'CorrectList4');
list = eis_readData(list, 'uservar', 'CorrectList5');
list = eis_readData(list, 'uservar', 'CorrectList6');
list = eis_readData(list, 'uservar', 'CorrectList7');
list = eis_readData(list, 'uservar', 'CorrectList8');
list = eis_readData(list, 'uservar', 'CorrectList9');
list = eis_readData(list, 'uservar', 'CorrectList10');

list = eis_readData(list, 'uservar', 'ResponseList1');
list = eis_readData(list, 'uservar', 'ResponseList2');
list = eis_readData(list, 'uservar', 'ResponseList3');
list = eis_readData(list, 'uservar', 'ResponseList4');
list = eis_readData(list, 'uservar', 'ResponseList5');
list = eis_readData(list, 'uservar', 'ResponseList6');
list = eis_readData(list, 'uservar', 'ResponseList7');
list = eis_readData(list, 'uservar', 'ResponseList8');
list = eis_readData(list, 'uservar', 'ResponseList9');
list = eis_readData(list, 'uservar', 'ResponseList10');

list = eis_readData(list, 'uservar', 'ResponseTimeList1');
list = eis_readData(list, 'uservar', 'ResponseTimeList2');
list = eis_readData(list, 'uservar', 'ResponseTimeList3');
list = eis_readData(list, 'uservar', 'ResponseTimeList4');
list = eis_readData(list, 'uservar', 'ResponseTimeList5');
list = eis_readData(list, 'uservar', 'ResponseTimeList6');
list = eis_readData(list, 'uservar', 'ResponseTimeList7');
list = eis_readData(list, 'uservar', 'ResponseTimeList8');
list = eis_readData(list, 'uservar', 'ResponseTimeList9');
list = eis_readData(list, 'uservar', 'ResponseTimeList10');

list = eis_readData(list, 'uservar', 'TargetOrientation1');
list = eis_readData(list, 'uservar', 'TargetOrientation2');
list = eis_readData(list, 'uservar', 'TargetOrientation3');
list = eis_readData(list, 'uservar', 'TargetOrientation4');
list = eis_readData(list, 'uservar', 'TargetOrientation5');
list = eis_readData(list, 'uservar', 'TargetOrientation6');
list = eis_readData(list, 'uservar', 'TargetOrientation7');
list = eis_readData(list, 'uservar', 'TargetOrientation8');
list = eis_readData(list, 'uservar', 'TargetOrientation9');
list = eis_readData(list, 'uservar', 'TargetOrientation10');

