function trialList = myPreprocessing(trialList)
for i = 1:length(trialList)
    [~, ia] = unique(trialList{i}.blinks.start);
    flds = fields(trialList{i}.blinks);
    for fi = 1:length(flds)
        tmp = trialList{i}.blinks.(flds{fi});
        trialList{i}.blinks.(flds{fi}) = tmp(ia);
    end
    
    [~, ia] = unique(trialList{i}.notracks.start);
    flds = fields(trialList{i}.notracks);
    for fi = 1:length(flds)
        tmp = trialList{i}.notracks.(flds{fi});
        trialList{i}.notracks.(flds{fi}) = tmp(ia);
    end
end
end