clc
clear all

subject = {'Ashley'};
machine = {'DDPI'};
noEyeTraces = 0;
pathToDataOpus ='Z:\Ashley\MiniScotoma\Data\Ashley';
%%

pt = sprintf('../../../../Data/%s', subject{1});
temp = dir(pt);
counter = 1;
for numFold = 1:length(temp)
    if (strcmp('Graphs', temp(numFold).name)) || ...
            (strcmp('.', temp(numFold).name)) || ...
            (strcmp('..', temp(numFold).name)) || ...
            (strcmp('Testing', temp(numFold).name)) || ...
            (strcmp('Calibration', temp(numFold).name))
        continue;
    end
    condSession{counter,1} = (temp(numFold).name);
    counter = counter + 1;
end

if ~strcmp('DDPI',machine)
    error('This code is only for dDPI Data')
end

%% Looping through conditions
for si = 1:length(condSession)
    %%Folder containing sessions of data
    temppathtodata = fullfile(pt, condSession{si});
    
    S = dir(temppathtodata);
    numSessions = sum([S(~ismember({S.name},{'.','..'})).isdir]);
    
    clear sesName
    for ii = 1:numSessions
        sesName{ii} = sprintf('ses%i',ii);
    end
    
    for numSess = 1:length(sesName)
        pathtodata = fullfile(pt, condSession{si}, sesName{numSess});
        %%Prevents re-writing of pptrials if they've already been analyzed
        if noEyeTraces == 0
            dirT = sprintf('%s/%s/%s/pptrials.mat', pt, (condSession{si}), sesName{numSess});
        else
            dirT = sprintf('%s/%s/%s/pptrials_noEyeTraces.mat', pt, (condSession{si}), sesName{numSess});
%             
        end
% % %         if exist(dirT, 'file') == 2
% % %             %             error('This pptrial already exists. Delete it first if you want to rewrite it.');
% % %             warning('Condition %s %s already exists. Will not overwrite pptrial.', condSession{si},...
% % %                 sesName{numSess})
% % %             continue;
% % %             
% % %         end
        
        fname1 = sprintf('pptrials.mat');
        
        fprintf('%s\n', fname1);
        
        % this is the raw read in of the data
        pathToNewData = fullfile(pathToDataOpus,condSession{si});
        if noEyeTraces == 1
           data = readdata(pathToNewData, CalListFacesNoTrace());
           pptrials = data.user';

        else
            data = readdata(pathToNewData, CalListFaces());
%             pptrials = preprocessing(data);
            pptrials =  preprocessingDDPI(data, 30, 330);
        end
        
%         pptrials = myPreprocessing(pptrials);
%         if noEyeTraces == 0
%             temp_pptrials =  preprocessingDDPI(data, 30, 330);
%             pptrials = temp_pptrials;
% %             pptrials = addDebug(pathToNewData, temp_pptrials);
%         elseif noEyeTraces == 1
%         end
        
        for ii = 1:length(pptrials)
            pptrials{ii}.checkedManually = 0;
            pptrials{ii}.machine = machine;
        end
        pptrials = myPreprocessing(pptrials);
        save(fullfile(pathtodata,  fname1), 'pptrials');
    end
end

for ii = 1:length(pptrials)
    if pptrials{ii}.Response == 0
        if contains(pptrials{ii}.image,'_LA_')
            dataAll(ii) = 1; %Correct!
        else
            dataAll(ii) = 0;
        end
    else
        if pptrials{ii}.Response == 0
            dataAll(ii) = 0;
        else
            dataAll(ii) = 1;
        end
    end
end









% % 
% % 
% % 
% % 
% % % This script processes EIS files from the myopia experiments (snellen and
% % % faces tasks). It creates SUBJ_TASK_pptrials.mat in the Box/Myopia/Data
% % % folder ONLY IF the file does not already exist. 
% % % author - Janis Intoy
% % 
% % % Notes: 
% % %   To keep this code universal, this script utilizes functions pathToOPUS
% % %   and pathToBOX. Please see Janis's myUtilities project to create your
% % %   own versions of these functions.
% % 
% % % path to EIS files
% % pteis = fullfile('Z:\', 'Myopia Projects', 'DataCollection');
% % 
% % % path to save pptrials.mat
% % ptsave = fullfile('C:\Users\Ruccilab\Box', 'APLab-Projects', 'Myopia', 'Myopia Empirical', 'Data');
% % 
% % if ~exist(ptsave, 'dir')
% %     mkdir(ptsave); 
% % end
% % 
% % % get list of subject folders
% % subjects = dir(pteis);
% % 
% % for si = 1:length(subjects)
% %     if strcmp(subjects(si).name, '.') || strcmp(subjects(si).name, '..') || ...
% %          strcmp(subjects(si).name, 'Archive') || strcmp(subjects(si).name, 'AllDataCal')
% %         continue;
% %     end
% %     
% %     %% snellen data
% %     pathtodata = fullfile(pteis, subjects(si).name, 'Snellen');
% %     
% %     fname = sprintf('%s_%s_pptrials.mat', subjects(si).name, 'Snellen');
% %     
% %     if ~exist(fullfile(ptsave, fname), 'file')
% %         fprintf('=========== %s -- Snellen ============\n', subjects(si).name);
% %         
% %         % this is the raw read in of the data
% %         data = readdata(fullfile(pathtodata), CalListSnellen());
% %         
% %         % this gets the EM data - separated into blinks, drifts, saccades,...
% %         pptrials = preprocessing(data);
% %         pptrials = myPreprocessing(pptrials);
% %         save(fullfile(ptsave,  fname), 'pptrials');
% %     end
% % 
% %    
% %     %% faces data
% %     pathtodata = fullfile(pteis, subjects(si).name, 'MicroFaces176','176x176');
% %     if ~exist(pathtodata, 'dir') && exist(fullfile(pteis, subjects(si).name, 'MSFaces'), 'dir')
% % %         pathtodata = fullfile(pteis, subjects(si).name, 'MSFaces', '176x176');
% % %         pathtodata = fullfile(pteis, subjects(si).name, 'MicroFaces176', '176x176','176x176');
% % 
% %     end
% %     
% %     fname = sprintf('%s_%s_pptrials.mat', subjects(si).name, 'MicroFaces');
% %     
% %     if ~exist(fullfile(ptsave, fname), 'file')
% %         fprintf('=========== %s -- Faces ============\n', subjects(si).name);
% %         
% %         % this is the raw read in of the data
% %         data = readdata(fullfile(pathtodata), CalListFaces());
% %         
% %         % this gets the EM data - separated into blinks, drifts, saccades,...
% %         pptrials = preprocessing(data);
% %         pptrials = myPreprocessing(pptrials);
% %         save(fullfile(ptsave,  fname), 'pptrials');
% %     end
% % end