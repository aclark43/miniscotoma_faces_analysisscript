function plotOldNewSpikeRemovedTraces( trialNum, pptrials )
%UNTITLED5 Summary of this function goes here
%     look at where the trace fixed the spikes

i = trialNum;
figure;
subplot(2,1,1)
hold on
plot(1:length(pptrials{i}.x.position),pptrials{i}.x_WithSpikes.position,'-','Color','g');
subplot(2,1,2)
hold on
plot(1:length(pptrials{i}.y.position),pptrials{i}.y_WithSpikes.position,'-','Color','g');

subplot(2,1,1)
hold on
plot(1:length(pptrials{i}.x.position),pptrials{i}.x.position);
ylim([-40 40]);
title('Horizontal Traces')
ylabel('Arcmin')
xlabel('Samples')
subplot(2,1,2)
hold on
plot(1:length(pptrials{i}.y.position),pptrials{i}.y.position);
ylim([-40 40]);
title('Vertical Traces')
ylabel('Arcmin')
xlabel('Samples')

subplot(2,1,1)
hold on
plot(pptrials{i}.spikesX.start,pptrials{i}.x.position(pptrials{i}.spikesX.start),...
    '.','Color','b','MarkerSize',20);
subplot(2,1,2)
hold on
plot(pptrials{i}.spikesY.start,pptrials{i}.y.position(pptrials{i}.spikesY.start),...
    '.','Color','r','MarkerSize',20);

end

