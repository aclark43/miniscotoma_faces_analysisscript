function list = CalListFacesNoTrace()

% stimulus duration: stimOff-saccOn.
% this is because the stimulus duration 
% is calculated from the moment the eye lands.

% list = eis_readData([], 'x');
% list = eis_readData(list, 'y');
%list = eis_readData(list, 'trigger', 'frame'); %% just added for test
% list = eis_readData([], 'trigger', 'blink');
% list = eis_readData(list, 'trigger', 'notrack');
% list = eis_readData(list, 'stream', 0, 'double');
% list = eis_readData(list, 'stream', 1, 'double');
% list = eis_readData(list, 'stream', 2, 'double');
% list = eis_readData(list, 'stream', 3, 'double');
%list = eis_readData(list, 'joypad', 'r1');

% % user variables
list = eis_readData([], 'uservar', 'image');
list = eis_readData(list, 'uservar', 'VResolution');
list = eis_readData(list, 'uservar', 'HResolution');
list = eis_readData(list, 'uservar', 'RRate');

% list = eis_readData(list, 'uservar', 'TimeImageOn');
list = eis_readData(list, 'uservar', 'TimeImageOff');
list = eis_readData(list, 'uservar', 'TimeResponse');
list = eis_readData(list, 'uservar', 'FixationTrial');
list = eis_readData(list, 'uservar', 'Task');
list = eis_readData(list, 'uservar', 'X_orig');
list = eis_readData(list, 'uservar', 'Y_orig');
list = eis_readData(list, 'uservar', 'Response');
list = eis_readData(list, 'uservar', 'NoiseLevel');
list = eis_readData(list, 'uservar', 'ImageSize');
list = eis_readData(list, 'uservar', 'xoffset');
list = eis_readData(list, 'uservar', 'yoffset');

list = eis_readData(list, 'uservar', 'pixelAngle');
list = eis_readData(list, 'uservar', 'pixelAngle0');
list = eis_readData(list, 'uservar', 'screenDist');
list = eis_readData(list, 'uservar', 'magnificationFactor');

