function [Trial] = cleanSpikes(spikeSize, TrialNumber, X, Y, Blink, NoTrack, DataValid, Fs)

%Remove 1 sample spikes from traces.
%   DO NOT ALSO RUN CREATE TRIALS OUTSIDE THIS FUNCTION.

clear SpikesX;

X_findspikes = abs(X);
for ii = 1:length(X)-4
    if X_findspikes(ii+1) > X_findspikes(ii) + spikeSize && ...
            X_findspikes(ii+2) < X_findspikes(ii) + spikeSize ||...
            X_findspikes(ii+1) > X_findspikes(ii) + spikeSize && ...
            X_findspikes(ii+2) > X_findspikes(ii) + spikeSize && ...
            X_findspikes(ii+3) < X_findspikes(ii) + spikeSize
        SpikesX(ii+1) = 1;
    elseif X_findspikes(ii+1) < X_findspikes(ii) - spikeSize && ...
            X_findspikes(ii+2) > X_findspikes(ii) - spikeSize ||...
            X_findspikes(ii+1) < X_findspikes(ii) - spikeSize && ...
            X_findspikes(ii+2) < X_findspikes(ii) - spikeSize && ...
            X_findspikes(ii+3) > X_findspikes(ii) - spikeSize
        SpikesX(ii+1) = 1;
    else
        SpikesX(ii+1) = 0;
    end
end

Trial.spikes = vector2events(SpikesX);
XTracesNoSpikes = X;
XTracesNoSpikes(Trial.spikes.start) = X(Trial.spikes.start + 1); %spike X will be replaced with next X instead;

clear SpikesY
spikeSize = 3;
Y_findspikes = abs(Y);
for ii = 1:length(X)-4
    if Y_findspikes(ii+1) > Y_findspikes(ii) + spikeSize && ...
            Y_findspikes(ii+2) < Y_findspikes(ii) + spikeSize ||...
            Y_findspikes(ii+1) > Y_findspikes(ii) + spikeSize && ...
            Y_findspikes(ii+2) > Y_findspikes(ii) + spikeSize && ...
            Y_findspikes(ii+3) < Y_findspikes(ii) + spikeSize
        SpikesY(ii+1) = 1;
    elseif Y_findspikes(ii+1) < Y_findspikes(ii) - spikeSize && ...
            Y_findspikes(ii+2) > Y_findspikes(ii) - spikeSize ||...
            Y_findspikes(ii+1) < Y_findspikes(ii) - spikeSize && ...
            Y_findspikes(ii+2) < Y_findspikes(ii) - spikeSize && ...
            Y_findspikes(ii+3) > Y_findspikes(ii) - spikeSize
        SpikesY(ii+1) = 1;
    else
        SpikesY(ii+1) = 0;
    end
end

Trial.spikes = vector2events(SpikesY);
YTracesNoSpikes = Y;
YTracesNoSpikes(Trial.spikes.start) = Y(Trial.spikes.start + 1); %spike X will be replaced with next X instead;

Trial = createTrial(TrialNumber, XTracesNoSpikes, YTracesNoSpikes, Blink, NoTrack, DataValid, false(size(NoTrack)), Fs);
OG_Trial = createTrial(TrialNumber, X, Y, Blink, NoTrack, DataValid, false(size(NoTrack)), Fs);

Trial.x_WithSpikes = OG_Trial.x;
Trial.y_WithSpikes = OG_Trial.y;

Trial.spikesX = vector2events(SpikesX); %add the spikes back into the trial
Trial.spikesY = vector2events(SpikesY); %add the spikes back into the trial


end

