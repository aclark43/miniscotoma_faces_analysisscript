function clipLevel = getLevel(histData,level)
% find the histogram value corresponding to a specific normalized quantity
% of the data

histDataVect = reshape(histData,[],1);  % make into a vector for sorting
histDataVectSorted = sort(histDataVect,'descend');  % sort probabilities in descending order
histDataVectSum = cumsum(histDataVectSorted);   % sum over sorted probabilities

levelIndex = find(histDataVectSum>level,1); % find index of first value to cross level threshold
clipLevel = histDataVectSorted(levelIndex); % get the value of the histogram data at this point

end