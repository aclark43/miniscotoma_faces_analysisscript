function runVideoStimVisibility(Xe,Ye,nameMovie)
% clear Xe; clear Ye;

%%%% Some Example Drifts
% [Xe, Ye] = EM_Brownian2(15, 1000, 500, 1,[90, 20, 0]); %toCreate

%%% Load in subject Data for 2 trials

stimulusMat(1:30,1:30) = 0; 
stimulusMat(13:17,13:17) = 1;
stimulusMat(14,14:17) = 0;
stimulusMat(16,14:17) = 0;

figure;imagesc(-15:15,-15:15,flipud(stimulusMat))
set(gca, 'YDir','normal')
colormap(flipud(gray))
hold on
scatter1 = scatter(Xe,Ye,3000,'MarkerFaceColor','w',...
    'MarkerEdgeColor','w','LineWidth',20);
scatter1.MarkerFaceAlpha = 0.2;
scatter1.MarkerEdgeAlpha = 0.1;

hold on

h = figure;
% Create file name variable
filename = sprintf('%s.gif',nameMovie);
h.Visible = 'off';
for i=1:10:length(Xe)
%     A = figure;
    imagesc(-15:15,-15:15,flipud(stimulusMat))
    colormap(flipud(gray))
    hold on
    scatter(Xe(i),Ye(i),3800,'MarkerFaceColor',[.5 .5 .5],...
        'MarkerFaceAlpha',.95,'MarkerEdgeAlpha',0);
    scatter(Xe(i),Ye(i),8000,'MarkerFaceColor',[.5 .5 .5],...
        'MarkerFaceAlpha',.65,'MarkerEdgeAlpha',0);
    scatter(Xe(i),Ye(i),16000,'MarkerFaceColor',[.5 .5 .5],...
        'MarkerFaceAlpha',.3,'MarkerEdgeAlpha',0);
    scatter(Xe(i),Ye(i),28000,'MarkerFaceColor',[.5 .5 .5],...
        'MarkerFaceAlpha',.15,'MarkerEdgeAlpha',0);
    %     axis([-5 5 -5 5])
    axis square
    title(nameMovie);
    pause(.1)
    frame = getframe(gcf);
    im = frame2im(frame);
    [imind,cm] = rgb2ind(im,256);
    if i == 1
        imwrite(imind,cm,filename,'gif', 'Loopcount',inf,...
        'DelayTime',0.1);
    else
        imwrite(imind,cm,filename,'gif','WriteMode','append',...
        'DelayTime',0.1);
    end
end

vidObj = VideoWriter('myMovie','MPEG-4');
open(vidObj)
writeAnimation(vidObj)
close(vidObj)
% 
% figure;
% movie(myMovie);
% 
% 
% figure
% axes("Position",[0 0 1 1])
% movie(M,5)
end
