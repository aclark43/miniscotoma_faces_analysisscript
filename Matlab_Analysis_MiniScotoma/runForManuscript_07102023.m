clear all
close all
clc
%
subjectsAll = {'Z055','A144','Ben','Z091','Z114'}; %'Z171','Z055',,|||'Z104',
% reProcess = [1 1 1 1];
% cleanTrials = [0 0 0 0];
clrs = parula(length(subjectsAll));
lengthTrial = 500;
conditions = {'Control','Scotoma'};

for ii = 1:length(subjectsAll)
    subjDataPath = strcat(sprintf('X:/Ashley/SingleOpto_VA_Scot/%s', subjectsAll{ii}));
    allfiles = dir(subjDataPath);
    data{ii}.Scotoma = [];
    data{ii}.Control = [];
    for ss = find(~[allfiles.isdir])
        clear temp
        if startsWith(string(allfiles(ss).name),["scot"]) %scotoma condition
            
            temp = load(sprintf('%s/%s',subjDataPath,allfiles(ss).name));
            dateCollect = datetime(str2double(temp.eis_data.filename(5:8)),...
                str2double(temp.eis_data.filename(9:10)),...
                str2double(temp.eis_data.filename(11:12)));
            temppptrials = convertDataToPPtrialsSinlgeOpto(temp.eis_data,dateCollect);
            processedPPTrialsS = newEyerisEISRead(temppptrials, subjectsAll{ii});
            data{ii}.Scotoma = [data{ii}.Scotoma processedPPTrialsS];
          
        elseif startsWith(string(allfiles(ss).name),["cont"])
            
            temp = load(sprintf('%s/%s',subjDataPath,allfiles(ss).name));
            dateCollect = datetime(str2double(temp.eis_data.filename(5:8)),...
                str2double(temp.eis_data.filename(9:10)),...
                str2double(temp.eis_data.filename(11:12)));
            
            temppptrials = convertDataToPPtrialsSinlgeOpto(temp.eis_data,dateCollect);
            processedPPTrialsC = newEyerisEISRead(temppptrials, subjectsAll{ii});
            data{ii}.Control = [data{ii}.Control processedPPTrialsC];
            
        else
            error('Change name of File');
        end
    end
   
   
end

%% Creat Table at Individual Trial Level
counter = 1;
for c = 1:2
    for ii = 1:length(subjectsAll)
        for t = 1:length(data{ii}.(conditions{c}))
            pathForCondition = (data{ii}.(conditions{c}));
            tbl(counter).Subject = ii;
            tbl(counter).Condition = c;
            tbl(counter).trialNumber= pathForCondition{t}.id;
            tbl(counter).fixationTrial=  pathForCondition{t}.fixationTrial;
            if pathForCondition{t}.fixationTrial
                tbl(counter).Correct = NaN;
            else
                tbl(counter).Correct =  pathForCondition{t}.Correct;
                
            end
            if isfield(pathForCondition{t},'ResponseTime')
                tbl(counter).RT =  pathForCondition{t}.ResponseTime;
            else
                tbl(counter).RT =  NaN;
            end
            tbl(counter).msAmplitude = pathForCondition{t}.microsaccades.amplitude;
            tbl(counter).msAngle = rad2deg(pathForCondition{t}.microsaccades.angle);
            tbl(counter).msRate = length(pathForCondition{t}.microsaccades.angle)/...
                (pathForCondition{t}.samples/pathForCondition{t}.sRate);
            
            starts = pathForCondition{t}.microsaccades.start;
            durs = pathForCondition{t}.microsaccades.duration;
            stops = starts + durs - 1;
            
            start = 1;
            stop = length(pathForCondition{t}.x);
            mi = (starts + stops) / 2;
            use = mi >= start & mi <= stop;
            
            trueStarts = starts(starts < stop);
            trueEnds = stops(stops<stop);
            
            tbl(counter).msStartX = pathForCondition{t}.x(...
                trueStarts);
            tbl(counter).msStartY = pathForCondition{t}.y(...
                trueStarts);
            tbl(counter).msEndX = pathForCondition{t}.x(...
                trueEnds);
            tbl(counter).msEndY = pathForCondition{t}.y(...
                trueEnds);
            counterD = 1;
            for d = 1:length(pathForCondition{t}.drifts.start)
                startIdx = pathForCondition{t}.drifts.start(d);
                endIdx = startIdx + pathForCondition{t}.drifts.duration(d);
                if endIdx > length(pathForCondition{t}.x)
                    endIdx = length(pathForCondition{t}.x);
                end
                if length([startIdx:1:endIdx]) < 15
                    continue
                end
                x = pathForCondition{t}.x(startIdx:endIdx);
                y = pathForCondition{t}.y(startIdx:endIdx);
                
                %                     sgFitVal = min([length([startIdx:1:endIdx]
                [~,instSpX{counterD},...
                    instSpY{counterD},...
                    speed(counterD),...
                    driftAngle{counterD},...
                    curvature(counterD),...
                    varx(counterD),...
                    vary(counterD),...
                    bcea(counterD),...
                    span(counterD), ...
                    amplitude(counterD), ...
                    prlDistance(counterD),...
                    prlDistanceX(counterD), ...
                    prlDistanceY(counterD)] = ...
                    getDriftChars(x, y, 11, 1, 250);
                counterD = counterD + 1;
            end
            tbl(counter).instSpX = instSpX;
            tbl(counter).instSpY= instSpY;
            tbl(counter).speed = speed;
            tbl(counter).mn_speed = nanmean((speed));
            tbl(counter).driftAngle = driftAngle;
            tbl(counter).curvature = curvature;
            tbl(counter).mn_curvature = nanmean(curvature);
            tbl(counter).varx = varx;
            tbl(counter).vary = vary;
            tbl(counter).bcea = bcea;
            tbl(counter).mn_bcea = nanmean(bcea);
            tbl(counter).span = span;
            tbl(counter).mn_span = nanmean(span);
            tbl(counter).amplitude = amplitude;
            tbl(counter).mn_amplitude = nanmean(amplitude);
            tbl(counter).prlDistance = prlDistance;
            tbl(counter).prlDistanceX = prlDistanceX;
            tbl(counter).prlDistanceY = prlDistanceY;
            
            tbl(counter).x = pathForCondition{t}.x;
            tbl(counter).y = pathForCondition{t}.y;
            counter = counter + 1;
        end
    end
end

%% Overall Distributions

% Control
figure;
fixID = find(isnan([tbl.Correct]) & [tbl.Condition] == 1);
taskID = find(~isnan([tbl.Correct]) & [tbl.Condition] == 1);

subplot(5,1,1)
histogram([tbl(taskID).msAmplitude]);
hold on
histogram([tbl(fixID).msAmplitude]);
title('MS Amplitudes');
xlim([0 30])

subplot(5,1,2)
histogram([tbl(taskID).mn_curvature]*10);
hold on
histogram([tbl(fixID).mn_curvature]*10);
title('Drift Curvature');
xlim([0 60])

subplot(5,1,3)
tempBCEA = [tbl(taskID).mn_bcea];
histogram(tempBCEA(tempBCEA<60));
hold on
tempBCEA = [tbl(fixID).mn_bcea];
histogram(tempBCEA(tempBCEA<60));
title('Drift BCEA');
xlim([0 60])

subplot(5,1,4)
tempSpan = [tbl(taskID).mn_span];
histogram(tempSpan(tempSpan<60));
hold on
tempSpan = [tbl(fixID).mn_span];
histogram(tempSpan(tempSpan<60));
title('Drift Span');
xlim([0 15])

subplot(5,1,5)
histogram([tbl(taskID).mn_speed]);
hold on
histogram([tbl(fixID).mn_speed]);
title('Drift Speed');
xlim([20 60])

xlabel('Control');
suptitle('All Subject Averages');

% Scotoma
figure;
fixID = find(isnan([tbl.Correct]) & [tbl.Condition] == 2);
taskID = find(~isnan([tbl.Correct]) & [tbl.Condition] == 2);

subplot(5,1,1)
histogram([tbl(taskID).msAmplitude]);
hold on
histogram([tbl(fixID).msAmplitude]);
title('MS Amplitudes');
xlim([0 30])

subplot(5,1,2)
histogram([tbl(taskID).mn_curvature]*10);
hold on
histogram([tbl(fixID).mn_curvature]*10);
title('Drift Curvature');
xlim([0 60])

subplot(5,1,3)
tempBCEA = [tbl(taskID).mn_bcea];
histogram(tempBCEA(tempBCEA<60));
hold on
tempBCEA = [tbl(fixID).mn_bcea];
histogram(tempBCEA(tempBCEA<60));
title('Drift BCEA');
xlim([0 60])

subplot(5,1,4)
tempSpan = [tbl(taskID).mn_span];
histogram(tempSpan(tempSpan<60));
hold on
tempSpan = [tbl(fixID).mn_span];
histogram(tempSpan(tempSpan<60));
title('Drift Span');
xlim([0 15])

subplot(5,1,5)
histogram([tbl(taskID).mn_speed]);
hold on
histogram([tbl(fixID).mn_speed]);
title('Drift Speed');
xlim([20 60])

xlabel('Scotoma');
suptitle('All Subject Averages');

%% Overall Distributions

% Control Correct and Incorrect
figure;
taskID_Correct = find([tbl.Condition] == 1 & [tbl.Correct] == 1);
taskID_Wrong = find([tbl.Condition] == 1 & [tbl.Correct] == 0);

subplot(5,1,1)
histogram([tbl(taskID_Correct).msAmplitude]);
hold on
histogram([tbl(taskID_Wrong).msAmplitude]);
title('MS Amplitudes');
xlim([0 30])

subplot(5,1,2)
histogram([tbl(taskID_Correct).mn_curvature]*10);
hold on
histogram([tbl(taskID_Wrong).mn_curvature]*10);
title('Drift Curvature');
xlim([0 60])

subplot(5,1,3)
tempBCEA = [tbl(taskID_Correct).mn_bcea];
histogram(tempBCEA(tempBCEA<60));
hold on
tempBCEA = [tbl(taskID_Wrong).mn_bcea];
histogram(tempBCEA(tempBCEA<60));
title('Drift BCEA');
xlim([0 60])

subplot(5,1,4)
tempSpan = [tbl(taskID_Correct).mn_span];
histogram(tempSpan(tempSpan<60));
hold on
tempSpan = [tbl(taskID_Wrong).mn_span];
histogram(tempSpan(tempSpan<60));
title('Drift Span');
xlim([0 15])

subplot(5,1,5)
histogram([tbl(taskID_Correct).mn_speed]);
hold on
histogram([tbl(taskID_Wrong).mn_speed]);
title('Drift Speed');
xlim([20 60])

xlabel('Control');
suptitle('All Subject Averages');

% Scotoma
figure;
taskID_Correct = find([tbl.Condition] == 2 & [tbl.Correct] == 1);
taskID_Wrong = find([tbl.Condition] == 2 & [tbl.Correct] == 0);

subplot(5,1,1)
histogram([tbl(taskID_Correct).msAmplitude]);
hold on
histogram([tbl(taskID_Wrong).msAmplitude]);
title('MS Amplitudes');
xlim([0 30])

subplot(5,1,2)
histogram([tbl(taskID_Correct).mn_curvature]*10);
hold on
histogram([tbl(taskID_Wrong).mn_curvature]*10);
title('Drift Curvature');
xlim([0 60])

subplot(5,1,3)
tempBCEA = [tbl(taskID_Correct).mn_bcea];
histogram(tempBCEA(tempBCEA<60));
hold on
tempBCEA = [tbl(taskID_Wrong).mn_bcea];
histogram(tempBCEA(tempBCEA<60));
title('Drift BCEA');
xlim([0 60])

subplot(5,1,4)
tempSpan = [tbl(taskID_Correct).mn_span];
histogram(tempSpan(tempSpan<60));
hold on
tempSpan = [tbl(taskID_Wrong).mn_span];
histogram(tempSpan(tempSpan<60));
title('Drift Span');
xlim([0 15])

subplot(5,1,5)
histogram([tbl(taskID_Correct).mn_speed]);
hold on
histogram([tbl(taskID_Wrong).mn_speed]);
title('Drift Speed');
xlim([20 60])

xlabel('Scotoma');
suptitle('All Subject Averages');

%% MS Landing Positions for Correct and Incorrect Trials

%%% Individual Subject
for c = 1:2
    for ii = 1:length(subjectsAll)
        p1 = [];
        p2 = [];
        dp = [];
        figure;
        
        indSub_Correct_Control_idx = ...
            find([tbl.Correct] == 1 & ...
            [tbl.Condition] == c &...
            [tbl.Subject] == ii);
        
        indSub_Wrong_Control_idx = ...
            find([tbl.Correct] == 0 & ...
            [tbl.Condition] == c &...
            [tbl.Subject] == ii);
        
        subplot(1,2,1)
                plot([tbl(indSub_Correct_Control_idx).msStartX],...
                    [tbl(indSub_Correct_Control_idx).msStartY],'+');
                hold on
                plot([tbl(indSub_Correct_Control_idx).msEndX],...
                    [tbl(indSub_Correct_Control_idx).msEndY],'x');
%         p1(1,:) = [tbl(indSub_Correct_Control_idx).msStartX];
%         p1(2,:) = [tbl(indSub_Correct_Control_idx).msStartY];                         % First Point
%         p2(1,:) = [tbl(indSub_Correct_Control_idx).msEndX];
%         p2(2,:) = [tbl(indSub_Correct_Control_idx).msEndY];                         % Second Point
%         dp = p2-p1;                         % Difference
% % figure
% quiver(p1(1,:),p1(2,:),dp(1,:),dp(2,:),0)

        title('Correct trials');
        axis([-20 20 -20 20])
        rectangle('Position',[-7/2,-7/2,7,7]);
        axis square
        
        if length(indSub_Correct_Control_idx) > 10
        eucDist_Offset_MSLanding_Correct(c,ii) = nanmean(abs(pdist(...
           [ [tbl(indSub_Correct_Control_idx).msEndX]',...
            [tbl(indSub_Correct_Control_idx).msEndY]'],...
            'euclidean')));
        else
             eucDist_Offset_MSLanding_Correct(c,ii) = NaN;
        end
        
        
        subplot(1,2,2)
        plot([tbl(indSub_Wrong_Control_idx).msStartX],...
            [tbl(indSub_Wrong_Control_idx).msStartY],'+');
        hold on
        plot([tbl(indSub_Wrong_Control_idx).msEndX],...
            [tbl(indSub_Wrong_Control_idx).msEndY],'x');
%         p1(1,:) = [tbl(indSub_Wrong_Control_idx).msStartX];
%         p1(2,:) = [tbl(indSub_Wrong_Control_idx).msStartY];                         % First Point
%         p2(1,:) = [tbl(indSub_Wrong_Control_idx).msEndX];
%         p2(2,:) = [tbl(indSub_Wrong_Control_idx).msEndY];                         % Second Point
%         dp = p2-p1;                         % Difference
% % figure
% quiver(p1(1,:),p1(2,:),dp(1,:),dp(2,:),0)
        title('Incorrect Trials');
        axis([-20 20 -20 20])
        rectangle('Position',[-7/2,-7/2,7,7]);
        axis square
        
        if length(indSub_Wrong_Control_idx) > 10
        eucDist_Offset_MSLanding_Wrong(c,ii) = nanmean(abs(pdist(...
           [[tbl(indSub_Wrong_Control_idx).msEndX]',...
            [tbl(indSub_Wrong_Control_idx).msEndY]'],...
            'euclidean')));
        else
             eucDist_Offset_MSLanding_Wrong(c,ii) = NaN;
        end
        
        
        suptitle(sprintf('%s,%s',subjectsAll{ii}, conditions{c}))
    end
end

figure;
for ii = 1:length(subjectsAll)
    plot([1 2],[eucDist_Offset_MSLanding_Correct(1,ii), ...
        eucDist_Offset_MSLanding_Wrong(1,ii)],'-o');
    plot([3 4],...
        [eucDist_Offset_MSLanding_Correct(2,ii),...
        eucDist_Offset_MSLanding_Wrong(2,ii)],'-o');
    hold on
end
errorbar([1 2],[nanmean(eucDist_Offset_MSLanding_Correct(1,:)), ...
    nanmean(eucDist_Offset_MSLanding_Wrong(1,:))],...
    [sem(eucDist_Offset_MSLanding_Correct(1,:)), ...
    sem(eucDist_Offset_MSLanding_Wrong(1,:))],...
    '-o','Color','k','MarkerFaceColor','k','LineWidth',5);

errorbar([3 4],[nanmean(eucDist_Offset_MSLanding_Correct(2,:)), ...
    nanmean(eucDist_Offset_MSLanding_Wrong(2,:))],...
    [sem(eucDist_Offset_MSLanding_Correct(2,:)), ...
    sem(eucDist_Offset_MSLanding_Wrong(2,:))],...
    '-o','Color','k','MarkerFaceColor','k','LineWidth',5);

xticks([1 2 3 4])
xlim([.5 4.5])
xticklabels({'Correct Control','Wrong Control',...
    'Correct Scotoma','Wrong Scotoma'});
ylabel('Euc. Distance of MS Landing Position');

for c = 1:2
    for ii = 1:length(subjectsAll)
        figure;
        
        indSub_Correct_Control_idx = ...
            find([tbl.Correct] == 1 & ...
            [tbl.Condition] == 1 &...
            [tbl.Subject] == ii);
        
        indSub_Wrong_Control_idx = ...
            find([tbl.Correct] == 0 & ...
            [tbl.Condition] == 1 &...
            [tbl.Subject] == ii);
        
        subplot(1,2,1)
        
         resultC = generateHeatMapSimple( ...
             [tbl(indSub_Correct_Control_idx).msEndX], ...
             [tbl(indSub_Correct_Control_idx).msEndY], ...
             'Bins', 10,...
             'StimulusSize', 7,...
             'AxisValue', 10,...
             'Uncrowded', 0,...
             'Borders', 1);
        
%         plot([tbl(indSub_Correct_Control_idx).msStartX],...
%             [tbl(indSub_Correct_Control_idx).msStartY],'+');
%         hold on
%         plot([tbl(indSub_Correct_Control_idx).msEndX],...
%             [tbl(indSub_Correct_Control_idx).msEndY],'x');
        title('Correct trials');
        axis([-20 20 -20 20])
        rectangle('Position',[-7/2,-7/2,7,7]);
        axis square

        subplot(1,2,2)
%         plot([tbl(indSub_Wrong_Control_idx).msStartX],...
%             [tbl(indSub_Wrong_Control_idx).msStartY],'+');
%         hold on
%         plot([tbl(indSub_Wrong_Control_idx).msEndX],...
%             [tbl(indSub_Wrong_Control_idx).msEndY],'x');
        resultW = generateHeatMapSimple( ...
             [tbl(indSub_Wrong_Control_idx).msEndX], ...
             [tbl(indSub_Wrong_Control_idx).msEndY], ...
             'Bins', 10,...
             'StimulusSize', 10,...
             'AxisValue', 10,...
             'Uncrowded', 0,...
             'Borders', 1);
        title('Incorrect Trials');
        axis([-20 20 -20 20])
        rectangle('Position',[-7/2,-7/2,7,7]);
        axis square
        
        suptitle(sprintf('%s,%s',subjectsAll{ii}, conditions{c}))
    end
end


% %%% Combined
% for c = 1:2
%         figure;
%         
%         indSub_Correct_Control_idx = ...
%             find([tbl.Correct] == 1 & ...
%             [tbl.Condition] == c);
%         
%         indSub_Wrong_Control_idx = ...
%             find([tbl.Correct] == 0 & ...
%             [tbl.Condition] == c);
%         
%         subplot(1,2,1)
%         plot([tbl(indSub_Correct_Control_idx).msStartX],...
%             [tbl(indSub_Correct_Control_idx).msStartY],'+');
%         hold on
%         plot([tbl(indSub_Correct_Control_idx).msEndX],...
%             [tbl(indSub_Correct_Control_idx).msEndY],'x');
%         title('Correct trials');
%         axis([-20 20 -20 20])
%         rectangle('Position',[-7/2,-7/2,7,7]);
%         axis square
% 
%         subplot(1,2,2)
%         plot([tbl(indSub_Wrong_Control_idx).msStartX],...
%             [tbl(indSub_Wrong_Control_idx).msStartY],'+');
%         hold on
%         plot([tbl(indSub_Wrong_Control_idx).msEndX],...
%             [tbl(indSub_Wrong_Control_idx).msEndY],'x');
%         title('Incorrect Trials');
%         axis([-20 20 -20 20])
%         rectangle('Position',[-7/2,-7/2,7,7]);
%         axis square
%         
%         suptitle(sprintf('%s', conditions{c}))
% end
% 
% for c = 1:2
%         figure;
%         
%         indSub_Correct_Control_idx = ...
%             find([tbl.Correct] == 1 & ...
%             [tbl.Condition] == c);
%         
%         indSub_Wrong_Control_idx = ...
%             find([tbl.Correct] == 0 & ...
%             [tbl.Condition] == c);
%         
%         subplot(1,2,1)
%         
%          resultC = generateHeatMapSimple( ...
%              [tbl(indSub_Correct_Control_idx).msEndX], ...
%              [tbl(indSub_Correct_Control_idx).msEndY], ...
%              'Bins', 10,...
%              'StimulusSize', 7,...
%              'AxisValue', 20,...
%              'Uncrowded', 0,...
%              'Borders', 1);
%         
% %         plot([tbl(indSub_Correct_Control_idx).msStartX],...
% %             [tbl(indSub_Correct_Control_idx).msStartY],'+');
% %         hold on
% %         plot([tbl(indSub_Correct_Control_idx).msEndX],...
% %             [tbl(indSub_Correct_Control_idx).msEndY],'x');
%         title('Correct trials');
%         axis([-20 20 -20 20])
%         rectangle('Position',[-7/2,-7/2,7,7]);
%         axis square
% 
%         subplot(1,2,2)
% %         plot([tbl(indSub_Wrong_Control_idx).msStartX],...
% %             [tbl(indSub_Wrong_Control_idx).msStartY],'+');
% %         hold on
% %         plot([tbl(indSub_Wrong_Control_idx).msEndX],...
% %             [tbl(indSub_Wrong_Control_idx).msEndY],'x');
%         resultW = generateHeatMapSimple( ...
%              [tbl(indSub_Wrong_Control_idx).msEndX], ...
%              [tbl(indSub_Wrong_Control_idx).msEndY], ...
%              'Bins', 10,...
%              'StimulusSize', 10,...
%              'AxisValue', 20,...
%              'Uncrowded', 0,...
%              'Borders', 1);
%         title('Incorrect Trials');
%         axis([-20 20 -20 20])
%         rectangle('Position',[-7/2,-7/2,7,7]);
%         axis square
%         
%         suptitle(sprintf('%s', conditions{c}))
% end

% %% Linear Model
% 
% taskOnlyTbl = tbl(find(~isnan([tbl.Correct])));
% 
% lm = fitlm(taskOnlyTbl,'Correct~msAmplitude|Subject')
% 


