function emRate = rateCalculationSingleOpto(pptrial, emType)

% if pptrial.TimeFixationOFF > 0 %FIXATION TRIAL
%     endTrial = pptrial.TimeFixationOFF;
%     startTrial = pptrial.TimeFixationON;
% % elseif pptrial.ResponseTime == 0
% %     endTrial = pptrial.TimeTargetOFF;
% %     startTrial
% else
    endTrial = pptrial.TimeTargetOFF;
    startTrial = 1;
% end

switch emType
    case 's'
        numEM = length(pptrial.saccades.start(pptrial.saccades.amplitude < 90 ...
            & pptrial.saccades.amplitude > 30));
%         numEM = sum(pptrial.saccades.start < endTrial &...
%             pptrial.saccades.start > startTrial);
    case 'ms'
        numEM = length(pptrial.microsaccades.start(pptrial.microsaccades.amplitude < 30));
%         numEM = sum(pptrial.microsaccades.start < endTrial &...
%             pptrial.microsaccades.start > startTrial);
end

emRate = (numEM/(endTrial-startTrial)*pptrial.sRate/1000);

end