function makeMovieInTime(timeBins,x,y,fileName)

counter = 0;
for t = 1:length(timeBins) - 1
    counter = counter + 1;
    time.X{counter} = x(timeBins(t):(timeBins(t+1)-1));
    time.Y{counter} = y(timeBins(t):(timeBins(t+1)-1));
end


figure
axes;
% Create and open the video object
vidObj = VideoWriter(sprintf('%s.avi', fileName));
open(vidObj);
%
% Loop over the data to create the video
for t=1:length(timeBins)-1
    % Plot the data
    sizeVec = (find(time.C{t}));
    generateHeatMapSimple( ...
        reshape(time.X{t},[1, sizeVec(end)]), ...
        reshape(time.Y{t},[1, sizeVec(end)]), ...
        'Bins', 10,...
        'StimulusSize', 5,...
        'AxisValue', axisVal,...
        'Uncrowded', 4,...
        'Borders', 1);
    
    % Get the current frame
    currFrame = getframe;
    
    % Write the current frame
    writeVideo(vidObj,currFrame);
    
    clf
    text(-30,30,sprintf('%i ms',timeBins(t)));
    
end

% Close (and save) the video object
close(vidObj);
