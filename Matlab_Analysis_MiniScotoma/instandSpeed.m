function instStruct = instandSpeed(x,y)
% smx = sgfilt(x, 3, smoothing, 0);
% smy = sgfilt(y, 3, smoothing, 0);
smx1 = sgfilt(x, 3, 41, 1);
smy1 = sgfilt(y, 3, 41, 1);

instSpX = smx1*1000; 
instSpY = smy1*1000;%transformed to acrmin/sec
instStruct.instSpX = instSpX;
instStruct.instSpY = instSpY;
