function contourData = getContour(X,Y,histData,level)
% get the contour data for a histogram distribution. Level is specified as
% the normalized quantity of the bivariate distribution to keep. So 0.68
% means we put the contour at the level corresponding to the top 68% of the
% data, or one standard deviation.

% find the level to put the contour at
clipLevel = getLevel(histData,level);
loops = 0;
while isempty(clipLevel)   % adjust threshold if necessary (need to find a valid endpoint)
    loops = loops+1;    % increment counter
    clipLevel = getLevel(histData,level-1*loops);
end
% make a contour map
figure
contourData = contour(X,Y,histData,[clipLevel,clipLevel]);
set(gca,'YDir','reverse')
close(gcf)  % close the figure because we don't need it

end