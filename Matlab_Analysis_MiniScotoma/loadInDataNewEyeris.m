function [dataAll] = loadInDataNewEyeris (ii,data)

conditionsAll = {'Scotoma','Control'};
for i = 1:length(conditionsAll)
    counter1 = 0;
    allX = [];
    allY = [];
    fixSamIdx = [];
    for j = 1:length(data{ii}.(conditionsAll{i}))
        actualTrialLocation = data{ii}.(conditionsAll{i}){j}.eis_data.user_data.variables;
        allTrialNames = fieldnames(actualTrialLocation);
        counterAllMS = 1;
        for s = 1:length(allTrialNames)
            if ~isfield(actualTrialLocation, (sprintf('trial%iData',s)))
                continue;
            end
            nameTrial = data{ii}.(conditionsAll{i}){j}...
                .eis_data.user_data.variables.(sprintf('trial%iData',s));
            if s == 1
                namePrevTrial = nameTrial;
            else
                namePrevTrial = data{ii}.(conditionsAll{i}){j}...
                .eis_data.user_data.variables.(allTrialNames{s-1});
            end
            counter1 = counter1 + 1;
            if nameTrial.Correct == 2 || nameTrial.FixationTrial == 1
                dataAll{ii}.(conditionsAll{i}).fixation(counter1) = 1;
                dataAll{ii}.(conditionsAll{i}).resp(counter1) = NaN;
                dataAll{ii}.(conditionsAll{i}).LA(counter1) = 3;
                dataAll{ii}.(conditionsAll{i}).correct(counter1) = NaN;
            else
%                 dataAll{ii}.(conditionsAll{i}).fixation(counter1) = 0;
%                 if nameTrial.Response == 0
%                     dataAll{ii}.(conditionsAll{i}).resp(counter1) = NaN;
%                     dataAll{ii}.(conditionsAll{i}).correct(counter1) = NaN;
%                     dataAll{ii}.(conditionsAll{i}).RT{counter1} = NaN;
%                     dataAll{ii}.(conditionsAll{i}).LA(counter1) = NaN;
%                     
%                 else
                    dataAll{ii}.(conditionsAll{i}).fixation(counter1) = 0;
                    dataAll{ii}.(conditionsAll{i}).resp(counter1) = nameTrial.Response;
                    dataAll{ii}.(conditionsAll{i}).correct(counter1) = nameTrial.Correct;
%                     if contains(nameTrial.image,'_LA_')
%                         if nameTrial.Response == 1
%                             dataAll{ii}.(conditionsAll{i}).resp(counter1) = 1;
% %                             dataAll{ii}.(conditionsAll{i}).LA(counter1) = 1;
%                             dataAll{ii}.(conditionsAll{i}).correct(counter1) = 0;
%                         elseif nameTrial.Response == 2 %correct
%                             dataAll{ii}.(conditionsAll{i}).resp(counter1) = 2;
% %                             dataAll{ii}.(conditionsAll{i}).LA(counter1) = 1;
%                             dataAll{ii}.(conditionsAll{i}).correct(counter1) = 1;
%                         end
%                     else
%                         if nameTrial.Response == 2
%                             dataAll{ii}.(conditionsAll{i}).resp(counter1) = 2;
% %                             dataAll{ii}.(conditionsAll{i}).LA(counter1) = 0;
%                             dataAll{ii}.(conditionsAll{i}).correct(counter1) = 0;
%                         elseif nameTrial.Response == 1
%                             dataAll{ii}.(conditionsAll{i}).resp(counter1) = 1;
% %                             dataAll{ii}.(conditionsAll{i}).LA(counter1) = 0;
%                             dataAll{ii}.(conditionsAll{i}).correct(counter1) = 1;
%                         end
%                     end
%                 end
            end
            XAll = data{ii}.(conditionsAll{i}){j}...
                .eis_data.eye_data.eye_1.calibrated_x;
            YAll = data{ii}.(conditionsAll{i}){j}...
                .eis_data.eye_data.eye_1.calibrated_y;
            
            %             startTime = nameTrial.FrameImageON;
            %             endTime = nameTrial.FrameImageOff;
            startTime = nameTrial.TimeImageOn;
            endTime = startTime + 1500;%nameTrial.TimeImageOff;
            if startTime == 0
                startTime = nameTrial.TimeFixationOn;
                endTime = nameTrial.TimeFixationOff;
            end
            xNoOff = (XAll(startTime:endTime));
            yNoOff = (YAll(startTime:endTime));
            
            xOff = nameTrial.xoffset * nameTrial.pixelAngle;
            yOff = nameTrial.yoffset * nameTrial.pixelAngle;
            
%             xOff = nameTrial.xoffset * nameTrial.pixelAngle;
%             yOff = nameTrial.yoffset * nameTrial.pixelAngle;
%             if s == 1
%                  xOff = nameTrial.xoffset * nameTrial.pixelAngle;
%                  yOff = nameTrial.yoffset * nameTrial.pixelAngle;
%             else
%                 xOff = nameTrial.xoffset * nameTrial.pixelAngle - ...
%                 namePrevTrial.xoffset * namePrevTrial.pixelAngle;
%                 yOff = nameTrial.yoffset * nameTrial.pixelAngle - ...
%                 namePrevTrial.yoffset * namePrevTrial.pixelAngle;
%             end
           
            
            x = xNoOff + xOff;
            y = yNoOff + yOff;
            
            %%
            MinSaccSpeed = 180;
            MinSaccAmp = 30;
            MinMSaccSpeed = 60;
            MinMSaccAmp = 2;
            MaxMSaccAmp = 60;
            MinVelocity = 50;
            
            Blink = zeros(1,length(x));
            NoTrack = zeros(1,length(x));
            DataValid = ones(1,length(x));
            DataInvalid = zeros(1,length(x));
            Trial = createTrial(counter1, x, y, Blink, NoTrack, DataValid, DataInvalid, 1000);
            Trial = preprocessSignals(Trial, 'minvel', MinVelocity, 'noanalysis');
            %%%% 'noanalysis' keeps the saccades at the very onset of the trial

            % Find all valid saccades with speed greater than 3 deg/sec
            % and bigger than 30 arcmin
            Trial = findSaccades(Trial, 'minvel',MinSaccSpeed, 'minsa',MinSaccAmp);
            Trial = findMicrosaccades(Trial, 'minvel',MinMSaccSpeed,'minmsa', MinMSaccAmp,'maxmsa', MaxMSaccAmp);
            Trial = findDrifts(Trial);
            fn=fieldnames(Trial);
            
            for ff=1:size(fn,1)
                entry = char(fn{ff});
                dataAll{ii}.(conditionsAll{i}).(entry){counter1} = ...
                    Trial.(entry);
            end
%             
            %             figure;
            %             plot(1:length(x),x)
            %             hold on
            %             plot(1:length(y),y)
            
            dataAll{ii}.(conditionsAll{i}).x{1,counter1} = x;
            dataAll{ii}.(conditionsAll{i}).y{1,counter1} = y;
            if ~nameTrial.Response == 0
                dataAll{ii}.(conditionsAll{i}).RT{counter1} = nameTrial.TimeResponse - nameTrial.TimeImageOn;
            else
                 dataAll{ii}.(conditionsAll{i}).RT{counter1} = NaN;
            end
            counter = 1;
            
            for m = 1:length(dataAll{ii}.(conditionsAll{i}).microsaccades{counter1}.start)
                if dataAll{ii}.(conditionsAll{i}).microsaccades{counter1}.start(counter) > 3
                    
                    dataAll{ii}.(conditionsAll{i}).msStart{counter1}(1,counter) =  ...
                        dataAll{ii}.(conditionsAll{i}).microsaccades{counter1}.start(m);
                    
                    dataAll{ii}.(conditionsAll{i}).msEnd{counter1}(1,counter) =  ...
                        dataAll{ii}.(conditionsAll{i}).microsaccades{counter1}.start(m) +...
                        dataAll{ii}.(conditionsAll{i}).microsaccades{counter1}.duration(m);
                    
                    dataAll{ii}.(conditionsAll{i}).msStart{counter1}(2,counter) =  ... %X
                        x(...
                        dataAll{ii}.(conditionsAll{i}).microsaccades{counter1}.start(m));
                    
                    dataAll{ii}.(conditionsAll{i}).msStart{counter1}(3,counter) =  ... % Y
                        y(...
                        dataAll{ii}.(conditionsAll{i}).microsaccades{counter1}.start(m));
                    
                    dataAll{ii}.(conditionsAll{i}).msEnd{counter1}(2,counter) =  ...
                        x(...
                        min(dataAll{ii}.(conditionsAll{i}).microsaccades{counter1}.start(m) +...
                        dataAll{ii}.(conditionsAll{i}).microsaccades{counter1}.duration(m), ...
                        length(dataAll{ii}.(conditionsAll{i}).x{counter1})));
                    
                    dataAll{ii}.(conditionsAll{i}).msEnd{counter1}(3,counter) =  ...
                        y(...
                        min(dataAll{ii}.(conditionsAll{i}).microsaccades{counter1}.start(m) +...
                        dataAll{ii}.(conditionsAll{i}).microsaccades{counter1}.duration(m), ...
                        length(dataAll{ii}.(conditionsAll{i}).y{counter1})));
                    
                    
                    x1 = dataAll{ii}.(conditionsAll{i}).msStart{counter1}(2,counter);
                    y1 = dataAll{ii}.(conditionsAll{i}).msStart{counter1}(3,counter);
                    x2 = dataAll{ii}.(conditionsAll{i}).msEnd{counter1}(2,counter);
                    y2 = dataAll{ii}.(conditionsAll{i}).msEnd{counter1}(3,counter);
                    
                    dataAll{ii}.(conditionsAll{i}).msAmpAll(counterAllMS) = ...
                        sqrt((x2 - x1)^2 + (y2 - y1)^2);
                    
                    counter = counter + 1;
                    counterAllMS = counterAllMS + 1;
                else
                    dataAll{ii}.(conditionsAll{i}).msStart{counter1}(1,counter) = NaN;%[NaN NaN NaN]';
                    dataAll{ii}.(conditionsAll{i}).msStart{counter1}(2,counter) = NaN;
                    dataAll{ii}.(conditionsAll{i}).msStart{counter1}(3,counter) = NaN;
                    dataAll{ii}.(conditionsAll{i}).msEnd{counter1}(1,counter) = NaN;%[NaN NaN NaN]';
                    dataAll{ii}.(conditionsAll{i}).msEnd{counter1}(2,counter) = NaN;
                    dataAll{ii}.(conditionsAll{i}).msEnd{counter1}(3,counter) = NaN;
                    counter = counter + 1;

                end
                
            end
            
            dataAll{ii}.(conditionsAll{i}).traceMatrixX(counter1,:) = ...
                x(1:500);
            dataAll{ii}.(conditionsAll{i}).traceMatrixY(counter1,:) = ...
                y(1:500);
            
            allX = [allX x];
            allY = [allY y];
            
%             if dataAll{ii}.(conditionsAll{i}).fixation(counter1) == 0
            if nameTrial.Correct == 2 || nameTrial.FixationTrial == 1
                fixSamIdx = [fixSamIdx zeros(1,length(x))];
            else
                fixSamIdx = [fixSamIdx ones(1,length(x))];
            end
            %             if counter > 1
            %                 for t = 1:length(dataAll{ii}.(conditionsAll{i}).x)
            %                     sac = find(dataAll{ii}.(conditionsAll{i}).msAmp{t} < 60);
            %                     temp = [temp dataAll{ii}.(conditionsAll{i}).msAmp{t}(sac)];
            %                     temp2 = [temp2 dataAll{ii}.(conditionsAll{i}).msStart{2,t}(sac)];
            %                     temp3 = [temp3 dataAll{ii}.(conditionsAll{i}).msStart{3,t}(sac)];
            %                     temp4 = [temp4 dataAll{ii}.(conditionsAll{i}).msEnd{2,t}(sac)];
            %                     temp5 = [temp5 dataAll{ii}.(conditionsAll{i}).msEnd{3,t}(sac)];
            %                 end
            %                 dataAll{ii}.(conditionsAll{i}).msAmpAll = temp;
            %                 dataAll{ii}.(conditionsAll{i}).msStartAllX = temp2;
            %                 dataAll{ii}.(conditionsAll{i}).msStartAllY = temp3;
            %                 dataAll{ii}.(conditionsAll{i}).msEndAllX = temp4;
            %                 dataAll{ii}.(conditionsAll{i}).msEndAllY = temp5;
            %             end
        end
    end
    dataAll{ii}.(conditionsAll{i}).allX = allX;
    dataAll{ii}.(conditionsAll{i}).allY = allY;
    dataAll{ii}.(conditionsAll{i}).fixSamIdx = fixSamIdx;
end
end

