clear all
clc
close all

%% Exact Ben 68% Contour Ellipse Code
load('AllProcessedData.mat');
binSizeArcmin = 1;
conditions = {'Control','Scotoma'};
tasks = {'Task','Fixation'};
counterType = 1;
for ii = find(whichDay == 1)
    for c = 1:2
        for t = 1:2
            %     figure;
            if t == 1
                var = tracesTask{ii}.(conditions{c});
            else
                var = tracesFix{ii}.(conditions{c});
            end
            varHist = [];
            varHist = generateHeatMapSimple( ...
                var.xAll, ...
                var.yAll, ...
                'Bins', 31,...
                'StimulusSize', 10,...
                'AxisValue', 15,...
                'Uncrowded', 0,...
                'Borders', 1);
            limsArcmin = [-15,15,-15,15];   % x- and y-axis limits for histograms (arcmin)
            x = (limsArcmin(1):binSizeArcmin:limsArcmin(2));    % x-values for bin centers
            y = (limsArcmin(3):binSizeArcmin:limsArcmin(4));    % y-values for bin centers
            xEdges = x-binSizeArcmin/2;     % shift by half of bin width for edges
            xEdges(end+1) = xEdges(end)+binSizeArcmin;  % add last edge in x
            yEdges = y-binSizeArcmin/2;     % shift by half of bin width
            yEdges(end+1) = yEdges(end)+binSizeArcmin;  % last edge in y
            [X,Y] = meshgrid(x,y);
            figure;
            
            Val68 = 30;

            [contourHeatMap{ii}{c,t}] = makeHeatmap(X,Y,varHist,...
                limsArcmin,'Heatmap and 68% contour for all control trials',...
                false,Val68);
  
             areaCondition{ii}(c,t) = polyarea(contourHeatMap{ii}{c,t}(:,1),...
                contourHeatMap{ii}{c,t}(:,2));
            counterType = counterType + 1;
        end
    end
end

AOAreas(3,:) = [45.1 26.9 43.6 38.2];
AOAreas(1,:) = [30.5 23.0 58.9 39.0];
AOAreas(4,:) = [58.1 51.0 161.9 84.8];
AOAreas(2,:) = [63.4 NaN 60.8 72.4];
AOAreas(5,:) = [89.7 56.8 87.7 74.9];

counter = 1;
for ii = find(whichDay == 1)
    TS(counter) = [areaCondition{ii}(1,2)];
    TC(counter) = [areaCondition{ii}(1,1)];
    FS(counter) = [areaCondition{ii}(2,2)];
    FC(counter) = [areaCondition{ii}(2,1)];
    counter = counter + 1;
end


forBoxPlot = [FC' FS' TC' TS'];


figure;
subplot(1,2,1)
boxplot([FC' FS' TC' TS'])
title('DDPI')
xticklabels({'Fix Cont', 'Fix Scot', ...
    'Task Cont', 'Task Scot'})
ylabel('68% Area');
subplot(1,2,2)
boxplot([AOAreas])
title('AO')
xticklabels({'Fix Cont', 'Fix Scot', ...
    'Task Cont', 'Task Scot'})
ylabel('68% Area');


% -------------------------------------------------------------------------

function [contourPts,figH] = makeHeatmap(X,Y,histData,lims,titleStr,drawScot,level)
% plots the stimulus position heatmap with a contour drawn to encompass 68%
% of the data. Returns the vertices of the contour (column 1 is x, column 2
% is y, column 3 is z: uniform height just for plotting purposes)
contourPts = [];

% keep top 99% of data for plotting
clipLevel = getLevel(histData,99);
loops = 0;  % counter for decreasing threshold if necessary
while isempty(clipLevel)   % adjust threshold if necessary (need to find a valid endpoint)
    loops = loops+1;    % increment counter
    clipLevel = getLevel(histData,99-1*loops);
end
histDataPlot = histData;    % make copy
% histDataPlot(histDataPlot<clipLevel) = NaN; % set values below clip level to NaN

% find 68% contour line
contour68 = getContour(X,Y,histDataPlot,level);
numVertices = contour68(2,1);   % number of vertices to include
if numVertices < 9  % first entry may just be a single point, not the whole contour
    startIndex = 1;
    while numVertices < 9
        startIndex = startIndex+numVertices+1;    % new index to start at for getting all vertices
        numVertices = contour68(2,startIndex);      % update the number of vertices
    end
    contourPts(:,1) = contour68(1,startIndex+1:startIndex+numVertices)';    % x-values for contour vertices
    contourPts(:,2) = contour68(2,startIndex+1:startIndex+numVertices)';    % y-values for contour vertices
    contourPts(:,3) = ones(numVertices,1);              % z-values for contour vertices (put at 1 to make visible on top of surface plot)
else
    contourPts(:,1) = contour68(1,2:numVertices+1)';    % x-values for contour vertices
    contourPts(:,2) = contour68(2,2:numVertices+1)';    % y-values for contour vertices
    contourPts(:,3) = ones(numVertices,1);              % z-values for contour vertices  (put at 1 to make visible on top of surface plot)
end

figH = figure;      % figure for heatmap plus contour
surf(X,Y,histDataPlot)
colormap('jet')
shading('interp')
view(2)
set(gca,'YDir','reverse');  % reverse y-axis direction so it matches with how the data is displayed on an image
daspect([1 1 1]);
hold on
plot3(contourPts(:,1),contourPts(:,2),contourPts(:,3),'m','LineWidth', 2)
% draw x- and y-axes to locate origin
line([0,0],[lims(3),lims(4)],[1,1],'Color','k')
line([lims(1),lims(2)],[0,0],[1,1],'Color','k')

if drawScot
    % add rectangle for scotoma
    plot3(scotRect(:,1),scotRect(:,2),scotRect(:,3),...
        'LineWidth',2,'LineStyle','--','Color','k');
end

hold off
axis(lims)
xticks([lims(1),lims(1)/2,0,lims(2)/2,lims(2)])
yticks([lims(3),lims(3)/2,0,lims(4)/2,lims(4)])
set(gca,'FontSize',10)
xlabel('X (arcmin)')
ylabel('Y (arcmin)')
title(titleStr)


end

function contourData = getContour(X,Y,histData,level)
% get the contour data for a histogram distribution. Level is specified as
% the normalized quantity of the bivariate distribution to keep. So 0.68
% means we put the contour at the level corresponding to the top 68% of the
% data, or one standard deviation.

% find the level to put the contour at
clipLevel = getLevel(histData,level);
loops = 0;
while isempty(clipLevel)   % adjust threshold if necessary (need to find a valid endpoint)
    loops = loops+1;    % increment counter
    clipLevel = getLevel(histData,level-1*loops);
end
% make a contour map
figure
contourData = contour(X,Y,histData,[clipLevel,clipLevel]);
set(gca,'YDir','reverse')
close(gcf)  % close the figure because we don't need it

end
% -------------------------------------------------------------------------

function clipLevel = getLevel(histData,level)
% find the histogram value corresponding to a specific normalized quantity
% of the data

histDataVect = reshape(histData,[],1);  % make into a vector for sorting
histDataVectSorted = sort(histDataVect,'descend');  % sort probabilities in descending order
histDataVectSum = cumsum(histDataVectSorted);   % sum over sorted probabilities

levelIndex = find(histDataVectSum>level,1); % find index of first value to cross level threshold
clipLevel = histDataVectSorted(levelIndex); % get the value of the histogram data at this point

end
% -------------------------------------------------------------------------

function addScaleBar(img,lengthArcmin,pixPerDeg)
% function for adding a scale bar of a certain length to a figure. Make
% sure 'hold on' is applied before calling this function

hSca = 20;      % height of 20 pixels for scale bar
wSca = (lengthArcmin/60)*pixPerDeg;   % width: 10 arcmin
xSca = 20;                      % left edge
ySca = size(img,1)-20-hSca;     % top edge
r = rectangle('Position',[xSca,ySca,wSca,hSca]);
r.FaceColor = [1 1 1];
r.EdgeColor = 'none';
textToDisplay = sprintf('%d arcmin',lengthArcmin);
t = text(xSca,ySca-1.2*hSca,textToDisplay);
t.Color = 'w';
t.FontSize = 12;
t.FontWeight = 'bold';

end

