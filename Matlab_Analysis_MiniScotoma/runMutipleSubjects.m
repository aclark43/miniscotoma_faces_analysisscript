clear
clc
close all

%% Subject Notes
%   AMC = New eyeris, Ashley = old eyeris
%   Z091 = Zac
%   A151 = Soma
%% Params
figures = struct(...
    'FIGURE_ON', 0,... %Show individual trial with traces in 2D space
    'VIDEO', 0,... %Wills save video of FIGURE_ON figures
    'FIXATION_ANALYSIS',0,... %Plots psychometric curves
    'QUICK_ANALYSIS',0,... %Only plots psychometric curves
    'CHECK_TRACES',0,... %will plot traces
    'COMPARE_SPANS',0); %0 = all spans, 1 = small spans, 2 = large spans

machine = {'D'}; %(D = DDPI, A = DPI)
makemovie = 0;
% subjectsAll = {'Z124','Z091','A144','Z162','A188','Z151'};
% subjectsAll = {'Z091', 'Sanjana', 'AMC'};
subjectsAll = {'Z124','Z091','A144','Z162','A188','A151','AMC'}; 
oldEyeris = [0];
conditionsAll = {'Control','Scotoma'}; %'Control',
imageSize = [72/2];
session = {'ALL'}; %%ALL


for ii = 1:length(subjectsAll)
    axisVal = imageSize(1);
    %% Load in Data
    subjFigPath = strcat('../../../Documents/Overleaf_MiniScotoma/figures');
    if oldEyeris(ii)
        [dataAll] = loadInData(ii, conditionsAll, session, figures, subjectsAll{ii});
    else
        subjDataPath = strcat(sprintf('X:/Ashley/FacesScotoma/%s', subjectsAll{ii}));
        allfiles = dir(subjDataPath);
        counterS = 1; 
        counterN = 1;
        for i = find(~[allfiles.isdir])
            if startsWith(string(allfiles(i).name),["scot"]) %scotoma condition
                %                 data{ii}.Scotoma{counterS}
                data{ii}.Scotoma{counterS} = load(sprintf('%s/%s',subjDataPath,allfiles(i).name));
%                 [data{ii}.Scotoma{counterS}] = loadInDataNewEyeris (ii,data);
%                 dateCollect = datetime(str2double(temp.eis_data.filename(5:8)),...
%                     str2double(temp.eis_data.filename(9:10)),...
%                     str2double(temp.eis_data.filename(11:12)));
%                 
%                 temppptrials = convertDataToPPtrials(temp.eis_data,dateCollect);
%                 processedPPTrials = newEyerisEISRead(temppptrials);
%                 data{ii}.Scotoma = [data{ii}.Scotoma processedPPTrials];
                counterS = counterS+1;
            else
                                

                data{ii}.Control{counterN} = load(sprintf('%s/%s',subjDataPath,allfiles(i).name));
%                 [data{ii}.Control{counterN}] = loadInDataNewEyeris (ii,data);
%                 dateCollect = datetime(str2double(temp.eis_data.filename(5:8)),...
%                     str2double(temp.eis_data.filename(9:10)),...
%                     str2double(temp.eis_data.filename(11:12)));
%                 
%                 temppptrials = convertDataToPPtrials(temp.eis_data,dateCollect);
%                 processedPPTrials = newEyerisEISRead(temppptrials);
%                 data{ii}.Control = [data{ii}.Scotoma processedPPTrials];
                counterN = counterN+1;
            end
            
        end
[dataAll] = loadInDataNewEyeris (ii,data);
    end
    
    fixIdxS = dataAll{ii}.Scotoma.fixation;
    fixIdxC = dataAll{ii}.Control.fixation ;
    
    dataAll{ii}.Control.allXClean = dataAll{ii}.Control.allX(...
        find(dataAll{ii}.Control.allX < axisVal & dataAll{ii}.Control.allX > -axisVal ...
        & dataAll{ii}.Control.allY < axisVal & dataAll{ii}.Control.allY > -axisVal));
    dataAll{ii}.Control.allYClean = dataAll{ii}.Control.allY(...
        find(dataAll{ii}.Control.allY < axisVal & dataAll{ii}.Control.allY > -axisVal ...
        & dataAll{ii}.Control.allX < axisVal & dataAll{ii}.Control.allX > -axisVal));
    
    dataAll{ii}.Control.fixCleanC = dataAll{ii}.Control.fixSamIdx(...
        find(dataAll{ii}.Control.allX < axisVal & dataAll{ii}.Control.allX > -axisVal ...
        & dataAll{ii}.Control.allY < axisVal & dataAll{ii}.Control.allY > -axisVal));
    
    
    dataAll{ii}.Scotoma.allXClean = dataAll{ii}.Scotoma.allX(...
        find(dataAll{ii}.Scotoma.allX < axisVal & dataAll{ii}.Scotoma.allX > -axisVal ...
        & dataAll{ii}.Scotoma.allY < axisVal & dataAll{ii}.Scotoma.allY > -axisVal));
    dataAll{ii}.Scotoma.allYClean = dataAll{ii}.Scotoma.allY(...
        find(dataAll{ii}.Scotoma.allY < axisVal & dataAll{ii}.Scotoma.allY > -axisVal ...
        & dataAll{ii}.Scotoma.allX < axisVal & dataAll{ii}.Scotoma.allX > -axisVal));
    
    dataAll{ii}.Scotoma.fixCleanS = dataAll{ii}.Scotoma.fixSamIdx(...
        find(dataAll{ii}.Scotoma.allX < axisVal & dataAll{ii}.Scotoma.allX > -axisVal ...
        & dataAll{ii}.Scotoma.allY < axisVal & dataAll{ii}.Scotoma.allY > -axisVal));
    %% Performance Figure per Subject
    %   Overall performance; cumalitive.
    
    %     for ii = 1:length(subjectsAll)
%     performanceC = sum(dataAll{ii}.Control.correct,'omitnan')/...
%         sum((~fixIdxC))*100;
%     performanceS = sum(dataAll{ii}.Scotoma.correct,'omitnan')/...
%         sum((~fixIdxS))*100;
    performanceC = nanmean(dataAll{ii}.Control.correct)*100;
    performanceS = nanmean(dataAll{ii}.Scotoma.correct)*100;
    %     end
    figure;
    % subplot(1,3,3)
    errorbar([0 1], [performanceC performanceS], ...
        [sem(dataAll{ii}.Control.correct * 100) sem(dataAll{ii}.Scotoma.correct*100)],...
        '-o','Color','k','MarkerSize',10,...
        'MarkerEdgeColor','k','MarkerFaceColor','k');
    ylim([40 100])
    xlim([-.5 1.5])
    xticks([0 1])
    xticklabels({'Control','Scotoma'});
    ylabel('Performance');
    % line([-.5 1.5],[50 50],'Color','red','LineStyle','--')
    yticks([50 60 70 80 90 100])
    
    saveas(gcf,strcat(subjFigPath,'/',sprintf('Performance%s',subjectsAll{ii}),'.png'));
    saveas(gcf,strcat(subjFigPath,'/',sprintf('Performance%s',subjectsAll{ii}),'.epsc'));
    
    %% Response Time
    figure;
    % subplot(1,3,3)
    errorbar([0 1], [mean([dataAll{ii}.Control.RT{find(~fixIdxC)}]) ...
        mean([dataAll{ii}.Scotoma.RT{find(~fixIdxS)}])], ...
        [sem([dataAll{ii}.Control.RT{find(~fixIdxC)}]) ...
        sem([dataAll{ii}.Scotoma.RT{find(~fixIdxS)}])],...
        '-o','Color','k','MarkerSize',10,...
        'MarkerEdgeColor','k','MarkerFaceColor','k');
    % ylim([40 100])
    xlim([-.5 1.5])
    xticks([0 1])
    xticklabels({'Control','Scotoma'});
    ylabel('Reaction Time');
    [~,P] = ttest2([dataAll{ii}.Control.RT{find(~fixIdxC)}],...
        [dataAll{ii}.Scotoma.RT{find(~fixIdxS)}]);
    % line([-.5 1.5],[50 50],'Color','red','LineStyle','--')
    % yticks([50 60 70 80 90 100])
    saveas(gcf,strcat(subjFigPath,'/',sprintf('ReactionTime%s',subjectsAll{ii}),'.png'));
    saveas(gcf,strcat(subjFigPath,'/',sprintf('ReactionTime%s',subjectsAll{ii}),'.epsc'));
%% MS Analysis    
    %% Histogram of MS
    figure;
    subplot(1,2,1)
    histogram(dataAll{ii}.Control.msAmpAll);
    title('Control')
    subplot(1,2,2)
    histogram(dataAll{ii}.Scotoma.msAmpAll);
    title('Scotoma')
    suptitle('Histogram MS');
    saveas(gcf,strcat(subjFigPath,'/',sprintf('MSCenterStartPolar%s',subjectsAll{ii}),'.png'));
    saveas(gcf,strcat(subjFigPath,'/',sprintf('MSCenterStartPolar%s',subjectsAll{ii}),'.epsc'));
    
    %% Plot MS Landing
    % Absolute MS Landing.
    figure;
    subplot(4,2,1)
    % Im=imread('1_Neutral_f_03_176px.bmp');
    Im = imread(strcat('..\..\..\Images\176x176\','1_Neutral_f_03_176px','.bmp'));
    Im = Im(:,:,1);
    Im = flipud(Im);
    imagesc([-axisVal axisVal], [-axisVal axisVal],Im)
    % image(-axisVal/2:axisVal, -axisVal:axisVal, Im, 'CDataMapping', 'scaled');
    colormap gray
    axis('xy')
    axis image
    hold on
    for i = find(~isnan(dataAll{ii}.Control.correct))
        if isempty(dataAll{ii}.Control.msStart{i})
            continue
        end
        scatterp(1) = scatter(dataAll{ii}.Control.msStart{i}(2,:),...
            dataAll{ii}.Control.msStart{i}(3,:),...
            1,'MarkerFaceColor','b','MarkerEdgeColor','b');
        hold on
    end
    ylabel('Control')
    title('Start MS')
    ylim([-axisVal axisVal])
    xlim([-axisVal axisVal])
    
    subplot(4,2,2)
    Im = imread(strcat('..\..\..\Images\176x176\','1_Neutral_f_03_176px','.bmp'));
    Im = Im(:,:,1);
    Im = flipud(Im);
    imagesc([-axisVal axisVal], [-axisVal axisVal],Im)
    % image(-axisVal/2:axisVal, -axisVal:axisVal, Im, 'CDataMapping', 'scaled');
    colormap gray
    axis('xy')
    axis image
    hold on
    controlAll = [];
    controlAllY = [];
    for i = find(~isnan(dataAll{ii}.Control.correct))
        if isempty(dataAll{ii}.Control.msEnd{i})
            continue
        end
        scatterp(2) = scatter(dataAll{ii}.Control.msEnd{i}(2,:),...
            dataAll{ii}.Control.msEnd{i}(3,:),...
            1,'MarkerFaceColor','r','MarkerEdgeColor','r');
        hold on
        controlAll = [controlAll dataAll{ii}.Control.msEnd{i}(2,:)];
        controlAllY = [controlAllY dataAll{ii}.Control.msEnd{i}(3,:)];
    end
    axis square
    ylim([-axisVal axisVal])
    xlim([-axisVal axisVal])
    title('End MS')
    
    subplot(4,2,3)
    Im = imread(strcat('..\..\..\Images\176x176\','1_Neutral_f_03_176px','.bmp'));
    Im = Im(:,:,1);
    Im = flipud(Im);
    imagesc([-axisVal axisVal], [-axisVal axisVal],Im)
    % image(-axisVal/2:axisVal, -axisVal:axisVal, Im, 'CDataMapping', 'scaled');
    colormap gray
    axis('xy')
    axis image
    hold on
    for i = find(~isnan(dataAll{ii}.Scotoma.correct))
        if isempty(dataAll{ii}.Scotoma.msStart{i})
            continue
        end
        scatterp(1) = scatter(dataAll{ii}.Scotoma.msStart{i}(2,:),...
            dataAll{ii}.Scotoma.msStart{i}(3,:),...
            1,'MarkerFaceColor','b','MarkerEdgeColor','b');
        hold on
    end
    
    axis square
    ylabel('Scotoma')
    title('Start MS')
    ylim([-axisVal axisVal])
    xlim([-axisVal axisVal])
    
    subplot(4,2,4)
    Im = imread(strcat('..\..\..\Images\176x176\','1_Neutral_f_03_176px','.bmp'));
    Im = Im(:,:,1);
    Im = flipud(Im);
    imagesc([-axisVal axisVal], [-axisVal axisVal],Im)
    % image(-axisVal/2:axisVal, -axisVal:axisVal, Im, 'CDataMapping', 'scaled');
    colormap gray
    axis('xy')
    axis image
    hold on
    scotomaEndAll = [];
    scotomaEndAllY = [];
     for i = find(~isnan(dataAll{ii}.Scotoma.correct))
         if isempty(dataAll{ii}.Scotoma.msEnd{i})
            continue
         end
        scotomaEndAll = [scotomaEndAll  dataAll{ii}.Scotoma.msEnd{i}(2,:)];
        scotomaEndAllY = [scotomaEndAllY  dataAll{ii}.Scotoma.msEnd{i}(3,:)];
        scatterp(2) = scatter(dataAll{ii}.Scotoma.msEnd{i}(2,:),...
            dataAll{ii}.Scotoma.msEnd{i}(3,:),...
            1,'MarkerFaceColor','r','MarkerEdgeColor','r');
        hold on
    end
    axis square
    title('End MS')
    ylim([-axisVal axisVal])
    xlim([-axisVal axisVal])
    
    subplot(4,2,5)
    axis([-axisVal axisVal -axisVal axisVal])
    hold on
    allMSStart = [];
    allMSStartY = [];
    for i = find(isnan(dataAll{ii}.Scotoma.correct))
        if i > length(dataAll{ii}.Scotoma.msStart)
            continue
        end
        if isempty(dataAll{ii}.Scotoma.msStart{i})
            continue;
        end
        allMSStart = [allMSStart dataAll{ii}.Scotoma.msStart{i}(2,:)];
        allMSStartY = [allMSStartY dataAll{ii}.Scotoma.msStart{i}(3,:)];
    end
    plot(allMSStart,allMSStartY,'o',...
            'MarkerFaceColor','b','MarkerEdgeColor','b');
        hold on
    axis square
    rectangle('Position',[-2.5 -2.5 5 5],'EdgeColor','r');
    ylabel('Fixation Scotoma')
    title('Start MS')
    ylim([-axisVal axisVal])
    xlim([-axisVal axisVal])
    
    subplot(4,2,6)
    axis([-axisVal axisVal -axisVal axisVal])
    hold on
    fixSctom = [];
    fixSctomY = [];
    for i = find(isnan(dataAll{ii}.Scotoma.correct))
         if i > length(dataAll{ii}.Scotoma.msEnd)
            continue
        end
        if isempty(dataAll{ii}.Scotoma.msEnd{i})
            continue;
        end
        fixSctom = [fixSctom dataAll{ii}.Scotoma.msEnd{i}(2,:)];
        fixSctomY = [fixSctomY dataAll{ii}.Scotoma.msEnd{i}(3,:)];
        plot(dataAll{ii}.Scotoma.msEnd{i}(2,:),...
            dataAll{ii}.Scotoma.msEnd{i}(3,:),...
            'o','MarkerFaceColor','b','MarkerEdgeColor','b');
        hold on
    end

    axis square
    rectangle('Position',[-2.5 -2.5 5 5],'EdgeColor','r');
    title('End MS')
    ylim([-axisVal axisVal])
    xlim([-axisVal axisVal])
    
    
    
    subplot(4,2,7)
    axis([-axisVal axisVal -axisVal axisVal])
    hold on
    for i = find(isnan(dataAll{ii}.Control.correct))
        if isempty(dataAll{ii}.Control.msStart{i})
            continue;
        end
        plot(dataAll{ii}.Control.msStart{i}(2,:),...
            dataAll{ii}.Control.msStart{i}(3,:),...
            'o','MarkerFaceColor','b','MarkerEdgeColor','b');
        hold on
    end
    
    axis square
    rectangle('Position',[-2.5 -2.5 5 5],'EdgeColor','r');
    ylabel('Fixation Control')
    title('Start MS')
    ylim([-axisVal axisVal])
    xlim([-axisVal axisVal])
    
    subplot(4,2,8)
    axis([-axisVal axisVal -axisVal axisVal])
    hold on
    fixCont = [];
    fixContT = [];
    for i = find(isnan(dataAll{ii}.Control.correct))
        if isempty(dataAll{ii}.Control.msEnd{i})
            continue;
        end
        fixCont = [fixCont dataAll{ii}.Control.msEnd{i}(2,:)];
        fixContT = [fixContT dataAll{ii}.Control.msEnd{i}(3,:)];
        plot(dataAll{ii}.Control.msEnd{i}(2,:),...
            dataAll{ii}.Control.msEnd{i}(3,:),...
            'o','MarkerFaceColor','b','MarkerEdgeColor','b');
        hold on
    end

    axis square
    rectangle('Position',[-2.5 -2.5 5 5],'EdgeColor','r');
    title('End MS')
    ylim([-axisVal axisVal])
    xlim([-axisVal axisVal])
    
    saveas(gcf,strcat(subjFigPath,'/',sprintf('MSStartLandPattern%s',subjectsAll{ii}),'.png'));
    saveas(gcf,strcat(subjFigPath,'/',sprintf('MSStartLandPattern%s',subjectsAll{ii}),'.epsc'));
    %% MS HeatMap of Landing
    
    h1 = figure;
    subplot(2,2,1)
    generateHeatMapSimple( ...
        fixCont,...
        fixContT,...
        'Bins', 5,...
        'StimulusSize', 10,...
        'AxisValue', axisVal,...
        'Uncrowded', 0,...
        'Borders', 1);
    axis square
    rectangle('Position',[-2.5 -2.5 5 5],'EdgeColor','r');
    caxis([-.02 1])
    title('Control')
    
     subplot(2,2,2)
%     I=imread('1_Neutral_f_03_176px.png');
%     image(-axisVal:axisVal, -axisVal:axisVal, flipud(I), 'CDataMapping', 'scaled');
%     axis('xy')
    axis image
    hold on
    generateHeatMapSimple( ...
        fixSctom,...
        fixSctomY,...
        'Bins', 5,...
        'StimulusSize', 10,...
        'AxisValue', axisVal,...
        'Uncrowded', 0,...
        'Borders', 1);
    axis square
        rectangle('Position',[-2.5 -2.5 5 5],'EdgeColor','r');
    caxis([-.02 1])
    title('Scotoma')
    
   
    
    subplot(2,2,4)
    I=imread('1_Neutral_f_03_176px.png');
    image(-axisVal:axisVal, -axisVal:axisVal, flipud(I), 'CDataMapping', 'scaled');
    axis('xy')
    axis image
    hold on
    generateHeatMapSimple( ...
        scotomaEndAll,...
        scotomaEndAllY,...
        'Bins', 6,...
        'StimulusSize', 10,...
        'AxisValue', axisVal,...
        'Uncrowded', 0,...
        'Borders', 1);
    axis square
    caxis([-.02 1])
    title('Scotoma')
    
    subplot(2,2,3)
    I=imread('1_Neutral_f_03_176px.png');
    image(-axisVal:axisVal, -axisVal:axisVal, flipud(I), 'CDataMapping', 'scaled');
    axis('xy')
    axis image
    hold on
    generateHeatMapSimple( ...
        controlAll,...
        controlAllY,...
        'Bins', 6,...
        'StimulusSize', 10,...
        'AxisValue', axisVal,...
        'Uncrowded', 0,...
        'Borders', 1);
    axis square
    caxis([-.02 1])
    title('Control')
    saveas(gcf,strcat(subjFigPath,'/',sprintf('HeatMapEnding%s',subjectsAll{ii}),'.png'));
    saveas(gcf,strcat(subjFigPath,'/',sprintf('HeatMapEnding%s',subjectsAll{ii}),'.epsc'));
    
    %% MS Analysis using Polar Plot
    %   Both the poloar plot and the histogram of all directions. Both are set
    %   to ms starting position set to 0,0.
    
    %%%%%control
    nX = [];
    nY = [];
    for i = find(dataAll{ii}.Control.fixation)
        if isempty(dataAll{ii}.Control.msEnd{i})
            continue
        end
        x = dataAll{ii}.Control.msEnd{i}(2,:) - dataAll{ii}.Control.msStart{i}(2,:);
        y = dataAll{ii}.Control.msEnd{i}(3,:) - dataAll{ii}.Control.msStart{i}(3,:);
        nX = [nX x];
        nY = [nY y];
    end
    figure;
    subplot(3,2,1)
    [theta,rho] = cart2pol(nX,nY);
    % [t, u] = sort(theta);
    polarplot(theta,rho,'*');
    rlim([0 axisVal]);
    title('Control')
    subplot(3,2,2)
    polarhistogram(theta);
    title('Control')
    
    %%%% scotoma
    nX = [];
    nY = [];
    for i = find(dataAll{ii}.Control.fixation)
        if isempty(dataAll{ii}.Control.msEnd{i})
            continue
        end
        x = dataAll{ii}.Control.msEnd{i}(2,:) - dataAll{ii}.Control.msStart{i}(2,:);
        y = dataAll{ii}.Control.msEnd{i}(3,:) - dataAll{ii}.Control.msStart{i}(3,:);
        nX = [nX x];
        nY = [nY y];
    end
    
    subplot(3,2,3)
    [theta,rho] = cart2pol(nX,nY);
    % [t, u] = unique(round(theta,2));
    polarplot(theta,rho,'*');
    rlim([0 axisVal]);
    title('Scotoma');
    
    subplot(3,2,4)
    polarhistogram(theta);
    title('Scotoma');
    
    %%%%fixation
    nX = [];
    nY = [];
     for i = find(dataAll{ii}.Control.fixation)
        if isempty(dataAll{ii}.Control.msEnd{i})
            continue
        end
        x = dataAll{ii}.Control.msEnd{i}(2,:) - dataAll{ii}.Control.msStart{i}(2,:);
        y = dataAll{ii}.Control.msEnd{i}(3,:) - dataAll{ii}.Control.msStart{i}(3,:);
        nX = [nX x];
        nY = [nY y];
    end
    for i = find(dataAll{ii}.Scotoma.fixation)
        x = dataAll{ii}.Scotoma.msEnd{i}(2,:) - dataAll{ii}.Scotoma.msStart{i}(2,:);
        y = dataAll{ii}.Scotoma.msEnd{i}(3,:) - dataAll{ii}.Scotoma.msStart{i}(3,:);
        nX = [nX x];
        nY = [nY y];
    end
    subplot(3,2,5)
    [theta,rho] = cart2pol(nX,nY);
    % [t, u] = unique(round(theta,2));
    polarplot(theta,rho,'*');
    rlim([0 axisVal]);
    title('Fixation');
    
    subplot(3,2,6)
    polarhistogram(theta); %rose(theta)
    title('Fixation');
    
    
    suptitle('MS Centered at Start');
    saveas(gcf,strcat(subjFigPath,'/',sprintf('MSCenterStartPolar%s',subjectsAll{ii}),'.png'));
    saveas(gcf,strcat(subjFigPath,'/',sprintf('MSCenterStartPolar%s',subjectsAll{ii}),'.epsc'));
    
    %% DotMap Over Face
    %   Similar to heat map, but with slightly transparents dots showing
    %   individual samples across time.
    h = figure;
    subplot(1,2,1)
    Im = imread(strcat('..\..\..\Images\176x176\','1_Neutral_f_03_176px','.bmp'));
    Im = Im(:,:,1);
    Im = flipud(Im);
    imagesc([-axisVal axisVal], [-axisVal axisVal],Im)
    % image(-axisVal/2:axisVal, -axisVal:axisVal, Im, 'CDataMapping', 'scaled');
    colormap gray
    axis('xy')
    axis image
    hold on
    scatter1 = scatter(...
        dataAll{ii}.Control.allXClean(find(~dataAll{ii}.Control.fixCleanC)), ...
        dataAll{ii}.Control.allYClean(find(~dataAll{ii}.Control.fixCleanC)), ...
        6,'MarkerFaceColor','b','MarkerEdgeColor','b');
    % Set property MarkerFaceAlpha and MarkerEdgeAlpha to <1.0
    scatter1.MarkerFaceAlpha = .007;
    scatter1.MarkerEdgeAlpha = .007;
    % plot(allX,allY,'.');
    axis square
    title('Control')
    
    subplot(1,2,2)
    Im = imread(strcat('..\..\..\Images\176x176\','1_Neutral_f_03_176px','.bmp'));
    Im = Im(:,:,1);
    Im = flipud(Im);
    imagesc([-axisVal axisVal], [-axisVal axisVal],Im)
    % image(-axisVal/2:axisVal, -axisVal:axisVal, Im, 'CDataMapping', 'scaled');
    colormap gray
    axis('xy')
    axis image
    hold on
    scatter1 = scatter(...
        dataAll{ii}.Scotoma.allXClean(find(~dataAll{ii}.Scotoma.fixCleanS)), ...
        dataAll{ii}.Scotoma.allYClean(find(~dataAll{ii}.Scotoma.fixCleanS)), ...
        6,'MarkerFaceColor','b','MarkerEdgeColor','b');
    % Set property MarkerFaceAlpha and MarkerEdgeAlpha to <1.0
    scatter1.MarkerFaceAlpha = .007;
    scatter1.MarkerEdgeAlpha = .007;
    % plot(allX,allY,'.');
    axis square
    title('Scotoma')
    saveas(gcf,strcat(subjFigPath,'/',sprintf('Pattern%s',subjectsAll{ii}),'.png'));
    saveas(gcf,strcat(subjFigPath,'/',sprintf('Pattern%s',subjectsAll{ii}),'.epsc'));
    
    % % %
    
    %% Heat Map of All X and Y Over Face
    
    h2 = figure;
    subplot(2,2,1)
    I=imread('1_Neutral_f_03_176px.png');
    image(-axisVal:axisVal, -axisVal:axisVal, flipud(I), 'CDataMapping', 'scaled');
    axis('xy')
    axis image
    hold on
    generateHeatMapSimple( ...
        dataAll{ii}.Control.allXClean(find(~dataAll{ii}.Control.fixCleanC)), ...
        dataAll{ii}.Control.allYClean(find(~dataAll{ii}.Control.fixCleanC)), ...
        'Bins', 20,...
        'StimulusSize', 10,...
        'AxisValue', axisVal,...
        'Uncrowded', 0,...
        'Borders', 1);
    axis square
    caxis([-.02 1])
    title('Control')
    % caxis([0.29 1])
    
    mean(dataAll{ii}.Control.allXClean(find(~dataAll{ii}.Control.fixCleanC)))
    mean(dataAll{ii}.Control.allYClean(find(~dataAll{ii}.Control.fixCleanC)))
    %     xticks([-axisVal*2 -40 -20 0 20 40 axisVal*2])
    %     yticks([-axisVal*2 -40 -20 0 20 40 axisVal*2])
    
    subplot(2,2,2)
    I=imread('1_Neutral_f_03_176px.png');
    image(-axisVal:axisVal, -axisVal:axisVal, flipud(I), 'CDataMapping', 'scaled');
    axis('xy')
    axis image
    hold on
    generateHeatMapSimple( ...
        dataAll{ii}.Scotoma.allXClean(find(~dataAll{ii}.Scotoma.fixCleanS)), ...
        dataAll{ii}.Scotoma.allYClean(find(~dataAll{ii}.Scotoma.fixCleanS)), ...
        'Bins', 20,...
        'StimulusSize', 10,...
        'AxisValue', axisVal,...
        'Uncrowded', 0,...
        'Borders', 1);
    axis square
    caxis([-.02 1])
    title('Scotoma')
    % caxis([.29 1])
    mean(dataAll{ii}.Scotoma.allXClean(find(~dataAll{ii}.Scotoma.fixCleanS)))
    mean(dataAll{ii}.Scotoma.allYClean(find(~dataAll{ii}.Scotoma.fixCleanS)))
    %     xticks([-axisVal*2 -40 -20 0 20 40 axisVal*2])
    %     yticks([-axisVal*2 -40 -20 0 20 40 axisVal*2])
    %     saveas(gcf,strcat(subjFigPath,'/',sprintf('PatternHeatMap%s',subjectsAll{ii}),'.png'));
    %     saveas(gcf,strcat(subjFigPath,'/',sprintf('PatternHeatMap%s',subjectsAll{ii}),'.epsc'));
    %
    %     figure;
    subplot(2,2,3)
    generateHeatMapSimple( ...
        [dataAll{ii}.Control.allXClean(find(dataAll{ii}.Control.fixCleanC))], ...
        [dataAll{ii}.Control.allYClean(find(dataAll{ii}.Control.fixCleanC))], ...
        'Bins', 20,...
        'StimulusSize', 5,...
        'AxisValue', axisVal,...
        'Uncrowded', 4,...
        'Borders', 1);
    axis square
    caxis([-.02 1])
    title('Control, Fixation')
    % caxis([.29 1])
    
    figure;
     generateHeatMapSimple( ...
        [dataAll{ii}.Control.allXClean(find(dataAll{ii}.Control.fixCleanC))], ...
        [dataAll{ii}.Control.allYClean(find(dataAll{ii}.Control.fixCleanC))], ...
        'Bins', 20,...
        'StimulusSize', 5,...
        'AxisValue', axisVal,...
        'Uncrowded', 4,...
        'Borders', 1);
    
    bcea = get_bcea([dataAll{ii}.Control.allXClean(find(dataAll{ii}.Control.fixCleanC))]/60,...
        [dataAll{ii}.Control.allYClean(find(dataAll{ii}.Control.fixCleanC))]/60);
    
    subplot(2,2,4)
    generateHeatMapSimple( ...
        [dataAll{ii}.Scotoma.allXClean(find(dataAll{ii}.Scotoma.fixCleanS)) ], ...
        [dataAll{ii}.Scotoma.allYClean(find(dataAll{ii}.Scotoma.fixCleanS))], ...
        'Bins', 20,...
        'StimulusSize', 5,...
        'AxisValue', axisVal,...
        'Uncrowded', 4,...
        'Borders', 1);
    axis square
    caxis([-.02 1])
    title('Scotoma, Fixation')
   
    
    
    %     xticks([-axisVal*2 -40 -20 0 20 40 axisVal*2])
    %     yticks([-axisVal*2 -40 -20 0 20 40 axisVal*2])
    saveas(gcf,strcat(subjFigPath,'/',sprintf('PatternHeatMapAll%s',subjectsAll{ii}),'.png'));
    saveas(gcf,strcat(subjFigPath,'/',sprintf('PatternHeatMapAll%s',subjectsAll{ii}),'.epsc'));
    
    %% Heatmaps for All X Y Over Time
    %   Heatmap for all x y position across time within 1x1 deg region. IE Section
    %   1 is each trials 1st 330 ms of traces. Full stimulus duration is 1
    %   second.
    %     for ii = 1:length(subjectsAll)
    spaceBins = 300;
    lengthOfIncluded = 1000;
    xC = [];
    yC = [];
    counter = 1;
    for i = find(~fixIdxC)%length(dataAll{ii}.Control.x)
        xC(counter,:) = dataAll{ii}.Control.x{i}(1:lengthOfIncluded);
        yC(counter,:) = dataAll{ii}.Control.y{i}(1:lengthOfIncluded);
        counter = counter + 1;
    end
    xS = [];
    yS = [];
    counter = 1;
    for i = find(~fixIdxS)
        xS(counter,:) = dataAll{ii}.Scotoma.x{i}(1:lengthOfIncluded);
        yS(counter,:) = dataAll{ii}.Scotoma.y{i}(1:lengthOfIncluded);
        counter = counter + 1;
    end
    xFC = [];
    yFC = [];
    counter = 1;
    for i = find(fixIdxC)
        xFC(counter,:) = dataAll{ii}.Control.x{i}(1:lengthOfIncluded);
        yFC(counter,:) = dataAll{ii}.Control.y{i}(1:lengthOfIncluded);
        counter = counter + 1;
    end
    xFS = [];
    yFS = [];
    counter = 1;
    for i = find(fixIdxS)
        xFS(counter,:) = dataAll{ii}.Scotoma.x{i}(1:lengthOfIncluded);
        yFS(counter,:) = dataAll{ii}.Scotoma.y{i}(1:lengthOfIncluded);
        counter = counter + 1;
    end
    
   
    if makemovie
        timeBins = 1:spaceBins:lengthOfIncluded;
    else
        timeBins = 1:spaceBins:lengthOfIncluded;
    end
    counter = 0;
    for t = 1:length(timeBins) - 1
        counter = counter + 1;
        timeXC{counter} = xC(:,timeBins(t):(timeBins(t+1)-1));
        timeYC{counter} = yC(:,timeBins(t):(timeBins(t+1)-1));
        timeXS{counter} = xS(:,timeBins(t):(timeBins(t+1)-1));
        timeYS{counter} = yS(:,timeBins(t):(timeBins(t+1)-1));
        timeXFC{counter} = xFC(:,timeBins(t):(timeBins(t+1)-1));
        timeYFC{counter} = yFC(:,timeBins(t):(timeBins(t+1)-1));
        timeXFS{counter} = xFS(:,timeBins(t):(timeBins(t+1)-1));
        timeYFS{counter} = yFS(:,timeBins(t):(timeBins(t+1)-1));
    end
    
    figure;
    binsOfFig = 10;
    for t1 = 1:counter
        tempXN = timeXC{t1}(:)';
        tempYN = timeYC{t1}(:)';
        timeXN{t1} = tempXN(...
            tempXN < axisVal & tempXN > -axisVal ...
            & tempYN < axisVal & tempYN > -axisVal);
        timeYN{t1} = tempYN(...
            tempYN < axisVal & tempYN > -axisVal ...
            & tempXN < axisVal & tempXN > -axisVal);
        
        subplot(4,round(lengthOfIncluded/spaceBins),t1)
        I=imread('1_Neutral_f_03_176px.png');
        image(-axisVal:axisVal, -axisVal:axisVal, flipud(I), 'CDataMapping', 'scaled');
        axis('xy')
        axis image
        hold on
        hold on
        generateHeatMapSimple( ...
            timeXN{t1}, ...
            timeYN{t1}, ...
            'Bins', binsOfFig,...
            'StimulusSize', 10,...
            'AxisValue', axisVal,...
            'Uncrowded', 0,...
            'Borders', 0);
        title(sprintf('Section %i',t1))
        caxis([-.02 1])
        line([-axisVal,axisVal],[0 0])
        line([0 0],[-axisVal,axisVal])
    end
    ylabel('Control')
   
    % saveas(gcf,'../../../Documents/Overleaf_MiniScotoma/figures/OverTimeControl.png');
    % saveas(gcf,'../../../Documents/Overleaf_MiniScotoma/figures/OverTimeControl.epsc');
    
    % figure;
    for t2 = counter+1:counter+round(lengthOfIncluded/spaceBins)
        tempXNS = timeXS{t2}(:)';
        tempYNS = timeYS{t2}(:)';
        timeXNS{t2} = tempXNS(...
            tempXNS < axisVal & tempXNS > -axisVal ...
            & tempYNS < axisVal & tempYNS > -axisVal);
        timeYNS{t2} = tempYNS(...
            tempYNS < axisVal & tempYNS > -axisVal ...
            & tempXNS < axisVal & tempXNS > -axisVal);
        
        subplot(4,round(lengthOfIncluded/spaceBins),t2)
        I=imread('1_Neutral_f_03_176px.png');
        image(-axisVal:axisVal, -axisVal:axisVal, flipud(I), 'CDataMapping', 'scaled');
        axis('xy')
        axis image
        hold on
        generateHeatMapSimple( ...
            timeXNS{t2}, ...
            timeYNS{t2}, ...
            'Bins', binsOfFig,...
            'StimulusSize', 10,...
            'AxisValue', axisVal,...
            'Uncrowded', 0,...
            'Borders', 0);
        title(sprintf('Section %i',t2))
        caxis([-.02 1])
        line([-axisVal,axisVal],[0 0])
        line([0 0],[-axisVal,axisVal])
    end
    ylabel('Scotoma')
    
    % figure;
    for t3 = (counter+counter+1):(counter+round(lengthOfIncluded/spaceBins)) + counter
        tempXNS = timeXFC{t3}(:)';
        tempYNS = timeYFC{t3}(:)';
        timeXNS{t3} = tempXNS(...
            tempXNS < axisVal & tempXNS > -axisVal ...
            & tempYNS < axisVal & tempYNS > -axisVal);
        timeYNS{t3} = tempYNS(...
            tempYNS < axisVal & tempYNS > -axisVal ...
            & tempXNS < axisVal & tempXNS > -axisVal);
        
        subplot(4,round(lengthOfIncluded/spaceBins),t3)
        %     I=imread('1_Neutral_f_03_176px.png');
        %     image(-axisVal:axisVal, -axisVal:axisVal, I, 'CDataMapping', 'scaled');
        %     hold on
        generateHeatMapSimple( ...
            timeXNS{t3}, ...
            timeYNS{t3}, ...
            'Bins', binsOfFig,...
            'StimulusSize', 5,...
            'AxisValue', axisVal,...
            'Uncrowded', 4,...
            'Borders', 0);
        title(sprintf('Section %i',t3))
        caxis([-.02 1])
        line([-axisVal,axisVal],[0 0])
        line([0 0],[-axisVal,axisVal])
        axis square
    end
    ylabel('Fixation, Control')
    
    for t4 = (counter*2+counter+1):(counter+round(lengthOfIncluded/spaceBins)) + counter*2
        tempXNS = timeXFS{t4}(:)';
        tempYNS = timeYFS{t4}(:)';
        timeXNS{t4} = tempXNS(...
            tempXNS < axisVal & tempXNS > -axisVal ...
            & tempYNS < axisVal & tempYNS > -axisVal);
        timeYNS{t4} = tempYNS(...
            tempYNS < axisVal & tempYNS > -axisVal ...
            & tempXNS < axisVal & tempXNS > -axisVal);
        
        subplot(4,round(lengthOfIncluded/spaceBins),t4)
        %     I=imread('1_Neutral_f_03_176px.png');
        %     image(-axisVal:axisVal, -axisVal:axisVal, I, 'CDataMapping', 'scaled');
        %     hold on
        generateHeatMapSimple( ...
            timeXNS{t4}, ...
            timeYNS{t4}, ...
            'Bins', binsOfFig,...
            'StimulusSize', 5,...
            'AxisValue', axisVal,...
            'Uncrowded', 4,...
            'Borders', 0);
        title(sprintf('Section %i',t4))
        caxis([-.02 1])
        line([-axisVal,axisVal],[0 0])
        line([0 0],[-axisVal,axisVal])
        axis square
    end
    ylabel('Fixation, Scotoma')
    
    saveas(gcf,strcat(subjFigPath,'/',sprintf('OverTimeAll%s',subjectsAll{ii}),'.png'));
    saveas(gcf,strcat(subjFigPath,'/',sprintf('OverTimeAll%s',subjectsAll{ii}),'.epsc'));
    
    % saveas(gcf,'../../../Documents/Overleaf_MiniScotoma/figures/OverTimeFixation.png');
    % saveas(gcf,'../../../Documents/Overleaf_MiniScotoma/figures/OverTimeFixation.epsc');
    %     end
    
    
    
%%% MAKE MOVIE %%%%
if makemovie
    % Generate some data
    t=0:.01:2*pi;
    sin_x=sin(t);
    cos_x=cos(t);
    % Open a figure and crate the axes
    figure
    axes;
    %
    % STEP 1:
    %
    % Create and open the video object
    vidObj = VideoWriter('SIN_X_COS_X.avi');
    open(vidObj);
    %
    % Loop over the data to create the video
    for i=1:length(t)
        % Plot the data
        h(1)=plot(t(i),sin_x(i),'o','markerfacecolor','r','markersize',5);
        hold on
        plot(t(1:i),sin_x(1:i),'r')
        plot(t(1:i),cos_x(1:i),'b')
        h(2)=plot(t(i),cos_x(i),'o','markerfacecolor','b','markersize',5);
        set(gca,'xlim',[0 2*pi],'ylim',[-1.3 1.3])
        %
        % STEP 2
        %
        % Get the current frame
        currFrame = getframe;
        %
        % STEP 3
        %
        % Write the current frame
        writeVideo(vidObj,currFrame);
        %
        delete(h)
    end
    %
    % STEP 4
    %
    % Close (and save) the video object
    close(vidObj);
end

    %% Performance in Time Figure per Subject
    %   Divided the session into 3 different times. Each point has the number
    %   of trials indicated in the title (ie XX#C means there were XX trials
    %   for each Control point in all 3 figures).
    divideBy = 4;
    
    timeAcrossSessionC = floor(length(dataAll{ii}.Control.correct)/divideBy-1);
    timeAcrossSessionS = floor(length(dataAll{ii}.Scotoma.correct)/divideBy-1);
    timeDivideC = 1:timeAcrossSessionC:floor(length(dataAll{ii}.Control.correct));
    timeDivideS = 1:timeAcrossSessionS:floor(length(dataAll{ii}.Scotoma.correct));
    
    figure;
    for t = 1:divideBy
        performanceC = [];
        performanceS = [];
        %         for ii = 1:length(subjectsAll)
        performanceC = nansum(dataAll{ii}.Control.correct(timeDivideC(t):timeDivideC(t+1)-1))/...
            sum(~isnan(dataAll{ii}.Control.correct(timeDivideC(t):timeDivideC(t+1)-1)))*100;
        valC = sum(~isnan(dataAll{ii}.Control.correct(timeDivideC(t):timeDivideC(t+1)-1)));
        performanceS = nansum(dataAll{ii}.Scotoma.correct(timeDivideS(t):timeDivideS(t+1)-1))/...
            sum(~isnan(dataAll{ii}.Scotoma.correct(timeDivideS(t):timeDivideS(t+1)-1)))*100;
        valS = sum(~isnan(dataAll{ii}.Scotoma.correct(timeDivideS(t):timeDivideS(t+1)-1)));
        %         end
        
        subplot(2,3,t)
        errorbar([0 1], [performanceC performanceS], ...
            [sem(dataAll{ii}.Control.correct * 100) sem(dataAll{ii}.Scotoma.correct*100)],...
            '-o','Color','k','MarkerSize',10,...
            'MarkerEdgeColor','k','MarkerFaceColor','k');
        ylim([30 100])
        xlim([-.5 1.5])
        xticks([0 1])
        xticklabels({'Control','Scotoma'});
        ylabel('Performance');
        % line([-.5 1.5],[50 50],'Color','red','LineStyle','--')
        yticks([50 60 70 80 90 100])
        title(sprintf('Time Section %i',t));
    end
    suptitle(sprintf('%i#C, %i#S',valC,valS))
    saveas(gcf,'../../../Documents/Overleaf_MiniScotoma/figures/PerformanceInTime.png');
    saveas(gcf,'../../../Documents/Overleaf_MiniScotoma/figures/PerformanceInTime.epsc');
end


