function [dataAll] = loadInData(ii, conditionsAll, session, figures, nameSubj)

fixSamIdx = [];


    for i = 1:length(conditionsAll)
        dataAll{ii}.(conditionsAll{i}).msAllInfo = [1];
        clear pptrials
        temp = []; temp2 = []; temp3 = []; temp4 = []; temp5 = [];
        x = []; y = [];
        filepath = sprintf...
            ('../../../Data/%s/%s', ...
            nameSubj, conditionsAll{i});
        
        params.session = session;
%         params.practice = 35;
        
        [ pptrials ] = reBuildPPtrials (filepath, params);
        
        if figures.CHECK_TRACES
            if strcmp(session{1},'ALL')
                error('Must select only one session to check traces');
            end
            ppt_snellen = cleaningTrialsVisually(pptrials,...
                'StartTime','1',...
                'EndTime','TimeImageOff',...
                'AxisWindow', 60,...
                'Folder',fullfile(filepath,session{1}),...
                'Conversion', 1000/330,...
                'NewFileName', 'pptrials.mat');%Check Traces
        end
        dataAll{ii}.(conditionsAll{i}).pptrials = pptrials;
        
        allX = [];
        allY = [];
        for p = 1:length(pptrials)
            if pptrials{p}.FixationTrial
                dataAll{ii}.(conditionsAll{i}).fixation(p) = 1;
                dataAll{ii}.(conditionsAll{i}).resp(p) = NaN;
                dataAll{ii}.(conditionsAll{i}).correct(p) = NaN;
            else
                dataAll{ii}.(conditionsAll{i}).fixation(p) = 0;
                
                if contains(pptrials{p}.image,'_LA_')
                    if pptrials{p}.Response == 1 
                        dataAll{ii}.(conditionsAll{i}).resp(p) = 1;
                        dataAll{ii}.(conditionsAll{i}).correct(p) = 0;
                    elseif pptrials{p}.Response == 2 %correct
                        dataAll{ii}.(conditionsAll{i}).resp(p) = 2;
                        dataAll{ii}.(conditionsAll{i}).correct(p) = 1;
                    end
                else
                    if pptrials{p}.Response == 1
                        dataAll{ii}.(conditionsAll{i}).resp(p) = 2;
                        dataAll{ii}.(conditionsAll{i}).correct(p) = 0;
                    elseif pptrials{p}.Response == 2
                        dataAll{ii}.(conditionsAll{i}).resp(p) = 1;
                        dataAll{ii}.(conditionsAll{i}).correct(p) = 1;
                    end
                end
            end
            xNoOff = (dataAll{ii}.(conditionsAll{i}).pptrials{p}.x.position.* ...
                dataAll{ii}.(conditionsAll{i}).pptrials{p}.pixelAngle);  
            yNoOff = (dataAll{ii}.(conditionsAll{i}).pptrials{p}.y.position.* ...
                dataAll{ii}.(conditionsAll{i}).pptrials{p}.pixelAngle);
           
            xOff = dataAll{ii}.(conditionsAll{i}).pptrials{p}.xoffset * ...
                dataAll{ii}.(conditionsAll{i}).pptrials{p}.pixelAngle;      
            yOff = dataAll{ii}.(conditionsAll{i}).pptrials{p}.yoffset * ...
                dataAll{ii}.(conditionsAll{i}).pptrials{p}.pixelAngle;
            
            x = xNoOff + xOff;
            y = yNoOff + yOff;
                
            
            dataAll{ii}.(conditionsAll{i}).x{1,p} = x;
            dataAll{ii}.(conditionsAll{i}).y{1,p} = y;
            
            dataAll{ii}.(conditionsAll{i}).RT{p} = dataAll{ii}.(conditionsAll{i}).pptrials{p}.TimeResponse;
            counter = 1;
            for m = 1:length(dataAll{ii}.(conditionsAll{i}).pptrials{p}.microsaccades.start)
                if dataAll{ii}.(conditionsAll{i}).pptrials{p}.microsaccades.start(counter) > 3
                    
                    dataAll{ii}.(conditionsAll{i}).msStart{1,p}(counter) =  ...
                        dataAll{ii}.(conditionsAll{i}).pptrials{p}.microsaccades.start(m);
                    
                    dataAll{ii}.(conditionsAll{i}).msEnd{1,p}(counter) =  ...
                        dataAll{ii}.(conditionsAll{i}).pptrials{p}.microsaccades.start(m) +...
                        dataAll{ii}.(conditionsAll{i}).pptrials{p}.microsaccades.duration(m);
                    
                    dataAll{ii}.(conditionsAll{i}).msStart{2,p}(counter) =  ... %X
                        x(...
                        dataAll{ii}.(conditionsAll{i}).pptrials{p}.microsaccades.start(m));
                    
                    dataAll{ii}.(conditionsAll{i}).msStart{3,p}(counter) =  ... % Y
                        y(...
                        dataAll{ii}.(conditionsAll{i}).pptrials{p}.microsaccades.start(m));
                    
                    dataAll{ii}.(conditionsAll{i}).msEnd{2,p}(counter) =  ...
                        x(...
                        min(dataAll{ii}.(conditionsAll{i}).pptrials{p}.microsaccades.start(m) +...
                        dataAll{ii}.(conditionsAll{i}).pptrials{p}.microsaccades.duration(m), ...
                        length(dataAll{ii}.(conditionsAll{i}).pptrials{p}.x.position)));
                    
                    dataAll{ii}.(conditionsAll{i}).msEnd{3,p}(counter) =  ...
                        y(...
                        min(dataAll{ii}.(conditionsAll{i}).pptrials{p}.microsaccades.start(m) +...
                        dataAll{ii}.(conditionsAll{i}).pptrials{p}.microsaccades.duration(m), ...
                        length(dataAll{ii}.(conditionsAll{i}).pptrials{p}.y.position)));
                    
                    
                    x1 = dataAll{ii}.(conditionsAll{i}).msStart{2,p}(counter);
                    y1 = dataAll{ii}.(conditionsAll{i}).msStart{3,p}(counter);
                    x2 = dataAll{ii}.(conditionsAll{i}).msEnd{2,p}(counter);
                    y2 = dataAll{ii}.(conditionsAll{i}).msEnd{3,p}(counter);
                    
                    dataAll{ii}.(conditionsAll{i}).msAmp{1,p}(counter) = ...
                        sqrt((x2 - x1)^2 + (y2 - y1)^2);
                    
                    counter = counter + 1;
                end
            end
            dataAll{ii}.(conditionsAll{i}).traceMatrixX(p,:) = ...
                dataAll{ii}.(conditionsAll{i}).pptrials{p}.x.position(1:500);
            dataAll{ii}.(conditionsAll{i}).traceMatrixY(p,:) = ...
                dataAll{ii}.(conditionsAll{i}).pptrials{p}.y.position(1:500);
            
            allX = [allX x];
            allY = [allY y];
            
            if dataAll{ii}.(conditionsAll{i}).pptrials{p}.FixationTrial == 0
                fixSamIdx = [fixSamIdx zeros(1,length(dataAll{ii}.(conditionsAll{i}).pptrials{p}.x.position))];
                
            else
                fixSamIdx = [fixSamIdx ones(1,length(dataAll{ii}.(conditionsAll{i}).pptrials{p}.x.position))];
            end
            if counter > 1
                for t = 1:length(dataAll{ii}.(conditionsAll{i}).x)
                    sac = find(dataAll{ii}.(conditionsAll{i}).msAmp{t} < 60);
                    temp = [temp dataAll{ii}.(conditionsAll{i}).msAmp{t}(sac)];
                    temp2 = [temp2 dataAll{ii}.(conditionsAll{i}).msStart{2,t}(sac)];
                    temp3 = [temp3 dataAll{ii}.(conditionsAll{i}).msStart{3,t}(sac)];
                    temp4 = [temp4 dataAll{ii}.(conditionsAll{i}).msEnd{2,t}(sac)];
                    temp5 = [temp5 dataAll{ii}.(conditionsAll{i}).msEnd{3,t}(sac)];
                end
                dataAll{ii}.(conditionsAll{i}).msAmpAll = temp;
                dataAll{ii}.(conditionsAll{i}).msStartAllX = temp2;
                dataAll{ii}.(conditionsAll{i}).msStartAllY = temp3;
                dataAll{ii}.(conditionsAll{i}).msEndAllX = temp4;
                dataAll{ii}.(conditionsAll{i}).msEndAllY = temp5;
            end
        end
        
        dataAll{ii}.(conditionsAll{i}).allX = allX;
        dataAll{ii}.(conditionsAll{i}).allY = allY;
        dataAll{ii}.(conditionsAll{i}).fixSamIdx = fixSamIdx;
    end
