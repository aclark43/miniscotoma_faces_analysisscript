
function [pAll, allVals1, allVals2, S] = distributionMapsEYX(subjectsAll, yAllSavedF, xAllSavedF, meanYFixTrial, meanXFixTrial)
%% Euc Drift Distribution Maps
figure;
for ii = 1:length(subjectsAll)
%     subplot(2,3,ii)
    allHistOut{ii,1} = histogram(eucDist(yAllSavedF{ii,1},xAllSavedF{ii,1}),'BinLimits',[-60 60],...
        'BinEdges', [-60:0.5:60]);
    hold on
    allHistOut{ii,2} = histogram(eucDist(yAllSavedF{ii,2},xAllSavedF{ii,2}),'BinLimits',[-60 60],...
        'BinEdges', [-60:0.5:60]);

    meanVals1(ii,1) = mean(allHistOut{ii,1}.Data);
    meanVals1(ii,2) = mean(allHistOut{ii,2}.Data);
%     
    
    temp = find(allHistOut{ii,1}.Values == max(allHistOut{ii,1}.Values));
    if length(temp) > 1
        maxBin(ii) = temp(1);
    else
        maxBin(ii) = temp;
    end
    valBin(ii,1) = allHistOut{ii,1}.BinEdges(maxBin(ii));
    
end
% [h,p] = ttest(meanVals(:,1), meanVals(:,2));

for ii = 1:length(subjectsAll)
    for i = 1:length(allHistOut{1}.BinEdges)-1
        meanBinC(ii,i) = (allHistOut{ii,1}.Values(i));
        meanBinS(ii,i) = (allHistOut{ii,2}.Values(i));
    end
    S.meanBinNormCE(ii,:) = normalizeVector(meanBinC(ii,:));
    S.meanBinNormSE(ii,:) = normalizeVector(meanBinS(ii,:));

figure; %normalizes
% subplot(2,1,1)
figsHist(1) = plot(allHistOut{ii}.BinEdges(1:end-1), ...
    ((S.meanBinNormCE(ii,:))),...
    'r');
hold on
figsHist(2) =plot(allHistOut{ii}.BinEdges(1:end-1), ...
    ((S.meanBinNormSE(ii,:))),...
    'b');

line([mean(meanVals1(ii,1)) mean(meanVals1(ii,1))],...
    [0 1],'Color','r');
line([nanmean(meanVals1(ii,2)) nanmean(meanVals1(ii,2))],...
    [0 1],'Color','b');
legend(figsHist,{'Control','Scotoma'})
ylabel('Normalized Bin Means');
xlabel('Euclidean Distance of Gaze');
xlabel('EucDist Position');

% xlabel('Y Position');

axis([-30 30 0 1.1])
% d = pdist([xAllSavedF{ii,1}',xAllSavedF{ii,2}']);
[h,pC(ii)] = ttest2(meanYFixTrial{ii,1}(:), meanYFixTrial{ii,2}(:))
meanEFixTrial = eucDist(meanXFixTrial{ii,1}(:), meanYFixTrial{ii,1}(:));
meanEFixTrial2 = eucDist(meanXFixTrial{ii,2}(:), meanYFixTrial{ii,2}(:))
[h,pE(ii)] = ttest2(meanEFixTrial,...
   meanEFixTrial2);

% % [h,pS(ii)] = ttest(meanBinC(ii,:))

title(sprintf('%s \n p = %.2f',subjectsAll{ii}, pE(ii)));
end


%% Y Drift Distribution Maps

for ii = 1:length(subjectsAll)

    allHistOut{ii,1} = histogram((yAllSavedF{ii,1}),'BinLimits',[-60 60],...
        'BinEdges', [-60:0.5:60]);
    hold on
    allHistOut{ii,2} = histogram((yAllSavedF{ii,2}),'BinLimits',[-60 60],...
        'BinEdges', [-60:0.5:60]);
    
    meanVals2(ii,1) = mean(allHistOut{ii,1}.Data);
    meanVals2(ii,2) = mean(allHistOut{ii,2}.Data);
%     allHistOut{ii,1} = histfit((yAllSavedF{ii,1}));
%     hold on
%     line([mean(yAllSavedF{ii,1}),mean(yAllSavedF{ii,1})], [0,4000]);
%     hold on
%     allHistOut{ii,2} = histfit((yAllSavedF{ii,2}));
%     hold on
%     line([mean(yAllSavedF{ii,2}),mean(yAllSavedF{ii,2})], [0,4000]);
    meanVals2(ii,1) = mean(allHistOut{ii,1}.Data);
    meanVals2(ii,2) = mean(allHistOut{ii,2}.Data);
%     
    
    temp = find(allHistOut{ii,1}.Values == max(allHistOut{ii,1}.Values));
    if length(temp) > 1
        maxBin(ii) = temp(1);
    else
        maxBin(ii) = temp;
    end
    valBin(ii,1) = allHistOut{ii,1}.BinEdges(maxBin(ii));
    
end
% [h,p] = ttest(meanVals(:,1), meanVals(:,2));

for ii = 1:length(subjectsAll)
    for i = 1:length(allHistOut{1}.BinEdges)-1
        meanBinC(ii,i) = (allHistOut{ii,1}.Values(i));
        meanBinS(ii,i) = (allHistOut{ii,2}.Values(i));
    end
    S.meanBinNormCY(ii,:) = normalizeVector(meanBinC(ii,:));
    S.meanBinNormSY(ii,:) = normalizeVector(meanBinS(ii,:));

figure; %normalizes
% subplot(2,1,1)
figsHist(1) = plot(allHistOut{ii}.BinEdges(1:end-1), ...
    ((S.meanBinNormCY(ii,:))),...
    'r');
hold on
figsHist(2) =plot(allHistOut{ii}.BinEdges(1:end-1), ...
    ((S.meanBinNormSY(ii,:))),...
    'b');

line([mean(meanVals2(ii,1)) mean(meanVals2(ii,1))],...
    [0 1],'Color','r');
line([nanmean(meanVals2(ii,2)) nanmean(meanVals2(ii,2))],...
    [0 1],'Color','b');
legend(figsHist,{'Control','Scotoma'})
ylabel('Normalized Bin Means');
xlabel('Y Distance of Gaze');
xlabel('Y Position');

% xlabel('Y Position');

axis([-30 30 0 1.1])
% d = pdist([xAllSavedF{ii,1}',xAllSavedF{ii,2}']);
[h,pY(ii)] = ttest2(meanYFixTrial{ii,1}(:), meanYFixTrial{ii,2}(:));
% [h,pC(ii)] = ttest2(eucDist(meanXFixTrial{ii,1}(:), meanYFixTrial{ii,1}(:)),...
%     eucDist(meanXFixTrial{ii,2}(:), meanYFixTrial{ii,2}(:)));

% % [h,pS(ii)] = ttest(meanBinC(ii,:))

title(sprintf('%s \n p = %.2f',subjectsAll{ii}, pY(ii)));
end

%% X Drift Distribution Maps

for ii = 1:length(subjectsAll)
%     subplot(2,3,ii)
%     allHistOut{ii,1} = histogram(eucDist(yAllSavedF{ii,1},xAllSavedF{ii,1}),'BinLimits',[-60 60],...
%         'BinEdges', [-60:0.5:60]);
%     hold on
%     allHistOut{ii,2} = histogram(eucDist(yAllSavedF{ii,2},xAllSavedF{ii,2}),'BinLimits',[-60 60],...
%         'BinEdges', [-60:0.5:60]);

    allHistOut{ii,1} = histogram((xAllSavedF{ii,1}),'BinLimits',[-60 60],...
        'BinEdges', [-60:0.5:60]);
    hold on
    allHistOut{ii,2} = histogram((xAllSavedF{ii,2}),'BinLimits',[-60 60],...
        'BinEdges', [-60:0.5:60]);
    
    meanVals3(ii,1) = mean(allHistOut{ii,1}.Data);
    meanVals3(ii,2) = mean(allHistOut{ii,2}.Data);
%     allHistOut{ii,1} = histfit((yAllSavedF{ii,1}));
%     hold on
%     line([mean(yAllSavedF{ii,1}),mean(yAllSavedF{ii,1})], [0,4000]);
%     hold on
%     allHistOut{ii,2} = histfit((yAllSavedF{ii,2}));
%     hold on
%     line([mean(yAllSavedF{ii,2}),mean(yAllSavedF{ii,2})], [0,4000]);
    meanVals(ii,1) = mean(allHistOut{ii,1}.Data);
    meanVals(ii,2) = mean(allHistOut{ii,2}.Data);
%     
    
    temp = find(allHistOut{ii,1}.Values == max(allHistOut{ii,1}.Values));
    if length(temp) > 1
        maxBin(ii) = temp(1);
    else
        maxBin(ii) = temp;
    end
    valBin(ii,1) = allHistOut{ii,1}.BinEdges(maxBin(ii));
    
end
% [h,p] = ttest(meanVals(:,1), meanVals(:,2));

for ii = 1:length(subjectsAll)
    for i = 1:length(allHistOut{1}.BinEdges)-1
        meanBinC(ii,i) = (allHistOut{ii,1}.Values(i));
        meanBinS(ii,i) = (allHistOut{ii,2}.Values(i));
    end
    S.meanBinNormCX(ii,:) = normalizeVector(meanBinC(ii,:));
    S.meanBinNormSX(ii,:) = normalizeVector(meanBinS(ii,:));

figure; %normalizes
% subplot(2,1,1)
figsHist(1) = plot(allHistOut{ii}.BinEdges(1:end-1), ...
    ((S.meanBinNormCX(ii,:))),...
    'r');
hold on
figsHist(2) =plot(allHistOut{ii}.BinEdges(1:end-1), ...
    ((S.meanBinNormSX(ii,:))),...
    'b');

line([mean(meanVals3(ii,1)) mean(meanVals3(ii,1))],...
    [0 1],'Color','r');
line([nanmean(meanVals3(ii,2)) nanmean(meanVals3(ii,2))],...
    [0 1],'Color','b');
legend(figsHist,{'Control','Scotoma'})
ylabel('Normalized Bin Means');
xlabel('X Distance of Gaze');
xlabel('X Position');

% xlabel('Y Position');

axis([-30 30 0 1.1])
% d = pdist([xAllSavedF{ii,1}',xAllSavedF{ii,2}']);
[h,pX(ii)] = ttest2(meanXFixTrial{ii,1}(:), meanXFixTrial{ii,2}(:));
% [h,pC(ii)] = ttest2(meanXFixTrial{ii,1}(:), meanXFixTrial{ii,1}(:)),...
%     eucDist(meanXFixTrial{ii,2}(:), meanYFixTrial{ii,2}(:)));

% % [h,pS(ii)] = ttest(meanBinC(ii,:))

title(sprintf('%s \n p = %.2f',subjectsAll{ii}, pX(ii)));
end

%% Combining Maps for Extremes
nameAllMeanVals{1} = [meanVals1];
nameAllMeanVals{2} = [meanVals2];
nameAllMeanVals{3} = [meanVals2];
for ii = 1:length(pY)
    allPs = [1 pY(ii) pX(ii)];
    extremeDistMap(ii) = find(allPs == (min([100 pY(ii) pX(ii)])));
    extremePVals(ii) = allPs(extremeDistMap(ii));
    averagesEach(1,ii) = mean(nameAllMeanVals{extremeDistMap(ii)}(ii,1));
    averagesEach(2,ii) = mean(nameAllMeanVals{extremeDistMap(ii)}(ii,2));
    %     ttestVals{ii,1} =
end

%  meanVals2 meanVals3];
 
names1 = {'meanBinNormCE', 'meanBinNormCY','meanBinNormCX'};
names2 = {'meanBinNormSE', 'meanBinNormSY','meanBinNormSX'};
nameAxisType = {'Euc','Y','X'};
% names3 = {'meanEFixTrial', 'meanYFixTrial','meanXFixTrial'};
  figure;
for ii = 1:length(subjectsAll)
    meanBinNormCX(ii,:) = S.((names1{extremeDistMap(ii)}))(ii,:);
    meanBinNormSX(ii,:) = S.((names2{extremeDistMap(ii)}))(ii,:);
    
    % end
  subplot(3,3,ii)
    figsHist(1) = plot(allHistOut{ii}.BinEdges(1:end-1), ...
        ((meanBinNormCX(ii,:))),...
        'r');
    hold on
    figsHist(2) =plot(allHistOut{ii}.BinEdges(1:end-1), ...
        ((meanBinNormSX(ii,:))),...
        'b');
    
    line([mean(meanVals(ii,1)) mean(meanVals(ii,1))],...
        [0 1],'Color','r');
    line([nanmean(meanVals(ii,2)) nanmean(meanVals(ii,2))],...
        [0 1],'Color','b');

%     figsHist(1) = plot(allHistOut{ii}.BinEdges(1:end-1), ...
%         (mean(meanBinNormCX)),'r');
%     hold on
%     figsHist(2) =plot(allHistOut{ii}.BinEdges(1:end-1), ...
%         (mean(meanBinNormSX)),...
%         'b');
%     line([mean(averagesEach(1,:)) ...
%         mean(averagesEach(1,:))],...
%         [0 1],'Color','r');
%     line([mean(averagesEach(2,:)) ...
%         mean(averagesEach(2,:))],...
%         [0 1],'Color','b');
    legend(figsHist,{'Control','Scotoma'})
    ylabel('Normalized Bin Means');
    xlabel(sprintf('Distance of Gaze for %s', nameAxisType{extremeDistMap(ii)}));
%     xlabel('Extreme Angle Position');
    axis([-30 30 0 1.1])
    title(subjectsAll{ii});
end
% d = pdist([xAllSavedF{ii,1}',xAllSavedF{ii,2}']);
[h,pAll] = ttest(meanBinNormCX(:),meanBinNormSX(:));
allVals1 = meanBinNormCX;
allVals2 = meanBinNormSX;
% [h,pC(ii)] = ttest2(meanXFixTrial{ii,1}(:), meanXFixTrial{ii,1}(:)),...
%     eucDist(meanXFixTrial{ii,2}(:), meanYFixTrial{ii,2}(:)));

% % [h,pS(ii)] = ttest(meanBinC(ii,:))

suptitle(sprintf('p = %.3f', pAll));
saveas(gcf,('../../../Documents/Overleaf_MiniScotoma/figures/ExtemeHists.png'));
saveas(gcf,('../../../Documents/Overleaf_MiniScotoma/figures/ExtemeHists.epsc'));